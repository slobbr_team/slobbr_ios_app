//
//  PWPBaseViewController.h
//  PawPark
//
//  Created by daffolap19 on 5/22/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPBaseViewController : UIViewController

-(void)selectDogFromPackToSendRequest:(void (^)(NSString *))selectedDogId withCancelblock:(void (^)(void))cancelBlock;

-(void)showLoader;
-(void)hideLoader;

@end
