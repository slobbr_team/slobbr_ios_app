//
//  PWPBaseViewController.m
//  PawPark
//
//  Created by daffolap19 on 5/22/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPBaseViewController.h"
#import "UIViewController+UBRComponents.h"
#import "PWPApplicationUtility.h"
#import "UIColor+PWPColor.h"


@interface PWPBaseViewController ()
{
    UIActivityIndicatorView * centerIndicatorView;

}
@end

@implementation PWPBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)selectDogFromPackToSendRequest:(void (^)(NSString *))selectedDogId withCancelblock:(void (^)(void))cancelBlock{

    NSArray * dogsArray=[PWPApplicationUtility getCurrentUserDogsArray];
    NSArray * dogsNameArray=[dogsArray valueForKeyPath:@"name"];
    
    [self sxc_showActionSheetWithTitle:@"Select your dog." pickerWithRows:dogsNameArray itemSelectedAtIndex:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        selectedDogId(dogsArray[selectedIndex][@"id"]);
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        cancelBlock();
    } origin:self.view];
}

-(void)initActivityLoader{
    
    centerIndicatorView=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    centerIndicatorView.hidden=YES;
    
    centerIndicatorView.color=[UIColor sxc_themeColor];
    
    
    [self.view addSubview:centerIndicatorView];
    
    [self.view bringSubviewToFront:centerIndicatorView];
    
    [centerIndicatorView setCenter:CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height/2.0f)];
    
    [centerIndicatorView setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin];
    
}
-(void)showLoader{
    
    if(centerIndicatorView==nil){
        [self initActivityLoader];
    }
    [self.view bringSubviewToFront:centerIndicatorView];
  
    [self.view setUserInteractionEnabled:NO];
    
    [centerIndicatorView startAnimating];
    
    centerIndicatorView.hidden=NO;
}
-(void)hideLoader{
    [self.view setUserInteractionEnabled:YES];
    [centerIndicatorView stopAnimating];
    centerIndicatorView.hidden=YES;
}

-(void)sxc_startLoading{}
-(void)sxc_stopLoading{}
@end
