//
//  PWPConstant.h
//  PawPark
//
//  Created by daffolap19 on 5/10/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#ifndef PawPark_PWPConstant_h
#define PawPark_PWPConstant_h

#define K_URL_HOSTNAME @"http://www.slobbr.com/"
#define K_URL_LIVE_BASE @"http://www.slobbr.com/apis/v1/"
#define K_URL_LIVE_HOSTNAME @"http://www.slobbr.com/"
#define K_URL_BASE @"http://www.slobbr.com/apis/v1/"

#define K_URL_LOGIN @"login"
#define K_URL_DOGS @"dogs"
#define K_URL_USER_PROFILE @"current/user"
#define K_URL_USER_DOGS @"current/dogs"
#define K_URL_PLACE @"placesubmissions"
#define K_URL_REGISTER @"users"
#define K_URL_DOGS_BREEDS @"dog/breeds"
#define K_URL_INVITIES @"invites"
#define K_URL_PARKS @"places"
#define K_URL_EVENTS @"events"

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)
#define IS_RETINA ([[UIScreen mainScreen] scale] == 2.0)

#endif
