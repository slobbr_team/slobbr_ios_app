//
//  PWPStringConstant.h
//  PawPark
//
//  Created by xyz on 05/04/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#ifndef PawPark_PWPStringConstant_h
#define PawPark_PWPStringConstant_h

    //Server Response error code string
#define K_ERROR_SERVER_SESSION_EXPIRED NSLocalizedString(@"Your session has been expired. Please login again.",nil)
#define K_ERROR_SERVER_BAD_REQUEST NSLocalizedString(@"Bad Request. Application has sent a request that server could not understand. Please contact Admin",nil)
#define K_ERROR_SERVER_UNAUTHORIZED NSLocalizedString(@"Please enter the correct credentials.",nil)
#define K_ERROR_SERVER_ACCOUNT_INACTIVE NSLocalizedString(@"Your account is not activated yet. Please contact the admin.",nil)
#define K_ERROR_SERVER_INVALID_USERNAME NSLocalizedString(@"Entered username doesn't exit. Please enter valid username.",nil)
#define K_ERROR_SERVER_ALREADY_REQUEST_WITHIN_24_HOURS NSLocalizedString(@"You have already requested this. Please try again after 24 hours. ",nil)
#define K_ERROR_SERVER_INVALID_PASSWORD NSLocalizedString(@"Invalid password, please try again.",nil)
#define K_ERROR_SERVER_INVALID_TOKEN NSLocalizedString(@"Invalid token, please try again.",nil)
#define K_ERROR_SERVER_INVALID_GENDER NSLocalizedString(@"Please select gender.",nil)
#define K_ERROR_SERVER_FORBIDDEN NSLocalizedString(@"You are not authorized to use the Slobbr's Services. Please contact Admin.",nil)
#define K_ERROR_SERVER_NOT_FOUND NSLocalizedString(@"Web service error. Please try again.",nil)
#define K_ERROR_SERVER_PARAMETER_MISSING NSLocalizedString(@"Request doesn't have all the required parameter. Please try again.",nil)
#define K_ERROR_SERVER_USER_ALREADY NSLocalizedString(@"User already exists. Please use other email address.",nil)
#define K_ERROR_SERVER_ERROR NSLocalizedString(@"Web service error. Please try again.",nil)
#define K_ERROR_SERVER_INTERNET_CONNECTION NSLocalizedString(@"Could not connect to the server. Please check your internet connection.",nil)
#define K_ERROR_USER_NOT_FOUND NSLocalizedString(@"Entered email address is not registered with Slobbr",nil)
#define K_ERROR_SERVER_NOT_FOUND NSLocalizedString(@"Web service error. Please try again.",nil)
#define K_ERROR_SERVER_PARAMETER_MISSING NSLocalizedString(@"Request doesn't have all the required parameter. Please try again.",nil)
#define K_ERROR_SERVER_USER_ALREADY NSLocalizedString(@"User already exists. Please use other email address.",nil)
#define K_ERROR_SERVER_ERROR NSLocalizedString(@"Web service error. Please try again.",nil)
#define K_ERROR_PASSWORD_NOTMATCH NSLocalizedString(@"Old password is incorrect.",nil)
#define K_ERROR_SERVER_ACCEPT_TERMS_CONDITION NSLocalizedString(@"Please accept the terms and conditions.",nil)
#define K_ERROR_NO_SHOPS NSLocalizedString(@"No shops available.",nil)
#define K_ERROR_NO_STORE_FOUND      NSLocalizedString(@"No shop found",nil)
#define K_ERROR_NO_STORE_INTERNET_ERROR @"Server is not responding.Please check your internet and try again."

#endif
