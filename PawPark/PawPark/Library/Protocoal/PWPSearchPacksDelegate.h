//
//  PWPSearchPacks.h
//  PawPark
//
//  Created by daffolap19 on 5/10/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PWPSearchPacksDelegate <NSObject>

-(void)applyFiltersAndRefreshSearch:(PWPPackFilter *)packFilter;

@end
