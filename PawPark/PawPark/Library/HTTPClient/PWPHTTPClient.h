//
//  MACHTTPClient.h
//  MACarpooling
//
//  Created by mac mini on 19/05/14.
//  Copyright (c) 2014 xyz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PWPHTTPClient : NSObject

+(id) sharedInstance;
@property NSURLRequestCachePolicy cachePolicy;


-(void)cancelAllOperation;

-(void)cancelOperationCorrespondingToUrlString:(NSString *)urlString;

-(void)requestGETServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

-(void)requestPOSTServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)( NSError * error))errorBlock;

-(void)requestPATCHServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)requestPOSTServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withProgress:(void (^)(float progress))progressBlock withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)requestDELETEServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)requestPUTServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)requestPOSTMultipartServiceOnURL:(NSString *) urlString withImageData:(NSData *)photoData imageName:(NSString *)imageName logoName:(NSString*)logoname withLogoData:(NSData*)logoData  parameters:(NSDictionary *)parameters WithProgressBlock:(void (^) (float progress))progressBlock WithSuccessBlock:(void (^)(id response))successBlock WithErrorBlock:(void (^)(NSError * error))errorBlock;


-(void)requestPUTMultipartServiceOnURL:(NSString *) urlString withImageData:(NSData *)photoData imageName:(NSString *)imageName parameters:(NSDictionary *)parameters WithProgressBlock:(void (^) (float progress))progressBlock WithSuccessBlock:(void (^)(id response))successBlock WithErrorBlock:(void (^)(NSError * error))errorBlock;


-(void)requestPOSTHTTPServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)requestGETHTTPServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)requestPUTHTTPServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)requestPATCHHTTPServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

@end
