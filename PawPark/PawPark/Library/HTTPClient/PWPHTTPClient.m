//
//  MACHTTPClient.m
//  MACarpooling
//
//  Created by mac mini on 19/05/14.
//  Copyright (c) 2014 xyz. All rights reserved.
//

#import "PWPHTTPClient.h"
#import <AFNetworking.h>
#import "PWPErrorUtility.h"



@interface PWPHTTPClient(){
    AFHTTPRequestOperationManager * globalManager;
}
@end

@implementation PWPHTTPClient

+(id) sharedInstance
{
    static PWPHTTPClient *sharedInstance;
    static dispatch_once_t pred;        // Lock
    dispatch_once(&pred, ^{             // This code is called at most once per app
        sharedInstance = [[PWPHTTPClient alloc] init];
    });
    return sharedInstance;
}


#pragma mark - Service request Methods

-(void)requestGETServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{

    NSLog(@"%@",urlString);
    
    AFHTTPRequestOperationManager *manager = [self requestOperationManager];
    [manager GET:urlString parameters:requestDictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
      successBlock(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

        errorBlock([self filterErrorMessageUsingResponseRequestOperation:operation requestError:error]);
    }];

}
-(void)requestPOSTServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    
    NSLog(@"%@",urlString);
    
    AFHTTPRequestOperationManager *manager = [self requestOperationManager];
    
    [manager POST:urlString parameters:requestDictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        successBlock(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock([self filterErrorMessageUsingResponseRequestOperation:operation requestError:error]);
    }];
    
    
}

-(void)requestPOSTServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withProgress:(void (^)(float progress))progressBlock withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    AFHTTPRequestOperationManager *manager = [self requestOperationManager];
    
    AFHTTPRequestOperation * operation = [manager POST:urlString parameters:requestDictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        successBlock(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock([self filterErrorMessageUsingResponseRequestOperation:operation requestError:error]);
    }];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        float progress = (float)bytesRead / totalBytesExpectedToRead;
        progressBlock(progress);
    }];
    
}




-(void)requestPOSTHTTPServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    
    NSLog(@"%@",urlString);
    
    AFHTTPRequestOperationManager *manager = [self requestOperationManagerForHTTPSerializr];
    
    [manager POST:urlString parameters:requestDictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        successBlock(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock([self filterErrorMessageUsingResponseRequestOperation:operation requestError:error]);
    }];
    
    
}

-(void)requestPUTHTTPServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    
    NSLog(@"%@",urlString);
    
    AFHTTPRequestOperationManager *manager = [self requestOperationManagerForHTTPSerializr];
    
    [manager PUT:urlString parameters:requestDictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        successBlock(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock([self filterErrorMessageUsingResponseRequestOperation:operation requestError:error]);
    }];
    
    
}



-(void)requestGETHTTPServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    
    NSLog(@"%@",urlString);
    
    AFHTTPRequestOperationManager *manager = [self requestOperationManagerForHTTPSerializr];
    
    [manager GET:urlString parameters:requestDictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        successBlock(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock([self filterErrorMessageUsingResponseRequestOperation:operation requestError:error]);
    }];
    
    
}

-(void)requestPATCHHTTPServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock

{
    
    NSLog(@"%@",urlString);
    
    AFHTTPRequestOperationManager *manager = [self requestOperationManagerForHTTPSerializr];
    
    [manager PATCH:urlString parameters:requestDictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        successBlock(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock([self filterErrorMessageUsingResponseRequestOperation:operation requestError:error]);
    }];
    
    
}


-(void)requestPATCHServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    
    NSLog(@"%@",urlString);
    
    AFHTTPRequestOperationManager *manager = [self requestOperationManager];
    
    [manager PATCH:urlString parameters:requestDictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        successBlock(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock([self filterErrorMessageUsingResponseRequestOperation:operation requestError:error]);
    }];
    
    
}

-(void)requestDELETEServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    
    NSLog(@"%@",urlString);

    
    AFHTTPRequestOperationManager *manager = [self requestOperationManager];
    
    [manager DELETE:urlString parameters:requestDictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"Success = %@",responseObject);
        successBlock(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorBlock([self filterErrorMessageUsingResponseRequestOperation:operation requestError:error]);
    }];
    
}
-(void)requestPUTServiceOnURL:(NSString *)urlString WithDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id  response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    
    NSLog(@"%@",urlString);
    AFHTTPRequestOperationManager *manager = [self requestOperationManager];
    
    [manager PUT:urlString parameters:requestDictionary success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"Success = %@",responseObject);
        successBlock(responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        errorBlock([self filterErrorMessageUsingResponseRequestOperation:operation requestError:error]);
    }];

}



-(void)requestPOSTMultipartServiceOnURL:(NSString *) urlString withImageData:(NSData *)photoData imageName:(NSString *)imageName logoName:(NSString*)logoname withLogoData:(NSData*)logoData  parameters:(NSDictionary *)parameters WithProgressBlock:(void (^) (float progress))progressBlock WithSuccessBlock:(void (^)(id response))successBlock WithErrorBlock:(void (^)(NSError * error))errorBlock{
   
    AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
    
        // 2. Create an `NSMutableURLRequest`.
    NSMutableURLRequest *request = [serializer multipartFormRequestWithMethod:@"POST" URLString:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if(photoData){
        [formData appendPartWithFileData:photoData
                                    name:imageName
                                fileName:@"imageFile"
                                mimeType:@"image/png"];
        }
        
        if(logoData){
            [formData appendPartWithFileData:logoData
                                        name:logoname
                                    fileName:@"imageFile"
                                    mimeType:@"image/png"];
        }
        
    }
            
                                                                        error:nil];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = serializer;
    
    AFHTTPRequestOperation *operation =
    [manager HTTPRequestOperationWithRequest:request
                                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                         NSLog(@"Success %@", responseObject);
                                         
                                         successBlock(responseObject);
                                         
                                     } failure:^(AFHTTPRequestOperation * operation, NSError* error) {
                                         NSLog(@"Failure %@", error.description);
                                         errorBlock([self filterErrorMessageUsingResponseRequestOperation:operation requestError:error]);
                                     }];
    
        // 4. Set the progress block of the operation.
    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                        long long totalBytesWritten,
                                        long long totalBytesExpectedToWrite) {
        float percentDone = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
        progressBlock (percentDone);
    }];
    
        // 5. Begin!
    [operation start];
    
}

-(void)requestPUTMultipartServiceOnURL:(NSString *) urlString withImageData:(NSData *)photoData imageName:(NSString *)imageName parameters:(NSDictionary *)parameters WithProgressBlock:(void (^) (float progress))progressBlock WithSuccessBlock:(void (^)(id response))successBlock WithErrorBlock:(void (^)(NSError * error))errorBlock{
    
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    
    // 2. Create an `NSMutableURLRequest`.
    NSMutableURLRequest *request = [serializer multipartFormRequestWithMethod:@"PUT" URLString:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if(photoData)
            [formData appendPartWithFileData:photoData
                                        name:imageName
                                    fileName:@"postfile"
                                    mimeType:@"image/png"];
    } error:nil];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = serializer;
    
    AFHTTPRequestOperation *operation =
    [manager HTTPRequestOperationWithRequest:request
                                     success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                         NSLog(@"Success %@", responseObject);
                                         
                                         successBlock(responseObject);
                                         
                                     } failure:^(AFHTTPRequestOperation * operation, NSError* error) {
                                         NSLog(@"Failure %@", error.description);
                                         errorBlock([self filterErrorMessageUsingResponseRequestOperation:operation requestError:error]);
                                     }];
    
    // 4. Set the progress block of the operation.
    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                        long long totalBytesWritten,
                                        long long totalBytesExpectedToWrite) {
        float percentDone = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
        progressBlock (percentDone);
    }];
    
    // 5. Begin!
    [operation start];
    
}


#pragma mark - Request Operation Manager

-(AFHTTPRequestOperationManager *)requestOperationManager
{
    if(globalManager==nil){
        globalManager = [AFHTTPRequestOperationManager manager];
    }
    
    globalManager.responseSerializer = [AFJSONResponseSerializer serializer];
    globalManager.requestSerializer=[AFJSONRequestSerializer serializer];
    globalManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    
    [globalManager.requestSerializer setValue:@"Accept" forHTTPHeaderField:@"application/json"];

    
    return  globalManager;
}


-(AFHTTPRequestOperationManager *)requestOperationManagerForHTTPSerializr
{
    if(globalManager==nil){
        globalManager = [AFHTTPRequestOperationManager manager];
    }
    globalManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    globalManager.requestSerializer=[AFHTTPRequestSerializer serializer];
    globalManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/x-www-form-urlencoded",@"application/json",nil];
    [globalManager.requestSerializer setValue:@"Accept" forHTTPHeaderField:@"application/json"];

    
    return  globalManager;
}


-(void)cancelAllOperation
{
    [globalManager.operationQueue cancelAllOperations];

}
-(void)cancelOperationCorrespondingToUrlString:(NSString *)urlString
{
    for (NSOperation *operation in globalManager.operationQueue.operations) {
        if([operation isKindOfClass:[AFHTTPRequestOperation class]]){
           
            AFHTTPRequestOperation *httpOperation = (AFHTTPRequestOperation *)operation;
            if([[[[httpOperation request] URL] absoluteString] isEqualToString:urlString])
            {
                [operation cancel];
            }
        }
    }
}

#pragma mark - Filter error message
-(NSError *)filterErrorMessageUsingResponseRequestOperation:(AFHTTPRequestOperation *) operation requestError:(NSError *) error
{
    if(operation.response.statusCode==0){
        return [PWPErrorUtility handlePredefinedErrorCode:error.code WithResponse:nil];
    }
    
    return [PWPErrorUtility handlePredefinedErrorCode:operation.response.statusCode WithResponse:operation.responseObject];
}

@end
