//
//  PWPService.h
//  PawPark
//
//  Created by daffolap19 on 5/10/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PWPService : NSObject

+(instancetype) sharedInstance;

-(void)registerWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)loginWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)loginUsingTwitterWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;


-(void)searchPacksWithRequestDictionary:(NSMutableDictionary *)requestDictionary withLimitStart:(NSNumber *)limitStart withSuccessBlock:(void (^)(id responseDicitionary, NSNumber * limitStart))successBlock withErrorBlock:(void (^)(NSError * error, NSNumber * limitStart))errorBlock;


-(void)currentUserProfileWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)dogsBreedWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)createNewDogWithRequestDictionary:(NSMutableDictionary *)requestDictionary withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)placeSubmissionRequestDictionary:(NSMutableDictionary *)requestDictionary pictureData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)deleteDogWithDogId:(NSString *)dogIdString withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;
-(void)sendDogFriendsRequest:(NSString *)dogIdString fromDog:(NSString *)fromDogId withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)cancelDogFriendsRequest:(NSString *)dogIdString fromDog:(NSString *)fromDogId withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)getParksWithRequestDictionary:(NSMutableDictionary *)requestDictionary withPageIndex:(NSNumber *)pageIndex withPlaceCategory:(NSString *)placeCategory withSuccessBlock:(void (^)(id response, NSNumber * pageIndex))successBlock withErrorBlock:(void (^)(NSError * error,  NSNumber * pageIndex))errorBlock;

-(void)checkInParkWithRequestDictionary:(NSMutableDictionary *)requestDictionary withParkId:(NSString *)parkIdString withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)newYorkZipCodesWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)currentUserDogsWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)dateParkWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)currentUserParkWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)sponsorDetailsWithSuccessBlock:(void (^)(NSString *name,NSString * image,NSString * link, NSString * sponsoreeName ,NSString * sponsoreeImageUrl,NSString * sponsoreeLink))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)friendRequestsWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)friendRequestsAcceptWithIntiatorString:(NSString *)initiatorId withReciepientString:(NSString *)reciepientID withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)friendRequestsDeclineWithIntiatorString:(NSString *)initiatorId withReciepientString:(NSString *)reciepientID withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)updateCurrentUserProfileWithRequestDictionary:(NSMutableDictionary *)requestDictionary WithUserId:(NSString *)userID  withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)logoutWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)newMessageWithIntiatorString:(NSString *)initiatorId withReciepientString:(NSString *)reciepientID withDictionary:(NSDictionary *)reqDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)messagesWithIntiatorString:(NSString *)userID withDictionary:(NSDictionary *)reqDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)sendMessagesWithIntiatorString:(NSString *)intiatingDogId WithThreadID:(NSString *)threadId withDictionary:(NSDictionary *)reqDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)updateDogWithRequestDictionary:(NSMutableDictionary *)requestDictionary withDogId:(NSString *)dogIdString withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)dateRequestsAcceptWithDateIdString:(NSString *)dateIDString withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)dateRequestsDeclineWithDateIdString:(NSString *)dateIDString withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)friendRejectWithIntiatorString:(NSString *)initiatorId withReciepientString:(NSString *)reciepientID withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)dogProfileWithDogId:(NSString *)dogId WithSuccessBlock:(void (^)(id response,NSString * dogID))successBlock withErrorBlock:(void (^)(NSError * error,NSString * dogID))errorBlock;

-(void)homePark:(NSString *)parkId withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)getAllCheckInWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)eventsWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)loginUsingFacebookWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)eventModewithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)eventParkIdwithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)eventParkDetailswithSuccessBlock:(void (^)(id))successBlock withErrorBlock:(void (^)(NSError *))errorBlock;

-(void)inquireSubmitForDogId:(NSString *)dogIDString WithDictionary:(NSMutableDictionary *)dictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)readThread:(NSString *)threadIDString withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)userInformation:(NSString *)userIDString withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)youtubeVideoUrlWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)deleteHomePark:(NSString *)parkId withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)parksCategoriesWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)KibbleCounterSponsorImageWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)forgotPasswordWithDictionary:(NSMutableDictionary *)requestDictionary WithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)skinModeWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)backgroundPathWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)parksCategoriesImageUrlsWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)zipCodesWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)splash:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)uploadNewDogImage:(NSMutableDictionary *)requestDictionary withDogId:(NSString *)dogId withProgress:(void (^)(float progress))progressBlock withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)deleteDogImageID:(NSString *)dogImageId withDogId:(NSString *)dogId withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)advertisement:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)uploadNewPlaceImage:(NSMutableDictionary *)requestDictionary withDogId:(NSString *)dogId withProgress:(void (^)(float progress))progressBlock withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)eventDetailWithDictionary:(NSDictionary *)requestDictionary WithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)currentUserEventWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)googleAPIGeoCodeWithLat:(NSNumber *)lat withLongi:(NSNumber *)longi withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock ;

-(void)homeBatchRequestWithSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)splashBatchRequestWithURL:(NSArray *)urls SuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)community:(NSString*)communityId homeBatchRequestWithSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)currentCommunityWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)createNewPostWithCommunityId:(NSString *)communityId WithRequestDictionary:(NSMutableDictionary *)requestDictionary withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)optOuttWithCommunityId:(NSString *)communityId WithRequestDictionary:(NSMutableDictionary *)requestDictionary withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)post:(NSString*)postId comment:(NSString *)communityId withRequestDictionary:(NSMutableDictionary *)requestDictionary withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)newPost:(NSString*)postId comment:(NSString *)communityId withRequestDictionary:(NSMutableDictionary *)requestDictionary withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)joinCommunityId:(NSString *)communityId WithRequestDictionary:(NSMutableDictionary *)requestDictionary withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)membersCommunityId:(NSString *)communityId WithRequestDictionary:(NSMutableDictionary *)requestDictionary withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;

-(void)checkedIdDogsOfParkId:(NSString *)parkId withPageIndex:(NSNumber *)pageIndex WithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary,NSNumber * pageIndex))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock;
@end
