

//
//  PWPService.m
//  PawPark
//
//  Created by daffolap19 on 5/10/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPService.h"
#import "PWPHTTPClient.h"
#import "PWPConstant.h"
#import "SXCUtility.h"
#import "PWPApplicationUtility.h"
#import <Mixpanel.h>

@implementation PWPService


+(instancetype) sharedInstance
{
    static PWPService *sharedInstance;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        sharedInstance = [[PWPService alloc] init];
    });
    return sharedInstance;
}

-(void)registerWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@%@",K_URL_BASE,K_URL_REGISTER];
    
    
    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:errorBlock];
}


-(void)loginWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@%@",K_URL_BASE,K_URL_LOGIN];
    
    
    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {

        successBlock(response);
        
    }withErrorBlock:errorBlock];
}

-(void)loginUsingTwitterWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@%@",K_URL_BASE,@"login/twitter"];
    
    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:errorBlock];
}

-(void)loginUsingFacebookWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@%@",K_URL_BASE,@"login/facebook"];
    
    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:errorBlock];
}

-(void)searchPacksWithRequestDictionary:(NSMutableDictionary *)requestDictionary withLimitStart:(NSNumber *)limitStart withSuccessBlock:(void (^)(id responseDicitionary, NSNumber * limitStart))successBlock withErrorBlock:(void (^)(NSError * error, NSNumber * limitStart))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@%@",K_URL_BASE,K_URL_DOGS];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        
        successBlock(response,limitStart);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error,limitStart);
    }];
}

-(void)currentUserParkWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSDictionary * reqDictionary=@{@"apikey":[SXCUtility getNSObject:@"apikey"],@"app_version":@"8.9.0"};
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@current/place",K_URL_BASE];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:(id)reqDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:errorBlock];
}


-(void)currentUserProfileWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSDictionary * reqDictionary=@{@"apikey":[SXCUtility getNSObject:@"apikey"]};
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@%@",K_URL_BASE,K_URL_USER_PROFILE];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:(id)reqDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:errorBlock];
}

-(void)updateCurrentUserProfileWithRequestDictionary:(NSMutableDictionary *)requestDictionary WithUserId:(NSString *)userID  withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@users/%@?apikey=%@",K_URL_BASE,userID,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestPUTServiceOnURL:urlString WithDictionary:(id)requestDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:errorBlock];
}

-(void)currentUserDogsWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSDictionary * reqDictionary=@{@"apikey":[SXCUtility getNSObject:@"apikey"]};
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@%@",K_URL_BASE,K_URL_USER_DOGS];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:(id)reqDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:errorBlock];
}

-(void)dogsBreedWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
   
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@breeds",K_URL_BASE];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:errorBlock];
}

-(void)createNewDogWithRequestDictionary:(NSMutableDictionary *)requestDictionary withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@%@?apikey=%@&wrap=1",K_URL_BASE,K_URL_DOGS,[SXCUtility getNSObject:@"apikey"]];
    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:errorBlock];
}



-(void)uploadNewDogImage:(NSMutableDictionary *)requestDictionary withDogId:(NSString *)dogId withProgress:(void (^)(float progress))progressBlock withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock {

    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];

    NSString * urlString=[NSString stringWithFormat:@"%@dogs/%@/images?apikey=%@",K_URL_BASE,dogId,[SXCUtility getNSObject:@"apikey"]];

    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:requestDictionary withProgress:^(float progress)
     {
         progressBlock(progress);
     }withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:errorBlock];

}

-(void)uploadNewPlaceImage:(NSMutableDictionary *)requestDictionary withDogId:(NSString *)dogId withProgress:(void (^)(float progress))progressBlock withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock {

    NSMutableDictionary * dictionary = [[NSMutableDictionary alloc] initWithDictionary:requestDictionary];
    [dictionary setValue:dogId forKey:@"place"];

    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];

    NSString * urlString=[NSString stringWithFormat:@"%@placeimagesubmissions?apikey=%@",K_URL_BASE,[SXCUtility getNSObject:@"apikey"]];

    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:dictionary withProgress:^(float progress)
    {
        progressBlock(progress);
    }withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:errorBlock];
    
}


-(void)deleteDogImageID:(NSString *)dogImageId withDogId:(NSString *)dogId withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock {

    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];

    NSString * urlString=[NSString stringWithFormat:@"%@dogs/%@/images/%@?apikey=%@",K_URL_BASE,dogId,dogImageId,[SXCUtility getNSObject:@"apikey"]];
    [httpClient requestDELETEServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:errorBlock];
}



-(void)placeSubmissionRequestDictionary:(NSMutableDictionary *)requestDictionary pictureData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@%@?apikey=%@&wrap=1",K_URL_BASE,K_URL_PLACE,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        successBlock(response);
    } withErrorBlock:^(NSError *error) {
        errorBlock(error);
    }];
}

-(void)updateDogWithRequestDictionary:(NSMutableDictionary *)requestDictionary withDogId:(NSString *)dogIdString withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@dogs/%@?apikey=%@",K_URL_BASE,dogIdString,[SXCUtility getNSObject:@"apikey"]];
    
    NSMutableDictionary * dictionary =requestDictionary.mutableCopy;
    if(data){
        NSString * imageDataString =[[NSString alloc] initWithFormat:@"data:image/jpeg;base64,%@",[data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
        [dictionary setValue:imageDataString forKey:@"imageFile"];
        
    }
    
    
    [httpClient requestPUTHTTPServiceOnURL:urlString WithDictionary:dictionary withSuccessBlock:^(id response) {
        successBlock(response);
    } withErrorBlock:errorBlock];
}

-(void)deleteDogWithDogId:(NSString *)dogIdString withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@%@/%@?apikey=%@",K_URL_BASE,K_URL_DOGS,dogIdString,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestDELETEServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:errorBlock];
    
}

-(void)sendDogFriendsRequest:(NSString *)dogIdString fromDog:(NSString *)fromDogId withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@%@/%@/%@/%@?apikey=%@",K_URL_BASE,K_URL_DOGS,fromDogId,K_URL_INVITIES,dogIdString,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:errorBlock];
    
}

-(void)cancelDogFriendsRequest:(NSString *)dogIdString fromDog:(NSString *)fromDogId withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@%@/%@/%@/%@/cancel?apikey=%@",K_URL_BASE,K_URL_DOGS,fromDogId,K_URL_INVITIES,dogIdString,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestPATCHServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:errorBlock];
    
}


-(void)getParksWithRequestDictionary:(NSMutableDictionary *)requestDictionary withPageIndex:(NSNumber *)pageIndex withPlaceCategory:(NSString *)placeCategory withSuccessBlock:(void (^)(id response, NSNumber * pageIndex))successBlock withErrorBlock:(void (^)(NSError * error,  NSNumber * pageIndex))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@%@",K_URL_BASE,K_URL_PARKS];
    
    if(placeCategory != nil && placeCategory.length>0){
        urlString=[NSString stringWithFormat:@"%@%@/%@",K_URL_BASE,K_URL_PARKS,placeCategory];
    }
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        
        successBlock(response,pageIndex);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error,pageIndex);
    }];
}

-(void)checkInParkWithRequestDictionary:(NSMutableDictionary *)requestDictionary withParkId:(NSString *)parkIdString withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@places/%@/checkins?apikey=%@",K_URL_BASE,parkIdString,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestPOSTHTTPServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}

-(void)dateParkWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@dates?apikey=%@",K_URL_BASE,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestPOSTHTTPServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}

-(void)eventsWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@events?apikey=%@",K_URL_BASE,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}


-(void)newYorkZipCodesWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@zipcode/list",K_URL_BASE];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}

-(void)zipCodesWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];

    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@zipcodes",K_URL_BASE];

    [httpClient requestGETServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {

        successBlock(response);

    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}



-(void)sponsorDetailsWithSuccessBlock:(void (^)(NSString *name,NSString * image,NSString * link, NSString * sponsoreeName ,NSString * sponsoreeImageUrl,NSString * sponsoreeLink))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    
    
    __block NSString * sponsorName;
    __block NSString * sponsorImage;
    __block NSString * sponsorURL;
    
    __block NSString * sponsoreeName;
    __block NSString * sponsoreeImage;
    __block NSString * sponsoreeURL;

    dispatch_group_t group = dispatch_group_create();
    
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];

    dispatch_group_enter(group);

    NSString * urlNameString=[NSString stringWithFormat:@"%@setting.json?key=sponsor.name",K_URL_BASE];

    [httpClient requestGETHTTPServiceOnURL:urlNameString WithDictionary:nil withSuccessBlock:^(id response) {
        
        sponsorName=[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        dispatch_group_leave(group);
        
    }withErrorBlock:^(NSError * error) {
        dispatch_group_leave(group);
    }];
  
    dispatch_group_enter(group);

    NSString * urlURLString=[NSString stringWithFormat:@"%@setting.json?key=sponsor.url",K_URL_BASE];
    
    [httpClient requestGETHTTPServiceOnURL:urlURLString WithDictionary:nil withSuccessBlock:^(id response) {
        
        sponsorURL=[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        dispatch_group_leave(group);
        
    }withErrorBlock:^(NSError * error) {
                dispatch_group_leave(group);
    }];

    
    dispatch_group_enter(group);

    NSString * urlImageString=[NSString stringWithFormat:@"%@setting.json?key=sponsor.image",K_URL_BASE];
    [httpClient requestGETHTTPServiceOnURL:urlImageString WithDictionary:nil withSuccessBlock:^(id response) {
        
        sponsorImage=[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        dispatch_group_leave(group);
        
    }withErrorBlock:^(NSError * error) {
                dispatch_group_leave(group);
    }];

    dispatch_group_enter(group);

    
    NSString * sponsoreeUrlImageString=[NSString stringWithFormat:@"%@setting.json?key=sponsoree.image",K_URL_BASE];
    [httpClient requestGETHTTPServiceOnURL:sponsoreeUrlImageString WithDictionary:nil withSuccessBlock:^(id response) {
        
        sponsoreeImage=[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        dispatch_group_leave(group);
        
    }withErrorBlock:^(NSError * error) {
        dispatch_group_leave(group);
    }];

    dispatch_group_enter(group);

    
    NSString * sponreeUrl=[NSString stringWithFormat:@"%@setting.json?key=sponsoree.url",K_URL_BASE];
    
    [httpClient requestGETHTTPServiceOnURL:sponreeUrl WithDictionary:nil withSuccessBlock:^(id response) {
        
        sponsoreeURL=[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        dispatch_group_leave(group);
        
    }withErrorBlock:^(NSError * error) {
        dispatch_group_leave(group);
    }];
    
    dispatch_group_enter(group);

    
    NSString * sponreeName=[NSString stringWithFormat:@"%@setting.json?key=sponsoree.name",K_URL_BASE];
    
    [httpClient requestGETHTTPServiceOnURL:sponreeName WithDictionary:nil withSuccessBlock:^(id response) {
        
        sponsoreeName=[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        dispatch_group_leave(group);
        
    }withErrorBlock:^(NSError * error) {
        dispatch_group_leave(group);
    }];

    dispatch_group_notify(group, dispatch_get_main_queue(), ^{        
        successBlock(sponsorName,sponsorImage,sponsorURL,sponsoreeName,sponsoreeImage ,sponsoreeURL);
    });

    

}

-(void)friendRequestsWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{

    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@current/notifications?apikey=%@",K_URL_BASE,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}

-(void)friendRequestsAcceptWithIntiatorString:(NSString *)initiatorId withReciepientString:(NSString *)reciepientID withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@dogs/%@/invites/%@/accept?apikey=%@",K_URL_BASE,reciepientID,initiatorId,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestPATCHServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}

-(void)friendRejectWithIntiatorString:(NSString *)initiatorId withReciepientString:(NSString *)reciepientID withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@dogs/%@/reject/%@/accept?apikey=%@",K_URL_BASE,reciepientID,initiatorId,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestPATCHServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}


-(void)dateRequestsAcceptWithDateIdString:(NSString *)dateIDString withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@daterecipients/%@/accept?apikey=%@",K_URL_BASE,dateIDString,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestPATCHServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}

-(void)dateRequestsDeclineWithDateIdString:(NSString *)dateIDString withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@daterecipients/%@/decline?apikey=%@",K_URL_BASE,dateIDString,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestPATCHServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}


-(void)friendRequestsDeclineWithIntiatorString:(NSString *)initiatorId withReciepientString:(NSString *)reciepientID withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{

    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@dogs/%@/invites/%@/reject?apikey=%@",K_URL_BASE,reciepientID,initiatorId,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestPATCHServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}

-(void)newMessageWithIntiatorString:(NSString *)initiatorId withReciepientString:(NSString *)reciepientID withDictionary:(NSDictionary *)reqDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@dogs/%@/threads/%@?apikey=%@",K_URL_BASE,initiatorId,reciepientID,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:(id)reqDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}


-(void)logoutWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@logout?apikey=%@",K_URL_BASE,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}


-(void)messagesWithIntiatorString:(NSString *)userID withDictionary:(NSDictionary *)reqDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{

    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    
//    http://test.pawparks.nyc/apis/v1/users/245/messages
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@users/%@/messages?apikey=%@",K_URL_BASE,userID,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];

}


-(void)homePark:(NSString *)parkId withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{


    [[Mixpanel sharedInstance] track:@"Favorite added"];

    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@places/%@/home?apikey=%@",K_URL_BASE,parkId,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
    
}

-(void)deleteHomePark:(NSString *)parkId withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@places/%@/home?apikey=%@",K_URL_BASE,parkId,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestDELETEServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
    
}



-(void)dogProfileWithDogId:(NSString *)dogId WithSuccessBlock:(void (^)(id response,NSString * dogID))successBlock withErrorBlock:(void (^)(NSError * error,NSString * dogID))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    
    //    http://test.pawparks.nyc/apis/v1/users/245/messages
    //url String
    NSString * urlString=[NSString stringWithFormat:@"%@dogs/%@?apikey=%@",K_URL_BASE,dogId,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        successBlock(response,dogId);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error,dogId);
    }];
    
}


///ap{api_kind}/v1/dogs/{id}

-(void)sendMessagesWithIntiatorString:(NSString *)intiatingDogId WithThreadID:(NSString *)threadId withDictionary:(NSDictionary *)reqDictionary withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    
    NSString * urlString=[NSString stringWithFormat:@"%@dogs/%@/threads/%@/replies?apikey=%@",K_URL_BASE,intiatingDogId,threadId,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:(id)reqDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
    
}

-(void)getAllCheckInWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    
    NSString * settingUrlString = [NSString stringWithFormat:@"calls[]=/apis/v1/settings&"];
    NSString * leadersBoardString = [NSString stringWithFormat:@"calls[]=/apis/v1/checkin/leaderboard?apikey=%@&user=%@",[SXCUtility getNSObject:@"apikey"],[PWPApplicationUtility getCurrentUserProfileDictionary][@"id"]];
    NSString * urlString=[NSString stringWithFormat:@"%@batch?%@%@",K_URL_BASE,settingUrlString,leadersBoardString];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:(id)nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
    
}

-(void)eventModewithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    
    NSString * eventModeUrl=[NSString stringWithFormat:@"%@setting.json?key=event.mode",K_URL_BASE];
    
    [httpClient requestGETHTTPServiceOnURL:eventModeUrl WithDictionary:nil withSuccessBlock:^(id response) {
        
        NSString * responseString=[[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"\"" withString:@""];

        successBlock(responseString);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
    
}

-(void)eventParkIdwithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    
    NSString * eventModeUrl=[NSString stringWithFormat:@"%@setting.json?key=event.place.id",K_URL_BASE];
    
    [httpClient requestGETHTTPServiceOnURL:eventModeUrl WithDictionary:nil withSuccessBlock:^(id response) {
        
       NSString * parkIdString=[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        successBlock([parkIdString stringByReplacingOccurrencesOfString:@"\"" withString:@""]);
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
    
}

-(void)parkDetails:(NSString *)parkString withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * eventModeUrl=[NSString stringWithFormat:@"%@places/%@?apikey=%@&app_version=8.9.0",K_URL_BASE,parkString,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestGETServiceOnURL:eventModeUrl WithDictionary:nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
    
}

-(void)eventParkDetailswithSuccessBlock:(void (^)(id))successBlock withErrorBlock:(void (^)(NSError *))errorBlock
{
    [self eventParkIdwithSuccessBlock:^(id response) {
        [self parkDetails:response withSuccessBlock:^(id response) {
            successBlock(response);
        } withErrorBlock:^(NSError *error) {
            errorBlock(error);
        }];
    } withErrorBlock:^(NSError *error) {
        errorBlock(error);
    }];
}

-(void)inquireSubmitForDogId:(NSString *)dogIDString  WithDictionary:(NSMutableDictionary *)dictionary  withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];

    NSString * urlString=[NSString stringWithFormat:@"%@adoptions/%@",K_URL_BASE,dogIDString];
    
    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:dictionary withSuccessBlock:^(id response) {
        successBlock(response);        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}

-(void)readThread:(NSString *)threadIDString withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@threads/%@/read?apikey=%@",K_URL_BASE,threadIDString,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestPATCHServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}

-(void)userInformation:(NSString *)userIDString withSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@users/%@?apikey=%@",K_URL_BASE,userIDString,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}

-(void)youtubeVideoUrlWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@setting.json?key=slbbr.video",K_URL_LIVE_BASE];
    
    [httpClient requestGETHTTPServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        NSError * error;
        
        NSString *jsonString = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&error];
        
        id formattedResponse = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
        successBlock(formattedResponse[@"Videos"]);
        
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}


-(void)KibbleCounterSponsorImageWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@setting.json?key=sponsor.ldbd.image",K_URL_LIVE_BASE];
    
    [httpClient requestGETHTTPServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        
        NSString *sponsorURL=[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        successBlock(sponsorURL);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}


-(void)parksCategoriesWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@place-categories",K_URL_BASE];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}

-(void)forgotPasswordWithDictionary:(NSMutableDictionary *)requestDictionary WithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@password/recover",K_URL_BASE];
    
    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}


-(void)skinModeWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@setting?key=isSkinModeEnabled",K_URL_BASE];
    
    [httpClient requestGETHTTPServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}

-(void)backgroundPathWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@setting?key=SkinPath",K_URL_BASE];
    
    [httpClient requestGETHTTPServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}


-(void)parksCategoriesImageUrlsWithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@place-markers",K_URL_BASE];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        successBlock(response);
        
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}

-(void)splash:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock {

    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];

    NSString * zipCodeString=[PWPApplicationUtility getCurrentUserProfileDictionary][@"zipcode"];

    NSString * urlString=[NSString stringWithFormat:@"%@skin?apikey=%@&zipcode=%@", K_URL_BASE, [SXCUtility getNSObject:@"apikey"], zipCodeString];

    [httpClient requestGETServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        successBlock(response);

    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}

-(void)advertisement:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock {

    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];

    NSString * urlString=[NSString stringWithFormat:@"%@advertisement?apikey=%@",K_URL_BASE,[SXCUtility getNSObject:@"apikey"]];
    [httpClient requestGETServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:^(NSError * error) {
        errorBlock(error);
    }];
}




-(void)eventDetailWithDictionary:(NSDictionary *)requestDictionary WithSuccessBlock:(void (^)(id response))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock{
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    // NSString * urlString=[NSString stringWithFormat:@"http://www.slobbr.com/apis/v1/events/%@",[requestDictionary valueForKey:@"id"]];
    // NSLog(@"%@",urlString);
    // [httpClient requestPOSTServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
    //    successBlock(response);
    
    //  }withErrorBlock:^(NSError * error) {
    //     errorBlock(error);
    //  }];
    
    
    
    
    
    // NSString * urlString=[NSString stringWithFormat:@"http://www.slobbr.com/apis/v1/events/%@",[requestDictionary valueForKey:@"id"]];
    
    NSString * urlString=[NSString stringWithFormat:@"%@%@/%@",K_URL_BASE,K_URL_EVENTS,[requestDictionary valueForKey:@"id"]];
    
    NSLog(@"%@",urlString);
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:(id)nil withSuccessBlock:^(id response) {
        
        successBlock(response);
        NSLog(@"response %@",response);
        
    }withErrorBlock:errorBlock];
    
}



-(void)currentUserEventWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    //NSDictionary * reqDictionary=@{@"apikey":[SXCUtility getNSObject:@"apikey"]};
    //url String
    //NSString * urlString=[NSString stringWithFormat:@"events"];
    NSString * urlString=[NSString stringWithFormat:@"%@%@",K_URL_BASE,K_URL_EVENTS];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:(id)requestDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        NSLog(@"response %@",response);
        
    }withErrorBlock:errorBlock];
}

-(void)googleAPIGeoCodeWithLat:(NSNumber *)lat withLongi:(NSNumber *)longi withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock {

    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    NSString * urlString=[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCywuPX8xyo8zvxxn6PiLRMNlY-eAWYkGQ&latlng=%@,%@",lat,longi];

    [httpClient requestGETServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:errorBlock];


}

-(void)homeBatchRequestWithSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock {

    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
  
    NSString * favPlaces = [NSString stringWithFormat:@"calls[]=/apis/v1/current/place&"];
   
    NSString * loggedInUser = [NSString stringWithFormat:@"calls[]=/apis/v1/current/user&"];

    NSString * advertisements = [NSString stringWithFormat:@"calls[]=/apis/v1/advertisement&"];
    
    NSString * notifications = [NSString stringWithFormat:@"calls[]=/apis/v1/current/notifications&"];
    
    NSString * eventMode = [NSString stringWithFormat:@"calls[]=/apis/v1/setting.json?key=event.mode"];
    
    NSString * urlString=[NSString stringWithFormat:@"%@batch?%@%@%@%@%@&apikey=%@&app_version=8.9.0",K_URL_BASE,favPlaces,loggedInUser,advertisements,notifications,eventMode,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:errorBlock];

}

-(void)community:(NSString*)communityId homeBatchRequestWithSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock {
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];

    NSString * favPlaces = [NSString stringWithFormat:@"calls[]=/apis/v1/current/place&"];
    NSString * communities = [NSString stringWithFormat:@"calls[]=/apis/v1/communities/%@/posts",communityId];
    
    NSString * urlString=[NSString stringWithFormat:@"%@batch?%@%@&apikey=%@&app_version=8.9.0",K_URL_BASE,favPlaces,communities,[SXCUtility getNSObject:@"apikey"]];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:nil withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:errorBlock];
    
}


-(void)splashBatchRequestWithURL:(NSArray *)urls SuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock {
    
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    NSString * urlString=[NSString stringWithFormat:@"%@batch",K_URL_BASE];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:@{@"calls":urls}.mutableCopy withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:errorBlock];
    
}

-(void)currentCommunityWithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSDictionary * reqDictionary=@{@"apikey":[SXCUtility cleanString:[[SXCUtility getNSObject:@"apikey"] description]],@"app_version":@"8.9.0"};
   
    NSString * urlString=[NSString stringWithFormat:@"%@current/communities",K_URL_BASE];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:(id)reqDictionary withSuccessBlock:^(id response) {
        
        successBlock(response);
        
    }withErrorBlock:errorBlock];
}

-(void)createNewPostWithCommunityId:(NSString *)communityId WithRequestDictionary:(NSMutableDictionary *)requestDictionary withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@communities/%@/posts?apikey=%@",K_URL_BASE,communityId,[SXCUtility getNSObject:@"apikey"]];
    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:errorBlock];
}


-(void)optOuttWithCommunityId:(NSString *)communityId WithRequestDictionary:(NSMutableDictionary *)requestDictionary withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@communities/%@/exit?apikey=%@",K_URL_BASE,communityId,[SXCUtility getNSObject:@"apikey"]];
    [httpClient requestPATCHHTTPServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:errorBlock];
}


-(void)post:(NSString*)postId comment:(NSString *)communityId withRequestDictionary:(NSMutableDictionary *)requestDictionary withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@communities/%@/posts/%@/comments?apikey=%@",K_URL_BASE,communityId,postId,[SXCUtility getNSObject:@"apikey"]];
    [httpClient requestGETServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:errorBlock];
}

-(void)newPost:(NSString*)postId comment:(NSString *)communityId withRequestDictionary:(NSMutableDictionary *)requestDictionary withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@communities/%@/posts/%@/comments?apikey=%@",K_URL_BASE,communityId,postId,[SXCUtility getNSObject:@"apikey"]];
    [httpClient requestPOSTServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:errorBlock];
}

-(void)joinCommunityId:(NSString *)communityId WithRequestDictionary:(NSMutableDictionary *)requestDictionary withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@communities/%@/join?apikey=%@",K_URL_BASE,communityId,[SXCUtility getNSObject:@"apikey"]];
    [httpClient requestPATCHServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:errorBlock];
}

-(void)membersCommunityId:(NSString *)communityId WithRequestDictionary:(NSMutableDictionary *)requestDictionary withData:(NSData *)data withSuccessBlock:(void (^)(id responseDicitionary))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@communities/%@/users?apikey=%@&app_version=8.9.0",K_URL_BASE,communityId,[SXCUtility getNSObject:@"apikey"]];
    [httpClient requestGETServiceOnURL:urlString WithDictionary:requestDictionary withSuccessBlock:^(id response) {
        successBlock(response);
    }withErrorBlock:errorBlock];
}

-(void)checkedIdDogsOfParkId:(NSString *)parkId withPageIndex:(NSNumber *)pageIndex WithRequestDictionary:(NSMutableDictionary *)requestDictionary withSuccessBlock:(void (^)(id responseDicitionary,NSNumber * pageIndex))successBlock withErrorBlock:(void (^)(NSError * error))errorBlock
{
    PWPHTTPClient * httpClient=[PWPHTTPClient sharedInstance];
    
    NSString * urlString=[NSString stringWithFormat:@"%@%@",K_URL_BASE,@"checkindogs"];
    
    [httpClient requestGETServiceOnURL:urlString WithDictionary:(id)requestDictionary withSuccessBlock:^(id response) {
        successBlock(response,pageIndex);
    }withErrorBlock:errorBlock];
}


@end
