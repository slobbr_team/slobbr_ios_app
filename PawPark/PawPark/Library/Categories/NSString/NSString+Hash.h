//
//  NSString+Hash.h
//  PawPark
//
//  Created by Samarth Singla on 20/11/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Hash)

- (NSString *)MD5;

@end
