//
//  UIColor+UBRColor.h
//  UrbanRunr
//
//  Created by xyz on 26/12/14.
//  Copyright (c) 2014 Damilola. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PWDColor)

+(UIColor *)sxc_themeColor;
+(UIColor *)sxc_themeColorWithAplha;
+(UIColor *)sxc_textfieldPlacholderTextColor;
+(UIColor *)sxc_textfieldAccessoryViewTintColor;

+(UIColor *)sxc_shopTabSelectedColor;
+(UIColor *)sxc_buttonIntialBackgroundColor1;
+(UIColor *)sxc_buttonIntialBackgroundColor2;
+(UIColor *)sxc_buttonIntialBackgroundColor3;
+(UIColor *)sxc_buttonIntialBackgroundColor4;

+(UIColor *)sxc_pendingColor;
+(UIColor *)sxc_rejectColor;
+(UIColor *)sxc_approvedColor;

+(UIColor *)sxc_appBackgroundWhiteColor;
+(UIColor *)sxc_appBackgroundDarkGrayColor;
+(UIColor *)sxc_appTextfieldTextColor;


+(UIColor *)sxc_hintColor;

+(UIColor *)sxc_sideMenuTextColor;

@end
