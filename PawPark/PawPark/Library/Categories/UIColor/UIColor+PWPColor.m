//
//  UIColor+UBRColor.m
//  UrbanRunr
//
//  Created by xyz on 26/12/14.
//  Copyright (c) 2014 Damilola. All rights reserved.
//

#import "UIColor+PWPColor.h"

@implementation UIColor (PWDColor)

+(UIColor *)sxc_themeColor
{

    return [UIColor colorWithRed:43/255.0f green:195/255.0f blue:154/255.0f alpha:1.0f];;
}

+(UIColor *)sxc_themeColorWithAplha
{
    
    return [UIColor colorWithRed:41/255.0f green:177/255.0f blue:114/255.0f alpha:0.3f];;
}

+(UIColor *)sxc_textfieldPlacholderTextColor
{
    return [UIColor colorWithRed:146.0f/255.0f green:146/255.0f blue:146/255.0f alpha:1.0f];
}

+(UIColor *)sxc_textfieldAccessoryViewTintColor
{
    return [UIColor colorWithRed:105.0/255.0f green:215.0f/255.0f blue:121/255.0f alpha:1.0f];
}

+(UIColor *)sxc_shopTabSelectedColor
{
    return [UIColor colorWithWhite:1 alpha:.2];
}

+(UIColor *)sxc_buttonIntialBackgroundColor1
{
    return [UIColor colorWithRed:255.0/255.0 green:127.0/255.0 blue:127.0/255.0 alpha:1.0];
}

+(UIColor *)sxc_buttonIntialBackgroundColor2
{
    return [UIColor colorWithRed:8.0/255.0 green:125.0/255.0 blue:186.0/255.0 alpha:1.0];
}

+(UIColor *)sxc_buttonIntialBackgroundColor3
{
    return [UIColor colorWithRed:246.0/255.0 green:141.0/255.0 blue:86.0/255.0 alpha:1.0];
}

+(UIColor *)sxc_buttonIntialBackgroundColor4
{
    return [UIColor colorWithRed:59.0/255.0 green:184.0/255.0 blue:120.0/255.0 alpha:1.0];
}

+(UIColor *)sxc_pendingColor
{
    return [UIColor colorWithRed:247.0/255.0 green:210.0/255.0 blue:8.0/255.0 alpha:1.0];
}

+(UIColor *)sxc_rejectColor
{
    return [UIColor colorWithRed:146.0/255.0 green:146.0f/255.0 blue:146.0f/255.0 alpha:1.0];
}

+(UIColor *)sxc_approvedColor
{
    return [UIColor colorWithRed:101.0/255.0 green:178.0/255.0 blue:8.0/255.0 alpha:1.0];
}

+(UIColor *)sxc_appBackgroundWhiteColor
{
    return [UIColor whiteColor];
}

+(UIColor *)sxc_appBackgroundDarkGrayColor
{
    return [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0];
}

+(UIColor *)sxc_appTextfieldTextColor
{
    return [UIColor colorWithRed:99.0/255.0 green:99.0/255.0 blue:99.0/255.0 alpha:1.0];
}

+(UIColor *)sxc_hintColor
{
    return [UIColor colorWithRed:146/255.0f green:146/255.0f blue:146.0f/255.0f alpha:1.0f];

}

+(UIColor *)sxc_sideMenuTextColor
{
    return [UIColor colorWithRed:161/255.0f green:161/255.0f blue:161.0f/255.0f alpha:1.0f];
    
}
@end
