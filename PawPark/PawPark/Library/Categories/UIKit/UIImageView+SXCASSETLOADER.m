//
//  UIImageView+SXCASSETLOADER.m
//  SixthContinent
//
//  Created by xyz Ahuja on 27/03/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import "UIImageView+SXCASSETLOADER.h"

@implementation UIImageView (SXCAsset_Loader)
-(void)setImageFromAssetLibrary:(NSString *)strUrl{
    
        ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
        {
            ALAssetRepresentation *rep = [myasset defaultRepresentation];
            CGImageRef iref = [rep fullResolutionImage];
            if (iref) {
                UIImage *image = [UIImage imageWithCGImage:iref];
                [self setImage:image];
            }
        };
    
        ALAssetsLibraryAccessFailureBlock failureblock  = ^(NSError *myerror)
        {
            NSLog(@"cant get image - %@",[myerror localizedDescription]);
            //setPlaceholder
            UIImage *image = [UIImage imageNamed:@"feed_Upload_image_placeholder"];
            [self setImage:image];
        };
    
        if(strUrl && [strUrl length] && ![[strUrl pathExtension] isEqualToString:@"assets-library"])
        {
            
            NSURL *asseturl = [NSURL URLWithString:strUrl];
            ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
            [assetslibrary assetForURL:asseturl
                           resultBlock:resultblock
                          failureBlock:failureblock];
        }
}
@end
