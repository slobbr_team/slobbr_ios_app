//
//  UIImageView+SXCASSETLOADER.h
//  SixthContinent
//
//  Created by xyz Ahuja on 27/03/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

@import UIKit;
@import AssetsLibrary;
typedef void (^ALAssetsLibraryAssetForURLResultBlock)(ALAsset *asset);
typedef void (^ALAssetsLibraryAccessFailureBlock)(NSError *error);
@interface UIImageView (SXCAsset_Loader)
-(void)setImageFromAssetLibrary:(NSString *)strUrl;
@end
