////
//  UIViewController+IGComponents.m
//  iGate
//
//  Created by imran on 9/28/14.
//  Copyright (c) 2014 STC. All rights reserved.
//

#import "UIViewController+UBRComponents.h"
#import "SVProgressHUD.h"
#import <CSNotificationView.h>
#import "PWPAppDelegate.h"
#import "AFNetworkReachabilityManager.h"
#import "UIActionSheet+BlocksKit.h"
#import "MainViewController.h"
//
@implementation UIViewController (UBRComponents)

//#pragma mark - Native Alert View Methods
//-(void)sxc_showAlertViewWithmessage:(NSString *)messageString
//{
////    UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:messageString message:nil delegate:nil cancelButtonTitle:k_STRING_ALERT_OK otherButtonTitles:nil];
////    [alertView show];
//
//}
//
-(UIAlertView *)sxc_showAlertViewWithTitle:(NSString *)alertTitle message:(NSString *)messageString WithDelegate:(id<UIAlertViewDelegate>)delegate WithCancelButtonTitle:(NSString *)cancelString WithAcceptButtonTitle:(NSString *)acceptString
{
    UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:alertTitle message:messageString delegate:delegate cancelButtonTitle:cancelString otherButtonTitles:acceptString, nil];
    [alertView show];
    
    return alertView;
}
//
//#pragma mark - Action Sheet Methods
-(void)sxc_showActionSheetWithActionTitle:(NSString *)actionSheetTitle optionTitle:(NSArray *)titleArray WithActionArray:(NSArray *)actionArray
{
    UIActionSheet * actionSheet = [UIActionSheet bk_actionSheetWithTitle:actionSheetTitle];
    
    for(int i=0;i<titleArray.count;i++){
        [actionSheet bk_addButtonWithTitle:titleArray[i] handler:actionArray[i]];
    }
    [actionSheet bk_setCancelButtonWithTitle:@"Cancel" handler:nil];
    [actionSheet showInView:self.view];
    
}
//
//#pragma mark - CSNotificaitonView Methods
-(void)sxc_showErrorViewWithMessage:(NSString *)messageString
{
    UIViewController * rootViewController=((PWPAppDelegate*)[[UIApplication sharedApplication] delegate]).window.rootViewController;
    
    
    UIViewController * topViewController=(self.navigationController?self.navigationController:rootViewController);
    
    
    [CSNotificationView showInViewController:topViewController
                                       style:CSNotificationViewStyleError
                                     message:messageString];
    [self sxc_stopLoading];
    [self dismissLoader];
}
//
//-(void)sxc_showErrorViewUsingRootViewControllerWithMessage:(NSString *)messageString
//{
//    UIViewController * rootViewController=((SXCAppDelegate*)[[UIApplication sharedApplication] delegate]).window.rootViewController;
//
//    [CSNotificationView showInViewController:rootViewController
//                                       style:CSNotificationViewStyleError
//                                     message:messageString];
//    [self sxc_stopLoading];
//}
//
//
-(void)sxc_showSuccessWithMessage:(NSString *)messageString
{
    UIViewController * rootViewController=((PWPAppDelegate*)[[UIApplication sharedApplication] delegate]).window.rootViewController;
    
    
    UIViewController * topViewController=(self.navigationController?self.navigationController:rootViewController);
    
    
    [CSNotificationView showInViewController:topViewController
                                       style:CSNotificationViewStyleSuccess
                                     message:messageString];
    
    [self sxc_stopLoading];
    
}
//
////#pragma mark - Action Sheet Methods
////-(void)ubr_showActionSheetWithActionTitle:(NSString *)actionSheetTitle optionTitle:(NSArray *)titleArray WithActionArray:(NSArray *)actionArray
////{
////    UIActionSheet * actionSheet = [UIActionSheet bk_actionSheetWithTitle:actionSheetTitle];
////
////    for(int i=0;i<titleArray.count;i++){
////        [actionSheet bk_addButtonWithTitle:titleArray[i] handler:actionArray[i]];
////    }
////    [actionSheet bk_setCancelButtonWithTitle:@"Cancel" handler:nil];
////    [actionSheet showInView:self.view];
////
////}
//
-(void)sxc_showActionSheetWithTitle:(NSString *)title pickerWithRows:(NSArray *)pickerValues itemSelectedAtIndex:(NSInteger)selectedIndex doneBlock:(ActionStringDoneBlock)doneBlock cancelBlock:(ActionStringCancelBlock)cancelBlockOrNil origin:(id)origin
{
    ActionSheetStringPicker * actionSheetPicker=[ActionSheetStringPicker showPickerWithTitle:title rows:pickerValues initialSelection:selectedIndex doneBlock:doneBlock cancelBlock:cancelBlockOrNil origin:origin];
    actionSheetPicker.toolbar.tintColor=[UIColor darkGrayColor];
    
    [origin resignFirstResponder];
    
}

-(void)sxc_showDateOfBirthActionSheetWithTitle:(NSString *)title pickerWithRows:(NSArray *)pickerValues dateSelected:(NSDate *)selectedDate doneBlock:(ActionDateDoneBlock)doneBlock cancelBlock:(ActionDateCancelBlock)cancelBlockOrNil origin:(id)origin
{
    
    ActionSheetDatePicker * actionSheetPicker=[ActionSheetDatePicker showPickerWithTitle:title datePickerMode:UIDatePickerModeDate selectedDate:selectedDate minimumDate:nil maximumDate:[NSDate date] doneBlock:doneBlock cancelBlock:cancelBlockOrNil origin:origin];
    
    actionSheetPicker.toolbar.tintColor=[UIColor darkGrayColor];
    [origin resignFirstResponder];
    
}
-(void)sxc_showDateTimeActionSheetWithTitle:(NSString *)title pickerWithRows:(NSArray *)pickerValues dateSelected:(NSDate *)selectedDate doneBlock:(ActionDateDoneBlock)doneBlock cancelBlock:(ActionDateCancelBlock)cancelBlockOrNil origin:(id)origin
{
    
    ActionSheetDatePicker * actionSheetPicker=[ActionSheetDatePicker showPickerWithTitle:title datePickerMode:UIDatePickerModeDateAndTime selectedDate:selectedDate minimumDate:[NSDate date] maximumDate:nil doneBlock:doneBlock cancelBlock:cancelBlockOrNil origin:origin];
    
    actionSheetPicker.toolbar.tintColor=[UIColor darkGrayColor];
    [origin resignFirstResponder];
    
}


-(void)sxc_showTimeActionSheetWithTitle:(NSString *)title pickerWithRows:(NSArray *)pickerValues dateSelected:(NSDate *)selectedDate doneBlock:(ActionDateDoneBlock)doneBlock cancelBlock:(ActionDateCancelBlock)cancelBlockOrNil origin:(id)origin
{
    
    ActionSheetDatePicker * actionSheetPicker=[ActionSheetDatePicker showPickerWithTitle:title datePickerMode:UIDatePickerModeTime selectedDate:[NSDate date] minimumDate:selectedDate maximumDate:nil doneBlock:doneBlock cancelBlock:cancelBlockOrNil origin:origin];
    
    actionSheetPicker.toolbar.tintColor=[UIColor darkGrayColor];
    [origin resignFirstResponder];
    
}


-(void)sxc_showDateTimeActionSheetWithoutMinOrMaxDateWithTitle:(NSString *)title pickerWithRows:(NSArray *)pickerValues dateSelected:(NSDate *)selectedDate doneBlock:(ActionDateDoneBlock)doneBlock cancelBlock:(ActionDateCancelBlock)cancelBlockOrNil origin:(id)origin
{
    
    ActionSheetDatePicker * actionSheetPicker=[ActionSheetDatePicker showPickerWithTitle:title datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] minimumDate:nil maximumDate:nil doneBlock:doneBlock cancelBlock:cancelBlockOrNil origin:origin];
    
    actionSheetPicker.toolbar.tintColor=[UIColor darkGrayColor];
    [origin resignFirstResponder];
    
}


//
//
//
#pragma mark - Navigation related Methods

-(void)sxc_navigationBarCustomizationWithTitle:(NSString *)navigationTitle
{
    [self.navigationItem setTitle:[navigationTitle uppercaseString]];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    
    self.navigationController.navigationBarHidden=false;
}
//
-(void)sxc_setNavigationBarMenuItem
{
    UIImage * image=[UIImage imageNamed:@"ic_menu"];
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setImage:image forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(sxc_toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    
    menuButton.frame = CGRectMake(0, 0,30,30);
    
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    [menuBarButton setTintColor:[UIColor clearColor]];
    
    
    [self.navigationItem setLeftBarButtonItem:menuBarButton];
}

-(void)sxc_setNavigationBarCrossItem
{
    UIImage * image=[UIImage imageNamed:@"ic_cross"];
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [menuButton setImage:image forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(sxc_dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setTintColor:[UIColor whiteColor]];
    
    
    menuButton.frame = CGRectMake(0, 0,30,30);
    
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    
    [self.navigationItem setLeftBarButtonItem:menuBarButton];
}


-(void)sxc_toggleLeftMenu
{
    if([kMainViewController isLeftViewAlwaysVisible]){
        [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
    }
    else{
        [kMainViewController showLeftViewAnimated:YES completionHandler:nil];
    }
    
}

-(void)sxc_dismissViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)sxc_customizeRightNavigationBarMenuItem
{
    UIImage *backButtonImage = [UIImage imageNamed:(@"ic_add_header_bar")];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 0 ,35, 35);
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton addTarget:self
                   action:@selector(rightMenuButtonClicked:)
         forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = backBarButtonItem;
    
}

-(IBAction)rightMenuButtonClicked:(id)sender{
}

-(void)sxc_customizeBackButton
{
    if(self.navigationController.viewControllers.count>1){
        self.navigationItem.hidesBackButton = YES;
        [self.navigationItem setHidesBackButton:YES animated:YES];
        
        UIImage *backButtonImage = [UIImage imageNamed:(@"btn_back")];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [backButton setImage:backButtonImage forState:UIControlStateNormal];
        backButton.frame = CGRectMake(0, 0 ,23, 23);
        
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        [backButton addTarget:self
                       action:@selector(sxc_backButtonClicked:)
             forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = backBarButtonItem;
        
    }
}



-(void)sxc_backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Loader Methods
-(void)showLoaderWithText:(NSString *)loadertext {
    [self.view endEditing:YES];
    [[[PWPAppDelegate sharedInstance] window] setUserInteractionEnabled:NO];
    [SVProgressHUD showWithStatus:loadertext];
}

-(void)startLoader {
    
    [self.view endEditing:YES];
    [[[PWPAppDelegate sharedInstance] window] setUserInteractionEnabled:NO];
    [SVProgressHUD show];
    
}

-(void)dismissLoaderWithSuccessText:(NSString *)successText {
    [SVProgressHUD showSuccessWithStatus:successText];
    [[[PWPAppDelegate sharedInstance] window] setUserInteractionEnabled:YES];
}

-(void)dismissLoaderWithErrorText:(NSString *)successText {
    [SVProgressHUD showErrorWithStatus:successText];
    [[[PWPAppDelegate sharedInstance] window] setUserInteractionEnabled:YES];
}

-(void)dismissLoader {
    
    
    [SVProgressHUD dismiss];
    [[[PWPAppDelegate sharedInstance] window] setUserInteractionEnabled:YES];
    
}

-(void)sxc_stopLoading{}

-(void)sxc_startLoading{}
@end
