//
//  UIViewController+IGComponents.h
//  iGate
//
//  Created by imran on 9/28/14.
//  Copyright (c) 2014 STC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActionSheetStringPicker.h"
#import "ActionSheetDatePicker.h"

@interface UIViewController (UBRComponents)

#pragma mark - Native Alert View Methods
/**
 *  To show the alert dialog with title and description
 *
 *  @param alertTitle    Title of the alert (NSString *)
 *  @param messageString Description of the alert (NSString *)
 */
-(UIAlertView *)sxc_showAlertViewWithTitle:(NSString *)alertTitle message:(NSString *)messageString WithDelegate:(id<UIAlertViewDelegate>)delegate WithCancelButtonTitle:(NSString *)cancelString WithAcceptButtonTitle:(NSString *)acceptString;
-(void)sxc_showActionSheetWithActionTitle:(NSString *)actionSheetTitle optionTitle:(NSArray *)titleArray WithActionArray:(NSArray *)actionArray;

-(void)sxc_showErrorViewWithMessage:(NSString *)messageString;


//-(void)sxc_showActionSheetWithPickerWithRows:(NSArray *)pickerValues  doneBlock:(ActionStringDoneBlock)doneBlock cancelBlock:(ActionStringCancelBlock)cancelBlockOrNil origin:(id)origin;

//-(void)sxc_showDateOfBirthActionSheetWithPickerWithRows:(NSArray *)pickerValues  doneBlock:(ActionDateDoneBlock)doneBlock cancelBlock:(ActionDateCancelBlock)cancelBlockOrNil origin:(id)origin;

-(void)sxc_showActionSheetWithTitle:(NSString *)title pickerWithRows:(NSArray *)pickerValues itemSelectedAtIndex:(NSInteger)selectedIndex doneBlock:(ActionStringDoneBlock)doneBlock cancelBlock:(ActionStringCancelBlock)cancelBlockOrNil origin:(id)origin;

-(void)sxc_showDateOfBirthActionSheetWithTitle:(NSString *)title pickerWithRows:(NSArray *)pickerValues dateSelected:(NSDate *)selectedDate doneBlock:(ActionDateDoneBlock)doneBlock cancelBlock:(ActionDateCancelBlock)cancelBlockOrNil origin:(id)origin;

-(void)sxc_showDateTimeActionSheetWithTitle:(NSString *)title pickerWithRows:(NSArray *)pickerValues dateSelected:(NSDate *)selectedDate doneBlock:(ActionDateDoneBlock)doneBlock cancelBlock:(ActionDateCancelBlock)cancelBlockOrNil origin:(id)origin;

-(void)sxc_showTimeActionSheetWithTitle:(NSString *)title pickerWithRows:(NSArray *)pickerValues dateSelected:(NSDate *)selectedDate doneBlock:(ActionDateDoneBlock)doneBlock cancelBlock:(ActionDateCancelBlock)cancelBlockOrNil origin:(id)origin;

-(void)sxc_showDateTimeActionSheetWithoutMinOrMaxDateWithTitle:(NSString *)title pickerWithRows:(NSArray *)pickerValues dateSelected:(NSDate *)selectedDate doneBlock:(ActionDateDoneBlock)doneBlock cancelBlock:(ActionDateCancelBlock)cancelBlockOrNil origin:(id)origin ;

-(void)sxc_showSuccessWithMessage:(NSString *)messageString;


-(void)sxc_navigationBarCustomizationWithTitle:(NSString *)navigationTitle;


/**
 *  It will set the navigation bar' left item to menu icon.
 */
-(void)sxc_setNavigationBarMenuItem;

-(void)sxc_setNavigationBarCrossItem;
/**
 *  It will toggle the left menu view
 */
-(void)sxc_toggleLeftMenu;

-(void)sxc_customizeRightNavigationBarMenuItem;

-(void)sxc_customizeBackButton;
-(void)sxc_backButtonClicked:(id)sender;


// Loader methods

-(void)showLoaderWithText:(NSString *)loadertext;



-(void)dismissLoaderWithErrorText:(NSString *)successText;

-(void)dismissLoaderWithSuccessText:(NSString *)successText;

-(void)dismissLoader;
-(void)startLoader;

-(void)sxc_stopLoading;
-(void)sxc_startLoading;

-(IBAction)rightMenuButtonClicked:(id)sender;
@end
