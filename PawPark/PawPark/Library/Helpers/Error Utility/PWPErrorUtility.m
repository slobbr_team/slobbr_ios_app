//
//  SCErrorUtility.m
//  SixthContinent
//
//  Created by Umang on 29/01/15.
//  Copyright (c) 2015 Umang. All rights reserved.
//

#import "PWPErrorUtility.h"
#import "PWPAppDelegate.h"
#import "PWPStringConstant.h"


NSString * const PWPServerError=@"pwp_server_error";

@implementation PWPErrorUtility

+(NSError *)handlePredefinedErrorCode:(NSInteger) errorcode WithResponse:(id)errorResponse{
    
    
    NSMutableDictionary * notificationPayloadDictionary=[[NSMutableDictionary alloc] init];
    [notificationPayloadDictionary setValue:[NSNumber numberWithInteger:errorcode] forKey:@"eventType"];
    
    if(errorResponse){
        [notificationPayloadDictionary setValue:errorResponse forKey:@"eventResponse"];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:PWPServerError object:nil userInfo:notificationPayloadDictionary];

    
    NSString * errorString;
    switch (errorcode) {
            
        case SXCBadRequest:
            errorString=K_ERROR_SERVER_ERROR;
            break;
            
        case SXCUnAuthorized:
            errorString=K_ERROR_SERVER_UNAUTHORIZED;
            break;
            
        case SXCForbidden:
            errorString=K_ERROR_SERVER_FORBIDDEN;
            break;
            
        case SXCNotFound:
            errorString=K_ERROR_SERVER_NOT_FOUND;
            break;
            
        case SXCParameterMissing:
            errorString=K_ERROR_SERVER_PARAMETER_MISSING;
            break;
            
        case SXCUserAlreadyExists:
            errorString=K_ERROR_SERVER_USER_ALREADY;
            break;
            
        case SXCServerError:
            errorString=K_ERROR_SERVER_ERROR;
            
            break;
        case SXCNoInternetConnection:
        case SXCCannotFindHost:
        case SXCCannotConnectToHost:
        case SXCNetworkConnectionLost:
            errorString=K_ERROR_SERVER_INTERNET_CONNECTION;
            break;
            
        case SXCNotModified:
            errorString=K_ERROR_SERVER_ERROR;
            break;
            
        default:
            errorString=K_ERROR_SERVER_ERROR;
            break;
    }
    
    NSError * error=[[NSError alloc]initWithDomain:errorString code:errorcode userInfo:nil];
    return error;
}

@end
