//
//  SCErrorUtility.h
//  SixthContinent
//
//  Created by Umang on 29/01/15.
//  Copyright (c) 2015 Umang. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const PWPServerError;

@interface PWPErrorUtility : NSObject

typedef NS_ENUM(NSInteger, SCErrorCodes)
{
    SXCBadRequest = 400,
    SXCUnAuthorized=401,
    SXCForbidden=403,
    SXCNotFound=404,
    SXCParameterMissing=406,
    SXCUserAlreadyExists=409,
    SXCServerError=500,
    SXCNotModified=304,
    SXCNoInternetConnection=-1009,
    SXCCannotFindHost=-1003,
    SXCCannotConnectToHost=-1004,
    SXCNetworkConnectionLost=-1005,
    SXCRequestStopped=-999

};

+(NSError *)handlePredefinedErrorCode:(NSInteger) errorcode WithResponse:(id)errorResponse;

@end
