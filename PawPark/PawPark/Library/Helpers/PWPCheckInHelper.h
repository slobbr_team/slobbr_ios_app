//
//  PWPCheckInHelper.h
//  PawPark
//
//  Created by Samarth Singla on 19/11/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PWPCheckInHelper : NSObject

+(BOOL)checkInAllowedForParkId:(NSString*)parkId forDogs:(NSArray *)dogs;
+(void)dogCheckedInParkId:(NSString *)parkId forDogs:(NSArray*)dogs;

@end
