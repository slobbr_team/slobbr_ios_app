//
//  PWPCheckInHelper.m
//  PawPark
//
//  Created by Samarth Singla on 19/11/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPCheckInHelper.h"
#import "NSDate+SXCDate.h"
#import "PWPApplicationUtility.h"
#import "SXCUtility.h"
#import <UIKit/UIKit.h>

@implementation PWPCheckInHelper

+(BOOL)checkInAllowedForParkId:(NSString*)parkId forDogs:(NSArray *)dogs
{
    NSDictionary * parkCheckInDictionary = (NSDictionary *)[SXCUtility getNSObject:@"park_checkin_data"];
    
    if(parkCheckInDictionary == nil){
        return YES;
    }
    

    NSString * userID = [[PWPApplicationUtility getCurrentUserProfileDictionary][@"id"] description];
    if([parkCheckInDictionary valueForKey:[NSString stringWithFormat:@"%@_%@",userID,parkId]] == nil){
        return YES;
    }
    
    
    NSMutableDictionary * dictionary =(NSMutableDictionary *) [parkCheckInDictionary valueForKey:[NSString stringWithFormat:@"%@_%@",userID,parkId]];
    
    for(NSString * dogId in dogs){
        if([dictionary valueForKey:[dogId description]]){
            NSDate * date = [dictionary valueForKey:[dogId description]];
            if([date minutesBeforeDate:[NSDate date] ] < 60){

                UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"You're already checked-in here!  Please wait until we check you out or check-in to a new location." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertView show];
            
                return NO;
            }
        }
    }

    return YES;
}

+(void)dogCheckedInParkId:(NSString *)parkId forDogs:(NSArray*)dogs{

    NSString * userID = [[PWPApplicationUtility getCurrentUserProfileDictionary][@"id"] description];

    
    NSDictionary * parkCheckInDictionary = (NSDictionary *)[SXCUtility getNSObject:@"park_checkin_data"];

    if(parkCheckInDictionary == nil){
        parkCheckInDictionary = [[NSMutableDictionary alloc] init];
    }
    
    NSMutableDictionary * dictionary =(NSMutableDictionary *) [parkCheckInDictionary valueForKey:[NSString stringWithFormat:@"%@_%@",userID,parkId]];

    if(dictionary == nil){
        dictionary = [[NSMutableDictionary alloc] init];
    }
    
    for(NSString * dogId in dogs){
        [dictionary setObject:[NSDate date] forKey:[dogId description]];
    }
    
    [parkCheckInDictionary setValue:dictionary forKey:[NSString stringWithFormat:@"%@_%@",userID,parkId]];

    [SXCUtility saveNSObject:parkCheckInDictionary forKey:@"park_checkin_data"];
}



@end
