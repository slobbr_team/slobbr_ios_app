//
//  MPWUtilityLibrairy.h
//  MasterPaintingWeek
//
//  Created by Laurent TARRAL on 10/03/2012.
//  Copyright (c) 2012 PEDesign. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SXCUtility : NSObject

//for clean a particular object
+ (int)cleanInt:(id)aValue;
+ (float)cleanFloat:(id)aValue;
+ (NSString *)cleanString:(id)aValue;
+ (NSMutableArray *)cleanArray:(id)aValue;
+ (NSMutableDictionary *)cleanDictionary:(id)aValue;


+(NSString *)trimString:(NSString *)string;

//to check the null value
+ (BOOL)isNotNullString:(id)aValue;
+ (BOOL)isEmptyString:(NSString *)aValue;
+ (BOOL)isNotNull:(id)aValue;
+ (BOOL)isString:(NSString *)string containsSubString:(NSString *)subString;

//String property
+ (NSString *)decodeHtmlTagFromString:(NSString *)aValue;
+ (NSString *)stringByStrippingHTMLfromString:(NSString *)str;


//For NSUserDefaults
+(BOOL) saveNSObject:(NSObject*)nsObject forKey:(NSString*)key;
+(NSObject*) getNSObject:(NSString*)key;
+(void) removeObject:(NSString*) key;
+(void)removeAllObjects;



// For BL's
+(BOOL) validPhone:(NSString*) phoneString;
+(BOOL) isValidEmail:(NSString *)checkString;
+(BOOL)isNumeric:(NSString*)inputString;
+(NSString *)getOSVersion;
+(NSString *)getVendorID;
+(NSString *)getCurrentTimeStamp;

#pragma mark  - Language related method

+(NSString *)getCurrentLanguage;

#pragma mark - Currency Related Methods
+(NSString *)getCurrencyStringFromNumber:(NSNumber *)number;

@end
