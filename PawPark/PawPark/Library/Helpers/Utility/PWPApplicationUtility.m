//
//  PWPApplicationUtility.m
//  PawPark
//
//  Created by daffomac on 4/28/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPApplicationUtility.h"
#import "PWPAppDelegate.h"
#import "SXCUtility.h"
#import <CSNotificationView.h>
#import <UIKit/UIKit.h>

#define SIZES @{@"1":@"Teacup",@"2":@"Small",@"3":@"Medium",@"4":@"Large",@"5":@"Very Large"}

@implementation PWPApplicationUtility


+(NSString *)pawSizesCorrespondingToID:(NSString *)IDString
{
    if([SIZES valueForKey:IDString])
    {
        return [SIZES valueForKey:IDString];
    }
    return @"";
}

+(NSArray *)pawSizes{
    return @[@"Teacup",@"Small",@"Medium",@"Large",@"Very Large"];
}

+(NSArray *)pawSizesForSearch{
    return @[@"All",@"Teacup",@"Small",@"Medium",@"Large",@"Very Large"];
}
+(NSArray *)pawSizesIdForSearch{
    return @[@"0",@"1",@"2",@"3",@"4",@"5"];
}

+(NSString *)pawSizeCorrespondingToSize:(NSString *)sizeString{
    
    if([sizeString isEqualToString:@"All"]||[sizeString isEqualToString:@""]){
        return nil;
    }
    
    NSArray * arrayOfSizedId=[self pawSizesIdForSearch];
  
    return arrayOfSizedId[[[self pawSizesForSearch] indexOfObject:sizeString]];
}

+(void)addToAlertParks:(NSDictionary *)park
{
    NSMutableArray * parks = [SXCUtility cleanArray:[SXCUtility getNSObject:@"Alert_Parks"]].mutableCopy;
    
    NSArray * parksIds = [parks valueForKeyPath:@"id"];
    
    if(![parksIds containsObject:park[@"id"]]){
        [parks addObject:park];
    }
    
    [SXCUtility saveNSObject:parks forKey:@"Alert_Parks"];
    
    
    PWPAppDelegate * delegate = [UIApplication sharedApplication].delegate;
    
    
    [CSNotificationView showInViewController:delegate.window.rootViewController style:CSNotificationViewStyleSuccess
                                     message:@"You will be alerted when you’re 0.25 miles from this place."];
}

+(void)removeAlertPark:(NSDictionary *)park
{
    NSMutableArray * parks = [SXCUtility cleanArray:[SXCUtility getNSObject:@"Alert_Parks"]].mutableCopy;
    
    NSArray * parksIds = [parks valueForKeyPath:@"id"];

    if([parksIds containsObject:park[@"id"]]){
        NSInteger parkInteger = [parksIds indexOfObject:park[@"id"]];
        [parks removeObjectAtIndex:parkInteger];
    }
    
    [SXCUtility saveNSObject:parks forKey:@"Alert_Parks"];
}

+(BOOL)isAlertPark:(NSDictionary *)park
{
    NSMutableArray * parks = [SXCUtility cleanArray:[SXCUtility getNSObject:@"Alert_Parks"]].mutableCopy;
    
    NSArray * parksIds = [parks valueForKeyPath:@"id"];
    
    if([parksIds containsObject:park[@"id"]]){
        return true;
    }
    return false;
}

+(NSArray*)favParks {

    return [SXCUtility cleanArray:[SXCUtility getNSObject:@"Alert_Parks"]];
}


+(NSString *)pawSizesCorrespondingToSizes:(NSString *)sizesString{
    
    NSArray * arratOfSizeArray=@[@"1",@"2",@"3",@"4",@"5"];
    return arratOfSizeArray[[[self pawSizes] indexOfObject:sizesString]];
}


+(NSArray *)pawSociability{
    return @[@"All",@"Shy",@"Normal",@"Friendly",@"Super-Friendly",@"In-Your-Face"];
}

+(NSArray *)pawSociabilityForNewDog{
    return @[@"Shy",@"Normal",@"Friendly",@"Super-Friendly",@"In-Your-Face"];
}

+(NSString *)pawSociabilityCorrespondingToSociability:(NSString *)sociabilityString{
    
    if([sociabilityString isEqualToString:@"All"]||[sociabilityString isEqualToString:@""]){
        return nil;
    }
    
    NSArray * arrayOfSociabilityArray=@[@"0",@"1",@"2",@"3",@"4",@"5"];
    return arrayOfSociabilityArray[[[self pawSociability] indexOfObject:sociabilityString]];
}

+(NSArray *)pawActivityLevel{
    return @[@"All",@"Couch Potato",@"A Little Active",@"Moderately Active",@"Very Active",@"A.D.D"];
}


+(NSArray *)pawActivityLevelForNewDog{
    return @[@"Couch Potato",@"A Little Active",@"Moderately Active",@"Very Active",@"A.D.D"];
}


+(NSString *)pawAcitivityLevelCorrespondingToActivityLevel:(NSString *)activityLevel{
    
    if([activityLevel isEqualToString:@"All"]||[activityLevel isEqualToString:@""]){
        return nil;
    }
    
    NSArray * arrayOfActivityLevelArray=@[@"0",@"1",@"2",@"3",@"4",@"5"];
    return arrayOfActivityLevelArray[[[self pawActivityLevel] indexOfObject:activityLevel]];
}

+(NSArray *)pawGender{
    return @[@"Female",@"Male"];
}

+(void)saveCurrentUserProfileDictionary:(NSMutableDictionary *)profileDictionary
{
    [SXCUtility saveNSObject:profileDictionary forKey:@"pwp_user_profile"];
}

+(NSMutableDictionary *)getCurrentUserProfileDictionary
{
    return (id)[SXCUtility getNSObject:@"pwp_user_profile"];
}

+(NSArray *)placeCategory{
    return (id)[SXCUtility getNSObject:@"parkCategory"];
}

+(NSArray *)getCurrentUserDogsArray
{
    NSDictionary * loginDictionary=[PWPApplicationUtility getCurrentUserProfileDictionary];
    
    if([loginDictionary valueForKey:@"dogs"]){
        return loginDictionary[@"dogs"];
    }
    else{
        return @[];
    }
}

+(void)saveCurrentUserDogsArray:(NSArray *)dogsArray
{
    [SXCUtility saveNSObject:dogsArray forKey:@"pwp_user_dogs"];
}

+(NSArray *)currentUserDogsIds
{
    NSArray * dogsArray=[self getCurrentUserDogsArray];
    
    return [dogsArray valueForKeyPath:@"id"];
}

+(BOOL)isShelter
{
    NSDictionary * loginDictionary=[PWPApplicationUtility getCurrentUserProfileDictionary];
    
    if([loginDictionary valueForKey:@"shelter"]){
        return [[loginDictionary[@"shelter"] description] boolValue];
    }
    return false;
}
+(BOOL)isEventMode
{
    return [[SXCUtility cleanString:[SXCUtility getNSObject:@"eventMode"]] boolValue];
    
}


+(void)saveCurrentEventListDictionary:(NSArray *)eventArray
{
    [SXCUtility saveNSObject:eventArray forKey:@"pwp_event_list"];
}

+(NSArray *)getCurrentUserEventList
{
    return (id)[SXCUtility getNSObject:@"pwp_event_list"];
    
}
@end

