//
//  MPWUtilityLibrairy.m
//  MasterPaintingWeek
//
//  Created by Laurent TARRAL on 10/03/2012.
//  Copyright (c) 2012 PEDesign. All rights reserved.
//

#import "SXCUtility.h"
#import <UIKit/UIKit.h>
#import "PWPAppDelegate.h"

@implementation SXCUtility


+ (int)cleanInt:(id)aValue {
    if([self isNotNull:aValue])
        return [aValue intValue];
    return 0;
}

+ (float)cleanFloat:(id)aValue {
    if([self isNotNull:aValue])
        return [aValue floatValue];
    return .0f;
}

+ (NSString *)cleanString:(id)aValue
{
    if([self isNotNullString:aValue])
        return [self decodeHtmlTagFromString:aValue];
    return @"";
}
+ (NSArray *)cleanArray:(id)aValue
{
    if([self isNotNull:aValue])
        return aValue;
    
    return [[NSArray alloc]init];
}

+ (NSMutableDictionary *)cleanDictionary:(id)aValue
{
    if([self isNotNull:aValue]&&[aValue isKindOfClass:[NSDictionary class]])
        return aValue;
    
    return [[NSMutableDictionary alloc]init];
}


+(NSString *)trimString:(NSString *)string
{
    NSString * returnedString=[self cleanString:string];
    return [returnedString stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
}



+ (NSString *)decodeHtmlTagFromString:(NSString *)aValue {
    NSString *rtn = aValue;;
    rtn = [rtn stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    rtn = [rtn stringByReplacingOccurrencesOfString:@"&#039;" withString:@"'"];
    rtn = [rtn stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    rtn = [rtn stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    rtn = [SXCUtility stringByStrippingHTMLfromString:rtn];
    return rtn;
}

+ (NSString *)stringByStrippingHTMLfromString:(NSString *)str
{
    
    NSString * string=str;
    NSRange res;
    while ((res = [string rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        string = [string stringByReplacingCharactersInRange:res withString:@""];
    return string;
}

+ (BOOL)isNotNullString:(id)aValue {
    if([self isNotNull:aValue] && [aValue isKindOfClass:[NSString class]] && ![aValue isEqualToString:@"null"]&& ![aValue isEqualToString:@"(null)"] && ![aValue isEqualToString:@"<null>"] &&![aValue isEqualToString:@""])
        return YES;
    return NO;
}

+ (BOOL)isEmptyString:(NSString *)aValue {
    aValue=[aValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(aValue == nil || [aValue length] < 1 || [aValue isEqualToString:@""])
        return YES;
    return NO;
}

+ (BOOL)isNotNull:(id)aValue {
    if(aValue != nil && aValue != [NSNull null])
        return YES;
    return NO;
}




+(BOOL)isString:(NSString *)string containsSubString:(NSString *)subString
{
    if ([string rangeOfString:subString].location == NSNotFound) {
        return NO;
    } else {
        return YES;
    }
}

#pragma mark - languuage Related method

+(NSString *)getCurrentLanguage
{
    NSString *currentLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([currentLanguage isEqualToString:@"it"]){
        return @"it";
    }
    else{
        return @"en";
    }
    return currentLanguage;
}

#pragma mark - User Defaults Methods
+(BOOL) saveNSObject:(NSObject*)nsObject forKey:(NSString*)key
{
    if(nsObject!=nil)
    {
        @try {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:nsObject];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:key];
            [[NSUserDefaults standardUserDefaults] synchronize];
            return YES;
            
        }
        @catch (NSException *exception) {
            
        }
    }
    else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    }
    return NO;
}
+(NSObject*) getNSObject:(NSString*)key
{
    NSObject *nsObject=NULL;
    if(key)
    {
        
        @try {
            NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
            NSData *nsData = [currentDefaults objectForKey:key];
            if (nsData != nil)
            {
                nsObject = [NSKeyedUnarchiver unarchiveObjectWithData:nsData];
                
                
                return nsObject;
            }
            
        }
        @catch (NSException *exception) {
            
        }
        
    }
    
    return nsObject;
    
}
+(void) removeObject:(NSString*) key
{
    @try {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    @catch (NSException *exception) {
        
    }
}
+(void)removeAllObjects
{
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
}

#pragma mark - Phone Number Validation

+(BOOL)isNumeric:(NSString*)inputString
{
    NSString *trimString = [SXCUtility trimString:inputString];
    NSCharacterSet *charcter =[[NSCharacterSet characterSetWithCharactersInString:@"0123456789+()- "] invertedSet];
    NSString *filtered;
    filtered = [[trimString componentsSeparatedByCharactersInSet:charcter] componentsJoinedByString:@""];
    return [trimString isEqualToString:filtered];
}


+(BOOL)validPhone:(NSString*) phoneString{
    
    NSString *phoneRegex = @"[\(00)+][0-9]{6,14}";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    //clean string for phone number
    phoneString=[phoneString stringByReplacingOccurrencesOfString:@"\\U00a0" withString:@""];
    phoneString=[phoneString stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneString=[phoneString stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneString=[phoneString stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phoneString=[phoneString stringByReplacingOccurrencesOfString:@")" withString:@""];
    phoneString=[phoneString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    BOOL phoneValidates = [phoneTest evaluateWithObject:phoneString];
    if (phoneValidates) {
        NSCharacterSet *characterSet =[NSCharacterSet characterSetWithCharactersInString:@"0123456789+"];
        if ([[phoneString stringByTrimmingCharactersInSet:characterSet]isEqualToString:@""]) {
            //valid phone length is between 10 to 16 with plus included
            if (phoneString.length>10 && phoneString.length<16) {
                return YES;
            }
        }
        return NO;
    }
    else
    {
        NSCharacterSet *characterSet =[NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        if ([[phoneString stringByTrimmingCharactersInSet:characterSet]isEqualToString:@""]) {
            if (phoneString.length>9 && phoneString.length<15) {
                return YES;
            }
        }
        return NO;
//        NSString *phoneRegex2 = @"^[789]\\d{9}$"; `
//        NSString *phoneRegex2 = @"^(\\+\\d{1,3}[- ]?)?\\d{10}$";
//        phoneString=[phoneString stringByReplacingOccurrencesOfString:@"+" withString:@""];
//        NSPredicate *phoneTest2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex2];
//        return [phoneTest2 evaluateWithObject:phoneString];
        
    }
    return phoneValidates;
}


#pragma mark - Valid Email
+(BOOL) isValidEmail:(NSString *)checkString
{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:checkString];
}



#pragma mark - HTTPHeader Methods version Methods

+(NSString *)getOSVersion{
    
    NSString * osVersion=[[NSString alloc]initWithFormat:@"%@ %@",[[UIDevice currentDevice] systemName],[[UIDevice currentDevice] systemVersion]];
    return osVersion;
}

+(NSString *)getVendorID{
    NSString * osVersion=[[NSString alloc]initWithFormat:@"%@",[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
    return osVersion;
}

+(NSString *)getCurrentTimeStamp
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    
    NSString  *currentTimeStamp = [dateFormatter stringFromDate:[NSDate date]];
    return currentTimeStamp;
}

#pragma mark - Currency Related Methods

+(NSString *)getCurrencyStringFromNumber:(NSNumber *)number
{
    if(number==nil){
        return [NSString stringWithFormat:@"0.00 %@",@"\u20AC"];
    }
    
    
    NSLocale *usLocale = [NSLocale currentLocale];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMinimumFractionDigits:2];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setLocale:usLocale];
    
    NSString *formattedNumberString = [numberFormatter stringFromNumber:number];
    return [NSString stringWithFormat:@"%@ %@",formattedNumberString,@"\u20AC"];
 
    /*
    NSLocale *french = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
    
    NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
    [currencyStyle setLocale:french];
    [currencyStyle setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    NSNumber *amount = [NSNumber numberWithDouble:number.doubleValue];
    NSString *amountString =  [currencyStyle stringFromNumber:amount];
    return amountString;
     */
}


@end
