//
//  PWPApplicationUtility.h
//  PawPark
//
//  Created by daffomac on 4/28/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PWPApplicationUtility : NSObject

+(NSString *)pawSizesCorrespondingToID:(NSString *)IDString;

+(NSArray *)pawSizes;
+(NSArray *)pawSociability;
+(NSArray *)pawActivityLevel;
+(NSArray *)pawSociabilityForNewDog;
+(NSArray *)pawActivityLevelForNewDog;

+(NSArray *)pawGender;

+(NSString *)pawSociabilityCorrespondingToSociability:(NSString *)sociabilityString;
+(NSString *)pawAcitivityLevelCorrespondingToActivityLevel:(NSString *)activityLevel;
+(NSString *)pawSizesCorrespondingToSizes:(NSString *)sizesString;

+(NSArray *)getCurrentUserDogsArray;
+(void)saveCurrentUserProfileDictionary:(NSMutableDictionary *)profileDictionary;
+(void)saveCurrentUserDogsArray:(NSArray *)dogsArray;
+(NSArray *)currentUserDogsIds;
+(NSMutableDictionary *)getCurrentUserProfileDictionary;

+(NSArray *)pawSizesForSearch;
+(NSArray *)pawSizesIdForSearch;
+(NSString *)pawSizeCorrespondingToSize:(NSString *)sizeString;
+(BOOL)isShelter;
+(BOOL)isEventMode;
+(NSArray *)placeCategory;

+(NSArray*)favParks;
+(void)addToAlertParks:(NSDictionary *)park;
+(void)removeAlertPark:(NSDictionary *)park;
+(BOOL)isAlertPark:(NSDictionary *)park;


+(void)saveCurrentEventListDictionary:(NSArray *)eventArray;
+(NSArray *)getCurrentUserEventList;
@end
