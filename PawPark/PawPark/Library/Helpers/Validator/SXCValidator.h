//
//  SCValidator.h
//  SixthContinent
//
//  Created by Umang on 29/01/15.
//  Copyright (c) 2015 Umang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SXCValidator : NSObject

+(int) code;

+(NSString *) messageForCode: (int)code;

+(int) validateImageWithData: (NSData *)data;
+(int) validateUsername: (NSString *)username;
+(int) validatePassword: (NSString *)password;
+(int) validateConfirmPassword: (NSString *)password;
+(int) validateEmail: (NSString *)email;
+(int) validateMobile: (NSString *)mobile;
+(int) validateGender: (NSString *)gender;
+(int) validateAge: (NSString *)age;
+(int) validateCountry: (NSString *)country;
+(int) validateName: (NSString *)name;
+(int) validateFirstName: (NSString *)name;
+(int) validateLastName: (NSString *)name;

+ (int)validateTermsAndCondition:(BOOL)status;
+ (int)validateProfileImage:(NSString*)key;
+ (int)validateYear:(NSString*)year;
+ (int)validateMake:(NSString*)make;
+ (int)validateModel:(NSString*)Model;
+ (int)matchPassword: (NSString *)password1 withConfirmed: (NSString *)password2;


@end
