//
//  SCValidator.m
//  SixthContinent
//
//  Created by Umang on 29/01/15.
//  Copyright (c) 2015 Umang. All rights reserved.
//

#import "SXCValidator.h"
#import "PWPStringConstant.h"

    //validator strings
#define K_STRING_RESULT_SUCCESS     NSLocalizedString(@"Success.",nil)
#define K_STRING_RESULT_DISAGREE_TERMS     NSLocalizedString(@"Agree terms and condition. Please check.",nil)
#define K_STRING_RESULT_BLANK_USER_NAME     NSLocalizedString(@"Username is blank. Please check.",nil)
#define K_STRING_RESULT_INVALID_USER_NAME_WITH_SPACES       NSLocalizedString(@"Username can not contain spaces. Please check.",nil)
#define K_STRING_RESULT_INVALID_USER_NAME_CHARACTERS        NSLocalizedString(@"Invalid characters in username. Please check.",nil)
#define K_STRING_RESULT_BLANK_PASSWORD      NSLocalizedString(@"Password is blank. Please check.",nil)
#define K_STRING_RESULT_MIN_PASSWORD_ERROR      NSLocalizedString(@"Password must be min. 6 characters. Please check.",nil)
#define K_STRING_RESULT_BLANK_CONFIRM_PASSWORD      NSLocalizedString(@"Confirm password is blank. Please check.",nil)
#define K_STRING_RESULT_MIN_CONFIRM_PASSWORD_ERROR      NSLocalizedString(@"Confirm password must be min. 6 characters. Please check.",nil)
#define K_STRING_RESULT_BLANK_EMAIL     NSLocalizedString(@"Email is blank. Please check.",nil)
#define K_STRING_RESULT_INVALID_EMAIL_WITH_SPACES       NSLocalizedString(@"Email can not contain spaces. Please check.",nil)
#define K_STRING_RESULT_INVALID_EMAIL       NSLocalizedString(@"Invalid email address. Please check.",nil)
#define K_STRING_RESULT_BLANK_MOBILE_NO     NSLocalizedString(@"Mobile no. is blank. Please check.",nil)
#define K_STRING_RESULT_MIN_MOBILE_NO_ERROR      NSLocalizedString(@"Mobile no. must be at least 10 digits. Please check.",nil)
#define K_STRING_RESULT_INVALID_MOBILE_NO_WITH_SPACES       NSLocalizedString(@"Mobile no. can not contain spaces. Please check.",nil)
#define K_STRING_RESULT_INVALID_MOBILE_NO       NSLocalizedString(@"Invalid mobile no. Please check.",nil)
#define K_STRING_RESULT_BLANK_GENDER      NSLocalizedString(@"Gender is blank. Please check.",nil)
#define K_STRING_RESULT_BLANK_AGE      NSLocalizedString(@"Age is blank. Please check.",nil)
#define K_STRING_RESULT_BLANK_COUNTRY      NSLocalizedString(@"Country is blank. Please check.",nil)
#define K_STRING_RESULT_BLANK_FIRST_NAME      NSLocalizedString(@"First Name is blank. Please check.",nil)
#define K_STRING_RESULT_NAME_WITH_INAVLID_CHARACTERS      NSLocalizedString(@"Invalid characters in name. Please check.",nil)
#define K_STRING_RESULT_SPECIFY_YOUR_GENDER      NSLocalizedString(@"Specify your gender.",nil)
#define K_STRING_RESULT_BLANK_LAST_NAME      NSLocalizedString(@"Last Name is blank. Please check.",nil)
#define K_STRING_RESULT_PIN_OR_EMAIL_REQUIRED      NSLocalizedString(@"BBM Pin or email, one is required.",nil)
#define K_STRING_RESULT_FILL_PIN_OR_EMAIL      NSLocalizedString(@"You need to fill either email or phone. Please check.",nil)
#define K_STRING_RESULT_PASSWORD_MISMATCH      NSLocalizedString(@"Password mismatch. Please check.",nil)
#define K_STRING_RESULT_SELECT_IMAGE      NSLocalizedString(@"Please select image",nil)
#define K_STRING_RESULT_UPLOAD_IMAGE      NSLocalizedString(@"Please upload profile image.",nil)
#define K_STRING_RESULT_SOMETHING_WRONG      NSLocalizedString(@"Something seems wrong. Please check.",nil)

@implementation SXCValidator

static const int CODE = 10000;

+(int) code{
    return CODE;
}

+(NSString *) messageForCode: (int)code{
    
    NSString *result;
    
    switch (code) {
        case 0:
            result = K_STRING_RESULT_SUCCESS;
            break;
        case 80:
            result = K_STRING_RESULT_DISAGREE_TERMS;
            break;
        case 100:
            result = K_STRING_RESULT_BLANK_USER_NAME;
            break;
        case 101:
            result = K_STRING_RESULT_INVALID_USER_NAME_WITH_SPACES;
            break;
        case 102:
            result = K_STRING_RESULT_INVALID_USER_NAME_CHARACTERS;
            break;
        case 200:
            result = K_STRING_RESULT_BLANK_PASSWORD;
            break;
        case 201:
            result = K_STRING_RESULT_MIN_PASSWORD_ERROR;
            break;
        case 203:
            result = K_STRING_RESULT_BLANK_CONFIRM_PASSWORD;
            break;
        case 204:
            result = K_STRING_RESULT_MIN_CONFIRM_PASSWORD_ERROR;
            break;
        case 400:
            result = K_STRING_RESULT_BLANK_EMAIL;
            break;
        case 401:
            result = K_STRING_RESULT_INVALID_EMAIL_WITH_SPACES;
            break;
        case 402:
            result = K_STRING_RESULT_INVALID_EMAIL;
            break;
        case 500:
            result = K_STRING_RESULT_BLANK_MOBILE_NO;
            break;
        case 501:
            result = K_STRING_RESULT_MIN_MOBILE_NO_ERROR;
            break;
        case 502:
            result = K_STRING_RESULT_INVALID_MOBILE_NO_WITH_SPACES;
            break;
        case 503:
            result = K_STRING_RESULT_INVALID_MOBILE_NO;
            break;
        case 600:
            result = K_STRING_RESULT_BLANK_GENDER;
            break;
        case 700:
            result = K_STRING_RESULT_BLANK_AGE;
            break;
        case 800:
            result = K_STRING_RESULT_BLANK_COUNTRY;
            break;
        case 900:
            result = K_STRING_RESULT_BLANK_FIRST_NAME;
            break;
        case 901:
            result = K_STRING_RESULT_NAME_WITH_INAVLID_CHARACTERS;
            break;
        case 903:result = K_STRING_RESULT_SPECIFY_YOUR_GENDER;
            break;
        case 904:
            result = K_STRING_RESULT_BLANK_LAST_NAME;
            break;
        case 920:result = K_STRING_RESULT_PIN_OR_EMAIL_REQUIRED;
            break;
        case CODE:
            result = K_STRING_RESULT_FILL_PIN_OR_EMAIL;
            break;
        case 1000:
            result = K_STRING_RESULT_PASSWORD_MISMATCH;
            break;
        case 1020:
            result = K_STRING_RESULT_SELECT_IMAGE;
            break;
        case 1200:
            result = K_STRING_RESULT_UPLOAD_IMAGE;
            break;
        default:
            result = K_STRING_RESULT_SOMETHING_WRONG;
            break;
    }
    
    return result;
}

+(int) validateUsername: (NSString *)username{
    
    if (!username || username.length==0) {
        return 100;
    }
    if ([username rangeOfString:@" "].location != NSNotFound) {
        return 101;
    }
    
    if ([username rangeOfString:@"@"].location != NSNotFound) {
        return 102;
    }

    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789_.-"] invertedSet];
    if ([username rangeOfCharacterFromSet:set].location != NSNotFound) {
        return 102;
    }
    return 0;
}

+(int) validatePassword: (NSString *)password{
    
    if (!password || password.length==0) {
        return 200;
    }
    
    if (password.length<6) {
        return 201;
    }
    
    return 0;
}


+(int) validateConfirmPassword: (NSString *)password{
    if (!password || password.length==0) {
        return 203;
    }
    
    if (password.length<6) {
        return 204;
    }
    
    return 0;
}

+(int) validateBBM: (NSString *)bbmpin{
    if (!bbmpin || bbmpin.length==0) {
        return 300;
    }
    
    if (bbmpin.length != 8) {
        return 302;
    }
    
    if ([bbmpin rangeOfString:@" "].location != NSNotFound) {
        return 301;
    }
    
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefABCDEF0123456789"] invertedSet];
    if ([bbmpin rangeOfCharacterFromSet:set].location != NSNotFound) {
        return 302;
    }
    return 0;
}

+ (BOOL) isAlphaNumeric:(NSString*)bbmPin
{
    NSCharacterSet *letterCharacterSet = [NSCharacterSet letterCharacterSet];
    NSCharacterSet *numberSet = [NSCharacterSet decimalDigitCharacterSet];
    NSString *test1 = [bbmPin stringByTrimmingCharactersInSet:letterCharacterSet];
    NSString *test2 = [test1 stringByTrimmingCharactersInSet:numberSet];
    
    return test1.length > 0 && [test2 isEqualToString:@""];
}

+(int) validateMobile: (NSString *)mobile{
    
    if (!mobile || mobile.length==0) {
        return 500;
    }
    
    if (mobile.length<10 || mobile.length>13) {
        return 501;
    }
    
    if ([mobile rangeOfString:@" "].location != NSNotFound) {
        return 502;
    }

    
    return 0;
}

+(int) validateName: (NSString *)name{
    
    if (!name || name.length==0) {
        return 900;
    }

    
    return 0;
}
+(int) validateImageWithData: (NSData *)data{
    if (!data || data.length==0) {
        return 1020;
    }
    
    return 0;
}

+(int) validateFirstName: (NSString *)name{
    if (!name || name.length==0) {
        return 900;
    }
    
    return 0;
}
+(int) validateLastName: (NSString *)name{
    
    if (!name || name.length==0) {
        return 904;
    }
    
    return 0;
}


+ (int)validateTermsAndCondition:(BOOL)status{
    if (!status) {
        return 80;
    }
    
    return 0;
}


+(int) validateEmail: (NSString *)email{
    
    if (!email || email.length==0) {
        return 400;
    }
    
    if ([email rangeOfString:@" "].location != NSNotFound) {
        return 401;
    }
    
    if ([email rangeOfString:@"@"].location == NSNotFound) {
        return 402;
    }
    
    NSArray *array = [email componentsSeparatedByString:@"@"];
    if ([array[1] rangeOfString:@"."].location == NSNotFound) {
        return 402;
    }
    NSArray *ar = [array[1] componentsSeparatedByString:@"."];
    if ([ar[0] length]==0 || [ar[1] length]==0) {
        return 402;
    }
    
    int code = [self validateUsername: array[0]];
    if (code) {
        return 402;
    }
    
    return 0;
}

+(int) validateGender: (NSString *)gender{
    
    if (!gender || gender.length==0) {
        return 600;
    }
    return 0;
}

+(int) validateAge: (NSString *)age{
    
    if (!age || age.length==0) {
        return 700;
    }
    
    return 0;
}

+(int) validateCountry: (NSString *)country{
    
    if (!country || country.length==0) {
        return 800;
    }
    
    return 0;
}

+(int)matchPassword: (NSString *)password1 withConfirmed: (NSString *)password2{
    
    if (!password1 || !password2 || ![password1 isEqualToString:password2]) {
        return 1000;
    }
    
    return 0;
}


+ (int)validateProfileImage:(NSString*)key{
    if (!key || key.length==0) {
        return 1200;
    }
    return 0;
}

+ (int)validateYear:(NSString*)year{
    
    if ([year integerValue] && [year integerValue]>0) {
        return 0;
    }
    
    return 1100;
}

+ (int)validateMake:(NSString*)make{
    
    if ([make length]>0 && ![make isEqualToString:@"Make"]) {
        return 0;
    }
    
    return 1101;
}

+ (int)validateModel:(NSString*)model{
    
    if ([model length]>0 && ![model isEqualToString:@"Model"]) {
        return 0;
    }
    
    return 1102;
}


@end
