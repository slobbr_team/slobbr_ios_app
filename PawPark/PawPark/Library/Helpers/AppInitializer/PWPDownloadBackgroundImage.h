//
//  PWPDownloadBackgroundImage.h
//  PawPark
//
//  Created by Samarth Singla on 14/11/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PWPDownloadBackgroundImage : NSObject
+(UIImage *)backgroundImage;
+(UIImage *)splashImage;
+(UIImage *)itemBG;
+(UIImage *)filterBg;
+(UIImage *)menuBackgroundImage;
+(NSString *)eventBackgroundImageURL;
+(BOOL)isSkinApplied;
@end
