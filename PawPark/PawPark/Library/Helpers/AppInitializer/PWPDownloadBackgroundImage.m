//
//  PWPDownloadBackgroundImage.m
//  PawPark
//
//  Created by Samarth Singla on 14/11/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPDownloadBackgroundImage.h"
#import <AFNetworking.h>
#import "SXCUtility.h"

@implementation PWPDownloadBackgroundImage
+(UIImage *)backgroundImage{
   
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"ThemeBck"];

    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]){
     
        NSString * bgBackgroundImage  = [path stringByAppendingPathComponent:@"Halo_Theme"];
        bgBackgroundImage  = [bgBackgroundImage stringByAppendingPathComponent:@"app_bg.png"];
        
        NSData *imgData = [NSData dataWithContentsOfFile:bgBackgroundImage];
        
        if(imgData == nil){
            return [UIImage imageNamed:@"bg"];
        }
        UIImage *thumbNail = [[UIImage alloc] initWithData:imgData];
        return thumbNail;
    }
    else{
        return [UIImage imageNamed:@"bg"];
        
    }
}

+(BOOL)isSkinApplied{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"ThemeBck"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]){
        return true;
    }
    else {
        return false;
    }
    
}


+(NSString *)eventBackgroundImageURL {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"ThemeBck"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString * eventUrl = [SXCUtility cleanString:[SXCUtility getNSObject:@"events_background"]];
    if ([fileManager fileExistsAtPath:path] && eventUrl.length > 0){
        return eventUrl;
    }
    else{
        return @"";
        
    }
}




+(UIImage *)splashImage{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"ThemeBck"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]){
        
        NSString * bgBackgroundImage  = [path stringByAppendingPathComponent:@"Halo_Theme"];
        bgBackgroundImage  = [bgBackgroundImage stringByAppendingPathComponent:@"splash_bg.png"];
        
        NSData *imgData = [NSData dataWithContentsOfFile:bgBackgroundImage];
        UIImage *thumbNail = [[UIImage alloc] initWithData:imgData];
        if(imgData == nil){
            return [UIImage imageNamed:@"new_splash"];
        }

        [SXCUtility saveNSObject:@true forKey:@"BackgroundMode"];
        return thumbNail;
    }
    else{
        return [UIImage imageNamed:@"new_splash"];
        
    }
}

+(UIImage *)menuBackgroundImage{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"ThemeBck"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]){
        
        NSString * bgBackgroundImage  = [path stringByAppendingPathComponent:@"Halo_Theme"];
        bgBackgroundImage  = [bgBackgroundImage stringByAppendingPathComponent:@"slide_menu_bg.png"];
        
        NSData *imgData = [NSData dataWithContentsOfFile:bgBackgroundImage];
        UIImage *thumbNail = [[UIImage alloc] initWithData:imgData];
        if(imgData == nil){
            return [UIImage imageNamed:@"bg"];
        }

        return thumbNail;
    }
    else{
        return [UIImage imageNamed:@"bg"];
        
    }
}

+(UIImage *)filterBg{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"ThemeBck"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]){
        
        NSString * bgBackgroundImage  = [path stringByAppendingPathComponent:@"Halo_Theme"];
        bgBackgroundImage  = [bgBackgroundImage stringByAppendingPathComponent:@"filter_bg.png"];
        
        NSData *imgData = [NSData dataWithContentsOfFile:bgBackgroundImage];
        UIImage *thumbNail = [[UIImage alloc] initWithData:imgData];
        
        return thumbNail;
    }
    else{
        return nil;
        
    }
}


+(UIImage *)itemBG
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"ThemeBck"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]){
        
        NSString * bgBackgroundImage  = [path stringByAppendingPathComponent:@"Halo_Theme"];
        bgBackgroundImage  = [bgBackgroundImage stringByAppendingPathComponent:@"item_bg.png"];
        
        NSData *imgData = [NSData dataWithContentsOfFile:bgBackgroundImage];
        UIImage *thumbNail = [[UIImage alloc] initWithData:imgData];
        
        return thumbNail;
    }
    else{
        return nil;
        
    }
}

@end
