//
//  LocationHandler.h
//  ChopChop
//
//  Created by Anil Khanna on 14/11/14.
//  Copyright (c) 2014 Anil Khanna. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define LOCATION_DATA_UPDATED @"LOCATION_DATA_UPDATED"

@interface UBRLocationHandler : NSObject

typedef void (^LocationHandlerBlock)(BOOL status);
typedef void (^LocationHandlerErrorBlock)(NSError* error);


+ (instancetype)shareHandler;

-(void)locationManagerStopUpdating;

-(void)startLocationHandlerWithCompletion:(LocationHandlerBlock)completion WithErrorBlock:(LocationHandlerErrorBlock )errorBlock;

- (BOOL) isLocationServicesEnable;

@property (nonatomic, copy) CLPlacemark *placeMark;
@property (nonatomic, copy) CLLocation *currentLocation;
@property(assign, nonatomic) CLLocationDistance distanceFilter;


@property (nonatomic, copy) NSString *locatedAt;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *area;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *countryArea;
@property (nonatomic, copy) NSString *zipCode;


@end
