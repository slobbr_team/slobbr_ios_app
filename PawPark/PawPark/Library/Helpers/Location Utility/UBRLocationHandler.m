//
//  LocationHandler.m
//  ChopChop
//
//  Created by Anil Khanna on 14/11/14.
//  Copyright (c) 2014 Anil Khanna. All rights reserved.
//

#import "UBRLocationHandler.h"
#import "PWPApplicationUtility.h"
#import <UIKit/UIKit.h>


@interface UBRLocationHandler()<CLLocationManagerDelegate>{
    
    CLLocationManager *locationManager;
    
}

@property (nonatomic, strong) LocationHandlerBlock completionBlock;
@property (nonatomic, strong) LocationHandlerErrorBlock errorBlock;

@end


@implementation UBRLocationHandler

+(instancetype)shareHandler{
    static dispatch_once_t onceToken;
    static UBRLocationHandler *shared;
    dispatch_once(&onceToken, ^{
        shared = [[self class] new];
    });
    return shared;
}

- (BOOL) isLocationServicesEnable{
    
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied){
        return YES;
    }else{
        return NO;
    }
}

-(void)startLocationHandlerWithCompletion:(LocationHandlerBlock)completion WithErrorBlock:(LocationHandlerErrorBlock )errorBlock
{
    _completionBlock = nil;
    _errorBlock = nil;
    
    _completionBlock = completion;
    _errorBlock=errorBlock;
    
    
    
    if([CLLocationManager locationServicesEnabled]&&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
    {
        
            //---- For getting current gps location
        locationManager = [CLLocationManager new];
        locationManager.delegate = self;
        locationManager.distanceFilter = 1000;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [locationManager performSelector:@selector(requestAlwaysAuthorization) withObject:nil];
        }
        
        [locationManager startUpdatingLocation];
    
    }
    else
    {
       
    }
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    [self checkParkNearMe:[locations firstObject]];

    _currentLocation = [locations firstObject];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:_currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (!(error))
         {
             if ([placemarks count]>0) {
                 _placeMark = placemarks[0];
             }else{
                 return ;
             }
             _currentLocation = _placeMark.location;

             _locatedAt = [[_placeMark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];

             _zipCode =_placeMark.addressDictionary[@"ZIP"];
             
             _address = [_placeMark.addressDictionary valueForKey:@"Name"];
             
             _area = [[NSString alloc]initWithString:_placeMark.locality?:@""];
             _country = [[NSString alloc]initWithString:_placeMark.country?:@""];
             _countryArea = [NSString stringWithFormat:@"%@, %@", _area,_country];
             
             [[NSNotificationCenter defaultCenter] postNotificationName:LOCATION_DATA_UPDATED object:nil];
             
             if (_completionBlock) {
                 _completionBlock(YES);
             }
         }
         else
         {
             if (_errorBlock) {
                 _errorBlock(error);
             }

         }
    }];
    
    
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    NSLog(@"Authorization status is :%d",status);
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if (_errorBlock) {
        _errorBlock(error);
    }

}

-(void)locationManagerStopUpdating
{
    [locationManager stopUpdatingLocation];
}

-(void)checkParkNearMe:(CLLocation *)location
{

    NSArray * parks =  [PWPApplicationUtility favParks];
    for(NSDictionary * park in parks){
    
        NSNumber * lat = [park valueForKey:@"lat"];
        NSNumber * lng = [park valueForKey:@"lng"];
        
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:lat.doubleValue longitude:lng.doubleValue];
        
        
        CLLocationDistance distance = [locA distanceFromLocation:location];
        if(distance <= 1000){
            [self showLocalNotificationForPark:park];
        }
        return;
    }
}

-(void)showLocalNotificationForPark:(NSDictionary *)parkDictionary {

    UILocalNotification * localNotification = [[UILocalNotification alloc]init];
    localNotification.alertTitle = @"Location Alert";
    localNotification.alertBody = @"Woof!  You are near one of your Favorite Places, Tap Here to Check-in your dog(s)!";
    localNotification.userInfo = parkDictionary;
    
    [localNotification setFireDate:[NSDate dateWithTimeIntervalSinceNow:2]];
    [localNotification setTimeZone:[NSTimeZone  defaultTimeZone]];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];

}


@end
