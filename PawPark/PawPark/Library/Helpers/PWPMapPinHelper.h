//
//  PWPMapPinHelper.h
//  PawPark
//
//  Created by Samarth Singla on 20/11/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PWPMapPinHelper : NSObject

+(void)downloadPlacesPins:(NSArray*)places withCompletionBlock:(void (^)(BOOL isSuccess))successBlock;
+(UIImage *)correspondingToLogo:(NSString *)mapLogo;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (UIImage *)resizeImageForUploading:(UIImage *)image;
@end
