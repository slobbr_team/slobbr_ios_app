//
//  SXCTableViewLoadMoreDelegate.m
//  SixthContinent
//
//  Created by xyz on 26/02/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import "SXCTableViewLoadMoreDelegate.h"
#import "SXCLoadMoreCell.h"
#import "UIColor+PWPColor.h"

NSString * const SXCLoadMoreCellIdentifier = @"load_more_cell_identifier";


@interface SXCTableViewLoadMoreDelegate()
{
    __weak UITableView * tableViewLoadMore;

    UIActivityIndicatorView * activityIndicator;
    UILabel * labelNoData;
}
@property(nonatomic,strong)NSNumber * isLoading;
@property(nonatomic,strong)NSNumber * isErrorOccured;
@property(nonatomic,strong)UIRefreshControl * refreshControl;
@property(nonatomic,strong)NSString * noDataStringValue;

@end


@implementation SXCTableViewLoadMoreDelegate

-(instancetype)initWithTableView:(UITableView *)theTableView WithTotalCount:(NSNumber *)theTotalCount WithPageSize:(NSNumber *)thePageSize WithPageIndex:(NSNumber *)thePageIndex
{

    self=[super init];
    if(self)
    {
        _totalCount=theTotalCount;
        _limitstart=thePageIndex;
        _limitsize=thePageSize;
       
        tableViewLoadMore=theTableView;
        tableViewLoadMore.delegate=self;
        tableViewLoadMore.dataSource=self;
        
        [self addNoDataLabel];
        [self addActivityLoader];
        
        _isLoading=@false;
        _isLoadMoreRequired=@true;
        
        [tableViewLoadMore registerClass:[SXCLoadMoreCell class] forCellReuseIdentifier:SXCLoadMoreCellIdentifier];
    }
    return self;
}

-(instancetype)initWithTableView:(UITableView *)theTableView WithTotalCount:(NSNumber *)theTotalCount WithPageSize:(NSNumber *)thePageSize WithPageIndex:(NSNumber *)thePageIndex WithLaodMore:(NSNumber *)isLoadMoreRequired
{

    
    self=[super init];
    if(self)
    {
        _totalCount=theTotalCount;
        _limitstart=thePageIndex;
        _limitsize=thePageSize;
        tableViewLoadMore=theTableView;
        tableViewLoadMore.delegate=self;
        tableViewLoadMore.dataSource=self;
        
        _isLoading=@false;
        _isLoadMoreRequired=isLoadMoreRequired;

        [self addNoDataLabel];
        [self addActivityLoader];
        
        [tableViewLoadMore registerClass:[SXCLoadMoreCell class] forCellReuseIdentifier:SXCLoadMoreCellIdentifier];
    }
    return self;

}


#pragma mark - UITableView Datasource And Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([_dataSource respondsToSelector:@selector(numberOfSectionsInTableView:)]){
        return [_dataSource numberOfSectionsInTableView:tableView];
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if([_delegate respondsToSelector:@selector(tableView:heightForHeaderInSection:)]){
        return [_delegate tableView:tableView heightForHeaderInSection:section];
    }
    return 0.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if([_delegate respondsToSelector:@selector(tableView:viewForHeaderInSection:)]){
        return [_delegate tableView:tableView viewForHeaderInSection:section];
    }
    return nil;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSInteger numberOfRows;
    
    if([_dataSource respondsToSelector:@selector(tableView:numberOfRowsInSection:)]){
        numberOfRows= [_dataSource tableView:tableView numberOfRowsInSection:section];
    }
    else{
        numberOfRows=_currentArrayCount.integerValue;
    }
    
    if((_currentArrayCount.integerValue < _totalCount.integerValue)&&(_limitstart.integerValue<(_totalCount.integerValue-_limitsize.integerValue))&&(!_isErrorOccured.boolValue)&&(_isLoadMoreRequired.boolValue))
    {
        return numberOfRows+1;
    }
    
    return numberOfRows;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==_currentArrayCount.integerValue)
    {
        SXCLoadMoreCell * loadMoreCell=[tableView dequeueReusableCellWithIdentifier:SXCLoadMoreCellIdentifier forIndexPath:indexPath];
        [loadMoreCell setBackgroundColor:[UIColor clearColor]];
        loadMoreCell.activityIndicatorView.hidden=NO;
        [loadMoreCell.activityIndicatorView startAnimating];
        return loadMoreCell;
    }
    else if([_dataSource respondsToSelector:@selector(tableView:cellForRowAtIndexPath:)])
    {
        UITableViewCell * cell=[_dataSource tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell;
    }
    
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{

     if(indexPath.row==_currentArrayCount.integerValue)
     {
         return 40.0f;
     }
     else if([_delegate respondsToSelector:@selector(tableView:estimatedHeightForRowAtIndexPath:)])
     {
         return [_delegate tableView:tableView estimatedHeightForRowAtIndexPath:indexPath];
     }
    
    return 0.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==_currentArrayCount.integerValue)
    {
        return 40.0f;
    }
    else if([_delegate respondsToSelector:@selector(tableView:heightForRowAtIndexPath:)])
    {
        return [_delegate tableView:tableView heightForRowAtIndexPath:indexPath];
    }
    
    return 0.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if((indexPath.row!=_currentArrayCount.integerValue)&&([_delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)]))
    {
        [_delegate tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if((indexPath.row!=_currentArrayCount.integerValue)&&([_delegate respondsToSelector:@selector(tableView:didDeselectRowAtIndexPath:)]))
    {
        [_delegate tableView:tableView didDeselectRowAtIndexPath:indexPath];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Index Path ===%ld",(long)indexPath.row);
    
    if((indexPath.row==_currentArrayCount.integerValue)&&(_currentArrayCount.integerValue < _totalCount.integerValue)&&(_limitstart.integerValue<(_totalCount.integerValue-_limitsize.integerValue))&&(!_isLoading.boolValue)&&(_isLoadMoreRequired.boolValue))
    {
        _isLoading=@true;
        [_loadMoreDelegate tableView:tableView loadMoreWithNextPageIndex:[NSNumber numberWithInteger:(_limitstart.integerValue+_limitsize.integerValue)] WithPageSize:_limitsize];
    }

}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {

    if([_delegate respondsToSelector:@selector(tableView:canEditRowAtIndexPath:)])
    {
        return [_dataSource tableView:tableView canEditRowAtIndexPath:indexPath];
    }
    return NO;
}

    // Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if([_delegate respondsToSelector:@selector(tableView:commitEditingStyle:forRowAtIndexPath:)])
    {
        return [_dataSource tableView:tableView commitEditingStyle:editingStyle forRowAtIndexPath:indexPath];
    }
}


#pragma mark - Buisness Methods
-(void)addNoDataLabel
{
    
    labelNoData=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 25)];
    labelNoData.backgroundColor=[UIColor clearColor];
    
    labelNoData.textColor=[UIColor darkGrayColor];
    labelNoData.font=[UIFont fontWithName:@"Open Sans" size:16.0f];
    labelNoData.textAlignment=NSTextAlignmentCenter;
    labelNoData.hidden=YES;
    
    UIView * superView=tableViewLoadMore.superview;
    
    [superView addSubview:labelNoData];
    [superView bringSubviewToFront:labelNoData];

    [labelNoData setCenter:CGPointMake(superView.frame.size.width/2.0f, superView.frame.size.height/2.0f)];
    
    [labelNoData setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin];
    /*
    [tableViewLoadMore addConstraint:[NSLayoutConstraint constraintWithItem:labelNoData
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superView                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1.0
                                                           constant:10.0]];
    
    [tableViewLoadMore addConstraint:[NSLayoutConstraint constraintWithItem:labelNoData
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:superView
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1.0
                                                           constant:10.0]];
    
    [tableViewLoadMore addConstraint:[NSLayoutConstraint constraintWithItem:labelNoData
                                                                  attribute:NSLayoutAttributeTrailing
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:superView
                                                                  attribute:NSLayoutAttributeTrailing
                                                                 multiplier:1.0
                                                                   constant:10.0]];

    
    [tableViewLoadMore addConstraint:[NSLayoutConstraint constraintWithItem:labelNoData
                                                       attribute:NSLayoutAttributeHeight
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:nil
                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                      multiplier:1.0
                                                        constant:30.0]];
    
    [tableViewLoadMore addConstraint:[NSLayoutConstraint constraintWithItem:labelNoData
                                                                  attribute:NSLayoutAttributeWidth
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:superView
                                                                  attribute:NSLayoutAttributeWidth
                                                                 multiplier:1.0
                                                                   constant:-20.0]];

//    [tableViewLoadMore.superview bringSubviewToFront:labelNoData];
*/
}

-(void)addActivityLoader
{
    activityIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.hidden=YES;
    activityIndicator.color=[UIColor sxc_themeColor];

    
    
    
    [tableViewLoadMore.superview addSubview:activityIndicator];
    [tableViewLoadMore.superview bringSubviewToFront:activityIndicator];

    UIView * superView=tableViewLoadMore.superview;
    
    [activityIndicator setCenter:CGPointMake(superView.frame.size.width/2.0f, superView.frame.size.height/2.0f)];
    
    [activityIndicator setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin];
    
//    [tableViewLoadMore addConstraint:[NSLayoutConstraint constraintWithItem:activityIndicator
//                                                                  attribute:NSLayoutAttributeCenterX
//                                                                  relatedBy:NSLayoutRelationEqual
//                                                                     toItem:tableViewLoadMore.superview
//                                                                  attribute:NSLayoutAttributeCenterX
//                                                                 multiplier:1.0
//                                                                   constant:0.0]];
//    
//    [tableViewLoadMore addConstraint:[NSLayoutConstraint constraintWithItem:activityIndicator
//                                                                  attribute:NSLayoutAttributeCenterY
//                                                                  relatedBy:NSLayoutRelationEqual
//                                                                     toItem:tableViewLoadMore.superview
//                                                                  attribute:NSLayoutAttributeCenterY
//                                                                 multiplier:1.0                                                                   constant:00.0]];
}

-(void)reloadData
{
    _isLoading=@false;

    [tableViewLoadMore reloadData];

    [labelNoData setHidden:YES];
    
    [self hideCenterLoader];
    [self endPullToRefresh];
}

- (void)refreshView:(UIRefreshControl *)sender {
    
    [_loadMoreDelegate tableView:tableViewLoadMore loadMoreWithNextPageIndex:@0 WithPageSize:_limitsize];
    [labelNoData setHidden:YES];
}


-(void)hideCenterLoader
{
    [activityIndicator setHidden:YES];
    [activityIndicator stopAnimating];
    
    [self endPullToRefresh];
}

-(void)addPullToRefresh
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor=[UIColor sxc_themeColor];
    [self.refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    [tableViewLoadMore addSubview:self.refreshControl];
}

-(void)showLoaderCenter
{
    [activityIndicator setHidden:NO];
    [activityIndicator startAnimating];
    [labelNoData setHidden:YES];
}

-(void)showNoDataLabel:(NSString *)noDataLabelString
{
    [labelNoData setCenter:CGPointMake(tableViewLoadMore.superview.frame.size.width/2.0f, tableViewLoadMore.superview.frame.size.height/2.0f)];

    [tableViewLoadMore.superview bringSubviewToFront:labelNoData];
    [labelNoData setHidden:NO];
    [labelNoData setText:noDataLabelString];
    [self hideCenterLoader];
}

-(void)endPullToRefresh
{
    [self.refreshControl endRefreshing];
}
-(void)automaticallyRefreshControllOnView
{
    [tableViewLoadMore setContentOffset:CGPointMake(0, -self.refreshControl.frame.size.height) animated:YES];
    [self.refreshControl beginRefreshing];
}

-(void)dealloc
{
    [tableViewLoadMore setEditing:NO];
}




@end
