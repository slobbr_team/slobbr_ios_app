//
//  SXCLoadMoreCell.m
//  SixthContinent
//
//  Created by xyz on 26/02/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import "SXCLoadMoreCell.h"
#import "UIColor+PWPColor.h"

@interface SXCLoadMoreCell()
{
}
@end


@implementation SXCLoadMoreCell

+(NSString*)defaultIdentifier{
    return NSStringFromClass([self class]);
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];

    if(self)
    {
        
        _activityIndicatorView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [_activityIndicatorView setColor:[UIColor sxc_themeColor]];
        _activityIndicatorView.center=CGPointMake(self.frame.size.width/2.0f, self.frame.size.height/2.0f);
        _activityIndicatorView.autoresizingMask=UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
        
        [_activityIndicatorView startAnimating];
        
        [self.contentView addSubview:_activityIndicatorView];
    
    }
    return self;
}

@end
