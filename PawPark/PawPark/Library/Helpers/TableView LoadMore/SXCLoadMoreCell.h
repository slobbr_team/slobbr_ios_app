//
//  SXCLoadMoreCell.h
//  SixthContinent
//
//  Created by xyz on 26/02/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SXCLoadMoreCell : UITableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

+(NSString*)defaultIdentifier;

@property (nonatomic,strong) UIActivityIndicatorView * activityIndicatorView;

@end
