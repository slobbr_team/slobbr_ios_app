//
//  SXCLoadMoreDelegate.h
//  SixthContinent
//
//  Created by xyz on 26/02/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@protocol SXCLoadMoreDelegate <NSObject>

-(void)tableView:(UITableView *)tableView loadMoreWithNextPageIndex:(NSNumber *)nextPageIndex WithPageSize:(NSNumber *)pageSize;


@end
