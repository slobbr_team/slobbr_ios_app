//
//  SXCTableViewLoadMoreDelegate.h
//  SixthContinent
//
//  Created by xyz on 26/02/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SXCLoadMoreDelegate.h"
#import <UIKit/UIKit.h>


@interface SXCTableViewLoadMoreDelegate : NSObject <UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)NSNumber * totalCount;
@property(nonatomic,strong)NSNumber * limitsize;
@property(nonatomic,strong)NSNumber * limitstart;
@property(nonatomic,strong)NSNumber * currentArrayCount;
@property(nonatomic,strong)NSNumber * loadmoreRequiredSection;


@property(nonatomic,weak)id <UITableViewDataSource> dataSource;
@property(nonatomic,weak)id <UITableViewDelegate> delegate;
@property(nonatomic,strong)NSNumber * isLoadMoreRequired;
@property(nonatomic,weak)id <SXCLoadMoreDelegate> loadMoreDelegate;


-(instancetype)initWithTableView:(UITableView *)theTableView WithTotalCount:(NSNumber *)theTotalCount WithPageSize:(NSNumber *)thePageSize WithPageIndex:(NSNumber *)thePageIndex;

-(instancetype)initWithTableView:(UITableView *)theTableView WithTotalCount:(NSNumber *)theTotalCount WithPageSize:(NSNumber *)thePageSize WithPageIndex:(NSNumber *)thePageIndex WithLaodMore:(NSNumber *)isLoadMoreRequired;

-(void)hideCenterLoader;

-(void)reloadData;

-(void)addPullToRefresh;

-(void)showLoaderCenter;

-(void)showNoDataLabel:(NSString *)noDataLabelString;

-(void)endPullToRefresh;

-(void)automaticallyRefreshControllOnView;
@end
