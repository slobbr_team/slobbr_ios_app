//
//  PWPMapPinHelper.m
//  PawPark
//
//  Created by Samarth Singla on 20/11/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPMapPinHelper.h"
#import "NSString+Hash.h"
#import <AFNetworking.h>
#import <SVProgressHUD.h>
#import "SXCUtility.h"

static int numberOfPinsFailed = 0;

@implementation PWPMapPinHelper

+(void)downloadPlacesPins:(NSArray*)urls withCompletionBlock:(void (^)(BOOL isSuccess))successBlock
{
    NSArray * placeLogos = urls;
    NSMutableArray * pendingLogos = [[NSMutableArray alloc] init];
    
    for(NSString * placeLogo in placeLogos){
        if([PWPMapPinHelper imageDownloaded:placeLogo] == false && [UIImage imageNamed:[placeLogo stringByReplacingOccurrencesOfString:@"/" withString:@":"]] == nil){
            [pendingLogos addObject:placeLogo];
        }
    }

    numberOfPinsFailed = 0;

    pendingLogos = [[NSSet setWithArray:pendingLogos] allObjects].mutableCopy;
    
    if(pendingLogos.count>0){
        
        dispatch_group_t group = dispatch_group_create();
        
        for(NSString * string in pendingLogos){
            [PWPMapPinHelper downloadImageAndSaveItToCacheDirectory:string WithDisPatchGroup:group];
        }
        
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{

            if (numberOfPinsFailed > 0){
                [SXCUtility saveNSObject:@true forKey:@"pins_failed"];
            }
            else{
                [SXCUtility saveNSObject:@false forKey:@"pins_failed"];
            }
            successBlock(true);
        });
    }
    else{
        successBlock(true);
    }
}

+(BOOL)imageDownloaded:(NSString *)placeLogoPath {

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * hashOfPlaceLogo = [placeLogoPath MD5];
    
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:hashOfPlaceLogo];
    NSFileManager *fileManager = [NSFileManager defaultManager];
   
    return [fileManager fileExistsAtPath:path];
}


+(void)downloadImageAndSaveItToCacheDirectory:(NSString *)imagePath WithDisPatchGroup:(dispatch_group_t) group {



    dispatch_group_enter(group);
    
    NSString * urlString = [NSString stringWithFormat:@"http://www.slobbr.com%@",imagePath];
    urlString = [urlString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * hashOfPlaceLogo = [imagePath MD5];
    
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:hashOfPlaceLogo];

    NSURL * url = [NSURL URLWithString:path];
    
    NSError * error;
    [url setResourceValue: [NSNumber numberWithBool: YES] forKey: NSURLIsExcludedFromBackupKey error: &error];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        dispatch_group_leave(group);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        numberOfPinsFailed += 1;
        dispatch_group_leave(group);
    }];
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
    }];
    
    [operation start];
}

+(UIImage *)correspondingToLogo:(NSString *)mapLogo {

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * hashOfPlaceLogo = [mapLogo MD5];

    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:hashOfPlaceLogo];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    UIImage * image =  [UIImage imageNamed:[mapLogo stringByReplacingOccurrencesOfString:@"/" withString:@":"]];

    if(image !=nil) {
        CGFloat scale = [UIScreen mainScreen].scale;
        return [PWPMapPinHelper imageWithImage:image scaledToSize:CGSizeMake(47*scale, 61*scale)];
    }
    else if ([fileManager fileExistsAtPath:path]){
        NSData *imgData = [NSData dataWithContentsOfFile:path];
        UIImage *thumbNail = [[UIImage alloc] initWithData:imgData];
        CGFloat scale = [UIScreen mainScreen].scale;
        thumbNail = [PWPMapPinHelper imageWithImage:thumbNail scaledToSize:CGSizeMake(47*scale, 61*scale)];
        return thumbNail;
    }
    else{
        return [UIImage imageNamed:@"ic_park_marker"];
    }
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {

    CGFloat scale = [UIScreen mainScreen].scale;
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(newSize.width/scale, newSize.height/scale), NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width/scale, newSize.height/scale)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)resizeImageForUploading:(UIImage *)image {
    
    CGFloat aspectRatio = image.size.width / image.size.height;
    
    if(image.size.width > 600) {
        return [self imageWithImage:image scaledToSize:CGSizeMake(600, 1 * 600/aspectRatio)];
    }
    else if (image.size.height > 600) {
        return [self imageWithImage:image scaledToSize:CGSizeMake(600* aspectRatio,600)];
    }
    else {
        return image;
    }

}

@end
