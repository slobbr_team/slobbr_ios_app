//
//  PWPHomeParkTableViewCell.m
//  PawPark
//
//  Created by daffomac on 4/26/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPHomeParkTableViewCell.h"
#import <UIImageView+WebCache.h>
#import "PWPConstant.h"
#import "SXCUtility.h"
#import "PWPAppDelegate.h"

@interface PWPHomeParkTableViewCell()
{

    NSDictionary * parkDictionary;
    
    __weak IBOutlet UIView *viewInner;
    __weak IBOutlet UILabel *labelParkTitle;
    __weak IBOutlet UILabel *labelOpeningHours;
    __weak IBOutlet UIImageView *imageViewParkImageView;
    __weak IBOutlet UIButton *buttonAroundMe;
    __weak IBOutlet UIButton *buttonDogCheckIn;
    
    UITapGestureRecognizer * tapGesture1;
    UITapGestureRecognizer * tapGesture2;
    UITapGestureRecognizer * tapGesture3;

    
    
}
@property (weak, nonatomic) IBOutlet UILabel *labels;
@property (weak, nonatomic) IBOutlet UILabel *labelT;
@property (weak, nonatomic) IBOutlet UILabel *labelA;

- (IBAction)settingClicked:(id)sender;
- (IBAction)checkinClicked:(id)sender;
- (IBAction)dogCheckedInClicked:(id)sender;
- (IBAction)aroundMeClicked:(id)sender;

@end

@implementation PWPHomeParkTableViewCell

- (void)awakeFromNib {

    [super awakeFromNib];

    viewInner.layer.borderColor=[UIColor colorWithRed:237/255.0f green:237/255.0f blue:237/255.0f alpha:1.0f].CGColor;
    viewInner.layer.borderWidth=2.0f;
    viewInner.layer.cornerRadius=2.0f;
    
    
    tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(staViewClicked:)];
    tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(staViewClicked:)];
    tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(staViewClicked:)];
    
    _labelA.userInteractionEnabled=YES;
    _labels.userInteractionEnabled=YES;
    _labelT.userInteractionEnabled=YES;
    
    [_labelA addGestureRecognizer:tapGesture1];
    [_labels addGestureRecognizer:tapGesture2];
    [_labelT addGestureRecognizer:tapGesture3];
    
    [buttonDogCheckIn.imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)configueWithData:(id)data{

    
    
    
    
    if(((NSNumber *)[SXCUtility getNSObject:@"isLocationHelp"]).boolValue){
        buttonAroundMe.hidden=NO;
    }
    else{
        buttonAroundMe.hidden=YES;
    }
   
    if([data isKindOfClass:[NSDictionary class]]){
      
        parkDictionary=data;
        
        labelParkTitle.text=data[@"name"];
        
        
        if([data valueForKey:@"operating_hours"]&&([SXCUtility cleanString:[data valueForKey:@"operating_hours"]].length>0)){
            labelOpeningHours.text=[NSString stringWithFormat:@"Opening Hours: %@",data[@"operating_hours"]];
        }
        else{
            labelOpeningHours.text=[NSString stringWithFormat:@"Opening Hours: Not Available"];
        }

        

        NSString * imageUrl=[[NSString alloc] initWithFormat:@"%@%@",K_URL_HOSTNAME,data[@"image_url"]];
        
        imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"uploads/park//" withString:@"media/cache/park_thumb/"];
        
        [imageViewParkImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"default_park"]];
        
        
        _labelA.clipsToBounds=YES;
        _labels.clipsToBounds=YES;
        _labelT.clipsToBounds=YES;
        
        _labelA.layer.cornerRadius=_labelA.frame.size.width/2.0f;
        _labelT.layer.cornerRadius=_labelT.frame.size.width/2.0f;
        _labels.layer.cornerRadius=_labels.frame.size.width/2.0f;
        
        
        if(([data valueForKey:@"sta_score"][@"t"])&&([data valueForKey:@"sta_score"][@"s"])&&([data valueForKey:@"sta_score"][@"a"])){
            _labelA.hidden=NO;
            _labels.hidden=NO;
            _labelT.hidden=NO;
            
            _labelT.text=[NSString stringWithFormat:@"t:%@",[data valueForKey:@"sta_score"][@"t"]];
            _labels.text=[NSString stringWithFormat:@"s:%@",[data valueForKey:@"sta_score"][@"s"]];
            _labelA.text=[NSString stringWithFormat:@"a:%@",[data valueForKey:@"sta_score"][@"a"]];
            
        }
        else{
            _labelA.hidden=YES;
            _labels.hidden=YES;
            _labelT.hidden=YES;
        }

        if([data valueForKey:@"active_dogs_count"])
        {
            NSString * checkInDogsCount = [SXCUtility cleanString:[[data valueForKey:@"active_dogs_count"] description]];
            
            [buttonDogCheckIn setTitle:[NSString stringWithFormat:@"%@ dogs here",checkInDogsCount] forState:UIControlStateNormal];
        }
        else{
            [buttonDogCheckIn setTitle:@"0 dogs here" forState:UIControlStateNormal];
        }


    }
}

-(IBAction)settingClicked:(id)sender{
    [_delegate settingClicked];
}

- (IBAction)checkinClicked:(id)sender {
    [_delegate checkInClicked:parkDictionary];

}

- (IBAction)dogCheckedInClicked:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(checkInDogsClicked:)]) {
        [_delegate checkInDogsClicked:parkDictionary];
    }
}

- (IBAction)aroundMeClicked:(id)sender {

    if (_delegate && [_delegate respondsToSelector:@selector(useMyLocationClicked)]) {
        [_delegate useMyLocationClicked];
    }
}

-(IBAction)staViewClicked:(id)sender{    
    PWPAppDelegate * delegate =(PWPAppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate showSTAHelpViewWithS:[parkDictionary valueForKey:@"sta_score"][@"s"] withT:[parkDictionary valueForKey:@"sta_score"][@"t"] withA:[parkDictionary valueForKey:@"sta_score"][@"a"]];
}

@end
