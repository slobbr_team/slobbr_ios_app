//
//  PWPDogImageCell.m
//  PawPark
//
//  Created by Samarth Singla on 01/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPDogImageCell.h"

@interface PWPDogImageCell() {

    NSIndexPath * indexPath;
    __weak id <PWPDogImageCellProtocoal> delegate;
}

@end

@implementation PWPDogImageCell
-(void)configureViewWithDelegate:(id)theDelegate WithIndexPath:(NSIndexPath *)theIndexPath {
    delegate = theDelegate;
    indexPath = theIndexPath;
}

- (IBAction)deleteButtonClicked:(id)sender {

    if(delegate && [delegate respondsToSelector:@selector   (imageDeleteClicked:)]){
        [delegate imageDeleteClicked:indexPath];
    }
}
@end
