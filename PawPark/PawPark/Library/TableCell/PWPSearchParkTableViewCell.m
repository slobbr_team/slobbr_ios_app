//
//  PWPSearchParkTableViewCell.m
//  PawPark
//
//  Created by daffolap19 on 5/23/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPSearchParkTableViewCell.h"
#import <UIImageView+WebCache.h>
#import "UIColor+PWPColor.h"
#import "PWPConstant.h"
#import "SXCUtility.h"
#import "PWPApplicationUtility.h"
#import "PWPAppDelegate.h"
#import <UIViewController+CWPopup.h>
#import "PWPImageInfoViewController.h"
#import "UBRLocationHandler.h"
#import "PWPAmentitiesCell.h"

@interface PWPSearchParkTableViewCell()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    UITapGestureRecognizer * tapGesture1;
    UITapGestureRecognizer * tapGesture2;
    UITapGestureRecognizer * tapGesture3;

    __weak IBOutlet UIView *viewDog4;
    __weak IBOutlet UIView *viewDog3;
    __weak IBOutlet UIView *viewDog2;
    __weak IBOutlet UIView *viewDog1;
    __weak IBOutlet UIButton *buttonCheckedInDogsNew;
    __weak IBOutlet UILabel *labelTotalDogsCheckedin;
    __weak IBOutlet NSLayoutConstraint *lcdog1width;
    __weak IBOutlet NSLayoutConstraint *lcdog2width;
    __weak IBOutlet NSLayoutConstraint *lcdog3width;
    __weak IBOutlet NSLayoutConstraint *lcdog4Width;
    __weak IBOutlet UIImageView *imageviewCheckedInDog4;
    __weak IBOutlet UIImageView *imageviewCheckinDog3;
    __weak IBOutlet UIImageView *imaegviewCheckedInDog2;
    __weak IBOutlet UIImageView *imageViewCheckedInDog1;
}

@property(nonatomic,strong)NSMutableDictionary * dataInfo;
@property(nonatomic,strong)NSArray * amenities;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintRatingHeight; //33
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintYelpIcon;//33

@property (weak, nonatomic) IBOutlet UIButton *buttonAddPhotos;
@property (weak, nonatomic) IBOutlet UILabel *labelParkName;
@property (weak, nonatomic) IBOutlet UILabel *labelParkNumberDogs;
@property (weak, nonatomic) IBOutlet UILabel *labelParkAddress;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPark;
@property (weak, nonatomic) IBOutlet UIButton *buttonFavorite;
@property (weak, nonatomic) IBOutlet UIButton *buttonQuickCheckIn;
@property (weak, nonatomic) IBOutlet UIButton *buttonInvite;
@property (weak, nonatomic) IBOutlet UIButton *buttonPhotos;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewRating;

- (IBAction)checkButtonClicked:(id)sender;
- (IBAction)buttonHomeClicked:(id)sender;
@end



@implementation PWPSearchParkTableViewCell

-(void)configureAccordingToCheckInDogs:(NSArray *)checkedinDogs{
    
    
    imageviewCheckedInDog4.layer.borderColor = [UIColor colorWithRed:44/255.0f green:166/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    imageviewCheckinDog3.layer.borderColor = [UIColor colorWithRed:44/255.0f green:166/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    imaegviewCheckedInDog2.layer.borderColor = [UIColor colorWithRed:44/255.0f green:166/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    imageViewCheckedInDog1.layer.borderColor = [UIColor colorWithRed:44/255.0f green:166/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    
    imageviewCheckedInDog4.layer.borderWidth = 1.0f;
    imageviewCheckinDog3.layer.borderWidth = 1.0f;
    imaegviewCheckedInDog2.layer.borderWidth = 1.0f;
    imageViewCheckedInDog1.layer.borderWidth = 1.0f;
    if(checkedinDogs.count==0){
        
        lcdog1width.constant = 0.0f;
        lcdog2width.constant = 0.0f;
        lcdog3width.constant = 0.0f;
        lcdog4Width.constant = 0.0f;
        
        viewDog4.hidden = true;
        viewDog3.hidden = true;
        viewDog2.hidden = true;
        viewDog1.hidden = true;
        
        buttonCheckedInDogsNew.hidden = true;
        
    }
    else if(checkedinDogs.count==1){
        
        lcdog1width.constant = 28.0f;
        lcdog2width.constant = 0.0f;
        lcdog3width.constant = 0.0f;
        lcdog4Width.constant = 0.0f;
        
        viewDog4.hidden = true;
        viewDog3.hidden = true;
        viewDog2.hidden = true;
        viewDog1.hidden = false;
        
        buttonCheckedInDogsNew.hidden = false;
        
        
        if (checkedinDogs[0][@"image_url"] != nil){
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[0][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[0][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[0][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[0][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imageViewCheckedInDog1 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        
        
    }
    else if(checkedinDogs.count==2){
        lcdog1width.constant = 28.0f;
        lcdog2width.constant = 28.0f;
        lcdog3width.constant = 0.0f;
        lcdog4Width.constant = 0.0f;
        
        viewDog4.hidden = true;
        viewDog3.hidden = true;
        viewDog2.hidden = false;
        viewDog1.hidden = false;
        
        buttonCheckedInDogsNew.hidden = false;
        
        if (checkedinDogs[0][@"image_url"] != nil){
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[0][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[0][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[0][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[0][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w1000"]];
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imageViewCheckedInDog1 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        if (checkedinDogs[1][@"image_url"] != nil){
            [imaegviewCheckedInDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[1][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[1][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[1][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[1][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [imaegviewCheckedInDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imaegviewCheckedInDog2 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        
    }
    else if(checkedinDogs.count==3){
        lcdog1width.constant = 28.0f;
        lcdog2width.constant = 28.0f;
        lcdog3width.constant = 28.0f;
        lcdog4Width.constant = 0.0f;
        
        viewDog4.hidden = true;
        viewDog3.hidden = false;
        viewDog2.hidden = false;
        viewDog1.hidden = false;
        
        buttonCheckedInDogsNew.hidden = false;
        
        if (checkedinDogs[0][@"image_url"] != nil){
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[0][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[0][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[0][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[0][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w1000"]];
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imageViewCheckedInDog1 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        if (checkedinDogs[1][@"image_url"] != nil){
            [imaegviewCheckedInDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[1][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[1][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[1][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[1][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [imaegviewCheckedInDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imaegviewCheckedInDog2 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        if (checkedinDogs[2][@"image_url"] != nil){
            [imageviewCheckinDog3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[2][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[2][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[2][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[2][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [imageviewCheckinDog3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imageviewCheckinDog3 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
    }
    else if(checkedinDogs.count>=4){
        
        lcdog1width.constant = 28.0f;
        lcdog2width.constant = 28.0f;
        lcdog3width.constant = 28.0f;
        lcdog4Width.constant = 28.0f;
        
        viewDog4.hidden = false;
        viewDog3.hidden = false;
        viewDog2.hidden = false;
        viewDog1.hidden = false;
        
        
        buttonCheckedInDogsNew.hidden = false;
        
        if (checkedinDogs[0][@"image_url"] != nil){
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[0][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[0][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[0][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[0][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w1000"]];
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imageViewCheckedInDog1 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        if (checkedinDogs[1][@"image_url"] != nil){
            [imaegviewCheckedInDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[1][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[1][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[1][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[1][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [imaegviewCheckedInDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imaegviewCheckedInDog2 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        if (checkedinDogs[2][@"image_url"] != nil){
            [imageviewCheckinDog3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[2][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[2][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[2][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[2][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [imageviewCheckinDog3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imageviewCheckinDog3 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        if (checkedinDogs[3][@"image_url"] != nil){
            [imageviewCheckedInDog4 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[3][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[3][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[3][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[3][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [imageviewCheckedInDog4 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imageviewCheckedInDog4 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
    }
 
}


- (void)awakeFromNib {

    [super awakeFromNib];

    tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(staViewClicked:)];
    tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(staViewClicked:)];
    
    tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(staViewClicked:)];
   
     tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(staViewClicked:)];
    
     tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(staViewClicked:)];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0,  [UIScreen mainScreen].bounds.size.width,237);
    
    // Add colors to layer
    UIColor *centerColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.50];
    UIColor *topColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.01];
    UIColor *endColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.95];
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[topColor CGColor],
                       (id)[centerColor CGColor],
                       (id)[endColor CGColor],
                       nil];
    self.imageViewPark.layer.name = @"Gradient";
    for (CALayer *layer in self.imageViewPark.layer.sublayers) {
        [layer removeFromSuperlayer];
    }
    [self.imageViewPark.layer insertSublayer:gradient atIndex:0];

    [_collectionView registerNib:[UINib nibWithNibName:@"PWPAmentitiesCell" bundle:nil] forCellWithReuseIdentifier:@"PWPAmentitiesCell"];
}

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(void)bindValueWithComponentsWithData:(NSMutableDictionary *)data reloadData:(BOOL)reloadCollectionView {
    
    _dataInfo=data.copy;
    _collectionView.delegate = nil;
    _collectionView.dataSource = nil;
    
    if([data valueForKey:@"yelp_url"] !=nil){
        self.layoutConstraintYelpIcon.constant = 33.0f;
    }else {
        self.layoutConstraintYelpIcon.constant = 0.0f;
    }
    
    if([data valueForKey:@"yelp_rating"] !=nil){
        self.layoutConstraintRatingHeight.constant = 33.0f;
        
        float ratingValue = [[[data valueForKey:@"yelp_rating"] description] floatValue];
        
        int integerRatingValue = ratingValue;
        
        float roundOffValue = integerRatingValue + 0.5f;
        if(ratingValue < roundOffValue) {
            roundOffValue = roundOffValue - 0.5f;
        }
        
        NSString* formattedNumber = [NSString stringWithFormat:@"star_%.01f", roundOffValue];
        [_imageViewRating setImage:[UIImage imageNamed:formattedNumber]];
    
    }else {
        self.layoutConstraintRatingHeight.constant = 0.0f;
    }
    
    _buttonQuickCheckIn.hidden = false;
    
    if([SXCUtility cleanInt:data[@"is_favourite"]] == 1){
        [_buttonFavorite setTintColor:[UIColor whiteColor]];
        _buttonFavorite.selected = true;


    }
    else{
        [_buttonFavorite setTintColor:[UIColor grayColor]];
        _buttonFavorite.selected = false;
    }
    
    _labelParkName.text=data[@"name"];
    _labelParkAddress.text=[NSString stringWithFormat:@"%@, %@, %@",data[@"address"],data[@"address_locality"],data[@"address_postal_code"]];
    
    NSMutableString * checkInDogs = [[NSMutableString alloc] init];
    
    if([data valueForKey:@"operating_hours"]&&([SXCUtility cleanString:[data valueForKey:@"operating_hours"]].length>0)){
         [checkInDogs appendString:[NSString stringWithFormat:@"Opening Hours: %@",data[@"operating_hours"]]];
    }
    else{
        [checkInDogs appendString:[NSString stringWithFormat:@"Opening Hours: Not Available"]];
    }
    
    checkInDogs = [[checkInDogs componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "].mutableCopy;
    
    _labelParkNumberDogs.text = checkInDogs;
    
     NSString * imageUrl = @"";
    
    if([data valueForKey:@"thumbs"] && [[data valueForKey:@"thumbs"] valueForKey:@"w400"]){
        imageUrl= [SXCUtility cleanString:[[SXCUtility cleanDictionary:data[@"thumbs"]] valueForKey:@"w400"]];
        imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"uploads/park//" withString:@"media/cache/park_thumb/"];
    }
    
    [_imageViewPark setContentMode:UIViewContentModeScaleAspectFill];
    [_imageViewPark setBackgroundColor:[UIColor clearColor]];
    
    [_buttonAddPhotos setHidden:false];
    if(imageUrl != nil && imageUrl.length > 0) {
        [_imageViewPark sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"default_park"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if(error == nil){
                [_buttonAddPhotos setHidden:true];
                [_imageViewPark setBackgroundColor:[UIColor clearColor]];
            } else {
                [_buttonAddPhotos setHidden:false];
            }
        }];
    }
    else {
        [_buttonAddPhotos setHidden:false];
        [_imageViewPark setImage:[UIImage imageNamed:@"default_park"]];
    }
    
    self.amenities = [SXCUtility cleanArray:[self.dataInfo valueForKey:@"filter_amenities"]];
    
    NSString * checkInDogsCount = [SXCUtility cleanString:[[self.dataInfo valueForKey:@"active_dogs_count"] description]];
    labelTotalDogsCheckedin.text = [NSString stringWithFormat:@"%@ Dogs here",checkInDogsCount];

    NSArray * checkInDogs1 = [SXCUtility cleanArray:[self.dataInfo valueForKey:@"last_active_dogs"]];
    [self configureAccordingToCheckInDogs:checkInDogs1];
    
    
    if(reloadCollectionView){
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        [_collectionView.collectionViewLayout invalidateLayout];
    }
}


- (IBAction)checkButtonClicked:(id)sender {
    if(_delegate&&[_delegate respondsToSelector:@selector(parkOptionClickedWithData:)]){
        [_delegate parkOptionClickedWithData:_dataInfo];
    }
}

- (IBAction)buttonHomeClicked:(id)sender {
    if(_delegate&&[_delegate respondsToSelector:@selector(parkOption3ClickedWithData:)]){
        [_delegate parkOption3ClickedWithData:_dataInfo];
    }
}

- (IBAction)inviteButtonClicked:(id)sender {
    
    if(_delegate&&[_delegate respondsToSelector:@selector(inviteOptionClicked:)]){
        [_delegate inviteOptionClicked:_dataInfo];
    }
 
}

- (IBAction)quuickCheckInButtonClicked:(id)sender {
    
    if(_delegate&&[_delegate respondsToSelector:@selector(parkQuickCheckInClicked:)]){
        [_delegate parkQuickCheckInClicked:_dataInfo];
    }
    
}


-(IBAction)directionClicked:(id)sender {

    if([_dataInfo valueForKey:@"lat"] != nil && [_dataInfo valueForKey:@"lng"] != nil) {
    
        NSString *current_lat = [NSString stringWithFormat:@"%f",[UBRLocationHandler shareHandler].currentLocation.coordinate.latitude];
        NSString *current_long = [NSString stringWithFormat:@"%f",[UBRLocationHandler shareHandler].currentLocation.coordinate.longitude];
        
        NSString *dest_lat = [[_dataInfo valueForKey:@"lat"] description];
        NSString *dest_longi = [[_dataInfo valueForKey:@"lng"] description];
        
        NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%@,%@&daddr=%@,%@",current_lat, current_long, dest_lat, dest_longi];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: directionsURL]];
    }
}

- (IBAction)addPhotosClicked:(id)sender {
    if(_delegate&&[_delegate respondsToSelector:@selector(addPlaceClickedWithData:)]){
        [_delegate addPlaceClickedWithData:_dataInfo];
    }
}

- (IBAction)infoButtonClicked:(id)sender {
    
    UIStoryboard * storyBoard =[UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    
    PWPImageInfoViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPImageInfoViewController"];
    viewController.dictionary = self.self.dataInfo;
    
    UINavigationController *navigationController =[[UINavigationController alloc] initWithRootViewController:viewController];
    
    navigationController.navigationBar.translucent=YES;
    
    [self.window.rootViewController presentViewController:navigationController animated:YES completion:nil];
}

- (BOOL)isYelpInstalled {
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"yelp:"]];
}

- (void)maybeDoSomethingWithYelp {
    if ([self isYelpInstalled]) {
        // Call into the Yelp app
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"yelp:///biz/the-sentinel-san-francisco"]];
    } else {
        // Use the Yelp touch site
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://yelp.com/biz/the-sentinel-san-francisco"]];
    }
}

- (IBAction)yelpClickedButtonClicked:(id)sender {
 
    if([_dataInfo valueForKey:@"yelp_url"] !=nil){
        if ([self isYelpInstalled]) {
            NSString * string = [[[_dataInfo valueForKey:@"yelp_url"] description] stringByReplacingOccurrencesOfString:@"http://yelp.com" withString:@"yelp://"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
        } else {
            // Use the Yelp touch site
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[_dataInfo valueForKey:@"yelp_url"] description]]];
        }
    }
    

    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.amenities.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
     return CGSizeMake(50,28);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)theCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PWPAmentitiesCell * amenitiesCell = [_collectionView dequeueReusableCellWithReuseIdentifier:@"PWPAmentitiesCell" forIndexPath:indexPath];
    amenitiesCell.imageViewAmenities.image = [UIImage imageNamed:self.amenities[indexPath.row]];
    return amenitiesCell;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (IBAction)checkedInDogsClicked:(id)sender {
    
    if(_delegate&&[_delegate respondsToSelector:@selector(parkOption2ClickedWithData:)]){
        [_delegate parkOption2ClickedWithData:_dataInfo];
    }
}
@end
