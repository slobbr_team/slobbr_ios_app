//
//  PWPNotificationFriendRequestCell.m
//  PawPark
//
//  Created by daffolap19 on 5/30/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPNotificationFriendRequestCell.h"

@interface PWPNotificationFriendRequestCell()
@property(nonatomic,weak)IBOutlet UILabel * title;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;

@property(nonatomic,weak)IBOutlet NSMutableDictionary * notificationDetail;

-(IBAction)acceptButtonClicked:(id)sender;
-(IBAction)cancelButtonClicked:(id)sender;
@end


@implementation PWPNotificationFriendRequestCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellWithData:(NSMutableDictionary *)aNotificationDictionary{

    _notificationDetail=aNotificationDictionary;
    
    _title.text = aNotificationDictionary[@"initiator"][@"name"];
    _labelDescription.text=[NSString stringWithFormat:@"has requested friendship with %@",aNotificationDictionary[@"recipient"][@"name"]];
}

-(void)acceptButtonClicked:(id)sender
{
    [_delegate notificationFriendRequestConfirmWithData:_notificationDetail];
    
}
-(void)cancelButtonClicked:(id)sender
{
    [_delegate notificationFriendRequestCancelWithData:_notificationDetail];
    
}
@end
