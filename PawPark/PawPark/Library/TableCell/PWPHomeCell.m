//
//  PWPHomeCollectionViewCell.m
//  PawPark
//
//  Created by xyz on 7/16/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPHomeCell.h"
#import <UIImageView+WebCache.h>
#import "PWPHomeCollectionViewCell.h"
#import "PWPConstant.h"
#import "SXCUtility.h"
#import "PWPDownloadBackgroundImage.h"
#import "UIColor+PWPColor.h"

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width

@interface PWPHomeCell() <UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,PWPHomeCollectionViewCellDelegate>
{
    __weak IBOutlet UIButton *btn_next;
    __weak IBOutlet UIButton *btn_previous;
    
    __weak IBOutlet UICollectionView * collectionView;
    __weak IBOutlet UIButton *buttonQuickCheckInStatus;
    NSArray * placesArray;
}
@end


@implementation PWPHomeCell


-(void)awakeFromNib{
    [super awakeFromNib];
    [collectionView registerNib:[UINib nibWithNibName:@"PWPHomeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"PWPHomeCollectionViewCell"];
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (placesArray.count == 0) {
        return  1;
    }
    else
    {
        return placesArray.count;
    }
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)acollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    PWPHomeCollectionViewCell * cell = [acollectionView dequeueReusableCellWithReuseIdentifier:@"PWPHomeCollectionViewCell" forIndexPath:indexPath];
    
    if(placesArray.count > indexPath.row){
        NSDictionary * placeInformation = placesArray[indexPath.row];
        
        cell.parkInformation = placeInformation;
        cell.parkLabel.text = placeInformation[@"name"];
        cell.parkTimingLabel.text = placeInformation[@"operating_hours"];
        
        if([placeInformation valueForKey:@"thumbs"] && [[placeInformation valueForKey:@"thumbs"] valueForKey:@"w1000"]){
            NSString *imageUrl= [SXCUtility cleanString:[[SXCUtility cleanDictionary:placeInformation[@"thumbs"]] valueForKey:@"w1000"]];
            imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"uploads/park//" withString:@"media/cache/park_thumb/"];
            [cell.parkImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else {
            
            [cell.parkImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,placeInformation[@"image_url"]]] placeholderImage:[UIImage imageNamed:@"default_park"]];
        }
        
        cell.imageviewDog.image = [UIImage imageNamed:@"img_dog_sample"];
        cell.imageviewDog.contentMode = UIViewContentModeScaleAspectFill;
        cell.delegate = self;
        cell.place = placeInformation;
        
        UIImage * itemBg = [PWPDownloadBackgroundImage itemBG];
        if(itemBg !=nil){
            cell.imageViewAdvertisement.hidden = NO;
            cell.imageViewAdvertisement.image = itemBg;
            cell.imageViewAdvertisement.contentMode = UIViewContentModeScaleAspectFill;
        }
        else{
            cell.imageViewAdvertisement.hidden = YES;
        }

        NSString * checkInDogsCount = [SXCUtility cleanString:[[placeInformation valueForKey:@"active_dogs_count"] description]];
        cell.numberOfDogsLabel.text = [NSString stringWithFormat:@"%@ Dogs here",checkInDogsCount];
        
        NSArray * dogsArray = [SXCUtility cleanArray:[placeInformation valueForKey:@"last_active_dogs"]];
        [cell configureAccordingToCheckInDogs:dogsArray];

        
        NSInteger kTemp = [SXCUtility cleanString:placeInformation[@"weather"][@"tempKelvin"]].integerValue;
        
        CGFloat fTemp = 0.0;
        if(kTemp != 0){
            fTemp = (kTemp*9/5) - 459.67f;
            cell.labelWeatherTemp.text = [NSString stringWithFormat:@"%.1f°F",fTemp];
        }
        else{
            cell.labelWeatherTemp.text = @"";
        }
        cell.labelWeatherTitle.text = [SXCUtility cleanString:placeInformation[@"weather"][@"nicedescription"]];
        [cell.imageViewWeather sd_setImageWithURL:[NSURL URLWithString:placeInformation[@"weather"][@"iconUrl"]]];


        if(dogsArray.count == 0){
            cell.layoutConstraintPhotosTopMargin.constant = -22;
        }
        else {
            cell.layoutConstraintPhotosTopMargin.constant = 5;
        }

        [cell updateConstraints];
    }
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    return CGSizeMake(SCREEN_WIDTH, 297.0f);
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    
    if(page >= placesArray.count -1){
        btn_next.alpha = 0.2f;
        btn_next.userInteractionEnabled = false;
    }
    else {
        btn_next.alpha = 1.0f;
        btn_next.userInteractionEnabled = true;
    }
    
    if(page == 0){
        btn_previous.alpha = 0.2f;
        btn_previous.userInteractionEnabled = false;
    }
    else {
        btn_previous.alpha = 1.0f;
        btn_previous.userInteractionEnabled = true;
    }

}

- (IBAction)nextButtonClicked:(id)sender {
    
    int page = collectionView.contentOffset.x / collectionView.frame.size.width;
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:page + 1 inSection:0];
    [collectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    
    if(page >= placesArray.count -1){
        btn_previous.alpha = 0.2f;
        btn_previous.userInteractionEnabled = false;
    }
    else {
        btn_previous.alpha = 1.0f;
        btn_previous.userInteractionEnabled = true;
    }
    
    
}
- (IBAction)previousButtonClicked:(id)sender {
     int page = collectionView.contentOffset.x / collectionView.frame.size.width;
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:page - 1 inSection:0];
    [collectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    
    if(nextItem.row == 0){
        btn_next.alpha = 0.2f;
        btn_next.userInteractionEnabled = false;
    }
    else {
        btn_next.alpha = 1.0f;
        btn_next.userInteractionEnabled = true;
    }

}


-(void)configureCellWithData:(NSArray *)thePlacesArray
{

    NSString * string = [SXCUtility cleanString:[SXCUtility getNSObject:@"quickCheckinDefaultMessage"]];
    if (string.length > 0) {
        [buttonQuickCheckInStatus setTitle:string forState:UIControlStateNormal];
    }
    else {
        [buttonQuickCheckInStatus setTitle:@"Quick Check-in Status" forState:UIControlStateNormal];
    }

    buttonQuickCheckInStatus.layer.borderColor = [UIColor whiteColor].CGColor;
    buttonQuickCheckInStatus.layer.borderWidth = 1.0f;

    
    placesArray = thePlacesArray;
    [collectionView reloadData];
    [collectionView updateConstraints];
    
    int page = collectionView.contentOffset.x / collectionView.frame.size.width;
    
    
    if(page >= placesArray.count -1){
        btn_next.alpha = 0.2f;
        btn_next.userInteractionEnabled = false;
    }
    else {
        btn_next.alpha = 1.0f;
        btn_next.userInteractionEnabled = true;
    }
    
    if(page == 0){
        btn_previous.alpha = 0.2f;
        btn_previous.userInteractionEnabled = false;
    }
    else {
        btn_previous.alpha = 1.0f;
        btn_previous.userInteractionEnabled = true;
    }


}

-(void)checkInClickedWithParkDictionary:(NSDictionary*)place
{
    if(_delegate && [_delegate respondsToSelector:@selector(checkInClickedWithPark:)]){
        [_delegate checkInClickedWithPark:place];
    }
}
-(void)checkInDogsClicked:(NSDictionary *)dictionary{
    
    if(_delegate && [_delegate respondsToSelector:@selector(checkInDogsClicked:)]){
        [_delegate checkInDogsClicked:dictionary];
    }
}
-(void)quickCheckInClicked:(NSDictionary *)dictionary
{
    if(_delegate && [_delegate respondsToSelector:@selector(quickCheckInClicked:)]){
        [_delegate quickCheckInClicked:dictionary];
    }
    
}
- (IBAction)buttonQuickCheckInStatusClicked:(id)sender {

    if(_delegate && [_delegate respondsToSelector:@selector(quickCheckInStatusClicked)]){
        [_delegate quickCheckInStatusClicked];
    }

}
@end
