//
//  PWPHomeMyDogTableViewCell.h
//  PawPark
//
//  Created by daffolap19 on 5/28/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPHomeDogDelegate <NSObject>

-(void)dogDeleteClickedWithDictionary:(NSDictionary *)dogInformation;
-(void)dogEditClickedWithDictionary:(NSDictionary *)dogInformation;
-(void)dogPhotosClickedWithDictionary:(NSDictionary *)dogInformation;

@end

@interface PWPHomeMyDogTableViewCell : UITableViewCell

@property(nonatomic,weak)id<PWPHomeDogDelegate>delegate;

-(void)configueCellWithData:(NSDictionary *)dictionary;

@end
