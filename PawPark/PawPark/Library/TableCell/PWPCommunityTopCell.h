//
//  PWPCommunityTopCell.h
//  PawPark
//
//  Created by Samarth Singla on 12/08/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPCommunityTopDelegate <NSObject>
-(void)optOutClicked;
-(void)communityMembersClicked;
@end


@interface PWPCommunityTopCell : UITableViewCell
@property(nonatomic,assign)id<PWPCommunityTopDelegate>delegate;
-(void)confugureUIWithData:(NSDictionary *)data;
@end
