//
//  PWPCommunityPostCell.h
//  PawPark
//
//  Created by Samarth Singla on 14/08/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPCommunityDelegate <NSObject>

-(void)communitySelected:(NSString *)communityId;

@end


@interface PWPCommunityPostCell : UITableViewCell

@property(nonatomic,weak) id <PWPCommunityDelegate> delegate;
@property(nonatomic,strong) NSString * selectedCategory;

-(void)reloadDataUI;
@end
