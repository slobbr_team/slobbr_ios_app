//
//  PWPEventTableViewCell.m
//  PawPark
//
//  Created by xyz on 19/07/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPEventTableViewCell.h"

@interface PWPEventTableViewCell(){

    __weak IBOutlet UILabel *labelEventTitle;
    __weak IBOutlet UILabel * labelEventDescription;
    __weak IBOutlet UILabel * labelEventLocation;
    __weak IBOutlet UILabel * labelEventDate;
}
@end



@implementation PWPEventTableViewCell


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

-(void)configureWithData:(NSDictionary *)dictionary
{
    labelEventTitle.text=dictionary[@"name"];
    labelEventLocation.text=[NSString stringWithFormat:@"%@, %@, %@, %@",dictionary[@"address"],dictionary[@"address_locality"],dictionary[@"address_region"],dictionary[@"address_postal_code"]];

    labelEventDate.text=dictionary[@"time"];
    labelEventDescription.text=dictionary[@"description"];
}

@end
