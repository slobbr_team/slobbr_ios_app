//
//  PWPCommentImageCell.m
//  PawPark
//
//  Created by Samarth Singla on 11/10/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPCommentImageCell.h"

@implementation PWPCommentImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
