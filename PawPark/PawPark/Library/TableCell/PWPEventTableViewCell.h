//
//  PWPEventTableViewCell.h
//  PawPark
//
//  Created by xyz on 19/07/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPEventTableViewCell : UITableViewCell

-(void)configureWithData:(NSDictionary *)dictionary;
@end
