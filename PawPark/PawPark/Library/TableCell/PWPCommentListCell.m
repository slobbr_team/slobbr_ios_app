//
//  PWPCommentListCell.m
//  PawPark
//
//  Created by Samarth Singla on 10/09/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPCommentListCell.h"
#import "PWPConstant.h"
#import <UIImageView+WebCache.h>

#import "SXCUtility.h"


@interface PWPCommentListCell(){

    __weak IBOutlet UILabel *labelDate;
    __weak IBOutlet UILabel *labelComment;
    __weak IBOutlet UILabel *labelUser;
    __weak IBOutlet UIImageView *imageViewUser;
}
@end

@implementation PWPCommentListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureUIWithData:(NSDictionary *)comment {

    labelComment.text = [SXCUtility cleanString:[comment valueForKey:@"body"]];

    
    NSDictionary * userData = [SXCUtility cleanDictionary:[comment valueForKey:@"user"]];
    
    NSString * dateString = [SXCUtility cleanString:[comment valueForKey:@"created_at"]];
    
    NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
    [outputDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [outputDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    
    NSDate * date = [outputDateFormatter dateFromString:dateString];
    [outputDateFormatter setDateFormat:@"hh:mm a dd MMM yyyy"];
    
    labelDate.text = [outputDateFormatter stringFromDate:date];
    
    labelUser.text = [SXCUtility cleanString:[userData valueForKey:@"username"]];
    
    NSString * urlString = [SXCUtility cleanString:[[SXCUtility cleanDictionary:[userData valueForKey:@"thumbs"]] valueForKey:@"w1000"]];
    [imageViewUser sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
    
}

@end
