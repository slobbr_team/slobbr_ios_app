//
//  PWPCommunityTopCell.m
//  PawPark
//
//  Created by Samarth Singla on 12/08/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPCommunityTopCell.h"
#import "SXCUtility.h"

@interface PWPCommunityTopCell()
{
    NSDictionary * communityData;

}

@property (weak, nonatomic) IBOutlet UILabel *labelCommunitiesTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelCommunitiesBurb;
@property (weak, nonatomic) IBOutlet UIButton *buttonLink;

@end

@implementation PWPCommunityTopCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)confugureUIWithData:(NSDictionary *)data {
    
    communityData = data;
    
    self.labelCommunitiesTitle.text = [SXCUtility cleanString:[data valueForKey:@"name"]];
    self.labelCommunitiesBurb.text = [SXCUtility cleanString:[data valueForKey:@"blurb"]];
    [self.buttonLink setTitle:[SXCUtility cleanString:[data valueForKey:@"link_text"]] forState:UIControlStateNormal];
}

- (IBAction)optionButtonClicked:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[SXCUtility cleanString:[communityData valueForKey:@"link_url"]]]];
}

- (IBAction)buttonOptClicked:(id)sender {
    [self.delegate optOutClicked];
}

- (IBAction)communityMembersClicked:(id)sender {
    [self.delegate communityMembersClicked];
}
@end
