//
//  PWPCommunityPostCell.m
//  PawPark
//
//  Created by Samarth Singla on 14/08/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPCommunityPostCell.h"
#import "PWPCommunityCollectionCell.h"

@interface PWPCommunityPostCell()<UICollectionViewDelegate, UICollectionViewDataSource> {

    __weak IBOutlet UICollectionView *collectionView;
    NSArray * titles;
}


@end


@implementation PWPCommunityPostCell

- (void)awakeFromNib {
    [super awakeFromNib];

    [collectionView registerNib:[UINib nibWithNibName:@"PWPCommunityCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"PWPCommunityCollectionCell"];
    collectionView.delegate= self;
    collectionView.dataSource = self;
    
    titles = @[@"All Posts",@"Community Pictures", @"Introduction", @"Events", @"Questions", @"Community Rules", @"Official Announcement"];
    
    [collectionView reloadData];
    [collectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:true scrollPosition:UICollectionViewScrollPositionLeft];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)reloadDataUI{
    [collectionView reloadData];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)aCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    PWPCommunityCollectionCell * cell = [aCollectionView dequeueReusableCellWithReuseIdentifier:@"PWPCommunityCollectionCell" forIndexPath:indexPath];
    cell.labelTitle.text = titles[indexPath.row];

    if([titles[indexPath.row] isEqualToString:_selectedCategory]){
        cell.labelTitle.textColor = cell.labelTitle.highlightedTextColor;
    }
    else {
        cell.labelTitle.textColor = [UIColor blackColor];
    }
    
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return titles.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
   
    NSString * title = titles[indexPath.row];
    
    CGRect r = [title boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 40.0f)
                                  options:NSStringDrawingUsesLineFragmentOrigin
                               attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Roboto-Light" size:13.0f]}
                                  context:nil];
    
    return CGSizeMake(r .size.width+20.0f,40.0f);
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.delegate communitySelected:titles[indexPath.row]];
}



@end
