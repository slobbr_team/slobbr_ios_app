//
//  PWPVideoCell.m
//  PawPark
//
//  Created by xyz on 7/26/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPVideoCell.h"
#import <UIImageView+WebCache.h>

@interface PWPVideoCell(){

    __weak IBOutlet UILabel *labelVideoTitle;
    __weak IBOutlet UILabel *labelVideoDescription;
    __weak IBOutlet UIImageView *imageViewVideo;

}
@end

@implementation PWPVideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellWithData:(NSDictionary *)dictionary{

    [imageViewVideo setImage:[UIImage imageNamed:@"default_park"]];
    
    NSString * youtubeURL = dictionary[@"url"];

    NSRange range = [youtubeURL rangeOfString:@"v="];
    
    if(range.location != NSNotFound){
        NSString * youtubeID = [youtubeURL substringFromIndex:range.location+2];
        NSString * imageUrl = [NSString stringWithFormat:@"http://i.ytimg.com/vi/%@/3.jpg",youtubeID];
        
        [imageViewVideo sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"default_park"]];
    
    }
    
    labelVideoTitle.text = dictionary[@"title"];
    labelVideoDescription.text = dictionary[@"summary"];

}

@end
