//
//  PWPDogImageCell.h
//  PawPark
//
//  Created by Samarth Singla on 01/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPDogImageCellProtocoal <NSObject>

-(void)imageDeleteClicked:(NSIndexPath *)indexPath;

@end


@interface PWPDogImageCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewDog;
@property (weak, nonatomic) IBOutlet UIButton *buttonDogImageDelete;

-(void)configureViewWithDelegate:(id)theDelegate WithIndexPath:(NSIndexPath *)theIndexPath;

@end
