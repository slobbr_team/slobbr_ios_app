//
//  PWPHomeTellFriendsTableViewCell.m
//  PawPark
//
//  Created by daffomac on 4/26/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPHomeTellFriendsTableViewCell.h"

@interface PWPHomeTellFriendsTableViewCell(){

    __weak IBOutlet UIView *viewInner;
    
    __weak IBOutlet UIButton *button1;
    __weak IBOutlet UIButton *button2;
}
@property (weak, nonatomic) IBOutlet UIButton *button3;

@end


@implementation PWPHomeTellFriendsTableViewCell


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configueWithData:(id)data{
    
    viewInner.layer.borderColor=[UIColor colorWithRed:237/255.0f green:237/255.0f blue:237/255.0f alpha:1.0f].CGColor;
    
    viewInner.layer.borderWidth=2.0f;
    
    viewInner.layer.cornerRadius=2.0f;
    
    button1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    button2.imageView.contentMode = UIViewContentModeScaleAspectFit;
    _button3.imageView.contentMode = UIViewContentModeScaleAspectFit;

}


@end
