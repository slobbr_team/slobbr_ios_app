//
//  PWPNotificationDateTableViewCell.h
//  PawPark
//
//  Created by xyz on 07/06/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPNotificationDateDelegate <NSObject>

-(void)acceptButtonClickedWithDate:(NSDictionary *)dateDictonary;
-(void)declineButtonClickedWithDate:(NSDictionary *)dateDictonary;

@end

@interface PWPNotificationDateTableViewCell : UITableViewCell

@property(nonatomic,weak)id<PWPNotificationDateDelegate>delegate;

-(void)configueCellWithData:(NSDictionary *)data;

@end
