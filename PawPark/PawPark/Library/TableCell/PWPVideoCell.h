//
//  PWPVideoCell.h
//  PawPark
//
//  Created by xyz on 7/26/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPVideoCell : UITableViewCell
-(void)configureCellWithData:(NSDictionary *)dictionary;
@end
