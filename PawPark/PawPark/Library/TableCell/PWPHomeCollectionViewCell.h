//
//  PWPHomeCollectionViewCell.h
//  PawPark
//
//  Created by xyz on 7/16/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPHomeCollectionViewCellDelegate <NSObject>

-(void)checkInClickedWithParkDictionary:(NSDictionary*)place;
-(void)checkInDogsClicked:(NSDictionary *)dictionary;
-(void)quickCheckInClicked:(NSDictionary *)dictionary;

@end


@interface PWPHomeCollectionViewCell : UICollectionViewCell
{
    __weak IBOutlet NSLayoutConstraint *layoutConstraintWeatherLeftMargin;

}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintPhotosTopMargin;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewAdvertisement;
@property (weak, nonatomic) IBOutlet UIButton *buttonAlert;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintFirstDogImageViewCenter;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewDog3;
@property (weak, nonatomic) IBOutlet UILabel *labelDog3;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewDog4;
@property (weak, nonatomic) IBOutlet UILabel *labelDog4;
@property (weak, nonatomic) IBOutlet UILabel *labelDog2;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewDog2;
@property (weak, nonatomic) IBOutlet UIButton *buttonQuickCheckIn;

@property(nonatomic,weak) IBOutlet UIImageView *imageViewWeather;
@property(nonatomic,weak) IBOutlet UILabel *labelWeatherTemp;
@property(nonatomic,weak) IBOutlet UILabel *labelWeatherTitle;;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintViewDogsHeight;
@property(nonatomic,weak) IBOutlet UILabel * parkLabel;
@property(nonatomic,weak) IBOutlet UILabel * parkTimingLabel;
@property(nonatomic,weak) IBOutlet UILabel * numberOfDogsLabel;
@property(nonatomic,weak) IBOutlet UIImageView * parkImageView;
@property(nonatomic,weak) IBOutlet UILabel *labelDogName;
@property(nonatomic,weak) IBOutlet UIImageView *imageviewDog;
@property(nonatomic,strong) NSDictionary * place;

@property(nonatomic,weak)id <PWPHomeCollectionViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *viewLocationAlarm;

@property(nonatomic,strong)NSDictionary * parkInformation;

- (IBAction)checkInClicked:(id)sender;
-(void)configureAccordingToCheckInDogs:(NSArray *)checkedinDogs;
- (IBAction)quickCheckInClicked:(id)sender;
@end
