


//
//  PWPNotificationDateTableViewCell.m
//  PawPark
//
//  Created by xyz on 07/06/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPNotificationDateTableViewCell.h"
#import "SXCUtility.h"

@interface PWPNotificationDateTableViewCell(){

    __weak IBOutlet UILabel *labelDescription;
}
@property (weak, nonatomic) IBOutlet UILabel *labelNotificationTitle;
@property(strong,nonatomic)NSDictionary * dictionary;

- (IBAction)buttonDeclineClicked:(id)sender;
- (IBAction)buttonAcceptClicked:(id)sender;


@end

@implementation PWPNotificationDateTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)buttonAcceptClicked:(id)sender {
    
    [_delegate acceptButtonClickedWithDate:_dictionary];
    
}
- (IBAction)buttonDeclineClicked:(id)sender {
    
    [_delegate declineButtonClickedWithDate:_dictionary];
    
}

-(void)configueCellWithData:(NSDictionary *)data{

    _dictionary=(id)data;
    
    NSString * initiatorDogName=[SXCUtility cleanString:data[@"dog"][@"name"]];
    NSString * recipientDogName=[SXCUtility cleanString:data[@"date"][@"dog"][@"name"]];
    NSString * parkNameString=[SXCUtility cleanString:data[@"date"][@"place"][@"name"]];

    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    [df setTimeZone:[NSTimeZone defaultTimeZone]];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    
    NSDate * date=[df dateFromString:data[@"date"][@"time"]];
    [df setDateFormat:@"MM/dd/yyyy HH:mm a"];
    
    NSString * dateString=[df stringFromDate:date];

    _labelNotificationTitle.text = initiatorDogName;
    labelDescription.text=[NSString stringWithFormat:@"has %@ on a date to %@ on %@",recipientDogName,parkNameString,dateString];
    
}

@end
