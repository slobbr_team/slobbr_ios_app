//
//  PWPSearchPackCellTableViewCell.h
//  PawPark
//
//  Created by daffolap19 on 5/10/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPSearckPackDelegate <NSObject>

-(void)optionPackClickedWithData:(id)data;

@optional
-(void)option2PackClickedWithData:(id)data;
-(void)option3PackClickedWithData:(id)data;
-(void)option4PackClickedWithData:(id)data;
-(void)option5PackClickedWithData:(id)data;

@end


@interface PWPSearchPackCellTableViewCell : UITableViewCell
@property(nonatomic,weak)id<PWPSearckPackDelegate> delegate;
@property(nonatomic,assign)BOOL isFromSearch;
@property(nonatomic,assign)BOOL isFromCheckInUser;


-(void)configureCellWithData:(NSMutableDictionary *)dictionary;


@end
