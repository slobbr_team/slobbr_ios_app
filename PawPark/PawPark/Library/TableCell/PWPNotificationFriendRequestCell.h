//
//  PWPNotificationFriendRequestCell.h
//  PawPark
//
//  Created by daffolap19 on 5/30/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPNotificationDelegate <NSObject>

-(void)notificationFriendRequestCancelWithData:(NSMutableDictionary *)dictionary;
-(void)notificationFriendRequestConfirmWithData:(NSMutableDictionary *)dictionary;

@end


@interface PWPNotificationFriendRequestCell : UITableViewCell

@property(nonatomic,weak)id<PWPNotificationDelegate> delegate;

-(void)configureCellWithData:(NSDictionary *)notificationDictionary;
@end
