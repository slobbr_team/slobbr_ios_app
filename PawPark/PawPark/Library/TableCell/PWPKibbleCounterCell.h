//
//  PWPKibbleCounterCell.h
//  PawPark
//
//  Created by Samarth Singla on 14/09/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPKibbleCounterCell : UITableViewCell

-(void)configureViewWithData:(NSDictionary *)user withIndexPath:(NSIndexPath *)indexPath;


@end
