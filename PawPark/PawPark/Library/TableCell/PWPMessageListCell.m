//
//  PWPMessageListCell.m
//  PawPark
//
//  Created by daffolap19 on 6/1/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPMessageListCell.h"
#import "SXCUtility.h"

@interface PWPMessageListCell()
@property (weak, nonatomic) IBOutlet UIImageView *imageViewDog;

@property (weak, nonatomic) IBOutlet UILabel *labelDogName;
@property (weak, nonatomic) IBOutlet UILabel *labelMessgaeDate;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;

@end

@implementation PWPMessageListCell



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}


-(void)configureCellWithData:(NSDictionary *)messgae{
    
    NSMutableDictionary * intiatorDictionary=[messgae valueForKey:@"initiator"];
    NSMutableDictionary * recipientDictionary=[messgae valueForKey:@"recipient"];
    
    _labelDogName.text=[NSString stringWithFormat:@"%@ & %@",[SXCUtility cleanString:[intiatorDictionary valueForKey:@"name"]],[SXCUtility cleanString:[recipientDictionary valueForKey:@"name"]]];
  
    if([SXCUtility cleanArray:messgae[@"messages"]].count>0){
        _labelMessage.text=[[[messgae[@"messages"] lastObject][@"body"] stringByReplacingOccurrencesOfString:@"[" withString:@""] stringByReplacingOccurrencesOfString:@"]" withString:@""];
        
        NSDateFormatter* df = [[NSDateFormatter alloc]init];
        [df setTimeZone:[NSTimeZone defaultTimeZone]];
        [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        
        NSDate * date=[df dateFromString:messgae[@"messages"][0][@"created_at"]];
        [df setDateFormat:@"dd MMM, HH:mm"];
        
        NSString * dateString=[df stringFromDate:date];
        _labelMessgaeDate.text=dateString;
    }
}

@end
