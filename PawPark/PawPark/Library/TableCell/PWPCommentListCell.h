//
//  PWPCommentListCell.h
//  PawPark
//
//  Created by Samarth Singla on 10/09/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPCommentListCell : UITableViewCell
-(void)configureUIWithData:(NSDictionary *)comment;
@end
