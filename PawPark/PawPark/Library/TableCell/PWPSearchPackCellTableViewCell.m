
//
//  PWPSearchPackCellTableViewCell.m
//  PawPark
//
//  Created by daffolap19 on 5/10/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPSearchPackCellTableViewCell.h"
#import "SXCUtility.h"
#import "PWPApplicationUtility.h"
#import <UIImageView+WebCache.h>
#import "PWPConstant.h"
#import "PWPApplicationUtility.h"
#import "UIColor+PWPColor.h"

@interface PWPSearchPackCellTableViewCell()
{
    NSDictionary * requestDictionary;

    UITapGestureRecognizer * tapGesture1;
    __weak IBOutlet UIButton *buttonEdit;
    __weak IBOutlet UIButton *buttonDelete;
    __weak IBOutlet UIButton *buttonPhotos;
    
    __weak IBOutlet UIButton *sendFriendRequestButton;
    __weak IBOutlet UILabel *labelPackBreed;
    __weak IBOutlet UIButton *buttonFriends;
    __weak IBOutlet UIButton *buttonAdopt;

    __weak IBOutlet UIButton *buttonStar;
    __weak IBOutlet UILabel *labelPackAge;
    __weak IBOutlet UIImageView *imageViewPack;
    __weak IBOutlet UILabel *labelPackName;

    __weak IBOutlet NSLayoutConstraint *layoutConstraintFriendButonWidth;
    __weak IBOutlet UILabel *labelSizeDetail;
    __weak IBOutlet UILabel *labelActivityLevelDetail;
    __weak IBOutlet UILabel *labelSociabilityDetail;
    __weak IBOutlet NSLayoutConstraint *layoutConstraintAdobtButtonWidth;
    __weak IBOutlet UILabel *labelDogBioInfo;
    __weak IBOutlet UIView *viewContent;
    __weak IBOutlet UILabel *labelLastCheckin;
}
- (IBAction)optionButtonClicked:(id)sender;
@end

@implementation PWPSearchPackCellTableViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    viewContent.layer.cornerRadius = 5.0f;
    imageViewPack.layer.cornerRadius =  30.0f;

    [buttonEdit setBackgroundImage:[self imageFromColor:[UIColor sxc_themeColor]] forState:UIControlStateNormal];
    [buttonDelete setBackgroundImage:[self imageFromColor:[UIColor sxc_themeColor]] forState:UIControlStateNormal];
    [buttonPhotos setBackgroundImage:[self imageFromColor:[UIColor sxc_themeColor]] forState:UIControlStateNormal];

    [buttonEdit setBackgroundImage:[self imageFromColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];
    [buttonDelete setBackgroundImage:[self imageFromColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];
    [buttonPhotos setBackgroundImage:[self imageFromColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}



-(void)configureCellWithData:(NSMutableDictionary *)dictionary
{
    requestDictionary=dictionary;
    
    NSMutableString * lastCheckinStatus =[[NSMutableString alloc] init];
    
    if([dictionary valueForKey:@"message"]){
        [lastCheckinStatus appendFormat:@"%@\n\n",[dictionary valueForKey:@"message"]];
    }

    
    if([dictionary valueForKey:@"last_checkin"] && [[dictionary valueForKey:@"last_checkin"] isKindOfClass:[NSDictionary class]])
    {
        NSDictionary * lastCheckInDictionary = [dictionary valueForKey:@"last_checkin"];
        
        NSString * checkInTime =lastCheckInDictionary[@"created_at"];
        
        NSDateFormatter* df = [[NSDateFormatter alloc]init];
        [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];

        NSDate * date=[df dateFromString:checkInTime];
        [df setDateFormat:@"MM/dd/yyyy HH:mma"];
        
        NSString * dateString=[df stringFromDate:date];
        
        [lastCheckinStatus appendFormat:@"Last check-in was at %@ %@",lastCheckInDictionary[@"place"][@"name"],dateString];
        
    }
    
    labelLastCheckin.numberOfLines=0;
    
    if(lastCheckinStatus.length==0){
        labelLastCheckin.text=@"";
    }
    else{
        
        labelLastCheckin.text=lastCheckinStatus;
    }
    
    imageViewPack.image=[UIImage imageNamed:@"img_dog_sample"];

    if (dictionary[@"image_url"] != nil){
        [imageViewPack sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,dictionary[@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
    }
    else if(dictionary[@"gallery"]!=nil && [SXCUtility cleanArray:dictionary[@"gallery"]].count > 0){

        NSDictionary * dogImageDictionary =[SXCUtility cleanArray:dictionary[@"gallery"]][0];
        NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
        [imageViewPack sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
    }
    else{
        [imageViewPack setImage:[UIImage imageNamed:@"img_dog_sample"]];
    }

    
    labelPackAge.text=[NSString stringWithFormat:@"%@ years old",[SXCUtility cleanString:[[dictionary valueForKey:@"age"] description]]];
    labelPackName.text=[SXCUtility cleanString:[dictionary valueForKey:@"name"]].capitalizedString;
    
    if([dictionary valueForKey:@"breed"]&&[[dictionary valueForKey:@"breed"] isKindOfClass:[NSDictionary class]]){
        labelPackBreed.text=[SXCUtility cleanString:[dictionary valueForKey:@"breed"][@"name"]].capitalizedString;
    }
    else if([dictionary valueForKey:@"breed"]&&[[dictionary valueForKey:@"breed"] isKindOfClass:[NSString class]]){
        labelPackBreed.text=[SXCUtility cleanString:[dictionary valueForKey:@"breed"]].capitalizedString;
    }
    else{
        labelPackBreed.text=@"";
    }
  
    
    labelSociabilityDetail.text=[PWPApplicationUtility pawSociability][[SXCUtility cleanString:[dictionary valueForKey:@"sociability"]].integerValue];
    labelSizeDetail.text=[PWPApplicationUtility pawSizesCorrespondingToID:[SXCUtility cleanString:[dictionary valueForKey:@"size"]]];
    labelActivityLevelDetail.text=[PWPApplicationUtility pawActivityLevel][[SXCUtility cleanString:[dictionary valueForKey:@"activityLevel"]].integerValue];
    
    
    if(([[dictionary valueForKey:@"adoptable"] description].boolValue)&&([PWPApplicationUtility isEventMode])){
       
        layoutConstraintAdobtButtonWidth.constant=75.0f;
        
        if([[PWPApplicationUtility currentUserDogsIds] containsObject:[dictionary valueForKey:@"id"]]){
            buttonAdopt.hidden=YES;
        }
        else{
            buttonAdopt.hidden=NO;
        }
        
        if(_isFromCheckInUser){
            buttonStar.hidden=NO;
        }
        else{
            buttonStar.hidden=YES;
        }

    }
    else{
        layoutConstraintAdobtButtonWidth.constant=0.0f;
        buttonAdopt.hidden=YES;
        buttonStar.hidden=YES;
    }
    
    
    if([[PWPApplicationUtility currentUserDogsIds] containsObject:[dictionary valueForKey:@"id"]]){
        layoutConstraintFriendButonWidth.constant=0;
    }
    else{
        layoutConstraintFriendButonWidth.constant=40;
    }
    
    
    if([SXCUtility cleanArray:dictionary[@"friends"]].count>0){
        buttonFriends.hidden=NO;
    }
    else{
        buttonFriends.hidden=YES;
    }
    
    labelDogBioInfo.text = [SXCUtility cleanString:[dictionary valueForKey:@"special_needs"]];
    
    [self updateConstraintsIfNeeded];
    [self layoutIfNeeded];
    
}
- (IBAction)option2ButtonClicked:(id)sender {
    if(_delegate&&[_delegate respondsToSelector:@selector(option2PackClickedWithData:)]){
        [_delegate option2PackClickedWithData:requestDictionary];
    }
}
- (IBAction)option3ButtonClicked:(id)sender {
    if(_delegate&&[_delegate respondsToSelector:@selector(option3PackClickedWithData:)]){
        [_delegate option3PackClickedWithData:requestDictionary];
    }
}

- (IBAction)optionButtonClicked:(id)sender {
    
    if(_delegate&&[_delegate respondsToSelector:@selector(optionPackClickedWithData:)]){
        [_delegate optionPackClickedWithData:requestDictionary];
    }
}
- (IBAction)option4ButtonClicked:(id)sender {
    if(_delegate&&[_delegate respondsToSelector:@selector(option4PackClickedWithData:)]){
        [_delegate option4PackClickedWithData:requestDictionary];
    }

}

- (IBAction)option5ButtonClicked:(id)sender {
    if(_delegate&&[_delegate respondsToSelector:@selector(option5PackClickedWithData:)]){
        [_delegate option5PackClickedWithData:requestDictionary];
    }
}
@end
