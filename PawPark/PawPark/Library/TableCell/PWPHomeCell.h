//
//  PWPHomeCollectionViewCell.h
//  PawPark
//
//  Created by xyz on 7/16/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPHomeCellDelegate <NSObject>

-(void)checkInClickedWithPark:(NSDictionary *)parkDictionary;
-(void)checkInDogsClicked:(NSDictionary *)dictionary;
-(void)quickCheckInClicked:(NSDictionary *)dictionary;

-(void)quickCheckInStatusClicked;


@end

@interface PWPHomeCell : UITableViewCell

@property(nonatomic,weak)id <PWPHomeCellDelegate> delegate;
-(void)configureCellWithData:(NSArray *)thePlacesArray;

@end
