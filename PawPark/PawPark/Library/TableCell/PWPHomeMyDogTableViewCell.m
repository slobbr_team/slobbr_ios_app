//
//  PWPHomeMyDogTableViewCell.m
//  PawPark
//
//  Created by daffolap19 on 5/28/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPHomeMyDogTableViewCell.h"
#import "SXCUtility.h"
#import "PWPConstant.h"
#import <UIImageView+WebCache.h>
#import "UIColor+PWPColor.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPHomeMyDogTableViewCell(){

    __weak IBOutlet UIImageView *imageViewAdvertisement;
    __weak IBOutlet UIImageView *imageViewDog;
    __weak IBOutlet UILabel *labelDogName;
    __weak IBOutlet UILabel *labelDogAge;
    __weak IBOutlet UILabel *labelDogBreedName;
    __weak IBOutlet UILabel *labelHeader;
    
    __weak IBOutlet UIButton *btnPhotos;
    __weak IBOutlet UIButton *btnEdit;
    __weak IBOutlet UIButton *btnDelete;
    NSDictionary * dogDictionary;

}
-(IBAction)dogDeletClicked:(id)sender;
-(IBAction)dogEditClicked:(id)sender;

@end


@implementation PWPHomeMyDogTableViewCell

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [btnEdit setBackgroundImage:[self imageFromColor:[UIColor sxc_themeColor]] forState:UIControlStateNormal];
    [btnDelete setBackgroundImage:[self imageFromColor:[UIColor sxc_themeColor]] forState:UIControlStateNormal];
    [btnPhotos setBackgroundImage:[self imageFromColor:[UIColor sxc_themeColor]] forState:UIControlStateNormal];

    [btnEdit setBackgroundImage:[self imageFromColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];
    [btnDelete setBackgroundImage:[self imageFromColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];
    [btnPhotos setBackgroundImage:[self imageFromColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];

    [self.contentView bringSubviewToFront:labelHeader];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)configueCellWithData:(NSDictionary *)dictionary{
    
    UIImage * advertisementImage = [PWPDownloadBackgroundImage itemBG];
    if(advertisementImage != nil){
        imageViewAdvertisement.image =  advertisementImage;
        imageViewAdvertisement.hidden = false;
    }
    else{
        imageViewAdvertisement.hidden = true;
    }
    
   /* btnDelete.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    btnDelete.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    btnDelete.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    
    btnEdit.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    btnEdit.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    btnEdit.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    */
    
    dogDictionary=dictionary;

    if (dictionary[@"image_url"] != nil){
        [imageViewDog sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,dictionary[@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
    }
    else if(dictionary[@"gallery"]!=nil && [SXCUtility cleanArray:dictionary[@"gallery"]].count > 0){

        NSDictionary * dogImageDictionary =[SXCUtility cleanArray:dictionary[@"gallery"]][0];
        NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
        [imageViewDog sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
    }
    else{
        [imageViewDog setImage:[UIImage imageNamed:@"img_dog_sample"]];
    }


    labelDogAge.text=[NSString stringWithFormat:@"%@ years old",[SXCUtility cleanString:[[dictionary valueForKey:@"age"] description]]];
    
    labelDogName.text=[SXCUtility cleanString:[dictionary valueForKey:@"name"]].capitalizedString;

    labelDogBreedName.text=[SXCUtility cleanString:[dictionary valueForKey:@"breed"]].capitalizedString;

    imageViewDog.layer.cornerRadius = 30.0f;
    imageViewDog.clipsToBounds = YES;
}


-(void)dogDeletClicked:(id)sender
{
    [_delegate dogDeleteClickedWithDictionary:dogDictionary];
}

-(void)dogEditClicked:(id)sender
{
    [_delegate dogEditClickedWithDictionary:dogDictionary];
}

- (IBAction)dogPhotosClicked:(id)sender {
    [_delegate dogPhotosClickedWithDictionary:dogDictionary];

}
@end
