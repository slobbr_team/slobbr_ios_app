//
//  PWPHomeTellFriendsTableViewCell.h
//  PawPark
//
//  Created by daffomac on 4/26/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPHomeTellFriendsTableViewCell : UITableViewCell

-(void)configueWithData:(id)data;

@end
