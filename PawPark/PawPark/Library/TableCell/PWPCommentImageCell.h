//
//  PWPCommentImageCell.h
//  PawPark
//
//  Created by Samarth Singla on 11/10/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPCommentImageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewComment;

@end
