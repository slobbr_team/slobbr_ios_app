

//
//  PWPHomeCollectionViewCell.m
//  PawPark
//
//  Created by xyz on 7/16/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPHomeCollectionViewCell.h"
#import <UIImageView+WebCache.h>
#import "PWPConstant.h"
#import "PWPImageInfoViewController.h"
#import "SXCUtility.h"
#import "PWPApplicationUtility.h"
#import <Mixpanel.h>

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width

@implementation PWPHomeCollectionViewCell

-(void)awakeFromNib {

    [super awakeFromNib];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0,  [UIScreen mainScreen].bounds.size.width,403-91);
    
    
    // Add colors to layer
    UIColor *centerColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.10];
    UIColor *center2Color = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.80];
    UIColor *topColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.01];
    UIColor *endColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.95];
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[topColor CGColor],
                       (id)[centerColor CGColor],
                       (id)[center2Color CGColor],
                       (id)[endColor CGColor],
                       nil];
    self.parkImageView.layer.name = @"Gradient";
    for (CALayer *layer in self.parkImageView.layer.sublayers) {
        [layer removeFromSuperlayer];
    }
    [self.parkImageView.layer insertSublayer:gradient atIndex:0];
}



- (IBAction)fetchAddPlaces:(id)sender {
}

- (IBAction)checkInClicked:(id)sender {
    
    if(_delegate && [_delegate respondsToSelector:@selector(checkInClickedWithParkDictionary:)]){
        [_delegate checkInClickedWithParkDictionary:_place];
    }
}
- (IBAction)numberOfDogsClicked:(id)sender {


    if(_delegate && [_delegate respondsToSelector:@selector(checkInDogsClicked:)]){
        [_delegate checkInDogsClicked:_place];
    }
}
- (IBAction)infoBUttonClicked:(id)sender {

    [[Mixpanel sharedInstance] track:@"Info Clicked Home Screen"];

    UIStoryboard * storyBoard =[UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    
    PWPImageInfoViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPImageInfoViewController"];
    viewController.dictionary = self.parkInformation;
    
    UINavigationController *navigationController =[[UINavigationController alloc] initWithRootViewController:viewController];
    
    navigationController.navigationBar.translucent=YES;
    
    [self.window.rootViewController presentViewController:navigationController animated:YES completion:nil];
    
}

-(void)configureAccordingToCheckInDogs:(NSArray *)checkedinDogs{


    _buttonAlert.selected = [PWPApplicationUtility isAlertPark:_parkInformation];

    if(_buttonAlert.selected) {
        _viewLocationAlarm.alpha = 0.2f;
    }
    else {
        _viewLocationAlarm.alpha = 1.0f;
    }
    
    layoutConstraintWeatherLeftMargin.constant = SCREEN_WIDTH/2.0 - 87/2.0;
    self.buttonQuickCheckIn.hidden = false;



    _layoutConstraintFirstDogImageViewCenter.constant = -39.0f;
    
    _imageviewDog.layer.borderColor = [UIColor colorWithRed:44/255.0f green:166/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    _imageviewDog2.layer.borderColor = [UIColor colorWithRed:44/255.0f green:166/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    _imageviewDog3.layer.borderColor = [UIColor colorWithRed:44/255.0f green:166/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    _imageviewDog4.layer.borderColor = [UIColor colorWithRed:44/255.0f green:166/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    
    _imageviewDog.layer.borderWidth = 1.0f;
    _imageviewDog2.layer.borderWidth = 1.0f;
    _imageviewDog3.layer.borderWidth = 1.0f;
    _imageviewDog4.layer.borderWidth = 1.0f;
    if(checkedinDogs.count==0){
        
        _layoutConstraintViewDogsHeight.constant = 0.0f;
        
        _labelDogName.hidden=YES;
        _labelDog2.hidden=YES;
        _labelDog3.hidden=YES;
        _labelDog4.hidden=YES;
        
        _imageviewDog.hidden=YES;
        _imageviewDog2.hidden=YES;
        _imageviewDog3.hidden=YES;
        _imageviewDog4.hidden=YES;
    }
    else if(checkedinDogs.count==1){
        
        _layoutConstraintViewDogsHeight.constant = 45.0f;
        
        _layoutConstraintFirstDogImageViewCenter.constant = -0.0f;
        _labelDogName.hidden=NO;
        _labelDog2.hidden=YES;
        _labelDog3.hidden=YES;
        _labelDog4.hidden=YES;
        
        _imageviewDog.hidden=NO;
        _imageviewDog2.hidden=YES;
        _imageviewDog3.hidden=YES;
        _imageviewDog4.hidden=YES;
        
        _labelDogName.text = checkedinDogs[0][@"name"];



        if (checkedinDogs[0][@"image_url"] != nil){
            [_imageviewDog sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[0][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[0][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[0][@"gallery"]].count > 0){

            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[0][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [_imageviewDog sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [_imageviewDog setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        


    }
    else if(checkedinDogs.count==2){
        _layoutConstraintViewDogsHeight.constant = 45.0f;
        _labelDogName.hidden=NO;
        _labelDog2.hidden=NO;
        _labelDog3.hidden=YES;
        _labelDog4.hidden=YES;
        
        _imageviewDog.hidden=NO;
        _imageviewDog2.hidden=NO;
        _imageviewDog3.hidden=YES;
        _imageviewDog4.hidden=YES;
        
        _labelDogName.text = checkedinDogs[0][@"name"];
        _labelDog2.text = checkedinDogs[1][@"name"];


        if (checkedinDogs[0][@"image_url"] != nil){
            [_imageviewDog sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[0][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[0][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[0][@"gallery"]].count > 0){

            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[0][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w1000"]];
            [_imageviewDog sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [_imageviewDog setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }

        if (checkedinDogs[1][@"image_url"] != nil){
            [_imageviewDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[1][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[1][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[1][@"gallery"]].count > 0){

            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[1][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [_imageviewDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [_imageviewDog2 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }


    }
    else if(checkedinDogs.count==3){
        _layoutConstraintViewDogsHeight.constant = 45.0f;
        _labelDogName.hidden=NO;
        _labelDog2.hidden=NO;
        _labelDog3.hidden=NO;
        _labelDog4.hidden=YES;
        
        _imageviewDog.hidden=NO;
        _imageviewDog2.hidden=NO;
        _imageviewDog3.hidden=NO;
        _imageviewDog4.hidden=YES;
        
        _labelDogName.text = checkedinDogs[0][@"name"];
        _labelDog2.text = checkedinDogs[1][@"name"];
        _labelDog3.text = checkedinDogs[2][@"name"];

        if (checkedinDogs[0][@"image_url"] != nil){
            [_imageviewDog sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[0][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[0][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[0][@"gallery"]].count > 0){

            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[0][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [_imageviewDog sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [_imageviewDog setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }

        if (checkedinDogs[1][@"image_url"] != nil){
            [_imageviewDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[1][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[1][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[1][@"gallery"]].count > 0){

            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[1][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [_imageviewDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [_imageviewDog2 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        if (checkedinDogs[2][@"image_url"] != nil){
            [_imageviewDog3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[2][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[2][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[2][@"gallery"]].count > 0){

            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[2][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [_imageviewDog3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [_imageviewDog3 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }

    }
    else if(checkedinDogs.count>=4){
        
        _layoutConstraintViewDogsHeight.constant = 45.0f;
        
        _labelDogName.hidden=NO;
        _labelDog2.hidden=NO;
        _labelDog3.hidden=NO;
        _labelDog4.hidden=NO;
        
        _imageviewDog.hidden=NO;
        _imageviewDog2.hidden=NO;
        _imageviewDog3.hidden=NO;
        _imageviewDog4.hidden=NO;
        
        _labelDogName.text = checkedinDogs[0][@"name"];
        _labelDog2.text = checkedinDogs[1][@"name"];
        _labelDog3.text = checkedinDogs[2][@"name"];
        _labelDog4.text = checkedinDogs[3][@"name"];

        if (checkedinDogs[0][@"image_url"] != nil){
            [_imageviewDog sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[0][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[0][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[0][@"gallery"]].count > 0){

            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[0][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [_imageviewDog sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [_imageviewDog setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }

        if (checkedinDogs[1][@"image_url"] != nil){
            [_imageviewDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[1][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[1][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[1][@"gallery"]].count > 0){

            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[1][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [_imageviewDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [_imageviewDog2 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }

        if (checkedinDogs[2][@"image_url"] != nil){
            [_imageviewDog3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[2][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[2][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[2][@"gallery"]].count > 0){

            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[2][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [_imageviewDog3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [_imageviewDog3 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }

        if (checkedinDogs[3][@"image_url"] != nil){
            [_imageviewDog4 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[3][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[3][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[3][@"gallery"]].count > 0){

            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[3][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [_imageviewDog4 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [_imageviewDog4 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
    }




}

- (IBAction)quickCheckInClicked:(id)sender {
    
    if(_delegate && [_delegate respondsToSelector:@selector(quickCheckInClicked:)]){
        [[Mixpanel sharedInstance] track:@"Quick CheckIn Clicked Home Screen"];

        [_delegate quickCheckInClicked:_place];
    }

}

- (IBAction)alertButtonClicked:(id)sender {

    [[Mixpanel sharedInstance] track:@"Alert Clicked Home Screen"];

    _buttonAlert.selected  = !_buttonAlert.selected;
    
    if(_buttonAlert.selected) {
        _viewLocationAlarm.alpha = 0.2f;
    }
    else {
        _viewLocationAlarm.alpha = 1.0f;
    }

    if(_buttonAlert.selected){
        [PWPApplicationUtility addToAlertParks:_parkInformation];
    }
    else{
        [PWPApplicationUtility removeAlertPark:_parkInformation];
    }
}

+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

@end
