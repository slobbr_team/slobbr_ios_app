//
//  PWPAmentitiesCell.h
//  PawPark
//
//  Created by Samarth Singla on 30/08/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPAmentitiesCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewAmenities;

@end
