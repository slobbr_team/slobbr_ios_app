//
//  PWPSearchParkTableViewCell.h
//  PawPark
//
//  Created by daffolap19 on 5/23/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPParkCellDelegate <NSObject>

-(void)parkOptionClickedWithData:(NSDictionary *)dictionary;
-(void)parkOption2ClickedWithData:(NSDictionary *)dictionary;
-(void)parkOption3ClickedWithData:(NSDictionary *)dictionary;
-(void)addPlaceClickedWithData:(NSDictionary *)dictionary;
-(void)inviteOptionClicked:(NSDictionary *)dictionary;
-(void)parkQuickCheckInClicked:(NSDictionary *)dictionary;

@optional
-(void)staOptionClickedWithData:(NSDictionary *)dictionary;

@end


@interface PWPSearchParkTableViewCell : UITableViewCell

-(void)bindValueWithComponentsWithData:(NSMutableDictionary *)data reloadData:(BOOL)reloadCollectionView;

@property(nonatomic,weak)id<PWPParkCellDelegate> delegate;

@end
