//
//  PWPHomeParkTableViewCell.h
//  PawPark
//
//  Created by daffomac on 4/26/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPHomeParkDelegate <NSObject>

-(void)settingClicked;
-(void)checkInClicked:(NSDictionary *)dictionary;
-(void)checkInDogsClicked:(NSDictionary *)dictionary;
-(void)useMyLocationClicked;
@end


@interface PWPHomeParkTableViewCell : UITableViewCell

@property(nonatomic,weak)id<PWPHomeParkDelegate> delegate;

-(void)configueWithData:(id)data;

@end
