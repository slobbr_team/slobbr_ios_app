//
//  PWPMessageListCell.h
//  PawPark
//
//  Created by daffolap19 on 6/1/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPMessageListCell : UITableViewCell


-(void)configureCellWithData:(NSDictionary *)messgae;

@end
