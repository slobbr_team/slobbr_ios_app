//
//  PWPKibbleCounterCell.m
//  PawPark
//
//  Created by Samarth Singla on 14/09/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPKibbleCounterCell.h"
#import "SXCUtility.h"
#import <UIImageView+WebCache.h>
#import "PWPConstant.h"
#import "PWPApplicationUtility.h"

@interface PWPKibbleCounterCell(){

    __weak IBOutlet NSLayoutConstraint *layoutConstraintLeftMargin;
    __weak IBOutlet NSLayoutConstraint *layoutConstraintRightMargin;
    __weak IBOutlet UILabel *labelCheckIn;
    __weak IBOutlet UILabel *labelName;
    __weak IBOutlet UIImageView *imageViewProfile;
    __weak IBOutlet UILabel *labelRank;
    __weak IBOutlet UIImageView *imageViewBg;
}
@end


@implementation PWPKibbleCounterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    imageViewProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    imageViewProfile.layer.borderWidth = 2.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureViewWithData:(NSDictionary *)user withIndexPath:(NSIndexPath *)indexPath
{
    
    if([SXCUtility cleanInt:user[@"id"]] == [SXCUtility cleanInt:[PWPApplicationUtility getCurrentUserProfileDictionary][@"id"]]){
        layoutConstraintLeftMargin.constant = 5;
        layoutConstraintRightMargin.constant = 5;
    }
    else{
        layoutConstraintLeftMargin.constant = 20;
        layoutConstraintRightMargin.constant = 20;
    }
    
    if( indexPath.row % 2 == 0){
        imageViewBg.image = [[UIImage imageNamed:@"bg_kibble_counter_orange"] stretchableImageWithLeftCapWidth:40 topCapHeight:40];
        labelName.textColor = [UIColor colorWithRed:246/255.0f green:192/255.0f blue:46/255.0f alpha:1.0f];
        labelRank.textColor = [UIColor colorWithRed:246/255.0f green:192/255.0f blue:46/255.0f alpha:1.0f];
    }
    else{
        imageViewBg.image = [[UIImage imageNamed:@"bg_kibble_counter_blue"] stretchableImageWithLeftCapWidth:40 topCapHeight:40];
        labelName.textColor = [UIColor colorWithRed:0/255.0f green:137/255.0f blue:173/255.0f alpha:1.0f];
        labelRank.textColor = [UIColor colorWithRed:0/255.0f green:137/255.0f blue:173/255.0f alpha:1.0f];
    }

    
    [imageViewProfile setImage:[UIImage imageNamed:@"img_dog_sample"]];
    
    NSArray * dogs = [SXCUtility cleanArray:[user valueForKeyPath:@"dogs"]];
    if(dogs.count >0){
        NSArray * imageUrls = [dogs valueForKeyPath:@"image_url"];
        
        if(imageUrls.count>0){
            [imageViewProfile sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,imageUrls[0]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
    }

    labelName.text =[SXCUtility cleanString:[user valueForKey:@"username"]];
    labelCheckIn.text = [NSString stringWithFormat:@"%d check-in(s)",[SXCUtility cleanInt:[user valueForKey:@"checkin_count"]]];
    labelRank.text = [NSString stringWithFormat:@"#%d",[SXCUtility cleanInt:[user valueForKey:@"checkin_position"]]];

}


@end
