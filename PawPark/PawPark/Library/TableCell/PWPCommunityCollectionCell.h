//
//  PWPCommunityCollectionCell.h
//  PawPark
//
//  Created by Samarth Singla on 14/08/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPCommunityCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@end
