//
//  PWPHomeSponsoringCell.m
//  PawPark
//
//  Created by daffolap19 on 5/30/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPHomeSponsoringCell.h"
#import <UIImageView+WebCache.h>
#import "PWPConstant.h"

@interface PWPHomeSponsoringCell()

@property (weak, nonatomic) IBOutlet UILabel *_labelSponsor;
@property (weak, nonatomic) IBOutlet UIImageView *_imageViewSponsor;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewSponsoree;
@end


@implementation PWPHomeSponsoringCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)configureDateWithImage:(NSString *)aImageUrl WithName:(NSString *)name WithSponsoreeImage:(NSString *)aSponsoreeImageUrl WithSponsoreeName:(NSString *)aSponsoreeName
{
    
    NSString * imageUrl=[[NSString alloc] initWithFormat:@"%@%@",K_URL_LIVE_HOSTNAME,aImageUrl];
    
    imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"uploads/park//" withString:@"media/cache/park_thumb/"];
    imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    
    NSString * sponsoreeImageUrl=[[NSString alloc] initWithFormat:@"%@%@",K_URL_LIVE_HOSTNAME,aSponsoreeImageUrl];
    
    sponsoreeImageUrl=[sponsoreeImageUrl stringByReplacingOccurrencesOfString:@"uploads/park//" withString:@"media/cache/park_thumb/"];
    sponsoreeImageUrl=[sponsoreeImageUrl stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    sponsoreeImageUrl=[sponsoreeImageUrl stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    
    
    
    [__imageViewSponsor sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"default_park"]];
    
    [_imageViewSponsoree sd_setImageWithURL:[NSURL URLWithString:sponsoreeImageUrl] placeholderImage:[UIImage imageNamed:@"default_park"]];
    
    [__labelSponsor setText:[NSString stringWithFormat:@"Every time you check-in on Slobbr, we donate a half-cup of kibble to %@ courtesy of %@! Helping a pup in need is just one click away, so don't forget to check-in!",aSponsoreeName,name]];

}

@end
