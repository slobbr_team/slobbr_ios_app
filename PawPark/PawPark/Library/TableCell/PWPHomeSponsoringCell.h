//
//  PWPHomeSponsoringCell.h
//  PawPark
//
//  Created by daffolap19 on 5/30/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPHomeSponsoringCell : UITableViewCell

-(void)configureDateWithImage:(NSString *)aImageUrl WithName:(NSString *)name WithSponsoreeImage:(NSString *)aSponsoreeImageUrl WithSponsoreeName:(NSString *)aSponsoreeName;

@end
