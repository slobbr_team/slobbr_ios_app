//
//  PWPMessageThreadTableViewCell.m
//  PawPark
//
//  Created by daffolap19 on 6/2/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPMessageThreadTableViewCell.h"
#import "PWPApplicationUtility.h"
#import "SXCUtility.h"
#import "UIColor+PWPColor.h"
#import "SXCLabelForDynamicHeight.h"

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width


@interface PWPMessageThreadTableViewCell()

@property(nonatomic,weak)IBOutlet SXCLabelForDynamicHeight * labelMessageBody;
@property(nonatomic,weak)IBOutlet SXCLabelForDynamicHeight * labelMessageTime;
@property (weak, nonatomic) IBOutlet SXCLabelForDynamicHeight *labelMessageTitle;
@end


@implementation PWPMessageThreadTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configueCellWithData:(NSDictionary *)dictionary{
    
    self.labelMessageBody.preferredMaxLayoutWidth = SCREEN_WIDTH - 148;

    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    
    [df setTimeZone:[NSTimeZone defaultTimeZone]];
    
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    
    NSDate * date=[df dateFromString:dictionary[@"created_at"]];
    
    [df setDateFormat:@"HH:mm"];
    
    NSString * dateString=[df stringFromDate:date];
    [_labelMessageTime setText:dateString];
    
    
    [_labelMessageBody setText:dictionary[@"body"] ];
    _labelMessageBody.clipsToBounds=YES;
    
    NSMutableDictionary * intiatorDictionary=[dictionary valueForKey:@"initiator"];
        
    [_labelMessageTitle setText:[NSString stringWithFormat:@"%@",[SXCUtility cleanString:[intiatorDictionary valueForKey:@"name"]]]];
    
}
@end
