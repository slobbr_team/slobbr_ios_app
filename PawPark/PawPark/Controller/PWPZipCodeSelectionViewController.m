//
//  PWPZipCodeSelectionViewController.m
//  PawPark
//
//  Created by xyz on 21/06/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPZipCodeSelectionViewController.h"
#import "SXCUtility.h"
#import "UIColor+PWPColor.h"
#import "UIViewController+UBRComponents.h"
#import <Google/Analytics.h>
#import "PWPDownloadBackgroundImage.h"

@interface PWPZipCodeSelectionViewController ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    __weak IBOutlet UIImageView * imageViewBG;
}

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property(nonatomic,weak)IBOutlet UITableView * tableView;
@property(nonatomic,strong)NSMutableArray * zipCodeArray;
@property(nonatomic,strong)NSMutableArray * searchedZipCodeArray;

@end

@implementation PWPZipCodeSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self sxc_customizeBackButton];
    [self sxc_navigationBarCustomizationWithTitle:@"Select ZipCode"];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ZipCodeCell"];
  
    _zipCodeArray=[[SXCUtility cleanArray:[SXCUtility getNSObject:@"newyorkzipcode"]] mutableCopy];
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"value"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [_zipCodeArray sortedArrayUsingDescriptors:sortDescriptors];
    
    _zipCodeArray=[sortedArray copy];
    
    _searchedZipCodeArray=[_zipCodeArray copy];
    
    for (UIView *subview in [[_searchBar.subviews lastObject] subviews]) {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [subview removeFromSuperview];
            break;
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ZipCode Selection Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegate and Datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"ZipCodeCell"];
    cell.backgroundColor =[UIColor clearColor];
    cell.contentView.backgroundColor =[UIColor clearColor];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font =[UIFont fontWithName:@"Roboto-Regular" size:14.0f];
    cell.textLabel.text=[_searchedZipCodeArray objectAtIndex:indexPath.row][@"title"];
    
    UIView *customColorView = [[UIView alloc] init];
    customColorView.backgroundColor = [UIColor sxc_themeColorWithAplha];
    cell.selectedBackgroundView =  customColorView;

    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _searchedZipCodeArray.count;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_parkDictionary!=nil){
        NSMutableDictionary * zipCodeDictionary=[_searchedZipCodeArray objectAtIndex:indexPath.row];
        [_parkDictionary setValue:zipCodeDictionary[@"value"] forKey:@"zipcode"];
        [_selectZipCodeTitle setString:zipCodeDictionary[@"title"]];
        [_parkDictionary removeObjectForKey:@"lng"];
        [_parkDictionary removeObjectForKey:@"lat"];
    }
    
    if(_delegate &&[_delegate respondsToSelector:@selector(zipCodeSelected:)]){
        [_delegate zipCodeSelected:_searchedZipCodeArray[indexPath.row]];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.view endEditing:YES];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{

    if(searchText.length==0){
        _searchedZipCodeArray=[_zipCodeArray copy];
    }
    else{
        
        NSPredicate * predicate=[NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@ OR value CONTAINS[cd] %@",searchText,searchText];
        
        _searchedZipCodeArray=(id)[_zipCodeArray filteredArrayUsingPredicate:predicate];
    }
    [_tableView reloadData];

}
@end
