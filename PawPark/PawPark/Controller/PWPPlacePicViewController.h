//
//  PWPPlacePicViewController.h
//  PawPark
//
//  Created by Samarth Singla on 19/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPPlacePicViewControllerDelegate <NSObject>

-(void)placeProfileUpdated;

@end

@interface PWPPlacePicViewController : UIViewController

@property(strong,nonatomic)NSMutableArray * placePics;
@property (weak, nonatomic) IBOutlet UILabel *placeName;
@property(strong,nonatomic)NSDictionary * data;
@property(strong,nonatomic)NSString * placeId;
@property(assign,nonatomic)id<PWPPlacePicViewControllerDelegate> delegate;
@property(assign, nonatomic)BOOL isGalleryOpen;
@end
