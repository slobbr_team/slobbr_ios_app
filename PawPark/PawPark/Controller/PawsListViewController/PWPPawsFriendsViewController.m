//
//  PWPPawsFriendsViewController.m
//  PawPark
//
//  Created by xyz on 07/06/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPPawsFriendsViewController.h"
#import "PWPSearchPackCellTableViewCell.h"
#import "UIViewController+UBRComponents.h"
#import "PWPService.h"
#import "PWPApplicationUtility.h"
#import "UIColor+PWPColor.h"
#import <Google/Analytics.h>
#import "PWPDownloadBackgroundImage.h"
#import "PWPDogPicViewController.h"
#import "SXCUtility.h"

@interface PWPPawsFriendsViewController ()<PWPSearckPackDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSInteger selectedIndexPath;
    NSDictionary * deletePackDictionary;
    
    NSArray * dogsIDArray;

    __weak IBOutlet UIImageView * imageViewBG;
}
@property(nonatomic,weak)IBOutlet UITableView * tableView;

@end

@implementation PWPPawsFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];    
    selectedIndexPath=-1;
    
    _tableView.delegate=self;
    _tableView.dataSource=self;
    
    [self sxc_navigationBarCustomizationWithTitle:[NSString stringWithFormat:@"Friends of %@",_friendsNameString]];
    [self sxc_customizeBackButton];

    dogsIDArray = [_friendDogsArray valueForKeyPath:@"id"];
    [self profilePicturePath];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Dog Friends Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- TableView Delegate Methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PWPSearchPackCellTableViewCell * tableViewCell =[tableView dequeueReusableCellWithIdentifier:@"PWPMyPawsCells"];
    [tableViewCell configureCellWithData:_friendDogsArray[indexPath.row]];
    tableViewCell.delegate=self;
    tableViewCell.isFromSearch=NO;
    tableView.clipsToBounds=YES;
    
    return tableViewCell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _friendDogsArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary * kuttaDictionary = _friendDogsArray[indexPath.row];
    
    if([kuttaDictionary valueForKey:@"last_checkin"] && [[kuttaDictionary valueForKey:@"last_checkin"] isKindOfClass:[NSDictionary class]] && indexPath.row==selectedIndexPath)
    {
        return 208.0f;
    }
    else if(indexPath.row==selectedIndexPath){
        return 165.0f;
    }
    return 95.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row!=selectedIndexPath){
        selectedIndexPath=indexPath.row;
    }
    else{
        selectedIndexPath=-1;
    }
    
    [tableView reloadRowsAtIndexPaths:[tableView indexPathsForVisibleRows]
                     withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Delegate Methods
-(void)optionPackClickedWithData:(id)data
{
    _friendsDogID=data[@"id"];

    [self startLoader];
    [[PWPService sharedInstance] friendRequestsDeclineWithIntiatorString:_dogID withReciepientString:_friendsDogID withSuccessBlock:^(id response) {
        [self dismissLoader];
        
        if(_friendDogsArray.count==1){
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            NSMutableArray * dogsArray=[[NSMutableArray alloc] initWithArray:_friendDogsArray];
            [dogsArray removeObject:deletePackDictionary];
         
            _friendDogsArray=[NSMutableArray arrayWithArray:dogsArray];
            
            [_tableView reloadData];
            deletePackDictionary=nil;
        }
        
        [self sxc_showSuccessWithMessage:@"Friend has been deleted successfully."];
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        [self sxc_showErrorViewWithMessage:error.domain];
    }];

}

-(void)option5PackClickedWithData:(id)data
{
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    PWPDogPicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPDogPicViewController"];
    viewController.dogsPics = [SXCUtility cleanArray:[data valueForKey:@"gallery"]].mutableCopy;
    viewController.data = data;
    viewController.dogId = data[@"id"];

    [self.navigationController pushViewController:viewController animated:YES];
}
-(void)profilePicturePath
{
    dispatch_group_t group = dispatch_group_create();

    for(NSString * dogsID in dogsIDArray){
        
        dispatch_group_enter(group);
        [[PWPService sharedInstance] dogProfileWithDogId:dogsID WithSuccessBlock:^(id response, NSString *dogID) {
            
            NSPredicate * predicate =[NSPredicate predicateWithFormat:@"id =%@",dogID];
            NSArray * filteredArray = [_friendDogsArray filteredArrayUsingPredicate:predicate];
            if(filteredArray.count>0){
               
                NSMutableDictionary * dogDictionary = [filteredArray[0] mutableCopy];
                [dogDictionary setValue:response[@"image_url"] forKey:@"image_url"];
                
                NSInteger index =[_friendDogsArray indexOfObject:filteredArray[0]];
                [_friendDogsArray replaceObjectAtIndex:index withObject:dogDictionary];
            }
            
            dispatch_group_leave(group);
  
        } withErrorBlock:^(NSError *error, NSString *dogID) {
            dispatch_group_leave(group);
        }];
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [_tableView reloadData];
    });
    
}
@end
