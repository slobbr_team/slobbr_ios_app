//
//  PWPPawsListViewController.m
//  PawPark
//
//  Created by daffomac on 4/22/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPPawsListViewController.h"
#import "UIViewController+UBRComponents.h"
#import "PWPService.h"
#import "PWPApplicationUtility.h"
#import "UIColor+PWPColor.h"
#import "PWPSearchPackCellTableViewCell.h"
#import "PWPAddDogViewController.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPPawsFriendsViewController.h"
#import <Google/Analytics.h>
#import "PWPDogPicViewController.h"
#import "SXCUtility.h"
#import <Mixpanel.h>

@interface PWPPawsListViewController()<UITableViewDataSource,UITableViewDelegate,PWPSearckPackDelegate,UIAlertViewDelegate,PWPAddDogViewDelegate, PWPDogPicViewControllerDelegate>{

    __weak IBOutlet UITableView * tableViewPaws;
    __weak IBOutlet UIImageView * imageViewBG;
    __weak IBOutlet UILabel *noDataLabel;
    
    NSMutableArray * currentUserPacks;
    NSDictionary * deletePackDictionary;


    NSInteger selectedIndexPath;
}

@property(nonatomic,strong)UIRefreshControl * refreshControl;
@end


@implementation PWPPawsListViewController


-(void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];
    [self addPullToRefresh];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self automaticallyRefresh];
    [self userProfile];
  
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Pack Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark- Buisness Methods

-(void)showNoDataLabel
{
    if(currentUserPacks.count==0){
        noDataLabel.hidden=NO;
        noDataLabel.text=@"You have no dogs in your pack.  Use \"+\" to add a dog to your pack";
        [self.view bringSubviewToFront:noDataLabel];
    }
    else{
        noDataLabel.hidden=YES;
        [self.view bringSubviewToFront:noDataLabel];
        
    }
    
}

-(void)addPullToRefresh
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor=[UIColor sxc_themeColor];
    [self.refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    [tableViewPaws addSubview:self.refreshControl];
}
- (void)refreshView:(UIRefreshControl *)sender {
    [self userProfile];
}

-(void)automaticallyRefresh{
    [tableViewPaws setContentOffset:CGPointMake(0, -self.refreshControl.frame.size.height) animated:YES];
    [self.refreshControl beginRefreshing];
}

-(void)initView
{
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    tableViewPaws.delegate=self;
    tableViewPaws.dataSource=self;
    [tableViewPaws reloadData];
    
    [self sxc_navigationBarCustomizationWithTitle:@"YOUR PROFILE"];
    [self sxc_customizeRightNavigationBarMenuItem];
    //[self sxc_setNavigationBarMenuItem];
    [self sxc_setNavigationBarCrossItem];
    selectedIndexPath=-1;
}

#pragma mark - Services Method
-(void)userProfile{
    
    [[PWPService sharedInstance] currentUserProfileWithRequestDictionary:nil withSuccessBlock:^(id responseDicitionary) {
        
        [PWPApplicationUtility saveCurrentUserProfileDictionary:responseDicitionary];
        currentUserPacks=[[NSMutableArray alloc] initWithArray:[PWPApplicationUtility getCurrentUserDogsArray]];
        [tableViewPaws reloadData];
        

        [self showNoDataLabel];
        
        [self dismissLoader];
        [self.refreshControl endRefreshing];
        
    } withErrorBlock:^(NSError *error) {

        [self sxc_showErrorViewWithMessage:error.domain];

        [self dismissLoader];
        [self.refreshControl endRefreshing];
        
        [self showNoDataLabel];

    }];

}

-(void)deletePackWithId:(NSString *)packIdString
{
    [self startLoader];
    [[PWPService sharedInstance] deleteDogWithDogId:packIdString withSuccessBlock:^(id responseDicitionary) {
        
        [self dismissLoaderWithSuccessText:@"Dog has been deleted successfully"];
        [currentUserPacks removeObject:deletePackDictionary];
        [tableViewPaws reloadData];
        deletePackDictionary=nil;
        
        [self showNoDataLabel];

        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        [self sxc_showErrorViewWithMessage:error.domain];
        deletePackDictionary=nil;
    }];

}

#pragma mark- TableView Delegate Methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PWPSearchPackCellTableViewCell * tableViewCell =[tableView dequeueReusableCellWithIdentifier:@"PWPMyPawsCells"];
    [tableViewCell configureCellWithData:currentUserPacks[indexPath.row]];
    tableViewCell.delegate=self;
    tableViewCell.isFromSearch=NO;
    tableView.clipsToBounds=YES;
    
    return tableViewCell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return currentUserPacks.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary * kuttaDictionary = currentUserPacks[indexPath.row];
    
    if([kuttaDictionary valueForKey:@"last_checkin"] && [[kuttaDictionary valueForKey:@"last_checkin"] isKindOfClass:[NSDictionary class]] && indexPath.row==selectedIndexPath)
    {
        return 243.0f;
    }
    else if(indexPath.row==selectedIndexPath){
        return 200.0f;
    }
    return 130.0f;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[Mixpanel sharedInstance] track:@"Dog Cell Clicked On Profile"];

    if(indexPath.row!=selectedIndexPath){
        selectedIndexPath=indexPath.row;
    }
    else{
        selectedIndexPath=-1;
    }
    
    [tableView reloadRowsAtIndexPaths:[tableView indexPathsForVisibleRows]
                     withRowAnimation:UITableViewRowAnimationAutomatic];
}



#pragma mark - Navigation Methods
-(void)rightMenuButtonClicked:(id)sender{

    [[Mixpanel sharedInstance] track:@"Add Dog Clicked On Profile"];

    [self performSegueWithIdentifier:@"segue_add_pawk" sender:nil];
}

#pragma mark - PWPPAWList Delegate

-(void)optionPackClickedWithData:(id)data{

    [[Mixpanel sharedInstance] track:@"Delete Dog Clicked On Profile"];

    deletePackDictionary=data;
    NSString *message = [NSString stringWithFormat:@"Do you want to delete %@",deletePackDictionary[@"name"]];
    [self sxc_showAlertViewWithTitle:@"Delete Dog" message:message WithDelegate:self WithCancelButtonTitle:@"No" WithAcceptButtonTitle:@"Yes"];
}



-(void)option2PackClickedWithData:(id)data
{
    [[Mixpanel sharedInstance] track:@"Edit Dog Clicked On Profile"];

    PWPAddDogViewController * addDogViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"PWPAddDogViewController"];
    addDogViewController.dogDictionary=data;
    addDogViewController.delegate=self;
    [self.navigationController pushViewController:addDogViewController animated:YES];
}




-(void)option3PackClickedWithData:(id)data
{
    [[Mixpanel sharedInstance] track:@"Dog Friends Clicked On Profile"];

    NSDictionary * dictionary=(NSDictionary *)data;
    
    PWPPawsFriendsViewController* pawFriendViewContorller=[self.storyboard instantiateViewControllerWithIdentifier:@"PWPPawsFriendsViewController"];
    pawFriendViewContorller.friendDogsArray=[dictionary[@"friends"] mutableCopy];
    pawFriendViewContorller.friendsNameString=dictionary[@"name"];
    pawFriendViewContorller.dogID=[dictionary[@"id"] description];
    [self.navigationController pushViewController:pawFriendViewContorller animated:YES];
}

-(void)option5PackClickedWithData:(id)data
{
    [[Mixpanel sharedInstance] track:@"Photos Clicked On Profile"];

    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    PWPDogPicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPDogPicViewController"];
    viewController.dogsPics = [SXCUtility cleanArray:[data valueForKey:@"gallery"]].mutableCopy;
    viewController.data = data;
    viewController.dogId = data[@"id"];
    viewController.delegate = self;

    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)dogAdded
{
    [self automaticallyRefresh];
    [self userProfile];
}

-(void)dogProfileUpdated
{
    [self automaticallyRefresh];
    [self userProfile];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1){
        [self deletePackWithId:deletePackDictionary[@"id"]];
    }
}


@end
