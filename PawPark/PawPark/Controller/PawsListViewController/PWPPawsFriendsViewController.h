//
//  PWPPawsFriendsViewController.h
//  PawPark
//
//  Created by xyz on 07/06/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPBaseViewController.h"

@interface PWPPawsFriendsViewController : PWPBaseViewController

@property(nonatomic,strong)NSMutableArray * friendDogsArray;
@property(nonatomic,strong)NSString * friendsNameString;
@property(nonatomic,strong)NSString * friendsDogID;
@property(nonatomic,strong)NSString * dogID;

@end
