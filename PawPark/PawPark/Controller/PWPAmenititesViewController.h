//
//  PWPAmenititesViewController.h
//  PawPark
//
//  Created by Samarth Singla on 18/09/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPAmenititesViewController : UIViewController
@property(nonatomic,strong)NSMutableArray * amenities;
@property(nonatomic,strong)NSMutableArray * selectedAmenities;
@property(nonatomic,strong)NSString * categoryName;

@end
