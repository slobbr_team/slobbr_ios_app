//
//  PWPKibbleCounterViewController.m
//  PawPark
//
//  Created by xyz on 21/06/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPKibbleCounterViewController.h"
#import "PWPService.h"
#import "SXCUtility.h"
#import "PWPConstant.h"
#import "PWPKibbleCounterCell.h"
#import "PWPDownloadBackgroundImage.h"
#import "UIViewController+UBRComponents.h"
#import <Google/Analytics.h>
#import <UIImageView+WebCache.h>
#import <Mixpanel.h>

@interface PWPKibbleCounterViewController ()<UITableViewDelegate, UITableViewDataSource>{

    __weak IBOutlet UILabel *labelNoData;
    __weak IBOutlet UILabel *labelTitle;

    __weak IBOutlet UIImageView *imageViewBAckground;
    NSArray * users;
    __weak IBOutlet UIImageView *imageViewSponsoree;
    __weak IBOutlet UIImageView *imageViewTopSponsor;

    NSString * sponsorImage;
    NSString * sponsorName;
    NSString * sponsorUrl;

    NSString * aSponsoreeImage;
    NSString * aSponsoreeName;
    NSString * aSponsoreeUrl;

    NSMutableSet * errorSet;

}
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewSponsor;

@end

#define K_NO_CHECKIN @"Don't forget to check-in! Every time you do, we donate a half-cup of kibble to a rescue/shelter in your region courtesy of BogoBowl!"


@implementation PWPKibbleCounterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self sxc_customizeBackButton];
    [self sxc_setNavigationBarMenuItem];
    [self sxc_navigationBarCustomizationWithTitle:@"Kibble Counter"];

    errorSet = [[NSMutableSet alloc] init];

    UITapGestureRecognizer * tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sponsoreClicked:)];
    [imageViewTopSponsor addGestureRecognizer:tapGesture1];

    UITapGestureRecognizer * tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sponsoreeClicked:)];
    [imageViewSponsoree addGestureRecognizer:tapGesture2];

    labelNoData.hidden = true;
}




-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Kibble Counter Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

    NSString * kibbleCounterImageUrl = [SXCUtility cleanString:[SXCUtility getNSObject:@"kibble_counter_image_url"]];
    if(kibbleCounterImageUrl.length > 0){
        [imageViewBAckground sd_setImageWithURL:[NSURL URLWithString:kibbleCounterImageUrl] placeholderImage: [PWPDownloadBackgroundImage backgroundImage]];
    }
    else{
        imageViewBAckground.image = [PWPDownloadBackgroundImage backgroundImage];
    }


    [self kibblerCounter];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    PWPKibbleCounterCell * kibbleCounter =  [tableView dequeueReusableCellWithIdentifier:@"PWPKibbleCounterCell"];
    [kibbleCounter configureViewWithData:users[indexPath.row] withIndexPath:indexPath];
    return kibbleCounter;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return users.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

-(void)kibblerCounter
{
    [super showLoader];

    [[PWPService sharedInstance] getAllCheckInWithSuccessBlock:^(id response) {

        for (NSDictionary * dictionary in response) {
        
            if([[dictionary valueForKey:@"request"] isEqualToString:@"/apis/v1/settings"]) {
            
                NSData *objectData = [[dictionary valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSDictionary *settingResponse = [NSJSONSerialization JSONObjectWithData:objectData
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:&jsonError];

                
                sponsorImage = [settingResponse valueForKey:@"sponsor.image"];
                sponsorName = [settingResponse valueForKey:@"sponsor.name"];
                sponsorUrl = [settingResponse valueForKey:@"sponsor.url"];
                
                aSponsoreeImage = [settingResponse valueForKey:@"sponsoree.image"];
                aSponsoreeName = [settingResponse valueForKey:@"sponsoree.name"];
                aSponsoreeUrl = [settingResponse valueForKey:@"sponsoree.url"];
                
                NSString * imageUrl=[[NSString alloc] initWithFormat:@"%@%@",K_URL_LIVE_HOSTNAME,[settingResponse valueForKey:@"sponsor.ldbd.image"]];
                
                imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"uploads/park//" withString:@"media/cache/park_thumb/"];
                imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
                
                [_imageViewSponsor sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"default_park"]];
 
                
                imageUrl=[[NSString alloc] initWithFormat:@"%@%@",K_URL_LIVE_HOSTNAME,sponsorImage];
                
                imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"uploads/park//" withString:@"media/cache/park_thumb/"];
                imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
                
                NSString * sponsoreeImageUrl=[[NSString alloc] initWithFormat:@"%@%@",K_URL_LIVE_HOSTNAME,aSponsoreeImage];
                
                sponsoreeImageUrl=[sponsoreeImageUrl stringByReplacingOccurrencesOfString:@"uploads/park//" withString:@"media/cache/park_thumb/"];
                sponsoreeImageUrl=[sponsoreeImageUrl stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                sponsoreeImageUrl=[sponsoreeImageUrl stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
                
                
                
                [imageViewTopSponsor sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"default_park"]];
                
                [imageViewSponsoree sd_setImageWithURL:[NSURL URLWithString:sponsoreeImageUrl] placeholderImage:[UIImage imageNamed:@"default_park"]];
                
                [labelTitle setText:[NSString stringWithFormat:@"Every time you check-in on Slobbr, we donate a half-cup of kibble to %@ courtesy of %@! Helping a pup in need is just one click away, so don't forget to check-in!",aSponsoreeName,sponsorName]];
                
                
    
            }
            else {
                
                NSData *objectData = [[dictionary valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSDictionary *countersResponse = [NSJSONSerialization JSONObjectWithData:objectData
                                                                                options:NSJSONReadingMutableContainers
                                                                                  error:&jsonError];

                
            
                users = [SXCUtility cleanArray:[countersResponse valueForKey:@"data"]];

            
                if (users.count == 0) {
                    labelNoData.hidden = false;
                    _tableview.hidden = true;
                }
                else {
                    labelNoData.hidden = true;
                    _tableview.hidden = false;
                }
                
                _label.text = [NSString stringWithFormat:@"Total Dogs Fed: %d",[SXCUtility cleanInt:[countersResponse valueForKey:@"total"]]];
            }
        }

        [self hideLoader];
        [_tableview reloadData];

    } withErrorBlock:^(NSError *error) {

        [self sxc_showErrorViewWithMessage:error.domain];

        if (users.count == 0) {
            labelNoData.hidden = false;
            _tableview.hidden = true;
        }
        else {
            labelNoData.hidden = true;
            _tableview.hidden = false;
        }
        [self hideLoader];

    }];
}


//-(void)services
//{
//    [super showLoader];
//
//    errorSet = [[NSMutableSet alloc] init];
//
//    dispatch_group_t group = dispatch_group_create();
//    
//
//    dispatch_group_enter(group);
//    [self kibblerCounter:group];
//
//    dispatch_group_enter(group);
//    [self sponsorImage:group];
//
//    dispatch_group_enter(group);
//    [self sponsorDetailsWithDispatch:group];
//
//    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
//
//        NSString * imageUrl=[[NSString alloc] initWithFormat:@"%@%@",K_URL_LIVE_HOSTNAME,sponsorImage];
//
//        imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"uploads/park//" withString:@"media/cache/park_thumb/"];
//        imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"\"" withString:@""];
//        imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
//
//        NSString * sponsoreeImageUrl=[[NSString alloc] initWithFormat:@"%@%@",K_URL_LIVE_HOSTNAME,aSponsoreeImage];
//
//        sponsoreeImageUrl=[sponsoreeImageUrl stringByReplacingOccurrencesOfString:@"uploads/park//" withString:@"media/cache/park_thumb/"];
//        sponsoreeImageUrl=[sponsoreeImageUrl stringByReplacingOccurrencesOfString:@"\"" withString:@""];
//        sponsoreeImageUrl=[sponsoreeImageUrl stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
//
//
//
//        [imageViewTopSponsor sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"default_park"]];
//
//        [imageViewSponsoree sd_setImageWithURL:[NSURL URLWithString:sponsoreeImageUrl] placeholderImage:[UIImage imageNamed:@"default_park"]];
//
//        [labelTitle setText:[NSString stringWithFormat:@"Every time you check-in on Slobbr, we donate a half-cup of kibble to %@ courtesy of %@! Helping a pup in need is just one click away, so don't forget to check-in!",aSponsoreeName,sponsorName]];
//        
//        
//        [self hideLoader];
//
//        if(errorSet.allObjects.count > 0){
//            NSString * errorString = [errorSet.allObjects componentsJoinedByString:@"\n"];
//            [self sxc_showErrorViewWithMessage:errorString];
//        }
//    });
//    
//}

-(void)sponsoreClicked:(UITapGestureRecognizer *)tapGesture {

    [[Mixpanel sharedInstance] track:@"Sponsor logo Clicked"];


    NSString * sponsorUrlString = sponsorUrl;

    sponsorUrlString=[sponsorUrlString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    sponsorUrlString=[sponsorUrlString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];

    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:sponsorUrlString]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sponsorUrlString]];
    }
    
}

-(void)sponsoreeClicked:(UITapGestureRecognizer *)tapGesture {

    [[Mixpanel sharedInstance] track:@"Sponsoree logo Clicked"];

    NSString * sponsoreeUrlString = aSponsoreeUrl;

    sponsoreeUrlString=[sponsoreeUrlString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    sponsoreeUrlString=[sponsoreeUrlString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    

    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:sponsoreeUrlString]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sponsoreeUrlString]];
    }
}

@end
