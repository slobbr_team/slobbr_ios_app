//
//  PWPItemSelectionViewController.h
//  PawPark
//
//  Created by Samarth Singla on 22/01/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    PWPSelectionTypeCity,
    PWPSelectionTypeState,
    PWPSelectionTypeZipCode

} PWPSelectionType;

@protocol PWPCitySelectionDelegate <NSObject>

@optional
-(void)itemSelected:(NSString *)selectedCity type:(PWPSelectionType)type;
-(void)selectedCity:(NSString *)selectedCity selectedState:(NSString *)selectedState selectedStateCode:(NSString *)selectedStateCode selectedZipCode:(NSString *)selectedZipCode type:(PWPSelectionType)type selectedString:(NSString *)selectedString;
@end

@interface PWPItemSelectionViewController : UIViewController

@property(nonatomic,weak)id <PWPCitySelectionDelegate> delegate;

@property(nonatomic,strong)NSString * selectedItem;
@property(nonatomic,assign)PWPSelectionType selectedType;
@property(nonatomic,strong)NSString * selectedCity;
@property(nonatomic,strong)NSString * selectedCountry;
@property(nonatomic,strong)NSString * selectedState;
@property(nonatomic,strong)NSString * selectedStateCode;
@property(nonatomic,strong)NSString * selectedZipCode;

@end
