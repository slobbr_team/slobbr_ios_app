//
//  PWPInvalidZipCodeViewController.m
//  PawPark
//
//  Created by Vibhore Sharma on 28/07/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPInvalidZipCodeViewController.h"
#import "UIViewController+UBRComponents.h"

#import "PWPDownloadBackgroundImage.h"

@interface PWPInvalidZipCodeViewController ()
{
    __weak IBOutlet UIImageView * imageViewBG;
}
@end

@implementation PWPInvalidZipCodeViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self sxc_navigationBarCustomizationWithTitle:@"Slobbr Home"];
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
