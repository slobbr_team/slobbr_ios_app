//
//  PWPSplashViewController.m
//  PawPark
//
//  Created by daffolap19 on 5/18/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPSplashViewController.h"
#import "PWPService.h"
#import "SXCUtility.h"
#import <AFNetworking.h>
#import "PWPAppDelegate.h"
#import "SCGIFImageView.h"
#import "SSZipArchive.h"
#import "PWPDownloadBackgroundImage.h"
#import "NSDate+SXCDate.h"
#import "PWPMapPinHelper.h"

@interface PWPSplashViewController ()
{
    __weak IBOutlet UILabel *loadingLabel;
    __weak IBOutlet UIImageView *imageViewLogo;
    __weak IBOutlet UIImageView * imageViewBG;
    __weak IBOutlet UIProgressView *progressBar;
    __weak IBOutlet UILabel *labelProgress;

    NSInteger failedServices;

    NSInteger totalServices;
    NSInteger doneServices;
    
    BOOL isServiceInProgress;
}

@property (weak, nonatomic) IBOutlet SCGIFImageView *imageViewLoader;

@end

@implementation PWPSplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];

  
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
  
    if(isServiceInProgress == false) {
        
        totalServices = 1;
        doneServices = 1;
        
        labelProgress.text = @"0%";
        progressBar.progress = 0.0f;
        
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        
        NSString* filePath = [[NSBundle mainBundle] pathForResource:@"loader-dog.gif" ofType:nil];
        NSData* imageData = [NSData dataWithContentsOfFile:filePath];
        [_imageViewLoader setData:imageData];
        
        imageViewBG.image = [PWPDownloadBackgroundImage splashImage];
        
        if([[[SXCUtility getNSObject:@"BackgroundMode"] description] boolValue]  == true){
            imageViewLogo.hidden = true;
        }
        else{
            imageViewLogo.hidden = false;
        }
        [self amenities];
        [self newYorkZipCodesWithGroup:nil];
        
        
        [self services];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)services {
    
    isServiceInProgress = true;
    
    failedServices = 0;

    dispatch_group_t group = dispatch_group_create();

    
    NSMutableArray * urls = [[NSMutableArray alloc] init];

    BOOL isMasterDataRequired = [self isMasterRequired];

    if(isMasterDataRequired){
        [urls addObject:@"/apis/v1/breeds"];
        [urls addObject:@"/apis/v1/place-categories"];
        [urls addObject:@"/apis/v1/place-markers"];
        
        dispatch_group_enter(group);
        totalServices += 1;
        [self statesCityZipcodesWithGroup:group];
    }
    
    [urls addObject:@"/apis/v1/settings"];

    dispatch_group_enter(group);
    totalServices += 1;
    [self batchRequestForSplash:group url:urls];
    
    if([self isSplashRequired] == true){
        dispatch_group_enter(group);
        totalServices += 1;
        [self updateProgressBar];
        [self skin:group];
    }

    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
        isServiceInProgress = false;
        
        if(isMasterDataRequired && failedServices < 5){
            [SXCUtility saveNSObject:[NSDate date] forKey:@"Last_Saved_Date"];
        }
        [self updateProgressBar];
        [self moveToNextViewController];
    });
}

-(void)batchRequestForSplash:(dispatch_group_t) group url:(NSArray *)urls {

    
    [[PWPService sharedInstance] splashBatchRequestWithURL:urls SuccessBlock:^(id responseDicitionary) {
        
        
        for (NSDictionary * dictionary in responseDicitionary) {
            
            if([[dictionary valueForKey:@"request"] isEqualToString:@"/apis/v1/breeds"]) {
                
                NSData *objectData = [[dictionary valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSDictionary *actualResponse = [NSJSONSerialization JSONObjectWithData:objectData
                                                                               options:NSJSONReadingMutableContainers
                                                                                 error:&jsonError];
                [self dogBreedsWithGroup:actualResponse];

                
            }
             if([[dictionary valueForKey:@"request"] isEqualToString:@"/apis/v1/place-categories"]) {
                
                NSData *objectData = [[dictionary valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSDictionary *actualResponse = [NSJSONSerialization JSONObjectWithData:objectData
                                                                               options:NSJSONReadingMutableContainers
                                                                                 error:&jsonError];
                [self parksCategoryWithGroup:actualResponse];
                
            }
            else  if([[dictionary valueForKey:@"request"] isEqualToString:@"/apis/v1/place-markers"]) {
                
                NSData *objectData = [[dictionary valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSArray *actualResponse = [NSJSONSerialization JSONObjectWithData:objectData
                                                                               options:NSJSONReadingMutableContainers
                                                                                 error:&jsonError];
                dispatch_group_enter(group);
                
                totalServices += 1;
                [self updateProgressBar];
                [self downloadLogos:group response:actualResponse];
                
                
            }
            else  if([[dictionary valueForKey:@"request"] isEqualToString:@"/apis/v1/settings"]) {
                
                NSData *objectData = [[dictionary valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSDictionary *actualResponse = [NSJSONSerialization JSONObjectWithData:objectData
                                                                          options:NSJSONReadingMutableContainers
                                                                            error:&jsonError];
                
                [self eventModeWithGroup:actualResponse] ;
                
            }
        }
        doneServices += 1;
        [self updateProgressBar];
        dispatch_group_leave(group);
        
        
    } withErrorBlock:^(NSError *error) {
        
        failedServices += 1;
        doneServices += 1;
        [self updateProgressBar];
        
        dispatch_group_leave(group);
    }];

}



-(void)moveToNextViewController {
    if([SXCUtility isNotNullString:[SXCUtility getNSObject:@"apikey"]]){
        PWPAppDelegate * appdelagte=[PWPAppDelegate sharedInstance];
        [appdelagte switchToHomeViewControllerWithRegistration:NO];
    }
    else{
        [self performSegueWithIdentifier:@"segue_splash" sender:nil];
    }
}


-(BOOL)isMasterRequired
{
    NSDate * lastySavedDate = (NSDate *)[SXCUtility getNSObject:@"Last_Saved_Date"];
    NSDate * currentDate = [NSDate date];
    
    NSInteger hoursDifference = [lastySavedDate hoursBeforeDate:currentDate];
    
    
    if(([SXCUtility isNotNull:lastySavedDate] && hoursDifference >= 24)||(lastySavedDate ==  nil)){
        return YES;
    }

    return NO;
    
}

-(BOOL)isSplashRequired
{
    NSDate * lastySavedDate = (NSDate *)[SXCUtility getNSObject:@"Last_Saved_Date_Splash"];
    NSDate * currentDate = [NSDate date];

    NSInteger hoursDifference = [lastySavedDate hoursBeforeDate:currentDate];

    if(([SXCUtility isNotNull:[SXCUtility getNSObject:@"apikey"]]) &&
       (([SXCUtility isNotNull:lastySavedDate] && hoursDifference >= 24)||
       (lastySavedDate ==  nil))) {
        return YES;
    }

    return NO;
}

#pragma mark - Services Methods
-(void)dogBreedsWithGroup:(NSDictionary *)response{
    [SXCUtility saveNSObject:response forKey:@"BreedList"];
}

#pragma mark - Services Methods
-(void)newYorkZipCodesWithGroup:(NSDictionary *)response{
    
    NSString * path =  [[NSBundle mainBundle] pathForResource:@"archive" ofType:@"dat"];
    NSMutableArray *theArray = [NSKeyedUnarchiver unarchiveObjectWithFile:path];

    [SXCUtility saveNSObject:theArray forKey:@"newyorkzipcode"];
}

-(void)statesCityZipcodesWithGroup:(dispatch_group_t) group {
    
    if([SXCUtility cleanArray:[SXCUtility getNSObject:@"newZipcodes"]].count > 0){
        doneServices += 1;
        [self updateProgressBar];
        dispatch_group_leave(group);
        return;
    }
    
    NSString * path =  [[NSBundle mainBundle] pathForResource:@"zipcodes" ofType:@"dat"];
    NSArray * array = [[NSArray alloc] initWithContentsOfFile:path];
    
    [SXCUtility saveNSObject:array forKey:@"newZipcodes"];
    dispatch_group_leave(group);
}

-(void)eventModeWithGroup:(NSDictionary *)response {
    [SXCUtility saveNSObject:[response valueForKey:@"event.mode"] forKey:@"eventMode"];
}

-(void)parksCategoryWithGroup:(NSDictionary *)response {
    
    NSArray * catgeories = [SXCUtility cleanArray:response];
    NSMutableArray * categoryArray = [[NSMutableArray alloc] init];
    
    for(NSDictionary * category in catgeories){
        [categoryArray addObject:category];
    }
    
    [SXCUtility saveNSObject:categoryArray forKey:@"parkCategory"];
}

-(void)amenities {
    if(![SXCUtility getNSObject:@"Amenities"] || ![SXCUtility isNotNull:[SXCUtility getNSObject:@"isAmenitiesLoaded"]]) {
        NSDictionary *dictRoot = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Amenities" ofType:@"plist"]];
        [SXCUtility saveNSObject:dictRoot forKey:@"Amenities"];
        [SXCUtility saveNSObject:@true forKey:@"isAmenitiesLoaded"];
    }
}

-(void)downloadSplashImage:(NSString *)stringPath group:(dispatch_group_t)group{

    NSString * urlString = [NSString stringWithFormat:@"http://www.slobbr.com%@",stringPath];
    urlString = [urlString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"ThemeImages.zip"];
    
    NSLog(@"%@",path);
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self unzipFile:path group:group];
        [SXCUtility saveNSObject:stringPath forKey:@"skinPath"];
        [SXCUtility saveNSObject:[NSDate date] forKey:@"Last_Saved_Date_Splash"];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

        doneServices += 1;
        [self updateProgressBar];

        dispatch_group_leave(group);
    }];
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        
    }];
    
    
    [operation start];
}


-(void)unzipFile:(NSString *)filePath group:(dispatch_group_t) group {

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"ThemeBck"];
    
    [[NSURL URLWithString:path] setResourceValue: [NSNumber numberWithBool: YES] forKey: NSURLIsExcludedFromBackupKey error: nil];
    
    [SXCUtility saveNSObject:@true forKey:@"BackgroundMode"];

    [self removeFile];
    
    [SSZipArchive unzipFileAtPath:filePath toDestination:path];;

    doneServices += 1;
    [self updateProgressBar];

    dispatch_group_leave(group);
}

-(void)removeFile{

    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"ThemeBck"];
   
    NSError *error = nil;
  
    for (NSString *file in [fm contentsOfDirectoryAtPath:path error:&error]) {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", path, file] error:&error];
        if (!success || error) {
        }
    }
    
    [fm removeItemAtPath:path error:&error];

}


-(void)downloadLogos:(dispatch_group_t) group response:(NSArray *)response{
    
    [PWPMapPinHelper downloadPlacesPins:response withCompletionBlock:^(BOOL isSuccess) {
        
        doneServices += 1;
        [self updateProgressBar];
        
        dispatch_group_leave(group);
    }];
    
}

-(void) skin:(dispatch_group_t) group {

    [[PWPService sharedInstance] splash:^(id response) {

        NSString * skinPath = [SXCUtility cleanString:[response valueForKey:@"skin"]];
        NSString * kibbleCounterImage = [SXCUtility cleanString:[response valueForKey:@"background_image"]];

        NSArray * actions = [SXCUtility cleanArray:[response valueForKey:@"actions"]];

        for(NSDictionary * action in actions){

            NSString * name = [SXCUtility cleanString:[action valueForKey:@"name"]];

            if ([name isEqualToString:@"slide_menu_products_url"]){
                [SXCUtility saveNSObject:[SXCUtility cleanString:[action valueForKey:@"link"]] forKey:@"side_menu_add_link"];
                [SXCUtility saveNSObject:[SXCUtility cleanString:[action valueForKey:@"image"]] forKey:@"side_menu_add_imageview"];
            }

            if ([name isEqualToString:@"filter_call_to_action_url"]){
                [SXCUtility saveNSObject:[SXCUtility cleanString:[action valueForKey:@"link"]] forKey:@"filter_link"];
                [SXCUtility saveNSObject:[SXCUtility cleanString:[action valueForKey:@"image"]] forKey:@"filter_imageview"];
            }

            if ([name isEqualToString:@"events_background"]){

                NSString * urlString = [NSString stringWithFormat:@"http://www.slobbr.com%@",[action valueForKey:@"image"]];
                [SXCUtility saveNSObject:[SXCUtility cleanString:urlString] forKey:@"events_background"];
            }

            if ([name isEqualToString:@"map_navigation_icon"]){

                NSString * urlString = [NSString stringWithFormat:@"http://www.slobbr.com%@",[action valueForKey:@"image"]];
                [SXCUtility saveNSObject:[SXCUtility cleanString:urlString] forKey:@"map_navigation_icon"];
                [SXCUtility saveNSObject:[SXCUtility cleanString:[action valueForKey:@"link"]] forKey:@"map_navigation_icon_title"];

            }

            if ([name isEqualToString:@"map_navigation_popup"]){
                NSString * urlString = [NSString stringWithFormat:@"http://www.slobbr.com%@",[action valueForKey:@"image"]];
                [SXCUtility saveNSObject:[SXCUtility cleanString:urlString] forKey:@"map_navigation_popup"];
                [SXCUtility saveNSObject:[action valueForKey:@"link"] forKey:@"map_navigation_popup_link"];
            }

            if ([name isEqualToString:@"map_navigation_popup_button_text"]){

                [SXCUtility saveNSObject:[SXCUtility cleanString:[action valueForKey:@"link"]] forKey:@"map_navigation_popup_button_text"];
            }

            if ([name isEqualToString:@"map_navigation_popup_header_text"]){

                [SXCUtility saveNSObject:[SXCUtility cleanString:[action valueForKey:@"link"]] forKey:@"map_navigation_popup_header_text"];
            }

            if ([name isEqualToString:@"map_navigation_popup_body_text"]){
                [SXCUtility saveNSObject:[SXCUtility cleanString:[action valueForKey:@"link"]] forKey:@"map_navigation_popup_body_text"];
            }

        }

        if(actions.count == 0){
            [SXCUtility saveNSObject:@"" forKey:@"side_menu_add_link"];
            [SXCUtility saveNSObject:@"" forKey:@"side_menu_add_imageview"];
            [SXCUtility saveNSObject:@"" forKey:@"filter_link"];
            [SXCUtility saveNSObject:@"" forKey:@"filter_imageview"];
            [SXCUtility saveNSObject:@"" forKey:@"events_background"];
            [SXCUtility saveNSObject:@"" forKey:@"map_navigation_icon"];
            [SXCUtility saveNSObject:@"" forKey:@"map_navigation_popup_body_text"];
            [SXCUtility saveNSObject:@"" forKey:@"map_navigation_popup_header_text"];
            [SXCUtility saveNSObject:@"" forKey:@"map_navigation_popup_button_text"];
            [SXCUtility saveNSObject:@"" forKey:@"map_navigation_popup"];
            [SXCUtility saveNSObject:@"" forKey:@"map_navigation_popup_link"];
            [SXCUtility saveNSObject:@"" forKey:@"map_navigation_icon_title"];

        }

        if(kibbleCounterImage.length > 0){
            NSString * urlString = [NSString stringWithFormat:@"http://www.slobbr.com%@",kibbleCounterImage];
            [SXCUtility saveNSObject:urlString forKey:@"kibble_counter_image_url"];
        }
        else{
            [SXCUtility saveNSObject:@"" forKey:@"kibble_counter_image_url"];
        }

        if ((skinPath.length > 0 && ![[SXCUtility cleanString:[SXCUtility getNSObject:@"skinPath"]] isEqualToString:skinPath])){
            [self downloadSplashImage:skinPath group:group];
        }
        else if(skinPath.length == 0){
            [self removeFile];
            [SXCUtility saveNSObject:@"" forKey:@"skinPath"];

            doneServices += 1;
            [self updateProgressBar];

            dispatch_group_leave(group);
        }
        else{

            doneServices += 1;
            [self updateProgressBar];

            dispatch_group_leave(group);
        }

    } withErrorBlock:^(NSError *error) {

        doneServices += 1;
        [self updateProgressBar];

        dispatch_group_leave(group);
    }];
}

-(void)updateProgressBar {

    labelProgress.text =  [NSString stringWithFormat:@"%.1f%%",doneServices * 100.0f /totalServices];
    progressBar.progress = doneServices * 1.0f /totalServices;

}


@end
