//
//  PWPBreedSelectionViewController.m
//  PawPark
//
//  Created by xyz on 20/07/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPBreedSelectionViewController.h"
#import "UIViewController+UBRComponents.h"
#import "SXCUtility.h"
#import "UIColor+PWPColor.h"
#import "SXCTextfieldWithLeftImage.h"
#import <Google/Analytics.h>
#import "PWPDownloadBackgroundImage.h"

@interface PWPBreedSelectionViewController ()<UITextFieldDelegate>
{
    __weak IBOutlet UIImageView * imageViewBG;
}
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property(nonatomic,weak)IBOutlet UITableView * tableView;
@property(nonatomic,strong)NSMutableArray * breadArray;
@property(nonatomic,strong)NSMutableArray * searchedBreedArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintTableViewBottomMargin;
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textfield;
@end

@implementation PWPBreedSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self sxc_customizeBackButton];
    [self sxc_navigationBarCustomizationWithTitle:@"Select Breed"];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ZipCodeCell"];
 
    NSMutableArray * unSortedBreadArray=[[SXCUtility cleanArray:[SXCUtility getNSObject:@"BreedList"]] mutableCopy];
    NSSortDescriptor * sortDiscriptor=[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    _breadArray=[unSortedBreadArray sortedArrayUsingDescriptors:@[sortDiscriptor]].mutableCopy;
    
    if([_from isEqualToString:@"Add"]){
        [_breadArray addObject:@{@"name":@"Other"}];
    }
    
    _searchedBreedArray=[_breadArray copy];
    _layoutConstraintTableViewBottomMargin.constant=0.0f;
    [_textfield resignFirstResponder];
    _textfield.placeholder=@"Enter other breed";
    
    _textfield.backgroundColor = [UIColor clearColor];
    _textfield.delegate=self;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Breed Selection Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegate and Datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"BreedSelectionCell"];
    cell.backgroundColor =[UIColor clearColor];
    cell.contentView.backgroundColor =[UIColor clearColor];
    cell.textLabel.text=[_searchedBreedArray objectAtIndex:indexPath.row][@"name"];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font =[UIFont fontWithName:@"Roboto-Regular" size:14.0f];
    
    UIView *customColorView = [[UIView alloc] init];
    customColorView.backgroundColor = [UIColor sxc_themeColorWithAplha];
    cell.selectedBackgroundView =  customColorView;
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _searchedBreedArray.count;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([[_searchedBreedArray objectAtIndex:indexPath.row][@"name"] isEqualToString:@"Other"]){
    
        _layoutConstraintTableViewBottomMargin.constant=55.0f;
        [_textfield becomeFirstResponder];
        [self.view layoutIfNeeded];
        
    }else{
    
    
        if(_delegate &&[_delegate respondsToSelector:@selector(breedSelected:)]){
            [_delegate breedSelected:_searchedBreedArray[indexPath.row]];
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.view endEditing:YES];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if(searchText.length==0){
        _searchedBreedArray=[_breadArray copy];
    }
    else{
        
        NSPredicate * predicate=[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@",searchText];
        
        _searchedBreedArray=(id)[_breadArray filteredArrayUsingPredicate:predicate];
    }
    [_tableView reloadData];
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.text.length>0){
        if(_delegate &&[_delegate respondsToSelector:@selector(breedSelected:)]){
            [_delegate breedSelected:@{@"name":textField.text,@"id":textField.text}.mutableCopy];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        [self sxc_showErrorViewWithMessage:@"Enter other breed name"];
    }
}

-(void)sxc_stopLoading{}
@end
