//
//  PWPCheckInArroundMeViewController.m
//  PawPark
//
//  Created by Samarth Singla on 04/12/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPCheckInArroundMeViewController.h"
#import "PWPSearchParkTableViewCell.h"
#import "PWPParkCheckInViewController.h"
#import "PWPParkCheckedInDogsViewController.h"
#import "SXCUtility.h"
#import "UIViewController+UBRComponents.h"
#import "UIColor+PWPColor.h"
#import "PWPService.h"
#import "PWPApplicationUtility.h"
#import "PWPSchedulePawDateViewController.h"
#import "UBRLocationHandler.h"
#import "PWPAddPlacesViewController.h"
#import "PWPMapPinHelper.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPPlacePicViewController.h"

@interface PWPCheckInArroundMeViewController ()<PWPParkCellDelegate,PWPParkCheckInDelegae, UITableViewDelegate, UITableViewDataSource, PWPPlacePicViewControllerDelegate>
{
    NSInteger selectedRow;
    __weak IBOutlet UIImageView * imageViewBG;

}
@property (weak, nonatomic) IBOutlet UITableView *tableViewParks;
@property (strong, nonatomic) NSMutableArray * parks;

@end

@implementation PWPCheckInArroundMeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectedRow = -1;
    self.title = @"Select place to check-in";
    [self sxc_customizeBackButton];

    [self.tableViewParks registerNib:[UINib nibWithNibName:@"PWPSearchParkTableViewCell" bundle:nil] forCellReuseIdentifier:@"PWPParkListiningCell"];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];

    [self startLoader];
    [self searchParks];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate And DataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PWPSearchParkTableViewCell * tableViewCell =[tableView dequeueReusableCellWithIdentifier:@"PWPParkListiningCell" forIndexPath:indexPath];
    
    [tableViewCell bindValueWithComponentsWithData:self.parks[indexPath.row] reloadData:(indexPath.row == selectedRow)];
    [tableViewCell setDelegate:self];
    return tableViewCell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.parks.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == selectedRow && [SXCUtility cleanArray:[self.parks[indexPath.row] valueForKey:@"filter_amenities"]].count > 0) {
        return 295.0f;
    }else {
        return 238.0f;
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == selectedRow && [SXCUtility cleanArray:[self.parks[indexPath.row] valueForKey:@"filter_amenities"]].count > 0) {
        return 295.0f;
    }else {
        return 238.0f;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([SXCUtility cleanArray:[self.parks[indexPath.row] valueForKey:@"filter_amenities"]].count == 0){
        return;
    }
    
   NSInteger previousSelectedRow = selectedRow;
    
    if(indexPath.row!=selectedRow){
        selectedRow=indexPath.row;
    }
    else{
        selectedRow=-1;
    }
    
    NSMutableArray * indexes = [[NSMutableArray alloc] init];
    if(previousSelectedRow != -1){
        [indexes addObject:[NSIndexPath indexPathForRow:previousSelectedRow inSection:0]];
    }
    
    if(selectedRow != -1){
        [indexes addObject:[NSIndexPath indexPathForRow:selectedRow inSection:0]];
    }
    
    [tableView reloadRowsAtIndexPaths:indexes
                     withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 65.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIButton * headerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [headerButton setTitle:@"  Add New Place" forState:UIControlStateNormal];
    
    UIImage * place = [UIImage imageNamed:@"ic_add_place"];
    
    
    [headerButton setImage:[PWPMapPinHelper imageWithImage:place scaledToSize:CGSizeMake(40, 40)] forState:UIControlStateNormal];
    [headerButton setTitleColor:[UIColor sxc_themeColor] forState:UIControlStateNormal];
    [headerButton addTarget:self action:@selector(addPlaceClicked:) forControlEvents:UIControlEventTouchUpInside];
    headerButton.imageView.contentMode =  UIViewContentModeScaleAspectFit;
    
    return headerButton;
}

-(CGFloat)getHeightOfParkBasedOnAmentis:(NSDictionary *)parkDictionary
{
    
    NSMutableString * amentiesString=[[NSMutableString alloc] init];
    
    for(NSString * amenity in parkDictionary[@"amenities"]){
        if(amentiesString.length>0){
            [amentiesString appendString:@"\n"];
        }
        [amentiesString appendString:amenity];
    }
    
    CGFloat amentiesHeight=[self getHeightOfLabelWithText:amentiesString withFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0f]];
    
    if(amentiesHeight==0){
        return 153;
    }
    return 153+amentiesHeight;
}

-(CGFloat)getHeightOfLabelWithText:(NSString *)text withFont:(UIFont *)font
{
    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    gettingSizeLabel.font = font;
    gettingSizeLabel.text = text;
    gettingSizeLabel.numberOfLines = 0;
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width-16, 9999);
    
    CGSize expectSize = [gettingSizeLabel sizeThatFits:maximumLabelSize];
    return expectSize.height;
}

-(void)parkOptionClickedWithData:(NSDictionary *)dictionary
{
    
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];

    PWPPlacePicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPPlacePicViewController"];
    viewController.placePics = [SXCUtility cleanArray:[dictionary valueForKey:@"gallery"]].mutableCopy;
    viewController.data = dictionary;
    viewController.placeId = dictionary[@"id"];
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)parkOption2ClickedWithData:(NSDictionary *)dictionary
{
    if([SXCUtility cleanString:[dictionary[@"active_dogs_count"] description]].integerValue > 0){

        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PWPParkCheckedInDogsViewController * parkCheckInDogsViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckedInDogsViewController"];
        parkCheckInDogsViewController.placeId = [SXCUtility cleanString:[[dictionary valueForKey:@"id"] description]];
        
        [self.navigationController pushViewController:parkCheckInDogsViewController animated:YES];
    }
}
-(void)parkOption3ClickedWithData:(NSDictionary *)dictionary
{
    [self startLoader];
    
    if([SXCUtility cleanInt:dictionary[@"is_favourite"]] ==1){
        
        [[PWPService sharedInstance] deleteHomePark:dictionary[@"id"] withSuccessBlock:^(id response) {
            [self dismissLoader];
            
            NSInteger index =[self.parks indexOfObject:dictionary];
            NSMutableDictionary * parkDictionary = dictionary.mutableCopy;
            
            if([SXCUtility cleanInt:parkDictionary[@"is_favourite"]] ==1){
                parkDictionary[@"is_favourite"]=@0;
            }
            else{
                parkDictionary[@"is_favourite"]=@1;
            }
            
            [self.parks replaceObjectAtIndex:index withObject:parkDictionary];
            [self.tableViewParks reloadData];
            [self sxc_showSuccessWithMessage:@"Place has been removed from 'Favorite' Places."];
            
        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:error.domain];
        }];
        
    }
    else{
        
        
        [[PWPService sharedInstance] homePark:dictionary[@"id"] withSuccessBlock:^(id response) {
            [self dismissLoader];
            
            NSInteger index =[self.parks indexOfObject:dictionary];
            NSMutableDictionary * parkDictionary = dictionary.mutableCopy;
            
            if([SXCUtility cleanInt:parkDictionary[@"is_favourite"]] ==1){
                parkDictionary[@"is_favourite"]=@0;
            }
            else{
                parkDictionary[@"is_favourite"]=@1;
            }
            
            [self.parks replaceObjectAtIndex:index withObject:parkDictionary];
            
            NSMutableDictionary * userDictionary=[[PWPApplicationUtility getCurrentUserProfileDictionary] mutableCopy];
            [userDictionary setValue:dictionary forKey:@"place"];
            [PWPApplicationUtility saveCurrentUserProfileDictionary:userDictionary];
            
            [self.tableViewParks reloadData];
            [self sxc_showSuccessWithMessage:@"Place has been marked as 'Favorite' Place."];
            
        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:error.domain];
        }];
    }
}

-(void)successFullCheckedInWithDictionary:(NSMutableDictionary *)data WithDogs:(NSMutableArray *)dogsArray{
    [self startLoader];
    [self searchParks];
}

-(void)inviteOptionClicked:(NSDictionary *)dictionary
{
    [self pushSchedulePawDateViewController:dictionary];
}

-(void)parkQuickCheckInClicked:(NSDictionary *)dictionary
{
    
    [self startLoader];
    
    [[PWPService sharedInstance] checkInParkWithRequestDictionary:[self requestDictionary] withParkId:dictionary[@"id"] withSuccessBlock:^(id response) {
        
        [self dismissLoader];
        [self sxc_showSuccessWithMessage:@"You have been checked-in successfully."];
        [self startLoader];
        [self searchParks];
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        
        if (error.code==403) {
            [self sxc_showErrorViewWithMessage:@"You have already checked-in."];
        }
        else{
            [self sxc_showErrorViewWithMessage:error.domain];
        }
        
    }];
}

-(void)addPlaceClickedWithData:(NSDictionary *)dictionary {    
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    
    PWPPlacePicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPPlacePicViewController"];
    viewController.placePics = [SXCUtility cleanArray:[dictionary valueForKey:@"gallery"]].mutableCopy;
    viewController.data = dictionary;
    viewController.placeId = dictionary[@"id"];
    viewController.delegate = self;
    viewController.isGalleryOpen = true;
    [self.navigationController pushViewController:viewController animated:YES];
}


-(NSMutableDictionary *)requestDictionary
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] init];
    
    if([PWPApplicationUtility getCurrentUserDogsArray].count>1){
        [requestDictionary setValue:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"] forKey:@"dogs"];
    }
    
    [requestDictionary setValue:[SXCUtility cleanString:[SXCUtility getNSObject:@"quickCheckinDefaultMessage"]] forKey:@"message"];
    
    return requestDictionary;
}


-(void)pushSchedulePawDateViewController:(NSDictionary *)parkDictionary{
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    PWPSchedulePawDateViewController *pawDateViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPSchedulePawDateViewController"];
    pawDateViewController.selectedPark = parkDictionary.mutableCopy;
    [self.navigationController pushViewController:pawDateViewController animated:YES];
    
}


-(void)searchParks{
    
    [[PWPService sharedInstance] getParksWithRequestDictionary:[self requestDictionaryForParks] withPageIndex:@0 withPlaceCategory:self.categorySlug withSuccessBlock:^(id response, NSNumber * pageIndex) {
        
        [self dismissLoader];
        
       
        
        NSMutableArray * tempFavPlaces = [[NSMutableArray alloc] init];
        for(NSDictionary * dict in response){
            
            NSMutableDictionary * tempPlace = dict.mutableCopy;
            NSMutableArray * tempAmenities = [[NSMutableArray alloc] init];
            NSMutableDictionary * amenities = [SXCUtility cleanDictionary:[tempPlace valueForKey:@"amenities"]].mutableCopy;
            
            for(NSString * value in amenities.allValues) {
                if([UIImage imageNamed:value] != nil){
                      [tempAmenities addObject:[value stringByReplacingOccurrencesOfString:@"/" withString:@":"]];
                }
            }
            [tempPlace setValue:tempAmenities forKey:@"filter_amenities"];
            [tempFavPlaces addObject:tempPlace];
        }
        
        self.parks = [[NSMutableArray alloc] initWithArray:tempFavPlaces];
        
        
        if(self.parks.count == 0){
                [self sxc_showErrorViewWithMessage:@"There are no places within that search radius. Please use the filter and increase the search radius to find more places."];
        }
        else{
            self.parks = [[NSMutableArray alloc] initWithArray:response];
        }
        
        [self.tableViewParks reloadData];
        
    } withErrorBlock:^(NSError *error, NSNumber * pageIndex) {
        [self dismissLoader];
        [self sxc_showErrorViewWithMessage:error.domain];
    }];
}


-(NSMutableDictionary *)requestDictionaryForParks
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] init];
    [requestDictionary setValue:@3 forKey:@"limit"];
    [requestDictionary setValue:@0 forKey:@"offset"];
    [requestDictionary setValue:@"8.9.0" forKey:@"app_version"];
    requestDictionary[@"lat"]=[NSNumber numberWithDouble:[UBRLocationHandler shareHandler].currentLocation.coordinate.latitude];
    requestDictionary[@"lng"]=[NSNumber numberWithDouble:[UBRLocationHandler shareHandler].currentLocation.coordinate.longitude];
    
    
    [requestDictionary setValue:[SXCUtility getNSObject:@"apikey"] forKey:@"apikey"];
    return requestDictionary;
}

-(void)sxc_stopLoading{}
-(void)sxc_startLoading{}

-(IBAction)addPlaceClicked:(id)sender{



    [[PWPService sharedInstance] googleAPIGeoCodeWithLat:[NSNumber numberWithFloat:[UBRLocationHandler shareHandler].currentLocation.coordinate.latitude] withLongi:[NSNumber numberWithFloat:[UBRLocationHandler shareHandler].currentLocation.coordinate.longitude] withSuccessBlock:^(id responseDicitionary) {


        NSString * zipcode = @"";
        NSString * city = @"";
        NSString * state = @"";
        NSString * formattedAddress = @"";

        NSArray * addresses = [SXCUtility cleanArray:[responseDicitionary valueForKey:@"results"]];
        if(addresses.count > 0) {

            NSDictionary * address = addresses[0];
            NSArray * addressComponents = [address valueForKey:@"address_components"];
            for(NSDictionary * addressComponent in addressComponents){

                if([[SXCUtility cleanArray:[addressComponent valueForKey:@"types"]] containsObject:@"administrative_area_level_1"]){
                    state = [SXCUtility cleanString:[addressComponents valueForKey:@"long_name"]];
                }

                if([[SXCUtility cleanArray:[addressComponent valueForKey:@"types"]] containsObject:@"administrative_area_level_2"]){
                    city = [SXCUtility cleanString:[addressComponents valueForKey:@"long_name"]];
                }

                if([[SXCUtility cleanArray:[addressComponent valueForKey:@"types"]] containsObject:@"postal_code"]){
                    zipcode = [SXCUtility cleanString:[addressComponents valueForKey:@"long_name"]];
                }
            }

            formattedAddress = [SXCUtility cleanString:[address valueForKey:@"formatted_address"]];

        }


        UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
        PWPAddPlacesViewController *parkFilterViewController=[storyBoard instantiateViewControllerWithIdentifier:@"PWPAddPlacesViewController"];
        parkFilterViewController.zipcode = zipcode;
        parkFilterViewController.city = city;
        parkFilterViewController.state = state;
        parkFilterViewController.formattedAddress = formattedAddress;
        parkFilterViewController.lat = [UBRLocationHandler shareHandler].currentLocation.coordinate.latitude;
        parkFilterViewController.longi = [UBRLocationHandler shareHandler].currentLocation.coordinate.longitude;
        [self.navigationController pushViewController:parkFilterViewController animated:YES];

        [self dismissLoader];

    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];

        UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
        PWPAddPlacesViewController *parkFilterViewController=[storyBoard instantiateViewControllerWithIdentifier:@"PWPAddPlacesViewController"];
        parkFilterViewController.lat = [UBRLocationHandler shareHandler].currentLocation.coordinate.latitude;
        parkFilterViewController.longi = [UBRLocationHandler shareHandler].currentLocation.coordinate.longitude;

        [self.navigationController pushViewController:parkFilterViewController animated:YES];
    }];



}

-(void)placeProfileUpdated {
    [self startLoader];
    [self searchParks];
}
@end
