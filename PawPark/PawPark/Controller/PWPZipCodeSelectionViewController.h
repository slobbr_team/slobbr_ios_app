//
//  PWPZipCodeSelectionViewController.h
//  PawPark
//
//  Created by xyz on 21/06/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPBaseViewController.h"

@protocol PWPZipCodeSelectionDelegate <NSObject>
-(void)zipCodeSelected:(NSMutableDictionary *)zipCodeDictionary;
@end



@interface PWPZipCodeSelectionViewController : PWPBaseViewController
@property(nonatomic,strong)NSMutableDictionary * parkDictionary;
@property(nonatomic,strong)NSMutableString * selectZipCodeTitle;
@property(nonatomic,weak)id<PWPZipCodeSelectionDelegate> delegate;
@end
