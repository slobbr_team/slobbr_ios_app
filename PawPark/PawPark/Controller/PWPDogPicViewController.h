//
//  PWPDogPicViewController.h
//  PawPark
//
//  Created by Samarth Singla on 02/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWPBaseViewController.h"

@protocol PWPDogPicViewControllerDelegate <NSObject>

-(void)dogProfileUpdated;

@end

@interface PWPDogPicViewController : PWPBaseViewController

@property(strong,nonatomic)NSMutableArray * dogsPics;
@property(strong,nonatomic)NSDictionary * data;
@property(strong,nonatomic)NSString * dogId;
@property(assign,nonatomic)id<PWPDogPicViewControllerDelegate> delegate;

@end
