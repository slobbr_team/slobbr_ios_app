//
//  PWPPlacePicViewController.m
//  PawPark
//
//  Created by Samarth Singla on 19/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPPlacePicViewController.h"
#import "UIViewController+UBRComponents.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPDogImageCell.h"
#import <UIImageView+WebCache.h>
#import "PWPApplicationUtility.h"
#import "SXCUtility.h"
#import <MWPhoto.h>
#import <MWPhotoBrowser.h>
#import <CTAssetsPickerController/CTAssetsPickerController.h>
#import <UIImagePickerController+BlocksKit.h>
#import "PWPService.h"
#import "PWPMapPinHelper.h"
#import "PWPAppDelegate.h"
#import <SVProgressHUD.h>

@interface PWPPlacePicViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MWPhotoBrowserDelegate, CTAssetsPickerControllerDelegate, PWPDogImageCellProtocoal>
{
    __weak IBOutlet UIImageView * imageViewBG;
    __weak IBOutlet UILabel * noDataLabel;
    __weak IBOutlet UICollectionView * picCollectionView;

    __weak IBOutlet UIBarButtonItem *uploadButton;
    NSMutableArray * photos;
    NSMutableArray * thumbs;
    NSString * fromString;
    NSString * toString;
    
    NSInteger numberOfPics;
    CGFloat totalProgress;
}
@end

@implementation PWPPlacePicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self sxc_customizeBackButton];
    [self sxc_navigationBarCustomizationWithTitle:@"Photos"];

    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];

    noDataLabel.hidden = true;

    if(_placePics.count > 0){
        [uploadButton setTitle:@"Upload"];
    }
    else{
        [uploadButton setTitle:@""];
    }
    _placeName.text = self.data[@"name"];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(self.isGalleryOpen){
        [self addPlacePic];
        self.isGalleryOpen = false;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.placePics.count + 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;

    return CGSizeMake((screenWidth-64)/3,(screenWidth-64)/3);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    if(indexPath.row == self.placePics.count){
        UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PWPDogImageAddCell" forIndexPath:indexPath];
        return cell;
    }

    PWPDogImageCell * collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PWPDogImageCell" forIndexPath:indexPath];

    NSString * dogUrlString = [SXCUtility cleanString:[[self.placePics[indexPath.row] valueForKey:@"thumbs"] valueForKey:@"w400"]];

    UIImage * image = [self.placePics[indexPath.row] valueForKey:@"image"];
    if (image != nil){
        collectionViewCell.buttonDogImageDelete.hidden = false;
        [collectionViewCell.imageViewDog setImage:image];
    }else {
        collectionViewCell. buttonDogImageDelete.hidden = true;
        [collectionViewCell.imageViewDog sd_setImageWithURL:[NSURL URLWithString:dogUrlString] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
    }
    [collectionViewCell configureViewWithDelegate:self WithIndexPath:indexPath];
    collectionViewCell.buttonDogImageDelete.hidden = true;

    return collectionViewCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row == self.placePics.count){
        [self addPlacePic];
        return;
    }

    photos = [[NSMutableArray alloc] init];
    thumbs = [[NSMutableArray alloc] init];

    for (NSDictionary * place in self.placePics) {

        if([place valueForKey:@"image"]){
            [photos addObject:[MWPhoto photoWithImage:[place valueForKey:@"image"]]];
            [thumbs addObject:[MWPhoto photoWithImage:[place valueForKey:@"image"]]];
        }
        else {
            NSString * fullDogUrlString = [SXCUtility cleanString:[[place valueForKey:@"thumbs"] valueForKey:@"w1000"]];
            [photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:fullDogUrlString]]];

            NSString * thumbDogUrlString = [SXCUtility cleanString:[[place valueForKey:@"thumbs"] valueForKey:@"w400"]];
            [thumbs addObject:[MWPhoto photoWithURL:[NSURL URLWithString:thumbDogUrlString]]];
        }
    }

    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];

    browser.displayActionButton = YES;
    browser.displayNavArrows = YES;
    browser.displaySelectionButtons = NO;
    browser.alwaysShowControls = YES;
    browser.zoomPhotosToFill = YES;
    browser.enableGrid = NO;
    browser.startOnGrid = NO;
    browser.enableSwipeToDismiss = YES;
    browser.autoPlayOnAppear = NO;
    [browser setCurrentPhotoIndex:indexPath.row];

    UINavigationController * controller = [[UINavigationController alloc] initWithRootViewController:browser];

    [self presentViewController:controller animated:YES completion:^{

    }];

}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < thumbs.count)
        return [thumbs objectAtIndex:index];
    return nil;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
}

- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index {
    return false;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected {
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)imageDeleteClicked:(NSIndexPath *)indexPath
{
    NSDictionary * dictionary = self.placePics[indexPath.row];

    NSString * imageId = [SXCUtility cleanString:[[dictionary valueForKey:@"id"] description]];
    if (imageId.length > 0){
    }
    else {
        [self.placePics removeObjectAtIndex:indexPath.row];
        [picCollectionView reloadData];
    }

}


-(void)addPlacePic {

    void (^picPhotosFromGallery)(void) = ^void() {

        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status){
            dispatch_async(dispatch_get_main_queue(), ^{

                // init picker
                CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];

                // set delegate
                picker.delegate = self;

                // Optionally present picker as a form sheet on iPad
                [self presentViewController:picker animated:YES completion:nil];
            });
        }];
    };
    void (^picPhotosFromCamera)(void) = ^void() {

        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePicker setAllowsEditing:YES];

        [imagePicker setBk_didCancelBlock:^(UIImagePickerController * imagePickerController) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];

        [imagePicker setBk_didFinishPickingMediaBlock:^(UIImagePickerController * controller, NSDictionary *info) {

            UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];

            [self createNewDogPic:image];

            [self dismissViewControllerAnimated:YES completion:nil];

            [picCollectionView reloadData];

        }];

        [self presentViewController:imagePicker animated:YES completion:nil];

    };


    if (TARGET_IPHONE_SIMULATOR) {
        [self sxc_showActionSheetWithActionTitle:nil optionTitle:@[@"Choose"] WithActionArray:@[[picPhotosFromGallery copy]]];
    }else{
        [self sxc_showActionSheetWithActionTitle:nil optionTitle:@[@"Take Photo",@"Choose"] WithActionArray:@[[picPhotosFromCamera copy],[picPhotosFromGallery copy]]];
    }

}

-(void)createNewDogPic:(UIImage *)image {
    NSMutableDictionary * dictionary =[[NSMutableDictionary alloc] init];
    dictionary[@"description"] = @"Upload New image Place picture from mobile app";
    dictionary[@"imageInfo"] = @[@{@"key":@"License",@"value":@"Mobile User"},@{@"key":@"by",@"value":@"Mobile User"}];
    dictionary[@"image"] = image;
    [self.placePics addObject:dictionary];

    if(_placePics.count > 0){
        [uploadButton setTitle:@"Upload"];
    } else {
        [uploadButton setTitle:@""];
    }
}

- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets {
    [self dismissViewControllerAnimated:YES completion:nil];

    PHImageRequestOptions * requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    requestOptions.synchronous = true;

    PHImageManager *manager = [PHImageManager defaultManager];
    for (PHAsset *asset in assets) {
        [manager requestImageForAsset:asset targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeDefault options:requestOptions resultHandler:^void(UIImage *image, NSDictionary *info) {
            [self createNewDogPic:image];
        }];
    }
    [picCollectionView reloadData];
}

-(void)uploadNewPicGroup:(dispatch_group_t)group {
    
    for (NSDictionary * dictionary in self.placePics){
        
        UIImage * image = [PWPMapPinHelper resizeImageForUploading:[dictionary valueForKey:@"image"]];
        
        if (image != nil){
            
            numberOfPics += 1;
            
            dispatch_group_enter(group);
            
            
            NSLog(@"********************height%f***********width%f",image.size.height,image.size.width);
            
            NSMutableDictionary * requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
            NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];
            
            NSString * imageDataString =[[NSString alloc] initWithFormat:@"data:image/jpeg;base64,%@",[imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
            [requestDictionary setValue:imageDataString forKey:@"imageFile"];
            [requestDictionary removeObjectForKey:@"image"];
            
            [[PWPService sharedInstance] uploadNewPlaceImage:requestDictionary withDogId:self.data[@"id"] withProgress:^(float progress){
                totalProgress += progress;
                [self updateProgressLoader];

            } withSuccessBlock:^(id responseDicitionary) {
                dispatch_group_leave(group);
            } withErrorBlock:^(NSError *error) {
                dispatch_group_leave(group);
            }];
            
        }
    }
}

-(void)updateProgressLoader {
   
    NSLog(@"%f",totalProgress);
    
    if (numberOfPics == 1) {
        NSString * uploadingText = [NSString stringWithFormat:@"Uploading\n(%.0f%%)...",totalProgress*1.0f/numberOfPics*100];
        [[[PWPAppDelegate sharedInstance] window] setUserInteractionEnabled:NO];
        [SVProgressHUD showProgress:totalProgress*1.0f/numberOfPics status:uploadingText];
    } else {
        
        NSInteger uploadedImageNumber = totalProgress;
        if(uploadedImageNumber >= numberOfPics) {
            uploadedImageNumber  = numberOfPics-1;
        }
        
        NSInteger totalProgressInInt = totalProgress;
        CGFloat percentage = (totalProgress - totalProgressInInt)*100;
        
        NSString * uploadingText = [NSString stringWithFormat:@"Uploading\n%.0f%%\n(%.0ld/%ld)",percentage,(long)uploadedImageNumber+1,(long)numberOfPics];
        [[[PWPAppDelegate sharedInstance] window] setUserInteractionEnabled:NO];
        [SVProgressHUD showProgress:totalProgress*1.0f/numberOfPics status:uploadingText];
    }
    
}

-(IBAction)doneBtnClicked:(id)sender{
    
    if(self.placePics.count == 0){
        return;
    }
    
    [self startLoader];

    dispatch_group_t group = dispatch_group_create();

    numberOfPics = 0;
    totalProgress = 0;

    [self uploadNewPicGroup:group];

    dispatch_group_notify(group, dispatch_get_main_queue(), ^{

        [self.navigationController popViewControllerAnimated:YES];

        [self sxc_showSuccessWithMessage:[NSString stringWithFormat:@"Your photo has been added for %@",self.data[@"name"]]];
        [self dismissLoader];

        if(_delegate&&[_delegate respondsToSelector:@selector(placeProfileUpdated)]){
            [_delegate placeProfileUpdated];
        }

    });
    
}


@end
