//
//  PWPSelectCommunityCategoryViewController.m
//  PawPark
//
//  Created by Samarth Singla on 09/09/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPSelectCommunityCategoryViewController.h"
#import "UIViewController+UBRComponents.h"
#import "SXCUtility.h"
#import "PWPApplicationUtility.h"
#import "UIColor+PWPColor.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPSelectCommunityCategoryViewController ()
{
    NSArray * categoryTitleArray;
    NSArray * categoryValueArray;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewBg;

@end

@implementation PWPSelectCommunityCategoryViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.imageViewBg.image = [PWPDownloadBackgroundImage backgroundImage];
    
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"community_category_cell"];
    
    categoryValueArray = @[@"pic",@"intro",@"event",@"question"];
    categoryTitleArray = @[@"Community Pictures", @"Introduction", @"Events", @"Questions"];
    
    [self sxc_navigationBarCustomizationWithTitle:@"SELECT CATEGORY"];
    [self sxc_customizeBackButton];

    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonClicked:)];
    self.navigationItem.rightBarButtonItem = backBarButtonItem;
    
    [_tableView reloadData];
    
}

-(IBAction)doneButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return categoryTitleArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"community_category_cell" forIndexPath:indexPath];
    cell.textLabel.text= categoryTitleArray[indexPath.row];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.backgroundColor =[UIColor clearColor];
    cell.contentView.backgroundColor =[UIColor clearColor];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font =[UIFont fontWithName:@"Roboto-Regular" size:14.0f];
    
    UIView *customColorView = [[UIView alloc] init];
    customColorView.backgroundColor = [UIColor sxc_themeColorWithAplha];
    cell.selectedBackgroundView =  customColorView;
    
    if([categoryValueArray[indexPath.row] isEqualToString:_selectedCategory]){
        cell.accessoryType=UITableViewCellAccessoryCheckmark;
        
    }else{
        cell.accessoryType=UITableViewCellAccessoryNone;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryCheckmark){
        
        [_selectedCategory setString:@""];
    }
    else{
        [_selectedCategory setString:categoryValueArray[indexPath.row]];
    }
    
    [tableView reloadRowsAtIndexPaths:tableView.indexPathsForVisibleRows withRowAnimation:UITableViewRowAnimationAutomatic];
}


- (IBAction)doneClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)sxc_stopLoading {}

-(void)sxc_startLoading {}

@end
