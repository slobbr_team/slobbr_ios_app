

#import <UIKit/UIKit.h>

@interface SplashScrollViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) NSArray *contentList;

@end
