

#import "SplashScrollViewController.h"
#import "SplashChildViewController.h"
#import "PWPApplicationUtility.h"
#import "PWPDownloadBackgroundImage.h"

static NSString *kNameKey = @"nameKey";
static NSString *kImageKey = @"imageKey";
static NSString *kDescriptionKey = @"descriptionKey";
static NSString *kBackgroundKey = @"backgroundImage";

@interface SplashScrollViewController ()
{
__weak IBOutlet UIImageView * imageViewBG;
}

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthLayoutConstraint;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (nonatomic, strong) NSMutableArray *viewControllers;
@property (weak, nonatomic) IBOutlet UIButton *buttonTodayEvent;

- (IBAction)signUpButtonClicked:(id)sender;
- (IBAction)todaysButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trainingConstraint;
@end

#pragma mark -

@implementation SplashScrollViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"content_iPhone" ofType:@"plist"];
    self.contentList = [NSArray arrayWithContentsOfFile:path];
    
    if([PWPApplicationUtility isEventMode]){
        _trainingConstraint.constant = SCREEN_WIDTH/2.0f - 16;
        _buttonTodayEvent.hidden=NO;
    }else{
        _trainingConstraint.constant =-16;
        _buttonTodayEvent.hidden=YES;
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.view layoutIfNeeded];
    self.pageControl.hidden = true;
    
    NSUInteger numberPages = self.contentList.count;
    
    // view controllers are created lazily
    // in the meantime, load the array with placeholders which will be replaced on demand
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < numberPages; i++)
    {
        [controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
    // a page is the width of the scroll view
    self.scrollView.pagingEnabled = YES;
    self.scrollView.contentSize =
    CGSizeMake(SCREEN_WIDTH * numberPages, SCREEN_HEIGHT);
    self.scrollView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
    self.scrollView.bounces=NO;
    self.scrollView.userInteractionEnabled = true;
    
    self.pageControl.numberOfPages = numberPages;
    self.pageControl.currentPage = 0;
    
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    [self loadScrollViewWithPage:2];
    [self loadScrollViewWithPage:3];
}



- (void)loadScrollViewWithPage:(NSUInteger)page
{
    if (page >= self.contentList.count)
        return;
    
    // replace the placeholder if necessary
    SplashChildViewController *controller = [self.viewControllers objectAtIndex:page];
    if ((NSNull *)controller == [NSNull null])
    {
        controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SplashChildViewController"];
        controller.pageNumber=page;
        
        if(page==0){
            controller.topImageView.hidden=NO;
        }
        else{
            controller.topImageView.hidden=YES;
        }
        
        [self.viewControllers replaceObjectAtIndex:page withObject:controller];
    }
    
    // add the controller's view to the scroll view
    if (controller.view.superview == nil)
    {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = CGRectGetWidth(frame) * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        
        [self addChildViewController:controller];
        [self.scrollView addSubview:controller.view];
        [controller didMoveToParentViewController:self];
        
        NSDictionary *numberItem = [self.contentList objectAtIndex:page];
        controller.numberImage.image = [UIImage imageNamed:[numberItem valueForKey:kImageKey]];
        controller.lableTitle.text = [numberItem valueForKey:kDescriptionKey];
        controller.labelDescription.text = [numberItem valueForKey:kNameKey];
        controller.topImageView.image = [UIImage imageNamed:[numberItem valueForKey:kBackgroundKey]];
        
    }
}

// at the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = CGRectGetWidth(self.scrollView.frame);
    NSUInteger page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    // a possible optimization would be to unload the views+controllers which are no longer visible
}

- (void)gotoPage:(BOOL)animated
{
    NSInteger page = self.pageControl.currentPage;
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    // update the scroll view to the appropriate page
    CGRect bounds = self.scrollView.bounds;
    bounds.origin.x = CGRectGetWidth(bounds) * page;
    bounds.origin.y = 0;
    [self.scrollView scrollRectToVisible:bounds animated:animated];
}

- (IBAction)changePage:(id)sender
{
    [self gotoPage:YES];    // YES = animate
}

- (IBAction)signUpButtonClicked:(id)sender {
    
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];
    [self.navigationController pushViewController:[storyBoard instantiateViewControllerWithIdentifier:@"LoginViewWithEventController"] animated:true];
    
}

- (IBAction)todaysButtonClicked:(id)sender {
}
@end
