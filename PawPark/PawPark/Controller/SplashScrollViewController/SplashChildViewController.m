

#import "SplashChildViewController.h"
#import "PWPConstant.h"

@interface SplashChildViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageViewComingSoon;
@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@end


#pragma mark -

@implementation SplashChildViewController


// load the view nib and initialize the pageNumber ivar
- (id)initWithPageNumber:(NSUInteger)page
{
    if (self = [super init])
    {
        _pageNumber = page;        
    }
    return self;
}


// set the label and background color when the view has finished loading
- (void)viewDidLoad
{
    [super viewDidLoad];
    
        self.numberImage.hidden=NO;
        self.labelDescription.hidden=NO;
        self.lableTitle.hidden=NO;
        self.imageview.hidden=YES;
    
    if(_pageNumber == 3){
        self.imageViewComingSoon.hidden = YES;
    }
    else{
        self.imageViewComingSoon.hidden = YES;
    }
    
    if (IS_IPHONE_5) {
        _viewHeightConstraints.constant = 80;
    }else if (IS_IPHONE_6) {
        _viewHeightConstraints.constant = 100;
    }
    else if (IS_IPHONE_6_PLUS) {
       _viewHeightConstraints.constant = 120;
    }
    
    
}

@end
