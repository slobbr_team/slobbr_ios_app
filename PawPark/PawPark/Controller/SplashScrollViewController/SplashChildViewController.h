
#import <UIKit/UIKit.h>

@interface SplashChildViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *labelDescription;
@property (nonatomic, weak) IBOutlet UILabel *lableTitle;
@property (nonatomic, weak) IBOutlet UIImageView *numberImage;
@property (nonatomic, weak) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeightConstraints;

@property (assign) NSInteger pageNumber;

- (id)initWithPageNumber:(NSUInteger)page;

@end
