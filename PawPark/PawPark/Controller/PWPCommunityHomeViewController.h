//
//  PWPCommunityHomeViewController.h
//  PawPark
//
//  Created by Samarth Singla on 13/08/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWPBaseViewController.h"


@protocol PWPCommunityHomeDelegate <NSObject>
-(void)commentCountUpdate:(NSInteger)commentCount withIndexPath:(NSIndexPath *)indexPath withPostId:(NSString *)postId;
@end

@interface PWPCommunityHomeViewController : PWPBaseViewController
@property(nonatomic, strong) NSDictionary * communitiesDictionary;
@end
