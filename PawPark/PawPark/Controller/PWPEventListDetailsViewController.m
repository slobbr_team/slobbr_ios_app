//
//  PWPEventListDetailsViewController.m
//  PawPark
//
//  Created by HupendraRaghuwanshi on 07/03/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPEventListDetailsViewController.h"
#import "PWPEventListingTableViewCell.h"
#import "UIViewController+UBRComponents.h"
#import "SXCUtility.h"
#import "PWPService.h"
#import "PWPApplicationUtility.h"
#import "UBRLocationHandler.h"
#import <UIImageView+WebCache.h>
#import "PWPDownloadBackgroundImage.h"
#import <Mixpanel.h>
#import <TwitterKit/TwitterKit.h>
#import <MessageUI/MessageUI.h>

@interface PWPEventListDetailsViewController ()<MFMailComposeViewControllerDelegate>
{
    NSString *dest_lat;
    NSString *dest_longi;

    __weak IBOutlet UIImageView * imageViewBG;

    NSString *sloberShareDetail;
    NSString *sloberShareDetailTwitter;
    NSMutableSet * errorSet;
    NSString *email;
    NSString *phone;
}
@end

@implementation PWPEventListDetailsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self sxc_navigationBarCustomizationWithTitle:@"EVENTS "];
    if(self.navigationController.viewControllers.count==1){
        [self sxc_setNavigationBarMenuItem];
    }
    else{
        [self sxc_customizeBackButton];
    }
    [self EventDetails];
    errorSet = [[NSMutableSet alloc] init];
    _fbWidthConstraints.constant = self.view.frame.size.width / 3;

    NSString * eventUrl = [PWPDownloadBackgroundImage eventBackgroundImageURL];
    if(eventUrl.length > 0){
        [imageViewBG sd_setImageWithURL:[NSURL URLWithString:eventUrl] placeholderImage: [PWPDownloadBackgroundImage backgroundImage]];
    }
    else{
        imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    }
}


-(void)EventDetails{
    
        [self startLoader];
        
        [[PWPService sharedInstance] eventDetailWithDictionary:[self eventDictionary] WithSuccessBlock:^(id response) {
            [self dismissLoader];
            
            _labelEventName.text= [response valueForKey:@"name"];
            [self sxc_navigationBarCustomizationWithTitle:_labelEventName.text];
            _labelEventDate.text= [self dateStringFromString:response[@"time"] sourceFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ" destinationFormat:@"MM-dd-yyyy hh:mm a"];
            _labelDescription.text= [response valueForKey:@"description"];
            dest_lat = response[@"lat"];
            dest_longi = response[@"lng"];
            if ([response valueForKey:@"address"] != nil)
            {
                _labelEventAddress.text=[NSString stringWithFormat:@"%@ %@ %@ %@",[response valueForKey:@"address"] ,[response valueForKey:@"address_locality"] ,[response valueForKey:@"address_region"] ,[response valueForKey:@"address_postal_code"]  ];
            }
            else{
                
                _labelEventAddress.text  = @"";
                
            }
            
            if ([response valueForKey:@"owner_name"] != nil)
            {
                _labelOwnerName.text=[response valueForKey:@"owner_name"];
            }
            else{
                _labelOwnerName.text  = @"";
            }
            
            if ([response valueForKey:@"owner_email"] != nil)
            {
                _labelEmail.text=[response valueForKey:@"owner_email"];
                email = [response valueForKey:@"owner_email"];
            }
            else{
                _labelEmail.text  = @"";
            }
            
            if ([response valueForKey:@"owner_phone"] != nil)
            {
                _labelPhone.text=[response valueForKey:@"owner_phone"];
                phone=[response valueForKey:@"owner_phone"];
            }
            else{
                _labelPhone.text  = @"";
            }
            
            
            
            if ([[response valueForKey:@"links"] isKindOfClass:[NSDictionary class]]){
                NSDictionary *links = [response valueForKey:@"links"];
                NSArray *keys = links.allKeys;
                
                if ([keys count] > 0 ){
                    _title2.text = keys.firstObject;
                    _labelUrl.text=[links valueForKey:keys.firstObject];
                    if ([keys count] > 1) {
                        _title1.text = keys[1];
                        _labelUrl2.text=[links valueForKey:keys[1]];
                    }

                }
                
            }
            
            NSString * imageUrl = response[@"thumbs"][@"w1000"];
            [_imageViewEvent sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"default_park"]];
            sloberShareDetailTwitter = [NSString stringWithFormat:@"Slobbr\n%@\n%@\n%@",[response valueForKey:@"name"],[self dateStringFromString:response[@"time"] sourceFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ" destinationFormat:@"MM-dd-yyyy hh:mm a"],_labelEventAddress.text];
            sloberShareDetail = [NSString stringWithFormat:@"Slobbr\n%@\n%@\n%@\n%@\n\nOwner Details\n%@\n%@\n%@",[response valueForKey:@"name"],_labelEventDate.text,_labelDescription.text,_labelEventAddress.text,[response valueForKey:@"owner_name"],[response valueForKey:@"owner_email"],[response valueForKey:@"owner_phone"]];
            
        } withErrorBlock:^(NSError *error) {
            [errorSet addObject:error.domain];
            if(errorSet.allObjects.count > 0 ){
                NSString * errorString = [errorSet.allObjects componentsJoinedByString:@"\n"];
                [self sxc_showErrorViewWithMessage:errorString];
                return;
            }
            [self dismissLoader];
        
        }];
        
    
}
- (IBAction)emailDidClicked:(id)sender {
    [[Mixpanel sharedInstance] track:@"Mail Clicked"];
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setToRecipients:@[email]];
        
        [mc setSubject:@"Slobbr"];
        //[mc setMessageBody:sloberShareDetail isHTML:NO];
        [self presentViewController:mc animated:YES completion:NULL];
    }
    else
    {
        [self sxc_showErrorViewWithMessage:@"Device is not configured with mail. Please check."];
    }
}
- (IBAction)phoneDidClicked:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phone]]];
}

- (NSString *)dateStringFromString:(NSString *)sourceString
                      sourceFormat:(NSString *)sourceFormat
                 destinationFormat:(NSString *)destinationFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:sourceFormat];
    NSDate *date = [dateFormatter dateFromString:sourceString];
    [dateFormatter setDateFormat:destinationFormat];
    return [dateFormatter stringFromDate:date];
}




- (IBAction)navigateClicked:(id)sender {
    
    NSString *current_lat = [NSString stringWithFormat:@"%f",[UBRLocationHandler shareHandler].currentLocation.coordinate.latitude];
    NSString *current_long = [NSString stringWithFormat:@"%f",[UBRLocationHandler shareHandler].currentLocation.coordinate.longitude];
    
    NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%@,%@&daddr=%@,%@",current_lat, current_long, dest_lat, dest_longi];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: directionsURL]];
}



- (IBAction)shareFacebook:(id)sender {
    [[Mixpanel sharedInstance] track:@"Facebook Clicked"];
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        
        //NSString *shareText = @"Slobbr http://www.slobbr.com";
        [mySLComposerSheet setInitialText:sloberShareDetail];
        
        //[mySLComposerSheet addURL:[NSURL URLWithString:@"http://www.slobbr.com"]];
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                default:
                    break;
            }
        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else{
        [self sxc_showErrorViewWithMessage:@"Please login with facebook in device settings. "];
    }
    
}

- (IBAction)shareTwitter:(id)sender {
    [[Mixpanel sharedInstance] track:@"Twitter Clicked"];
    
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    [composer setText:sloberShareDetailTwitter];
    //[composer setImage:[UIImage imageNamed:@"ic_paw_pack_home"]];
    
    [composer showWithCompletion:^(TWTRComposerResult result) {
    }];
}

- (IBAction)shareMail:(id)sender {
    [[Mixpanel sharedInstance] track:@"Mail Clicked"];
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        
        [mc setSubject:@"Slobbr"];
        [mc setMessageBody:sloberShareDetail isHTML:NO];
        [self presentViewController:mc animated:YES completion:NULL];
    }
    else
    {
        [self sxc_showErrorViewWithMessage:@"Device is not configured with mail. Please check."];
    }
}


#pragma mark - MailComposerDelegate methods

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            
            break;
        case MFMailComposeResultSaved:
            
            break;
        case MFMailComposeResultSent:
            
            break;
        case MFMailComposeResultFailed:
            
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (IBAction)link1Clicked:(id)sender {
    NSURL *url = [NSURL URLWithString:_labelUrl2.text];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (IBAction)link2Clicked:(id)sender {
    NSURL *url = [NSURL URLWithString:_labelUrl.text];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}


@end
