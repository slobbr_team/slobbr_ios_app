//
//  PWPEventListViewController.h
//  PawPark
//
//  Created by HupendraRaghuwanshi on 07/03/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface PWPEventListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;

@property(nonatomic,weak)IBOutlet UILabel *labelTopFilterInformation;


@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet UIButton *buttonHome;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *filterButton;
@property (nonatomic,strong) NSMutableDictionary *localEvents;
@property (nonatomic,strong) NSMutableDictionary *allEvents;



@end
