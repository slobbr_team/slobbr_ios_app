//
//  PWPParkSelectionViewController.m
//  PawPark
//
//  Created by xyz on 3/25/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPParkSelectionViewController.h"
#import "UIViewController+UBRComponents.h"
#import "SXCUtility.h"
#import <Google/Analytics.h>
#import "UIColor+PWPColor.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPParkSelectionViewController ()
{
    __weak IBOutlet UIImageView * imageViewBG;
}

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property(nonatomic,weak)IBOutlet UITableView * tableView;
@property(nonatomic,strong)NSMutableArray * searchedParksArray;

@end

@implementation PWPParkSelectionViewController

- (void)viewDidLoad {
  
    [super viewDidLoad];
    [self sxc_customizeBackButton];
    [self sxc_navigationBarCustomizationWithTitle:@"Select Park"];
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ParkCell"];
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [_parksArray sortedArrayUsingDescriptors:sortDescriptors];
    
    _parksArray=[sortedArray copy];
    _searchedParksArray=[_parksArray copy];
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"ZipCode Selection Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegate and Datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"ParkCell"];
    cell.textLabel.text=[_searchedParksArray objectAtIndex:indexPath.row][@"name"];
    
    
    cell.backgroundColor =[UIColor clearColor];
    cell.contentView.backgroundColor =[UIColor clearColor];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font =[UIFont fontWithName:@"Roboto-Regular" size:14.0f];
    
    UIView *customColorView = [[UIView alloc] init];
    customColorView.backgroundColor = [UIColor sxc_themeColorWithAplha];
    cell.selectedBackgroundView =  customColorView;

    if([[_selectedPark[@"id"] description] isEqualToString:[_searchedParksArray[indexPath.row][@"id"] description]]){
        cell.accessoryType=UITableViewCellAccessoryCheckmark;
    }
    else{
        cell.accessoryType=UITableViewCellAccessoryNone;
    }
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _searchedParksArray.count;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(_delegate &&[_delegate respondsToSelector:@selector(parkSelected:)]){
        [_delegate parkSelected:_searchedParksArray[indexPath.row]];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.view endEditing:YES];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if(searchText.length==0){
        _searchedParksArray=[_parksArray copy];
    }
    else{
        
        NSPredicate * predicate=[NSPredicate predicateWithFormat:@"name CONTAINS[cd]%@",searchText];
        _searchedParksArray=(id)[_parksArray filteredArrayUsingPredicate:predicate];
    }
    [_tableView reloadData];
    
}
@end
