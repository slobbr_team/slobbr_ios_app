//
//  PWPBreedSelectionViewController.h
//  PawPark
//
//  Created by xyz on 20/07/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPBreedSelectionDelegate <NSObject>
-(void)breedSelected:(NSMutableDictionary *)zipCodeDictionary;
@end

@interface PWPBreedSelectionViewController : UIViewController
@property(nonatomic,weak)id<PWPBreedSelectionDelegate> delegate;
@property(nonatomic,strong)NSString * from;

@end
