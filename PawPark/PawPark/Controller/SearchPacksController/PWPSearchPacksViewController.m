//
//  PWPSearchPacksViewController.m
//  PawPark
//
//  Created by daffomac on 4/26/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPSearchPacksViewController.h"
#import "UIViewController+UBRComponents.h"
#import "PWPService.h"
#import "PWPPackFilter.h"
#import "PWPSearchPackCellTableViewCell.h"
#import "PWPSearchPacksDelegate.h"
#import "PWPSearchPackFilterViewController.h"
#import "PWPApplicationUtility.h"
#import "SXCTableViewLoadMoreDelegate.h"
#import "SXCUtility.h"
#import <Google/Analytics.h>
#import "PWPDownloadBackgroundImage.h"
#import "PWPDogPicViewController.h"
#import "SXCUtility.h"
#import <Mixpanel.h>

#define K_PACKS_SEARCH_LIMIT_SIZE @10

@interface PWPSearchPacksViewController ()<UITableViewDataSource,UITableViewDelegate,PWPSearchPacksDelegate,PWPSearckPackDelegate,UIAlertViewDelegate,SXCLoadMoreDelegate>
{
    __weak IBOutlet UITableView * tableViewSearchPacks;
    __weak IBOutlet UILabel *labelNoData;
    __weak IBOutlet UIImageView * imageViewBG;
    
    NSInteger selectedIndexPath;
    NSMutableArray * packsArray;
    NSString * toString;
    NSString * fromString;

    SXCTableViewLoadMoreDelegate * tableViewLoadMoreDelegate;

    PWPPackFilter * packFilter;

}
@end

@implementation PWPSearchPacksViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    [self initView];
    [self initViewTableviewLoadMoreDelegate];
    [self searchPacksWithOffset:@0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Search Dog Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
#pragma mark - Navigation Methods

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"segue_pack_filters"]){

        [[Mixpanel sharedInstance] track:@"Filter Clicked On Pals Page"];

        PWPSearchPackFilterViewController * searchPackController=[segue destinationViewController];
        searchPackController.delegate=self;
        searchPackController.packFilter=packFilter;
    
    }
}

#pragma mark - BL Methods

-(void)initViewTableviewLoadMoreDelegate
{
    tableViewLoadMoreDelegate=[[SXCTableViewLoadMoreDelegate alloc] initWithTableView:tableViewSearchPacks WithTotalCount:@0 WithPageSize:K_PACKS_SEARCH_LIMIT_SIZE WithPageIndex:@0];
    
    tableViewLoadMoreDelegate.loadMoreDelegate=self;
    tableViewLoadMoreDelegate.delegate=self;
    tableViewLoadMoreDelegate.dataSource=self;
    [tableViewLoadMoreDelegate reloadData];
    [tableViewLoadMoreDelegate addPullToRefresh];
    [tableViewLoadMoreDelegate automaticallyRefreshControllOnView];
    
}

-(NSMutableDictionary *)requestDictionaryForOffset:(NSNumber *)offset
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] initWithDictionary:packFilter.toDictionary];

    [requestDictionary removeObjectForKey:@"stateCode"];
    [requestDictionary setValue:K_PACKS_SEARCH_LIMIT_SIZE forKey:@"limit"];
    [requestDictionary setValue:offset forKey:@"offset"];
    [requestDictionary setValue:[SXCUtility getNSObject:@"apikey"] forKey:@"apikey"];
    return requestDictionary;
}


-(void)initView{

    [self sxc_navigationBarCustomizationWithTitle:@"FIND PALS"];
    
    [self sxc_setNavigationBarCrossItem];

    selectedIndexPath=-1;

    packFilter=[[PWPPackFilter alloc] initWithDictionary:[NSDictionary new] error:nil];
}

-(NSDictionary *)requestDictionaryForPack
{
    return packFilter.toDictionary;
}

#pragma mark - Service Methods
-(void)searchPacksWithOffset:(NSNumber *)offset
{
    
    [[PWPService sharedInstance] searchPacksWithRequestDictionary:(id)[self requestDictionaryForOffset:offset] withLimitStart:offset withSuccessBlock:^(id response,NSNumber * limitStart) {

        if([limitStart isEqualToNumber:@0])
            packsArray=[[NSMutableArray alloc] initWithArray:response];
       else
           [packsArray addObjectsFromArray:response];
        
        if(packsArray.count==(K_PACKS_SEARCH_LIMIT_SIZE.integerValue+limitStart.integerValue)){
            tableViewLoadMoreDelegate.totalCount=[NSNumber numberWithInteger:packsArray.count+1];
        }
        else{
            tableViewLoadMoreDelegate.totalCount=[NSNumber numberWithInteger:packsArray.count];
        }
        
        tableViewLoadMoreDelegate.limitstart=limitStart;
        tableViewLoadMoreDelegate.currentArrayCount=[NSNumber numberWithInteger:packsArray.count];;
        [tableViewLoadMoreDelegate reloadData];
        
        if(packsArray.count==0){
            labelNoData.text=@"No Results Found";
            labelNoData.hidden=NO;
            [self.view bringSubviewToFront:labelNoData];
        }
        else{
            labelNoData.hidden=YES;
        }
        
        
        
    } withErrorBlock:^(NSError *error,NSNumber * limitStart) {
        
        [tableViewLoadMoreDelegate reloadData];
        
        [self sxc_showErrorViewWithMessage:error.domain];

        if(packsArray.count==0){
            labelNoData.text=@"No Results Found";
            labelNoData.hidden=NO;
            [self.view bringSubviewToFront:labelNoData];
        }
        else{
            labelNoData.hidden=YES;
        }

    }];

}

-(void)sendFriendRequestServiceCallWithData:(id)data fromDogId:(NSString *)fromDogId
{
    fromString=fromDogId;
    toString=data[@"id"];

    [[Mixpanel sharedInstance] track:@"Add Dog Clicked On Pals Page" properties:@{@"DogID":toString}];
    
    [self startLoader];
    
    [[PWPService sharedInstance]sendDogFriendsRequest:data[@"id"] fromDog:fromDogId withSuccessBlock:^(id responseDicitionary) {
        
        [self dismissLoaderWithSuccessText:@"Request has been sent successfully."];
        
    } withErrorBlock:^(NSError *error) {

        if(error.code==403){
            [self sxc_showAlertViewWithTitle:@"Error" message:@"You have already sent the friend request to this dog. Do you want to cancel the friend reqeust?" WithDelegate:self WithCancelButtonTitle:@"NO" WithAcceptButtonTitle:@"YES"];
            [self dismissLoader];
        }else{
            [self dismissLoaderWithErrorText:@"An error has occurred. Please try again"];
        }
    }];
}


#pragma mark- TableView Delegate Methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PWPSearchPackCellTableViewCell * tableViewCell =[tableView dequeueReusableCellWithIdentifier:@"PWPSearchPawsCells"];
    [tableViewCell configureCellWithData:packsArray[indexPath.row]];
    tableViewCell.clipsToBounds=YES;
    tableViewCell.delegate=self;
    tableViewCell.isFromSearch=YES;

    return tableViewCell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return packsArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * kuttaDictionary = packsArray[indexPath.row];
    
    if([kuttaDictionary valueForKey:@"last_checkin"] && [[kuttaDictionary valueForKey:@"last_checkin"] isKindOfClass:[NSDictionary class]] && indexPath.row==selectedIndexPath)
    {
        return 208.0f;
    }
    else if(indexPath.row==selectedIndexPath){
        return 165.0f;
    }
    return 95.0f;
    
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * kuttaDictionary = packsArray[indexPath.row];
    
    if([kuttaDictionary valueForKey:@"last_checkin"] && [[kuttaDictionary valueForKey:@"last_checkin"] isKindOfClass:[NSDictionary class]] && indexPath.row==selectedIndexPath)
    {
        return 208.0f;
    }
    else if(indexPath.row==selectedIndexPath){
        return 165.0f;
    }
    return 95.0f;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[Mixpanel sharedInstance] track:@"Dog Clicked On Pals Page"];
    
    NSInteger previousSelectedIndex = selectedIndexPath;
    
    if(indexPath.row!=selectedIndexPath){
        selectedIndexPath=indexPath.row;
    }
    else{
        selectedIndexPath=-1;
    }

    NSMutableArray * indexPaths = [[NSMutableArray alloc] init];
    if(selectedIndexPath != -1){
        [indexPaths addObject:[NSIndexPath indexPathForRow:selectedIndexPath inSection:0]];
    }
    if(previousSelectedIndex != -1){
        [indexPaths addObject:[NSIndexPath indexPathForRow:previousSelectedIndex inSection:0]];
    }
    
    [tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void)tableView:(UITableView *)tableView loadMoreWithNextPageIndex:(NSNumber *)nextPageIndex WithPageSize:(NSNumber *)pageSize{

    [self searchPacksWithOffset:nextPageIndex];
}

#pragma mark - PWPSearchPacksDelegate
-(void)applyFiltersAndRefreshSearch:(PWPPackFilter *)aPackFilter
{
    selectedIndexPath=-1;
    
    packFilter = aPackFilter;
    [tableViewLoadMoreDelegate automaticallyRefreshControllOnView];
    [self searchPacksWithOffset:@0];
}

-(void)optionPackClickedWithData:(id)data
{
    if([PWPApplicationUtility getCurrentUserDogsArray].count==0){
        [self sxc_showErrorViewWithMessage:@"Please add a dog to your packs."];
        return;
    }

    [super selectDogFromPackToSendRequest:^(NSString * dogId) {
        
        [self sendFriendRequestServiceCallWithData:data fromDogId:dogId];
    } withCancelblock:^{
    }];
   
}

-(void)option5PackClickedWithData:(id)data
{
    [[Mixpanel sharedInstance] track:@"Photos Clicked On Pals Page" properties:@{@"DogID":data[@"id"]}];

    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    PWPDogPicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPDogPicViewController"];
    viewController.dogsPics = [SXCUtility cleanArray:[data valueForKey:@"gallery"]].mutableCopy;
    viewController.data = data;
    viewController.dogId = data[@"id"];

    [self.navigationController pushViewController:viewController animated:YES];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1){
    
        [[PWPService sharedInstance] cancelDogFriendsRequest:toString fromDog:fromString withSuccessBlock:^(id responseDicitionary) {

            [self dismissLoaderWithSuccessText:@"Request has been cancelled successfully."];
            
        } withErrorBlock:^(NSError *error) {
            
            [self dismissLoaderWithErrorText:@"An error has occurred. Please try again"];

        }];
    }
}
-(void)sxc_stopLoading{}
-(void)sxc_startLoading{}

@end
