//
//  PWPEventsCollectionViewCell.m
//  PawPark
//
//  Created by Samarth Singla on 21/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPEventsCollectionViewCell.h"
#import "SXCUtility.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPEventsCollectionViewCell()
{
    __weak IBOutlet UILabel *labelEventTitle;
    __weak IBOutlet UILabel * labelEventDescription;
    __weak IBOutlet UILabel * labelEventLocation;
    __weak IBOutlet UILabel * labelEventDate;
    __weak IBOutlet UIImageView *imageViewAdv;
}
@end


@implementation PWPEventsCollectionViewCell

-(void)configureWithData:(NSDictionary *)dictionary
{

    UIImage * advertisementImage = [PWPDownloadBackgroundImage itemBG];
    if(advertisementImage != nil){
        imageViewAdv.image =  advertisementImage;
        imageViewAdv.hidden = false;
        imageViewAdv.contentMode = UIViewContentModeScaleAspectFill;
    }
    else{
        imageViewAdv.hidden = true;
    }


    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];

    NSDate * date =[dateFormatter dateFromString:dictionary[@"time"]];
    [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm a"];

    labelEventTitle.text=dictionary[@"name"];
    labelEventLocation.text=[NSString stringWithFormat:@"%@, %@, %@, %@",dictionary[@"address"],dictionary[@"address_locality"],dictionary[@"address_region"],dictionary[@"address_postal_code"]];

    labelEventDate.text=[dateFormatter stringFromDate:date];
    labelEventDescription.text=dictionary[@"description"];
}

-(void)configureForActivities:(NSDictionary *)dictionary {


    UIImage * advertisementImage = [PWPDownloadBackgroundImage itemBG];
    if(advertisementImage != nil){
        imageViewAdv.image =  advertisementImage;
        imageViewAdv.hidden = false;
    }
    else{
        imageViewAdv.hidden = true;
    }


    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];

    NSDate * date =[dateFormatter dateFromString:dictionary[@"created_at"]];
    [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm a"];

    labelEventTitle.text=dictionary[@"message"];
    labelEventLocation.text=[NSString stringWithFormat:@"%@, %@",dictionary[@"place"][@"name"],dictionary[@"place"][@"address"]];
    labelEventDate.text=[dateFormatter stringFromDate:date];
    labelEventDescription.text=[NSString stringWithFormat:@"%lu dogs here",(unsigned long)[SXCUtility cleanArray:dictionary[@"dogs"]].count];
}


@end
