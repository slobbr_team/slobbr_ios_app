//
//  PWPPlaceCollectionViewCell.h
//  PawPark
//
//  Created by Samarth Singla on 21/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPPlaceCollectionViewDelegate <NSObject>

-(void)parkOptionClickedWithData:(NSDictionary *)dictionary;
-(void)parkOption2ClickedWithData:(NSDictionary *)dictionary;
-(void)parkOption3ClickedWithData:(NSDictionary *)dictionary;

-(void)inviteOptionClicked:(NSDictionary *)dictionary;
-(void)parkQuickCheckInClicked:(NSDictionary *)dictionary;

@optional
-(void)staOptionClickedWithData:(NSDictionary *)dictionary;

@end



@interface PWPPlaceCollectionViewCell : UICollectionViewCell

@property(nonatomic,assign)id <PWPPlaceCollectionViewDelegate> delegate;
-(void)bindValueWithComponentsWithData:(NSMutableDictionary *)data;

@end
