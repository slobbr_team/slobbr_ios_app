//
//  PWPAdvertismentCell.h
//  PawPark
//
//  Created by Samarth Singla on 21/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPAdvertismentCellDelegate <NSObject>

-(void)parkOptionClickedWithData:(NSDictionary *)dictionary;
-(void)parkOption2ClickedWithData:(NSDictionary *)dictionary;
-(void)parkOption3ClickedWithData:(NSDictionary *)dictionary;

-(void)inviteOptionClicked:(NSDictionary *)dictionary;
-(void)parkQuickCheckInClicked:(NSDictionary *)dictionary;
-(void)optionPackClickedWithData:(id)data;
-(void)option5PackClickedWithData:(id)data;


@optional
-(void)staOptionClickedWithData:(NSDictionary *)dictionary;

@end


@interface PWPAdvertismentCell : UITableViewCell
- (void)configureWithData:(NSMutableArray *)advertisement;

@property(nonatomic,assign)id<PWPAdvertismentCellDelegate> delegate;
@end
