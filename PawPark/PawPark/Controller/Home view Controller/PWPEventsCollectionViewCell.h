//
//  PWPEventsCollectionViewCell.h
//  PawPark
//
//  Created by Samarth Singla on 21/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPEventsCollectionViewCell : UICollectionViewCell
-(void)configureWithData:(NSDictionary *)dictionary;
-(void)configureForActivities:(NSDictionary *)dictionary;
@end
