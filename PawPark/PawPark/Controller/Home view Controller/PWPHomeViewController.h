//
//  PWPHomeViewController.h
//  PawPark
//
//  Created by daffomac on 4/26/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWPBaseViewController.h"

@interface PWPHomeViewController : PWPBaseViewController
@property(nonatomic,assign)BOOL isFromRegistration;
@property (nonatomic, retain) UIDocumentInteractionController *dic;

@end
