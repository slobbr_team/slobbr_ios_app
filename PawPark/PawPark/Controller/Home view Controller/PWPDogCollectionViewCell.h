//
//  PWPDogCollectionViewCell.h
//  PawPark
//
//  Created by Samarth Singla on 21/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPDogCollectioViewCellDelegate

-(void)optionPackClickedWithData:(id)data;

@optional
-(void)option5PackClickedWithData:(id)data;

@end


@interface PWPDogCollectionViewCell : UICollectionViewCell

@property(nonatomic,assign)id <PWPDogCollectioViewCellDelegate> delegate;

-(void)configureCellWithData:(NSMutableDictionary *)dictionary;

@end
