//
//  PWPPlaceCollectionViewCell.m
//  PawPark
//
//  Created by Samarth Singla on 21/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPPlaceCollectionViewCell.h"
#import "SXCUtility.h"
#import "UIColor+PWPColor.h"
#import "PWPConstant.h"
#import <UIImageView+WebCache.h>
#import "PWPImageInfoViewController.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPPlaceCollectionViewCell()

@property(nonatomic,strong)NSMutableDictionary * dataInfo;

@property (weak, nonatomic) IBOutlet UILabel *labelParkName;
@property (weak, nonatomic) IBOutlet UILabel *labelParkNumberDogs;
@property (weak, nonatomic) IBOutlet UILabel *labelOpeningHours;
@property (weak, nonatomic) IBOutlet UILabel *labelParkAddress;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewPark;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewAdv;

@property (weak, nonatomic) IBOutlet UIButton *buttonFavorite;
@property (weak, nonatomic) IBOutlet UIButton *buttonQuickCheckIn;


- (IBAction)checkButtonClicked:(id)sender;
- (IBAction)buttonHomeClicked:(id)sender;
@end



@implementation PWPPlaceCollectionViewCell


-(void)bindValueWithComponentsWithData:(NSMutableDictionary *)data{


    UIImage * advertisementImage = [PWPDownloadBackgroundImage itemBG];
    if(advertisementImage != nil){
        self.imageViewAdv.image =  advertisementImage;
        self.imageViewAdv.hidden = false;
        self.imageViewAdv.contentMode = UIViewContentModeScaleAspectFill;
    }
    else{
        self.imageViewAdv.hidden = true;
    }

    _buttonQuickCheckIn.hidden = false;

    if([SXCUtility cleanInt:data[@"is_favourite"]] == 1){
        [_buttonFavorite setBackgroundColor:[UIColor sxc_themeColor]];
        [_buttonFavorite setTintColor:[UIColor whiteColor]];
    }
    else{
        [_buttonFavorite setBackgroundColor:[UIColor whiteColor]];
        [_buttonFavorite setTintColor:[UIColor grayColor]];
    }


    _dataInfo=data;

    _labelParkName.text=data[@"name"];
    _labelParkAddress.text=[NSString stringWithFormat:@"%@, %@, %@",data[@"address"],data[@"address_locality"],data[@"address_postal_code"]];

    if([data valueForKey:@"active_dogs_count"])
    {
        NSString * checkInDogsCount = [SXCUtility cleanString:[[data valueForKey:@"active_dogs_count"] description]];

        _labelParkNumberDogs.text=[NSString stringWithFormat:@"%@ dogs here",checkInDogsCount];
    }
    else{
        _labelParkNumberDogs.text=@"0 dogs here";
    }

    if([data valueForKey:@"operating_hours"]&&([SXCUtility cleanString:[data valueForKey:@"operating_hours"]].length>0)){
        _labelOpeningHours.text=[NSString stringWithFormat:@"Opening Hours: %@",data[@"operating_hours"]];
    }
    else{
        _labelOpeningHours.text=[NSString stringWithFormat:@"Opening Hours: Not Available"];
    }


    NSString * imageUrl=[[NSString alloc] initWithFormat:@"%@%@",K_URL_HOSTNAME,data[@"image_url"]];

    imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"uploads/park//" withString:@"media/cache/park_thumb/"];

    [_imageViewPark setContentMode:UIViewContentModeScaleAspectFill];
    [_imageViewPark setBackgroundColor:[UIColor clearColor]];

    [_imageViewPark sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"default_park"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

        if(!error){
            [_imageViewPark setBackgroundColor:[UIColor clearColor]];
        }
    }];


    NSArray * amenities=[SXCUtility cleanArray:data[@"amenities"]];

    NSMutableString * amentiesString=[[NSMutableString alloc] init];

    for(NSString * amenity in amenities){
        if(amentiesString.length>0){
            [amentiesString appendString:@"\n"];
        }
        [amentiesString appendString:amenity];
    }

}


-(CGFloat)getHeightOfLabelWithText:(NSString *)text withFont:(UIFont *)font{
    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    gettingSizeLabel.font = font;
    gettingSizeLabel.text = text;
    gettingSizeLabel.numberOfLines = 0;
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(self.frame.size.width-16, 9999);

    CGSize expectSize = [gettingSizeLabel sizeThatFits:maximumLabelSize];
    return expectSize.height;
}

- (IBAction)checkButtonClicked:(id)sender {
    if(_delegate&&[_delegate respondsToSelector:@selector(parkOptionClickedWithData:)]){
        [_delegate parkOptionClickedWithData:_dataInfo];
    }
}

- (IBAction)buttonHomeClicked:(id)sender {
    if(_delegate&&[_delegate respondsToSelector:@selector(parkOption3ClickedWithData:)]){
        [_delegate parkOption3ClickedWithData:_dataInfo];
    }
}

-(IBAction)numberOfDogsClicked:(id)sender{
    if(_delegate&&[_delegate respondsToSelector:@selector(parkOption2ClickedWithData:)]){
        [_delegate parkOption2ClickedWithData:_dataInfo];
    }
}
- (IBAction)inviteButtonClicked:(id)sender {

    if(_delegate&&[_delegate respondsToSelector:@selector(inviteOptionClicked:)]){
        [_delegate inviteOptionClicked:_dataInfo];
    }

}

- (IBAction)quuickCheckInButtonClicked:(id)sender {

    if(_delegate&&[_delegate respondsToSelector:@selector(parkQuickCheckInClicked:)]){
        [_delegate parkQuickCheckInClicked:_dataInfo];
    }

}

- (IBAction)infoButtonClicked:(id)sender {

    UIStoryboard * storyBoard =[UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];

    PWPImageInfoViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPImageInfoViewController"];
    viewController.dictionary = self.self.dataInfo;

    UINavigationController *navigationController =[[UINavigationController alloc] initWithRootViewController:viewController];

    navigationController.navigationBar.translucent=YES;

    [self.window.rootViewController presentViewController:navigationController animated:YES completion:nil];
}



@end
