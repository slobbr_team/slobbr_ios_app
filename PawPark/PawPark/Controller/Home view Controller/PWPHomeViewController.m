//
//  PWPHomeViewController.m
//  PawPark
//
//  Created by daffomac on 4/26/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPCreateMessageViewController.h"
#import "PWPHomeViewController.h"
#import "UIViewController+UBRComponents.h"
#import "PWPHomeParkTableViewCell.h"
#import "PWPHomeTellFriendsTableViewCell.h"
#import "PWPService.h"
#import "PWPApplicationUtility.h"
#import "PWPAddDogViewController.h"
#import "PWPParkCheckInViewController.h"
#import "PWPSearchParkViewController.h"
#import "PWPHomeMyDogTableViewCell.h"
#import "PWPSchedulePawDateViewController.h"
#import "UIColor+PWPColor.h"
#import "PWPHomeSponsoringCell.h"
#import <TwitterKit/TwitterKit.h>
#import <MessageUI/MessageUI.h>
#import "SXCUtility.h"
#import "PWPMessageListViewController.h"
#import "PWPKibbleCounterViewController.h"
#import <Google/Analytics.h>
#import "PWPEventTableViewCell.h"
#import "PWPParkCheckedInDogsViewController.h"
#import "UBRLocationHandler.h"
#import "PWPHomeCell.h"
#import "PWPNoParkCell.h"
#import "PWPAppDelegate.h"
#import "MainViewController.h"
#import "PWPParkSwitchViewController.h"
#import <XCDYouTubeKit/XCDYouTubeKit.h>
#import "PWPSearchParkMapViewController.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPCheckInHelper.h"
#import "PWPCheckInArroundMeViewController.h"
#import "PWPSelectStatusViewController.h"
#import "PWPDogPicViewController.h"
#import "PWPPlacePicViewController.h"
#import "PWPAdvertismentCell.h"
#import <Mixpanel.h>

@interface PWPHomeViewController ()<UITableViewDataSource,UITableViewDelegate,PWPParkCheckInDelegae,PWPAddDogViewDelegate,PWPHomeDogDelegate,UIAlertViewDelegate,MFMailComposeViewControllerDelegate,PWPHomeCellDelegate, PWPAdvertismentCellDelegate, PWPPlacePicViewControllerDelegate>
{
    __weak IBOutlet UIImageView *imageViewBg;
    
    __weak IBOutlet UIView *viewHomeHeader;
    
    NSMutableArray * homeParkDictionary;
    NSMutableArray * currentUserPacks;
    NSMutableArray * advertisements;

    NSArray * events;
    
    NSString * sponsorImage;
    NSString * sponsorName;
    NSString * sponsorUrl;
    
    NSString * aSponsoreeImage;
    NSString * aSponsoreeName;
    NSString * aSponsoreeUrl;
    NSString * toDogIdString;
    NSString * fromDogIdString;

    NSDictionary *deletePackDictionary;
    NSDictionary * eventDictionary;
    
    NSMutableString * selectedStatus;
    NSMutableSet * errorSet;

    BOOL isLoading;
    
}
- (IBAction)mailButtonClicked:(id)sender;
- (IBAction)twitterButtonClicked:(id)sender;
- (IBAction)facebookButtonClicked:(id)sender;
- (IBAction)findParkButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIRefreshControl * refreshControl;

@end

@implementation PWPHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self addPullToRefresh];
    [_tableView setHidden:YES];
    
    errorSet = [[NSMutableSet alloc] init];

    _isFromRegistration=YES;

     selectedStatus = [SXCUtility cleanString:[SXCUtility getNSObject:@"quickCheckinDefaultMessage"]].mutableCopy;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"PWPHomeCell" bundle:nil] forCellReuseIdentifier:@"PWPHomeCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"PWPNoParkCell" bundle:nil] forCellReuseIdentifier:@"PWPNoParkCell"];
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [SXCUtility saveNSObject:selectedStatus forKey:@"quickCheckinDefaultMessage"];

    [_tableView reloadData];

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Home Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(currentUserPacks){
        [self automaticallyRefresh];
    }
    else{
        [self showLoader];
    }
    [self services];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark- TableView Delegate Methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if((indexPath.section==0)&&(indexPath.row==0)&&(homeParkDictionary.count>0)){
        PWPHomeCell * tableViewCell =[tableView dequeueReusableCellWithIdentifier:@"PWPHomeCell"];
        tableViewCell.delegate = self;
        [tableViewCell configureCellWithData:homeParkDictionary];
        return tableViewCell;
    }
    else if((indexPath.section==0)&&(indexPath.row==0)&&(homeParkDictionary.count == 0)){
        PWPNoParkCell *tableViewcell = [tableView dequeueReusableCellWithIdentifier:@"PWPNoParkCell"];
        tableViewcell.btnFavPlace.tag=indexPath.row;
        [tableViewcell.btnFavPlace addTarget:self action:@selector(aMethod:) forControlEvents:UIControlEventTouchDown];
        return tableViewcell;
    }
    else if((indexPath.section==5)&&(indexPath.row==0)){
        PWPHomeTellFriendsTableViewCell * tableViewCell=[tableView dequeueReusableCellWithIdentifier:@"PWPHomeTellFriendsTableViewCell"];
        [tableViewCell configueWithData:nil];
        return tableViewCell;
    }
    else if(indexPath.section==1 && indexPath.row == 0){
        UITableViewCell * tableViewCell=[tableView dequeueReusableCellWithIdentifier:@"PWPCurrentLocation"];
        return tableViewCell;
    }
    
    else if(indexPath.section==1 && indexPath.row == 1){
        UITableViewCell * tableViewCell=[tableView dequeueReusableCellWithIdentifier:@"PWPEventPark"];
        return tableViewCell;
    }
    else if(indexPath.section==3 && indexPath.row == 0){
        PWPHomeMyDogTableViewCell * tableViewCell=[tableView dequeueReusableCellWithIdentifier:@"PWPHomeMyDogCell2"];
        tableViewCell.delegate=self;
        
        [tableViewCell configueCellWithData:currentUserPacks[indexPath.row]];
        return tableViewCell;
    }
    else if(indexPath.section ==3&&indexPath.row !=0){
        PWPHomeMyDogTableViewCell * tableViewCell=[tableView dequeueReusableCellWithIdentifier:@"PWPHomeMyDogCell"];
        tableViewCell.delegate=self;
        [tableViewCell configueCellWithData:currentUserPacks[indexPath.row]];
        
        return tableViewCell;
    }
    else if(indexPath.section==2){
        PWPAdvertismentCell * tableViewCell=[tableView dequeueReusableCellWithIdentifier:@"PWPAdvertismentCell"];
        tableViewCell.delegate = self;
        [tableViewCell configureWithData:advertisements];
        return tableViewCell;
    }
    else if(indexPath.section==4){
        
        PWPEventTableViewCell * tableViewCell=[tableView dequeueReusableCellWithIdentifier:@"PWPEventsCell"];
        [tableViewCell configureWithData:events[indexPath.row]];
        return tableViewCell;
    }
    return nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(errorSet.allObjects.count > 0 ){
        return 0;
    }

    if(section==0)
        return 1;
    else if(section==1)
        return 2;
    else if(section==3)
        return currentUserPacks.count;
    else if(section==5)
        return 1;
    else if(section==4)
        return events.count;
    else if(section==2)
        return 1;
    return 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(errorSet.allObjects.count > 0 ){
        return 0;
    }

    return 7;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(errorSet.allObjects.count > 0 ){
        return 0;
    }

    if(indexPath.section==0&&indexPath.row==0&&homeParkDictionary.count>0){
        return 290.0f;
    }
    else if(indexPath.section==0&&indexPath.row==0&&homeParkDictionary.count ==0){
        return 78.0f;
        
    }
    else if(indexPath.section==5&&indexPath.row==0){
        return 62.0f;
    }
    else if(indexPath.section==3&&indexPath.row ==0){
        return 136.0f;
    }
    else if(indexPath.section ==3&&indexPath.row !=0){
        return 99.0f;
    }
    else if((indexPath.section==1)&& indexPath.row == 1&& ([[[SXCUtility getNSObject:@"eventMode"] description] boolValue])){
        return 49.0f;
    }
    else if((indexPath.section==1)&& indexPath.row == 1&& (![[[SXCUtility getNSObject:@"eventMode"] description] boolValue])){
        return 0.0f;
    }
    else if(indexPath.section==1 && indexPath.row == 0){
        return  49.0f;
    }
    else if(indexPath.section==2)
    {
        return 226.0f;
    }
    else if(indexPath.section==4)
    {
        return 156.0f;
    }
    return 0.0f;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    if(errorSet.allObjects.count > 0 ){
        return 0;
    }

    return 1.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    if(errorSet.allObjects.count > 0 ){
        return 0;
    }

    return 1.0f;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(errorSet.allObjects.count > 0 ){
        return 0;
    }

    if(indexPath.section==0&&indexPath.row==0&&homeParkDictionary.count>0){
        return 290.0f;
    }
    else if(indexPath.section==0&&indexPath.row==0&&homeParkDictionary.count == 0){
        return 180.0f;
    }
    else if(indexPath.section==5&&indexPath.row==0){
        return 62.0f;
    }
    else if(indexPath.section==3&&indexPath.row ==0){
        return 153.0f;
    }
    else if(indexPath.section ==3&&indexPath.row !=0){
        return 123.0f;
    }
    else if((indexPath.section==1)&& indexPath.row == 1&& ([[[SXCUtility getNSObject:@"eventMode"] description] boolValue])){
        return 49.0f;
    }
    else if((indexPath.section==1)&& indexPath.row == 1&& (![[[SXCUtility getNSObject:@"eventMode"] description] boolValue])){
        return 0.0f;
    }
    else if(indexPath.section==1 && indexPath.row == 0 &&  (![[[SXCUtility getNSObject:@"isLocationHelp"] description] boolValue]))
    {
        return  0.0f;
    }
    else if(indexPath.section==1 && indexPath.row == 0 &&  ([[[SXCUtility getNSObject:@"isLocationHelp"] description] boolValue]))
    {
        return  49.0f;
    }
    else if(indexPath.section==2)
    {
        return 226.0f;
    }
    else if(indexPath.section==4)
    {
        return 156.0f;
    }
    return 0.0f;
}

- (CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    
    
    CGSize size= [events[indexPath.row][@"description"] boundingRectWithSize:CGSizeMake(_tableView.frame.size.width-52, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:14.0f] }  context:nil].size;

    if(size.height>37){
        return 140+size.height-37;
    }
    else{
        return 140.0f;
    }
    
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingExpandedSize];
    return size.height + 1.0f; // Add 1.0f for the cell separator height
}



#pragma mark - Buisness Methods

-(void)initView{
    [self sxc_setNavigationBarMenuItem];
    [self sxc_navigationBarCustomizationWithTitle:@"SLOBBR HOME"];
    
    imageViewBg.image = [PWPDownloadBackgroundImage backgroundImage];
}

-(void)addPullToRefresh
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor=[UIColor sxc_themeColor];
    [self.refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:self.refreshControl];
}
- (void)refreshView:(UIRefreshControl *)sender {
    [self services];
}

-(void)automaticallyRefresh{
    [_tableView setContentOffset:CGPointMake(0, -self.refreshControl.frame.size.height) animated:YES];
    [self.refreshControl beginRefreshing];
}

-(void)shareUsingTwitter{

    [[Mixpanel sharedInstance] track:@"Twitter Clicked"];

    TWTRComposer *composer = [[TWTRComposer alloc] init];
    [composer setText:@"Slobbr http://www.slobbr.com #.VWnIOCAgeiw.twitter"];
    [composer setImage:[UIImage imageNamed:@"ic_paw_pack_home"]];
    
    [composer showWithCompletion:^(TWTRComposerResult result) {
    }];
    
}

-(void)shareUsingFacebook
{
    [[Mixpanel sharedInstance] track:@"Facebook Clicked"];

    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        
        NSString *shareText = @"Slobbr http://www.slobbr.com";
        [mySLComposerSheet setInitialText:shareText];
        
        [mySLComposerSheet addURL:[NSURL URLWithString:@"http://www.slobbr.com"]];
        
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"Post Sucessful");
                    break;
                default:
                    break;
            }
        }];
        
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
    }
    else{
        [self sxc_showErrorViewWithMessage:@"Please login with facebook in device settings. "];
    }
}

-(void)shareUsingMail
{
    [[Mixpanel sharedInstance] track:@"Mail Clicked"];

    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        
        [mc setSubject:@"Slobbr"];
        [mc setMessageBody:@"Slobbr http://www.slobbr.com" isHTML:NO];
        [self presentViewController:mc animated:YES completion:NULL];
    }
    else
    {
        [self sxc_showErrorViewWithMessage:@"Device is not configured with mail. Please check."];
    }
    
}

-(void)sxc_startLoading{}
-(void)sxc_stopLoading{}

#pragma mark - MailComposerDelegate methods

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            
            break;
        case MFMailComposeResultSaved:
            
            break;
        case MFMailComposeResultSent:
            
            break;
        case MFMailComposeResultFailed:
            
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - Services

-(void)advertisement:(NSDictionary *) response
{
   
        advertisements= [[NSMutableArray alloc] init];

        NSDictionary * advertisementDictionary=[SXCUtility cleanDictionary:response];

        NSArray * checkins = [SXCUtility cleanArray:[advertisementDictionary valueForKey:@"checkins"]];
        for(NSDictionary * checkin in checkins) {
            NSMutableDictionary * tempCheckIn = [[NSMutableDictionary alloc] initWithDictionary:checkin];
            [tempCheckIn setValue:@"place" forKey:@"type"];
            [advertisements addObject:tempCheckIn];
        }

        NSArray * friends = [SXCUtility cleanArray:[advertisementDictionary valueForKey:@"friend"]];
        for(NSDictionary * friend in friends) {
            NSMutableDictionary * tempFriend = [[NSMutableDictionary alloc] initWithDictionary:friend];
            [tempFriend setValue:@"dog" forKey:@"type"];
            [advertisements addObject:tempFriend];
        }

        NSArray * dogs = [SXCUtility cleanArray:[advertisementDictionary valueForKey:@"dogs"]];
        for(NSDictionary * dog in dogs) {
            NSMutableDictionary * tempDog = [[NSMutableDictionary alloc] initWithDictionary:dog];
            [tempDog setValue:@"dog" forKey:@"type"];
            [advertisements addObject:tempDog];
        }
        NSArray * tempEvents = [SXCUtility cleanArray:[advertisementDictionary valueForKey:@"events"]];
        for(NSDictionary * event in tempEvents) {
            NSMutableDictionary * tempEvent = [[NSMutableDictionary alloc] initWithDictionary:event];
            [tempEvent setValue:@"event" forKey:@"type"];
            [advertisements addObject:tempEvent];
        }

        NSArray * partners = [SXCUtility cleanArray:[advertisementDictionary valueForKey:@"partners"]];
        for(NSDictionary * partner in partners) {
            NSMutableDictionary * tempPartner = [[NSMutableDictionary alloc] initWithDictionary:partner];
            [tempPartner setValue:@"partner" forKey:@"type"];
            [advertisements addObject:tempPartner];
        }


        NSArray * activities = [SXCUtility cleanArray:[advertisementDictionary valueForKey:@"activities"]];
        for(NSDictionary * activity in activities) {
            NSMutableDictionary * tempActivity = [[NSMutableDictionary alloc] initWithDictionary:activity];
            [tempActivity setValue:@"activity" forKey:@"type"];
            [advertisements addObject:tempActivity];
        }

}


-(void)eventParkDetails:(NSDictionary *) response
{
    eventDictionary=[SXCUtility cleanDictionary:response].mutableCopy;
}

-(void)homeParkWithDispatch:(NSDictionary *) response
{
    homeParkDictionary=[SXCUtility cleanArray:response].mutableCopy;
    
    if(!homeParkDictionary)
        [SXCUtility saveNSObject:@YES forKey:@"HomeParkEdit"];
}

-(void)userProfileWithDispatch:(NSMutableDictionary *) responseDicitionary
{
    [PWPApplicationUtility saveCurrentUserProfileDictionary:responseDicitionary];
    
    currentUserPacks=[[NSMutableArray alloc] initWithArray:[PWPApplicationUtility getCurrentUserDogsArray]];
}




-(void)notificationsWithDispatch:(NSDictionary *) response
{
    
        NSMutableArray * _friendsRequestArray=[[NSMutableArray alloc] init];
        if([response[@"friendships"] isKindOfClass:[NSArray class]]){
            NSArray * friendsShpArray=[SXCUtility cleanArray:[response valueForKey:@"friendships"]];
            [_friendsRequestArray addObjectsFromArray:friendsShpArray];
        }
        else if([response[@"friendships"] isKindOfClass:[NSDictionary class]]){
            NSDictionary * friendshipDictionary=[response valueForKey:@"friendships"];
            for(NSString * key in friendshipDictionary.allKeys){
                [_friendsRequestArray addObjectsFromArray:friendshipDictionary[key]];
            }
        }
        
        NSMutableArray * _datesRequestArray=[[NSMutableArray alloc] init];
        if([response[@"dates"] isKindOfClass:[NSArray class]]){
            NSArray * friendsShpArray=[SXCUtility cleanArray:[response valueForKey:@"dates"]];
            [_datesRequestArray addObjectsFromArray:friendsShpArray];
        }
        else if([response[@"dates"] isKindOfClass:[NSDictionary class]]){
            NSDictionary * datesDictionary=[response valueForKey:@"dates"];
            for(NSString * key in datesDictionary.allKeys){
                [_datesRequestArray addObjectsFromArray:datesDictionary[key]];
            }
        }
        
        NSInteger totalNotifications=_datesRequestArray.count+_friendsRequestArray.count;
                
        [SXCUtility saveNSObject:[NSNumber numberWithInteger:totalNotifications] forKey:@"NotificationCount"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationCount" object:nil];
    
    
}


-(void)services
{
    if(isLoading)
        return;
    
    isLoading=YES;

    errorSet = [[NSMutableSet alloc] init];
    
    
    [[PWPService sharedInstance] homeBatchRequestWithSuccessBlock:^(id responseDicitionary) {
        
        
        NSString * favPlaces = [NSString stringWithFormat:@"/apis/v1/current/place"];
        
        NSString * loggedInUser = [NSString stringWithFormat:@"/apis/v1/current/user"];
        
        NSString * advertisementsRequest = [NSString stringWithFormat:@"/apis/v1/advertisement"];
        
        NSString * notifications = [NSString stringWithFormat:@"/apis/v1/current/notifications"];
        
        NSString * eventMode = [NSString stringWithFormat:@"/apis/v1/setting.json?key=event.mode"];

        
        
        for (NSDictionary * dictionary in responseDicitionary) {
        
            if([[dictionary valueForKey:@"request"] isEqualToString:favPlaces]) {
                
                NSData *objectData = [[dictionary valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSDictionary *actualResponse = [NSJSONSerialization JSONObjectWithData:objectData
                                                                                options:NSJSONReadingMutableContainers
                                                                                  error:&jsonError];
                [self homeParkWithDispatch:actualResponse];
                
            }
            else  if([[dictionary valueForKey:@"request"] isEqualToString:loggedInUser]) {
                
                NSData *objectData = [[dictionary valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSDictionary *actualResponse = [NSJSONSerialization JSONObjectWithData:objectData
                                                                               options:NSJSONReadingMutableContainers
                                                                                 error:&jsonError];
                
                [self userProfileWithDispatch:actualResponse.mutableCopy];
                
            }
            else  if([[dictionary valueForKey:@"request"] isEqualToString:advertisementsRequest]) {
                
                NSData *objectData = [[dictionary valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSDictionary *actualResponse = [NSJSONSerialization JSONObjectWithData:objectData
                                                                               options:NSJSONReadingMutableContainers
                                                                                 error:&jsonError];
                [self advertisement:actualResponse];
                
                
            }
            else  if([[dictionary valueForKey:@"request"] isEqualToString:notifications]) {
                
                NSData *objectData = [[dictionary valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSDictionary *actualResponse = [NSJSONSerialization JSONObjectWithData:objectData
                                                                               options:NSJSONReadingMutableContainers
                                                                                 error:&jsonError];
                [self notificationsWithDispatch:actualResponse];

                
            }
            else  if([[dictionary valueForKey:@"request"] isEqualToString:eventMode]) {
                
                NSData *objectData = [[dictionary valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSDictionary *actualResponse = [NSJSONSerialization JSONObjectWithData:objectData
                                                                               options:NSJSONReadingMutableContainers
                                                                                 error:&jsonError];

                [self eventModeWithGroup:actualResponse];

            }
        
        }
        
        
        isLoading=NO;
        
        [_tableView reloadData];
        [_tableView setHidden:NO];
        [super hideLoader];
        [self.refreshControl endRefreshing];
        
        
        if(eventDictionary){
            NSArray * favoriteParkId = [homeParkDictionary valueForKeyPath:@"id"];
            if(![favoriteParkId containsObject:eventDictionary[@"id"]]){
                [homeParkDictionary insertObject:eventDictionary atIndex:0];
            }
        }
        
        if((currentUserPacks.count==0)&&([self.navigationController.viewControllers.lastObject isKindOfClass:[PWPHomeViewController class]]) && _isFromRegistration){
            
            NSString * message =@"If you have a Pup, feel free to create their Profile, so you can check them into today's event\nOr\nIf you\'d don\'t and want to see some of the great Pups up for adoption or Pals at the event, Fetch Today\'s Event!";
            
            if(([PWPApplicationUtility isEventMode])&&(_isFromRegistration)){
                UIAlertView * alertView= [self sxc_showAlertViewWithTitle:@"Thanks for Signing In!" message:message WithDelegate:self WithCancelButtonTitle:@"Fetch Today's Event" WithAcceptButtonTitle:@"Create My Pup's Profile"];
                alertView.tag=-23;
            }
            else if((![PWPApplicationUtility isEventMode])&&(_isFromRegistration)){
                
                PWPAddDogViewController * addDgViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"PWPAddDogViewController"];
                addDgViewController.isFromRegistration=_isFromRegistration;
                addDgViewController.delegate=self;
                [self.navigationController pushViewController:addDgViewController animated:YES];
            }
            _isFromRegistration=NO;
            
            return ;
        }
        else if((![SXCUtility isNotNull:[PWPApplicationUtility getCurrentUserProfileDictionary][@"zipcode"]])&&([self.navigationController.viewControllers.lastObject isKindOfClass:[PWPHomeViewController class]]))
        {
            UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];
            
            UIViewController *  settingsViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPSettingViewController"];
            
            UINavigationController * navigationController =[[UINavigationController alloc] initWithRootViewController:settingsViewController];
            
            [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
            [kMainViewController presentViewController:navigationController animated:YES completion:nil];
            
            return ;
        }
        else if((!homeParkDictionary)&&([self.navigationController.viewControllers.lastObject isKindOfClass:[PWPHomeViewController class]])){
            [self sxc_showErrorViewWithMessage:@"You haven't set your Favorite PLACE or PARK! Go to PLACES to mark your favs!"];
            return ;
        }
        
        
    } withErrorBlock:^(NSError *error) {
       
        isLoading=NO;
        
        [_tableView reloadData];
        [_tableView setHidden:NO];
        [super hideLoader];
        [self.refreshControl endRefreshing];
        
        [self sxc_showErrorViewWithMessage:error.domain];
        [SXCUtility saveNSObject:[NSNumber numberWithInteger:0] forKey:@"NotificationCount"];

    }];
    
    
}

-(void)deletePackWithPackDictionary:(NSMutableDictionary *)packDictionary
{
    [self startLoader];
    [[PWPService sharedInstance] deleteDogWithDogId:packDictionary[@"id"] withSuccessBlock:^(id responseDicitionary) {
        
        [self dismissLoaderWithSuccessText:@"Pack has been deleted successfully"];
        [currentUserPacks removeObject:packDictionary];
        [_tableView reloadData];
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        [self dismissLoaderWithErrorText:@"Some error has been occured."];
        
    }];
    
}

-(void)eventModeWithGroup:(NSDictionary *) response{
    
        [SXCUtility saveNSObject:response forKey:@"eventMode"];
}

#pragma mark - Delegate Methods


-(void)quickCheckInStatusClicked
{
    [[Mixpanel sharedInstance] track:@"Status Change Clicked Home Screen"];

    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];

    PWPSelectStatusViewController * selectStatusViewController = [storyboard instantiateViewControllerWithIdentifier:@"PWPSelectStatusViewController"];
    selectStatusViewController.selectedStatus = selectedStatus;
    [self.navigationController pushViewController:selectStatusViewController animated:YES];

}



-(void)settingClicked{
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];    
    UIViewController * settingsViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPSettingViewController"];
    [self.navigationController pushViewController:settingsViewController animated:YES];
}
-(void)checkInClicked:(NSDictionary *)dictionary
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PWPParkCheckInViewController * parkCheckInViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckInViewController"];    parkCheckInViewController.parkDictionary=[[NSMutableDictionary alloc] initWithDictionary:dictionary];
    parkCheckInViewController.delegate=self;
    [self.navigationController pushViewController:parkCheckInViewController animated:YES];
    
}

-(void)checkInDogsClicked:(NSDictionary *)dictionary
{
    if([SXCUtility cleanString:[dictionary[@"active_dogs_count"] description]].integerValue > 0){

        [[Mixpanel sharedInstance] track:@"Check-In Dogs Clicked Home Screen"];


        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PWPParkCheckedInDogsViewController * parkCheckInDogsViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckedInDogsViewController"];
        parkCheckInDogsViewController.placeId = [SXCUtility cleanString:[[dictionary valueForKey:@"id"] description]];
        
        [self.navigationController pushViewController:parkCheckInDogsViewController animated:YES];
    }
    
}

-(void)pushSearchParkViewController{

    [[Mixpanel sharedInstance] track:@"Check-in my current location Home Screen"];

    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    
    PWPCheckInArroundMeViewController * aroundViewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPCheckInArroundMeViewController"];
    [self.navigationController pushViewController:aroundViewController animated:true];
    
}

-(IBAction)currentLocationClicked:(id)sender{
    [self pushSearchParkViewController];
}


-(void)dogAdded{
}

-(void)dogDeleteClickedWithDictionary:(NSDictionary *)dogInformation
{
    [[Mixpanel sharedInstance] track:@"Delete Dog Clicked Home Screen"];

    deletePackDictionary=dogInformation;
    NSString *message = [NSString stringWithFormat:@"Do you want to delete %@",deletePackDictionary[@"name"]];
    [self sxc_showAlertViewWithTitle:@"Delete Dog" message:message WithDelegate:self WithCancelButtonTitle:@"No" WithAcceptButtonTitle:@"Yes"];
}

-(void)dogEditClickedWithDictionary:(NSDictionary *)dogInformation
{
    [[Mixpanel sharedInstance] track:@"Edit Dog Clicked Home Screen"];

    PWPAddDogViewController * addDogViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"PWPAddDogViewController"];
    addDogViewController.dogDictionary=dogInformation;
    addDogViewController.delegate=self;
    [self.navigationController pushViewController:addDogViewController animated:YES];
}

-(void)dogPhotosClickedWithDictionary:(NSDictionary *)dogInformation
{
    [[Mixpanel sharedInstance] track:@"Photos Dog Clicked Home Screen"];

    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    PWPDogPicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPDogPicViewController"];
    viewController.dogsPics = [SXCUtility cleanArray:[dogInformation valueForKey:@"gallery"]].mutableCopy;
    viewController.data = dogInformation;
    viewController.dogId = dogInformation[@"id"];

    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)deletePackWithId:(NSString *)packIdString
{
    [self startLoader];
    [[PWPService sharedInstance] deleteDogWithDogId:packIdString withSuccessBlock:^(id responseDicitionary) {
        
        [self dismissLoaderWithSuccessText:@"Dog has been deleted successfully"];
        [currentUserPacks removeObject:deletePackDictionary];
        [_tableView reloadData];
        deletePackDictionary=nil;
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        [self dismissLoaderWithErrorText:@"Some error has been occured."];
        deletePackDictionary=nil;
        
    }];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if((alertView.tag==-23)&&(buttonIndex==1)){
        PWPAddDogViewController * addDgViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"PWPAddDogViewController"];
        addDgViewController.isFromRegistration=_isFromRegistration;
        addDgViewController.delegate=self;
        [self.navigationController pushViewController:addDgViewController animated:YES];
    }
    else if((alertView.tag==-23)&&(buttonIndex==0)){
        [self performSegueWithIdentifier:@"segue_event" sender:nil];
    }
    else if(alertView.tag == -999 && buttonIndex==1){

        [[PWPService sharedInstance] cancelDogFriendsRequest:toDogIdString fromDog:fromDogIdString withSuccessBlock:^(id responseDicitionary) {

            [self dismissLoaderWithSuccessText:@"Request has been cancelled successfully."];

        } withErrorBlock:^(NSError *error) {

            [self dismissLoaderWithErrorText:@"An error has occurred. Please try again"];
            
        }];
    }
    else if(buttonIndex==1){
        [self deletePackWithId:deletePackDictionary[@"id"]];
    }
}

#pragma mark - IBAction methods
- (IBAction)mailButtonClicked:(id)sender {
    [self shareUsingMail];
}

- (IBAction)twitterButtonClicked:(id)sender {
    [self shareUsingTwitter];
}

- (IBAction)facebookButtonClicked:(id)sender {
    [self shareUsingFacebook];
}


- (IBAction)findParkButtonClicked:(id)sender {
    
    NSMutableDictionary * filterDictionary=[[NSMutableDictionary alloc] init];
    filterDictionary[@"zipcode"]=[PWPApplicationUtility getCurrentUserProfileDictionary][@"zipcode"];
    filterDictionary[@"distance"]=@"10";
    
    
    ((UITabBarController *)kTabViewController).selectedIndex = 0;
    
    UINavigationController * navigationController = ((UITabBarController *)kTabViewController).selectedViewController;
    
    PWPParkSwitchViewController * switchViewController = navigationController.viewControllers.firstObject;
    switchViewController.filterDictionary = filterDictionary;
    [switchViewController searchParksForPageOffset:@0 forDragingMap:false];
    
}


-(NSMutableDictionary *)requestDictionaryForQuickCheckIn
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] init];
    
    if([PWPApplicationUtility getCurrentUserDogsArray].count>1){
        [requestDictionary setValue:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"] forKey:@"dogs"];
    }
    
    [requestDictionary setValue:[SXCUtility cleanString:[SXCUtility getNSObject:@"quickCheckinDefaultMessage"]] forKey:@"message"];
    
    return requestDictionary;
}


-(void)quickCheckInClicked:(NSDictionary *)dictionary
{
    
    if([PWPCheckInHelper checkInAllowedForParkId:dictionary[@"id"] forDogs:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"]] == false){
        return;
    }

    
    [self startLoader];
    
    [[PWPService sharedInstance] checkInParkWithRequestDictionary:[self requestDictionaryForQuickCheckIn] withParkId:dictionary[@"id"] withSuccessBlock:^(id response) {
        
        [PWPCheckInHelper dogCheckedInParkId:dictionary[@"id"] forDogs:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"]];

        [self dismissLoader];
        [self sxc_showSuccessWithMessage:@"You have been checked-in successfully."];
        [self automaticallyRefresh];
        [self services];
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        
        if (error.code==403) {
            [self sxc_showErrorViewWithMessage:@"You have already checked-in."];
        }
        else{
            [self sxc_showErrorViewWithMessage:error.domain];
        }
    }];
}

-(void)checkInClickedWithPark:(NSDictionary *)parkDictionary;
{
//    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    PWPParkCheckInViewController * parkCheckInViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckInViewController"];
//    parkCheckInViewController.parkDictionary=[[NSMutableDictionary alloc] initWithDictionary:parkDictionary];
//    parkCheckInViewController.delegate=self;
//    [self.navigationController pushViewController:parkCheckInViewController animated:YES];

    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    PWPPlacePicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPPlacePicViewController"];
    viewController.placePics = [SXCUtility cleanArray:[parkDictionary valueForKey:@"gallery"]].mutableCopy;
    viewController.data = parkDictionary;
    viewController.placeId = parkDictionary[@"id"];
    viewController.delegate = self;

    [self.navigationController pushViewController:viewController animated:YES];

}
-(void)aMethod:(UIButton*)sender
{
    //UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];
    //PWPSearchParkMapViewController * parkFilterViewController=[storyBoard instantiateViewControllerWithIdentifier:@"PWPSearchParkMapViewController"];
    //parkFilterViewController.delegate=self;
    // parkFilterViewController.parkFilterDictionary=_filterDictionary;
    // parkFilterViewController.selectedPlaceCategory = _selectedCategoryPlace;
    //[self.navigationController pushViewController:parkFilterViewController animated:YES];
    ((UITabBarController *)kTabViewController).selectedIndex = 1;
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
    
}
- (IBAction)plusClicked:(id)sender {
    ((UITabBarController *)kTabViewController).selectedIndex = 1;
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
}



#pragma mark - PWPParkDelegate
-(void)parkOptionClickedWithData:(NSDictionary *)dictionary
{
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    PWPPlacePicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPPlacePicViewController"];
    viewController.placePics = [SXCUtility cleanArray:[dictionary valueForKey:@"gallery"]].mutableCopy;
    viewController.data = dictionary;
    viewController.placeId = dictionary[@"id"];

    [self.navigationController pushViewController:viewController animated:YES];

}


-(void)addPlaceClickedWithData:(NSDictionary *)dictionary {
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    
    PWPPlacePicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPPlacePicViewController"];
    viewController.placePics = [SXCUtility cleanArray:[dictionary valueForKey:@"gallery"]].mutableCopy;
    viewController.data = dictionary;
    viewController.placeId = dictionary[@"id"];
    viewController.delegate = self;
    viewController.isGalleryOpen = true;
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)parkOption2ClickedWithData:(NSDictionary *)dictionary
{
    if([SXCUtility cleanString:[dictionary[@"active_dogs_count"] description]].integerValue > 0){

        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

        [[Mixpanel sharedInstance] track:@"Check-In Dogs Clicked Home Screen"];

        PWPParkCheckedInDogsViewController * parkCheckInDogsViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckedInDogsViewController"];
        parkCheckInDogsViewController.placeId = [SXCUtility cleanString:[[dictionary valueForKey:@"id"] description]];
        
        [self.navigationController pushViewController:parkCheckInDogsViewController animated:YES];
    }
}
-(void)parkOption3ClickedWithData:(NSDictionary *)dictionary
{
    [self startLoader];

    if([SXCUtility cleanInt:dictionary[@"is_favourite"]] ==1){

        [[PWPService sharedInstance] deleteHomePark:dictionary[@"id"] withSuccessBlock:^(id response) {
            [self dismissLoader];
            [self sxc_showSuccessWithMessage:@"Place has been removed from 'Favorite' Places."];
            [self automaticallyRefresh];
            [self services];

        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:error.domain];
        }];

    }
    else{


        [[PWPService sharedInstance] homePark:dictionary[@"id"] withSuccessBlock:^(id response) {
            [self dismissLoader];
            [self sxc_showSuccessWithMessage:@"Place has been marked as 'Favorite' Place."];
            [self automaticallyRefresh];
            [self services];

        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:error.domain];
        }];
    }
}

-(void)successFullCheckedInWithDictionary:(NSMutableDictionary *)data WithDogs:(NSMutableArray *)dogsArray{

    [self automaticallyRefresh];
    [self services];

}

-(void)inviteOptionClicked:(NSDictionary *)dictionary
{
    [self pushSchedulePawDateViewController:dictionary];
}

-(void)parkQuickCheckInClicked:(NSDictionary *)dictionary
{

    [self startLoader];

    [[PWPService sharedInstance] checkInParkWithRequestDictionary:[self requestDictionary] withParkId:dictionary[@"id"] withSuccessBlock:^(id response) {

        [self dismissLoader];
        [self automaticallyRefresh];
        [self services];

        [self sxc_showSuccessWithMessage:@"You have been checked-in successfully."];

    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];

        if (error.code==403) {
            [self sxc_showErrorViewWithMessage:@"You have already checked-in."];
        }
        else{
            [self sxc_showErrorViewWithMessage:error.domain];
        }

    }];
}

-(NSMutableDictionary *)requestDictionary
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] init];

    if([PWPApplicationUtility getCurrentUserDogsArray].count>1){
        [requestDictionary setValue:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"] forKey:@"dogs"];
    }

    [requestDictionary setValue:[SXCUtility cleanString:[SXCUtility getNSObject:@"quickCheckinDefaultMessage"]] forKey:@"message"];

    return requestDictionary;
}


-(void)pushSchedulePawDateViewController:(NSDictionary *)parkDictionary{

    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    PWPSchedulePawDateViewController *pawDateViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPSchedulePawDateViewController"];
    pawDateViewController.selectedPark = parkDictionary.mutableCopy;
    [self.navigationController pushViewController:pawDateViewController animated:YES];
    
}



-(void)sendFriendRequestServiceCallWithData:(id)data fromDogId:(NSString *)fromDogId
{
    fromDogIdString=fromDogId;
    toDogIdString=data[@"id"];

    [self startLoader];

    [[PWPService sharedInstance]sendDogFriendsRequest:data[@"id"] fromDog:fromDogId withSuccessBlock:^(id responseDicitionary) {

        [self dismissLoaderWithSuccessText:@"Request has been sent successfully."];

    } withErrorBlock:^(NSError *error) {

        if(error.code==403){
            UIAlertView * alertView = [self sxc_showAlertViewWithTitle:@"Error" message:@"You have already sent the friend request to this dog. Do you want to cancel the friend reqeust?" WithDelegate:self WithCancelButtonTitle:@"NO" WithAcceptButtonTitle:@"YES"];
            alertView.tag = -999;
            [self dismissLoader];
        }else{
            [self dismissLoaderWithErrorText:@"An error has occurred. Please try again"];
        }
    }];
}
-(void)optionPackClickedWithData:(id)data
{
    if([PWPApplicationUtility getCurrentUserDogsArray].count==0){
        [self sxc_showErrorViewWithMessage:@"Please add a dog to your packs."];
        return;
    }

    [super selectDogFromPackToSendRequest:^(NSString * dogId) {

        [self sendFriendRequestServiceCallWithData:data fromDogId:dogId];
    } withCancelblock:^{
    }];

}

-(void)option5PackClickedWithData:(id)data
{
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    PWPDogPicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPDogPicViewController"];
    viewController.dogsPics = [SXCUtility cleanArray:[data valueForKey:@"gallery"]].mutableCopy;
    viewController.data = data;
    viewController.dogId = data[@"id"];

    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)placeProfileUpdated {
    [self automaticallyRefresh];
    [self services];
}




@end
