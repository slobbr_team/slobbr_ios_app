//
//  PWPActivityCollectionViewCell.m
//  PawPark
//
//  Created by Samarth Singla on 21/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPPartnerCollectionViewCell.h"
#import <UIImageView+WebCache.h>
#import "PWPConstant.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPPartnerCollectionViewCell(){

    __weak IBOutlet UIImageView *imageView;
    __weak IBOutlet UILabel *labelEventTitle;
    __weak IBOutlet UILabel * labelEventDescription;
    __weak IBOutlet UIImageView *imageViewAdv;
}

@end

@implementation PWPPartnerCollectionViewCell

-(void)configureWithData:(NSDictionary *)dictionary
{

    UIImage * advertisementImage = [PWPDownloadBackgroundImage itemBG];
    if(advertisementImage != nil){
        imageViewAdv.image =  advertisementImage;
        imageViewAdv.hidden = false;
        imageViewAdv.contentMode = UIViewContentModeScaleAspectFill;
    }
    else{
        imageViewAdv.hidden = true;
    }

    labelEventTitle.text=dictionary[@"name"];
    labelEventDescription.text=dictionary[@"description"];

    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    [imageView setBackgroundColor:[UIColor clearColor]];

    [imageView sd_setImageWithURL:[NSURL URLWithString:dictionary[@"image_url"]] placeholderImage:[UIImage imageNamed:@"default_park"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

        if(!error){
            [imageView setBackgroundColor:[UIColor clearColor]];
        }
    }];
}


@end
