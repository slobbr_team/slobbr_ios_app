//
//  PWPActivityCollectionViewCell.h
//  PawPark
//
//  Created by Samarth Singla on 21/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPPartnerCollectionViewCell : UICollectionViewCell
-(void)configureWithData:(NSDictionary *)dictionary;

@end
