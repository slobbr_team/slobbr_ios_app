//
//  PWPAdvertismentCell.m
//  PawPark
//
//  Created by Samarth Singla on 21/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPAdvertismentCell.h"
#import "PWPDogCollectionViewCell.h"
#import "PWPPlaceCollectionViewCell.h"
#import "PWPEventsCollectionViewCell.h"
#import "PWPPartnerCollectionViewCell.h"
#import "SXCUtility.h"

@interface PWPAdvertismentCell()<UICollectionViewDelegate, UICollectionViewDataSource,PWPPlaceCollectionViewDelegate,PWPDogCollectioViewCellDelegate>
{

    NSMutableArray * advertisments;
    __weak IBOutlet UICollectionView *collectionView;
    __weak IBOutlet UIPageControl *pageControl;

}
@end

@implementation PWPAdvertismentCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)configureWithData:(NSMutableArray *)theAdvertisement {

    advertisments = theAdvertisement;

    pageControl.numberOfPages = advertisments.count;
    pageControl.userInteractionEnabled = false;
    
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.pagingEnabled = true;
    [collectionView reloadData];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = scrollView.contentOffset.x / scrollView.frame.size.width;
    pageControl.currentPage = page;

}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return advertisments.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;

    return CGSizeMake(screenWidth,179);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)theCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * dictionary = advertisments[indexPath.row];
    if ([[dictionary valueForKey:@"type"] isEqualToString:@"place"]){

        PWPPlaceCollectionViewCell * collectionViewCell = [theCollectionView dequeueReusableCellWithReuseIdentifier:@"PWPPlaceCollectionViewCell" forIndexPath:indexPath];
        collectionViewCell.delegate = self;
        [collectionViewCell bindValueWithComponentsWithData:dictionary.mutableCopy];
        return collectionViewCell;
    }
    else if ([[dictionary valueForKey:@"type"] isEqualToString:@"event"]){
        PWPEventsCollectionViewCell * collectionViewCell = [theCollectionView dequeueReusableCellWithReuseIdentifier:@"PWPEventsCollectionViewCell" forIndexPath:indexPath];
        [collectionViewCell configureWithData:dictionary.mutableCopy];
        return collectionViewCell;
    }
    else if ([[dictionary valueForKey:@"type"] isEqualToString:@"partner"]){
        PWPPartnerCollectionViewCell * collectionViewCell = [theCollectionView dequeueReusableCellWithReuseIdentifier:@"PWPPartnerCollectionViewCell" forIndexPath:indexPath];
        [collectionViewCell configureWithData:dictionary.mutableCopy];
        return collectionViewCell;
    }
    else if ([[dictionary valueForKey:@"type"] isEqualToString:@"activity"]){
        PWPEventsCollectionViewCell * collectionViewCell = [theCollectionView dequeueReusableCellWithReuseIdentifier:@"PWPEventsCollectionViewCell" forIndexPath:indexPath];
        [collectionViewCell configureForActivities:dictionary.mutableCopy];
        return collectionViewCell;
    }


    PWPDogCollectionViewCell * collectionViewCell = [theCollectionView dequeueReusableCellWithReuseIdentifier:@"PWPDogCollectionViewCell" forIndexPath:indexPath];
    collectionViewCell.delegate=self;
    [collectionViewCell configureCellWithData:dictionary.mutableCopy];
    return collectionViewCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    NSDictionary * dictionary = advertisments[indexPath.row];

    if ([[dictionary valueForKey:@"type"] isEqualToString:@"partner"]){
        NSString * linkString = [SXCUtility cleanString:dictionary[@"link"]];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:linkString]]){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:linkString]];
        }
    }
}

-(void)parkOptionClickedWithData:(NSDictionary *)dictionary{
    [self.delegate parkOptionClickedWithData:dictionary];
}

-(void)parkOption2ClickedWithData:(NSDictionary *)dictionary{
    [self.delegate parkOption2ClickedWithData:dictionary];
}

-(void)parkOption3ClickedWithData:(NSDictionary *)dictionary{
    [self.delegate parkOption3ClickedWithData:dictionary];
}

-(void)inviteOptionClicked:(NSDictionary *)dictionary{
    [self.delegate inviteOptionClicked:dictionary];
}

-(void)parkQuickCheckInClicked:(NSDictionary *)dictionary{
    [self.delegate parkQuickCheckInClicked:dictionary];
}

-(void)optionPackClickedWithData:(id)data{
    [self.delegate optionPackClickedWithData:data];
}

-(void)option5PackClickedWithData:(id)data{
    [self.delegate option5PackClickedWithData:data];
}

@end
