//
//  PWPDogCollectionViewCell.m
//  PawPark
//
//  Created by Samarth Singla on 21/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPDogCollectionViewCell.h"
#import <UIImageView+WebCache.h>
#import "PWPConstant.h"
#import "SXCUtility.h"
#import "PWPApplicationUtility.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPDogCollectionViewCell()
{
    __weak IBOutlet UIImageView * imageViewDog;
    __weak IBOutlet UILabel *labelDogName;
    __weak IBOutlet UILabel *labelNumberOfDogs;
    __weak IBOutlet UILabel *labelBreed;
    __weak IBOutlet UILabel *labelSocialibity;
    __weak IBOutlet UILabel *labelActivityLevel;
    __weak IBOutlet UILabel *labelSize;
    __weak IBOutlet UIImageView *imageViewAdvertisment;
    
    NSDictionary * requestDictionary;
}
@end

@implementation PWPDogCollectionViewCell

-(void)configureCellWithData:(NSMutableDictionary *)dictionary
{

    UIImage * advertisementImage = [PWPDownloadBackgroundImage itemBG];
    if(advertisementImage != nil){
        imageViewAdvertisment.image =  advertisementImage;
        imageViewAdvertisment.hidden = false;
        imageViewAdvertisment.contentMode = UIViewContentModeScaleAspectFill;
    }
    else{
        imageViewAdvertisment.hidden = true;
    }

    requestDictionary=dictionary;

    if (dictionary[@"image_url"] != nil){
        [imageViewDog sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,dictionary[@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
    }
    else if(dictionary[@"gallery"]!=nil && [SXCUtility cleanArray:dictionary[@"gallery"]].count > 0){

        NSDictionary * dogImageDictionary =[SXCUtility cleanArray:dictionary[@"gallery"]][0];
        NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
        [imageViewDog sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
    }
    else{
        [imageViewDog setImage:[UIImage imageNamed:@"img_dog_sample"]];
    }

    labelNumberOfDogs.text=[NSString stringWithFormat:@"%@ years old",[SXCUtility cleanString:[[dictionary valueForKey:@"age"] description]]];
    labelDogName.text=[SXCUtility cleanString:[dictionary valueForKey:@"name"]].capitalizedString;

    if([dictionary valueForKey:@"breed"]&&[[dictionary valueForKey:@"breed"] isKindOfClass:[NSDictionary class]]){
        labelBreed.text=[SXCUtility cleanString:[dictionary valueForKey:@"breed"][@"name"]].capitalizedString;
    }
    else if([dictionary valueForKey:@"breed"]&&[[dictionary valueForKey:@"breed"] isKindOfClass:[NSString class]]){
        labelBreed.text=[SXCUtility cleanString:[dictionary valueForKey:@"breed"]].capitalizedString;
    }
    else{
        labelBreed.text=@"";
    }


    labelSocialibity.text=[PWPApplicationUtility pawSociability][[SXCUtility cleanString:[dictionary valueForKey:@"sociability"]].integerValue];
    labelSize.text=[PWPApplicationUtility pawSizesCorrespondingToID:[SXCUtility cleanString:[dictionary valueForKey:@"size"]]];
    labelActivityLevel.text=[PWPApplicationUtility pawActivityLevel][[SXCUtility cleanString:[dictionary valueForKey:@"activityLevel"]].integerValue];

}
- (IBAction)photosButtonClicked:(id)sender {
    [_delegate option5PackClickedWithData:requestDictionary];
}
- (IBAction)friendsButtonClicked:(id)sender {
    [_delegate optionPackClickedWithData:requestDictionary];
}
@end
