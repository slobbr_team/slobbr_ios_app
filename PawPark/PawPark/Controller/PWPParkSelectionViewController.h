//
//  PWPParkSelectionViewController.h
//  PawPark
//
//  Created by xyz on 3/25/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPParkSelectionDelegate <NSObject>

-(void)parkSelected:(NSDictionary *)parkDictionary;

@end

@interface PWPParkSelectionViewController : UIViewController

@property(nonatomic,strong)NSArray * parksArray;
@property(nonatomic,weak)id <PWPParkSelectionDelegate> delegate;
@property(nonatomic,strong)NSDictionary * selectedPark;

@end
