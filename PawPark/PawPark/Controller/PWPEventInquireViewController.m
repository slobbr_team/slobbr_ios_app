//
//  PWPEventInquireViewController.m
//  PawPark
//
//  Created by admin on 9/13/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPEventInquireViewController.h"
#import "UIViewController+UBRComponents.h"
#import "UBRTextfield.h"
#import "SZTextView.h"
#import "UIColor+PWPColor.h"
#import "PWPService.h"
#import "SXCUtility.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPEventInquireViewController ()
{
    __weak IBOutlet UIImageView * imageViewBG;
}
@property (weak, nonatomic) IBOutlet UBRTextfield *textfieldFirstName;
@property (weak, nonatomic) IBOutlet UBRTextfield *textfieldLastName;
@property (weak, nonatomic) IBOutlet UBRTextfield *textfieldEmailAddress;
@property (weak, nonatomic) IBOutlet SZTextView *textViewMessage;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@end

@implementation PWPEventInquireViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - BL Methods
-(void)initView
{
    [self sxc_customizeBackButton];
    [self sxc_navigationBarCustomizationWithTitle:@"Inquire"];
    [self shelterID];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    _textViewMessage.placeholderTextColor=[UIColor whiteColor];
    _textViewMessage.font=[UIFont fontWithName:@"Roboto-Light" size:14.0f];
    _textViewMessage.layer.borderColor=[UIColor whiteColor].CGColor;
    _textViewMessage.layer.borderWidth=1.0f;
    _textViewMessage.placeholder=@"Your Message";
    
    _textfieldLastName.textColor=[UIColor whiteColor];
    [_textfieldLastName setValue:[UIColor whiteColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    _textfieldLastName.backgroundColor = [UIColor clearColor];
    
    _textfieldFirstName.textColor=[UIColor whiteColor];
    [_textfieldFirstName setValue:[UIColor whiteColor]
                      forKeyPath:@"_placeholderLabel.textColor"];
    _textfieldFirstName.backgroundColor = [UIColor clearColor];
    
    _textfieldEmailAddress.textColor=[UIColor whiteColor];
    [_textfieldEmailAddress setValue:[UIColor whiteColor]
                       forKeyPath:@"_placeholderLabel.textColor"];
    _textfieldEmailAddress.backgroundColor = [UIColor clearColor];
}

-(NSMutableDictionary *)requestDictionary
{
    
    NSMutableDictionary * requestDictionary = [[NSMutableDictionary alloc] init];
    [requestDictionary setValue:_textfieldEmailAddress.text forKey:@"email"];
    [requestDictionary setValue:_textfieldFirstName.text forKey:@"first_name"];
    [requestDictionary setValue:_textfieldLastName.text forKey:@"last_name"];
    [requestDictionary setValue:_textViewMessage.text forKey:@"message"];
    
    return requestDictionary;

}

-(BOOL)isValid
{
    if(_textfieldFirstName.text.length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter the first name."];
        return NO;
    }
    else if(_textfieldLastName.text.length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter the last name."];
        return NO;
    }
    else if(_textfieldEmailAddress.text.length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter the email address."];
        return NO;
    }
    else if(![SXCUtility isValidEmail:_textfieldEmailAddress.text]){
        [self sxc_showErrorViewWithMessage:@"Please enter the valid email address."];
        return NO;
    }
    else if(_textViewMessage.text.length==0)
    {
        [self sxc_showErrorViewWithMessage:@"Please enter the message."];
        return NO;
    }
    return YES;
}

-(void)sxc_startLoading{}
-(void)sxc_stopLoading{}

#pragma mark - Services
-(void)inquireSubmit
{
    if([self isValid])
    {
        [self showLoaderWithText:@"Loading..."];
        [[PWPService sharedInstance] inquireSubmitForDogId:_dogIdString WithDictionary:[self requestDictionary] withSuccessBlock:^(id response)
        {
            [self dismissLoader];
            [self sxc_showSuccessWithMessage:@"Thank you for your inquiry! We've sent the note along to the shelter"];
            [self.navigationController popViewControllerAnimated:YES];
            
        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:@"An error has occured. Please try again."];

        }];
    }
}

-(void)shelterID
{
    [self showLoaderWithText:@"Loading..."];
    [[PWPService sharedInstance] userInformation:_shelterUserIdString withSuccessBlock:^(id response) {
        [self dismissLoader];
        _labelTitle.text=[NSString stringWithFormat:@"Inquire %@ about adopting %@",response[@"username"],_dogNameString];
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        [self sxc_showErrorViewWithMessage:error.domain];
        _labelTitle.text=[NSString stringWithFormat:@"Inquire about adopting %@",_dogNameString];
    }];
}



#pragma mark - IBAction Methods
-(IBAction)submitClicked:(id)sender
{
    
    [self inquireSubmit];
}
@end
