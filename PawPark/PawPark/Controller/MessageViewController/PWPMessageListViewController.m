//
//  PWPMessageListViewController.m
//  PawPark
//
//  Created by daffolap19 on 6/1/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//
#import "PWPCreateMessageViewController.h"
#import "PWPMessageListViewController.h"
#import "PWPMessageListCell.h"
#import "UIViewController+UBRComponents.h"
#import "PWPService.h"
#import "PWPApplicationUtility.h"
#import "UIColor+PWPColor.h"
#import "PWPMessageThreadViewController.h"
#import <Google/Analytics.h>
#import "PWPDownloadBackgroundImage.h"

@interface PWPMessageListViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UIImageView * imageViewBG;
}

@property (strong, nonatomic) IBOutlet UIRefreshControl *refreshControl;

@property (weak, nonatomic) IBOutlet UILabel *labelNoData;
@property(nonatomic,weak)IBOutlet UITableView * tableView;
@property(nonatomic,strong)NSArray *messages;
@property(nonatomic,strong)NSMutableDictionary *messagesDictonary;
@end

@implementation PWPMessageListViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self sxc_navigationBarCustomizationWithTitle:@"Messages"];
    [self sxc_setNavigationBarCrossItem];
    
    [self addPullToRefresh];
    [self automaticallyRefresh];
    [self messagesService];

    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Message List"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


-(void)addPullToRefresh
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor=[UIColor sxc_themeColor];
    [self.refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:self.refreshControl];
}
- (void)refreshView:(UIRefreshControl *)sender {
    [self messagesService];
}

-(void)automaticallyRefresh{
    [_tableView setContentOffset:CGPointMake(0, -self.refreshControl.frame.size.height) animated:YES];
    [self.refreshControl beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PWPMessageListCell * cell=[tableView dequeueReusableCellWithIdentifier:@"PWPMessageListCell"];
    [cell configureCellWithData:_messages[indexPath.row]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78.0f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _messages.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * messageDictionary=[_messages objectAtIndex:indexPath.row];
    
    PWPMessageThreadViewController * messageThreadViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"PWPMessageThreadViewController"];
    messageThreadViewController.threadIDString=messageDictionary[@"id"];
    messageThreadViewController.initiatorIDString=messageDictionary[@"initiator"][@"id"];
    messageThreadViewController.recipientIDString=messageDictionary[@"recipient"][@"id"];

    [self.navigationController pushViewController:messageThreadViewController animated:YES];
}

#pragma mark -IBAction Methods
- (IBAction)newMessageButtonClicked:(id)sender {
    
    UIViewController * viewController=[self.storyboard instantiateViewControllerWithIdentifier:@"PWPCreateMessageViewController"];
    
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - Service

-(void)messagesService
{
    
     NSString * userID=[PWPApplicationUtility getCurrentUserProfileDictionary][@"id"];
    [[PWPService sharedInstance] messagesWithIntiatorString:userID withDictionary:nil withSuccessBlock:^(id response) {
        
        _messages=[self parseMessagesFromDictionary:response];
        [_tableView reloadData];
        
        if((_messages.count==0)||(_messages == nil)){
            _labelNoData.hidden=NO;
            [self.view bringSubviewToFront:_labelNoData];
        }else{
            _labelNoData.hidden=YES;
            [self.view sendSubviewToBack:_labelNoData];
        }
        [self.refreshControl endRefreshing];
        
    } withErrorBlock:^(NSError *error) {
        [self sxc_showErrorViewWithMessage:error.domain];
        [self.refreshControl endRefreshing];
    }];

}

#pragma mark - BL 
-(NSArray *)parseMessagesFromDictionary:(NSMutableDictionary *)messageDic{

    if(![messageDic isKindOfClass:[NSDictionary class]]){
        return @[];
    }
    
    NSMutableArray * messagesArray=[[NSMutableArray alloc] init];;
    
    for(NSString * key in messageDic.allKeys){
    
        [messagesArray addObjectsFromArray:messageDic[key]];
    
    }

    return messagesArray;
}

-(void)sxc_startLoading{}
-(void)sxc_stopLoading{}

@end
