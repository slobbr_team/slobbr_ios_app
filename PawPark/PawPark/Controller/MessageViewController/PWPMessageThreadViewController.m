//
//  PWPMessageThreadViewController.m
//  PawPark
//
//  Created by daffolap19 on 6/2/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPMessageThreadViewController.h"
#import "PWPService.h"
#import "UIViewController+UBRComponents.h"
#import "PWPMessageThreadTableViewCell.h"
#import "SXCUtility.h"
#import "SXCTextfieldWithLeftImage.h"
#import "PWPApplicationUtility.h"
#import <Google/Analytics.h>
#import "PWPDownloadBackgroundImage.h"

@interface PWPMessageThreadViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    __weak IBOutlet UIImageView *imageViewTextfield;
    __weak IBOutlet UIImageView * imageViewBG;
}

@property(nonatomic,weak)IBOutlet UITableView * tableView;
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textfield;

@end

@implementation PWPMessageThreadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    imageViewTextfield.image =[[UIImage imageNamed:@"chat_bg"] stretchableImageWithLeftCapWidth:20 topCapHeight:20];
    
    
    
    [self sxc_navigationBarCustomizationWithTitle:@"Message"];
    [self sxc_customizeBackButton];

    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];    
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self startLoader];
    [self messagesService];
    [self readThread];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)messagesService
{
    NSString * userID=[PWPApplicationUtility getCurrentUserProfileDictionary][@"id"];
    [[PWPService sharedInstance] messagesWithIntiatorString:userID withDictionary:nil withSuccessBlock:^(id response) {
        
        NSArray * allmessages=[self parseMessagesFromDictionary:response];
        NSPredicate * predictae=[NSPredicate predicateWithFormat:@"id=%@",_threadIDString];
        NSArray * filteredArray=[allmessages filteredArrayUsingPredicate:predictae];
        if(filteredArray.count>0)
        {
            NSDictionary * messageDictioanry=filteredArray[0];
            _messages=messageDictioanry[@"messages"];
        }
        [_tableView reloadData];
        
        [self dismissLoader];
        
    } withErrorBlock:^(NSError *error) {
        [self sxc_showErrorViewWithMessage:error.domain];
    }];
    
}
-(NSArray *)parseMessagesFromDictionary:(NSMutableDictionary *)messageDic{
    
    NSMutableArray * messagesArray=[[NSMutableArray alloc] init];;
    
    for(NSString * key in messageDic.allKeys){
        
        [messagesArray addObjectsFromArray:messageDic[key]];
        
    }
    
    return messagesArray;
}

-(void)sendMessage{
    if([SXCUtility cleanString:_textfield.text].length>0){
        
        [self startLoader];
        
        NSString * userId = @"";
        if([[PWPApplicationUtility currentUserDogsIds] containsObject:_initiatorIDString]){
            userId = _initiatorIDString;
        }
        else if([[PWPApplicationUtility currentUserDogsIds] containsObject:_recipientIDString]){
            userId = _recipientIDString;
        }
        
        
        [[PWPService sharedInstance] sendMessagesWithIntiatorString:userId WithThreadID:_threadIDString withDictionary:@{@"body":_textfield.text} withSuccessBlock:^(id response) {
            
            [self messagesService];
            _textfield.text=@"";
            
        } withErrorBlock:^(NSError *error) {
            [self sxc_showErrorViewWithMessage:error.domain];
        }];
    }
    else{
        [self sxc_showErrorViewWithMessage:@"Please enter the message"];
    }
}



-(void)readThread{
    [[PWPService sharedInstance] readThread:_threadIDString withSuccessBlock:^(id response) {
        
    } withErrorBlock:^(NSError *error) {
        
    }];
}


-(IBAction)sendButtonClicked:(id)sender{
    [self sendMessage];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary * message = self.messages[indexPath.row];
    if([[PWPApplicationUtility currentUserDogsIds] containsObject:message[@"initiator"][@"id"]]){
        PWPMessageThreadTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"RWBasicCell"];
        [cell configueCellWithData:_messages[indexPath.row]];
        return cell;
    }
    else{
        PWPMessageThreadTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"RWBasicCell2"];
        [cell configueCellWithData:_messages[indexPath.row]];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;

}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self heightForBasicCellAtIndexPath:indexPath];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _messages.count;
}

- (CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static PWPMessageThreadTableViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:@"RWBasicCell"];
    });
        
    [sizingCell configueCellWithData:_messages[indexPath.row]];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 5.0f;
}

-(void)sxc_startLoading{}
-(void)sxc_stopLoading{}
@end
