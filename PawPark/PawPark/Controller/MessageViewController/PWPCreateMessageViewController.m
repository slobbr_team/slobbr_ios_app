//
//  PWPCreateMessageViewController.m
//  PawPark
//
//  Created by daffolap19 on 5/31/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPCreateMessageViewController.h"
#import "SXCTextfieldWithLeftImage.h"
#import "UIViewController+UBRComponents.h"
#import "PWPService.h"
#import "SXCUtility.h"
#import "UIColor+PWPColor.h"
#import "UIImageView+WebCache.h"
#import <Google/Analytics.h>
#import "PWPConstant.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPCreateMessageViewController (){

    __weak IBOutlet UIImageView *imageViewTO;
    __weak IBOutlet UIImageView *imageviewFrom;
    __weak IBOutlet UIView *viewToDog;
    __weak IBOutlet UIView *viewFromDog;
    __weak IBOutlet UIImageView * imageViewBG;
    
    NSArray * userDogsArray;
    NSArray * selectedDogFriendssArray;
    NSMutableDictionary * selectedDogFriend;
    NSMutableDictionary * selectedDog;
    NSMutableDictionary * selectedPark;

}

@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textfieldFrom;
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textfieldTo;
@property (weak, nonatomic) IBOutlet UITextView *textViewMessage;

- (IBAction)fromButtonClicked:(id)sender;
- (IBAction)toButtonClicked:(id)sender;

@end

@implementation PWPCreateMessageViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];

    [self sxc_customizeBackButton];
    [self sxc_navigationBarCustomizationWithTitle:@"New Message"];
   
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    [_textfieldTo setValue:[UIColor colorWithRed:43/255.0f green:195/255.0f blue:154/255.0f alpha:1.0f]
                  forKeyPath:@"_placeholderLabel.textColor"];

    [_textfieldFrom setValue:[UIColor colorWithRed:43/255.0f green:195/255.0f blue:154/255.0f alpha:1.0f]
                forKeyPath:@"_placeholderLabel.textColor"];
    
    _textfieldFrom.textColor=[UIColor colorWithRed:43/255.0f green:195/255.0f blue:154/255.0f alpha:1.0f];
    _textfieldTo.textColor=[UIColor colorWithRed:43/255.0f green:195/255.0f blue:154/255.0f alpha:1.0f];
    
    _textfieldFrom.placeholder = @"Select dog from your pack";
    _textfieldTo.placeholder = @"Select a friend of your dog";
    
    _textViewMessage.backgroundColor=[UIColor colorWithRed:244.0/255.0f green:244/255.0f blue:244/255.0f alpha:1.0f];
    _textViewMessage.font=[UIFont fontWithName:@"Roboto" size:14.0f];
    _textViewMessage.textColor=[UIColor sxc_appTextfieldTextColor];
    
    viewToDog.layer.borderColor=[UIColor sxc_hintColor].CGColor;
    viewToDog.layer.borderWidth=1.0f;
    viewFromDog.layer.borderColor=[UIColor sxc_hintColor].CGColor;
    viewFromDog.layer.borderWidth=1.0f;
    
    imageviewFrom.image = [UIImage imageNamed:@"img_dog_sample"];
    imageViewTO.image = [UIImage imageNamed:@"img_dog_sample"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Create Message Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
    [self dogs];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)isValid{
    if(_textfieldFrom.text.length==0){
        [self sxc_showErrorViewWithMessage:@"Please select your dog from pack."];
        return NO;
    }
    else if(_textfieldTo.text.length==0){
        [self sxc_showErrorViewWithMessage:@"Please select your selected dog friends."];
                return NO;
    }
    else if(_textViewMessage.text.length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter the message body."];
        return NO;
        
    }
    return YES;

}


-(void)dogs{
    
    [self startLoader];
    
    [[PWPService sharedInstance] currentUserDogsWithRequestDictionary:nil withSuccessBlock:^(id response) {
        
        userDogsArray=[SXCUtility cleanArray:response];
        
        selectedDog=nil;
        selectedDogFriend=nil;
        selectedDogFriendssArray=nil;
        selectedPark=nil;
        _textfieldFrom.text=@"";
        _textfieldTo.text=@"";
        
        [self dismissLoader];
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        [self sxc_showErrorViewWithMessage:error.domain];
        
    }];
    
}


- (IBAction)fromButtonClicked:(id)sender {
    
    
    [self sxc_showActionSheetWithTitle:@"From:" pickerWithRows:[userDogsArray valueForKeyPath:@"name"] itemSelectedAtIndex:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        _textfieldFrom.text=selectedValue;
        _textfieldTo.text=@"";
        
        if(userDogsArray.count>selectedIndex){
            
            selectedDogFriendssArray=[SXCUtility cleanArray:[userDogsArray[selectedIndex] valueForKey:@"friends"] ];
            
            [imageviewFrom sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,[SXCUtility cleanArray:[userDogsArray[selectedIndex] valueForKey:@"image_url"] ]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];

            
            selectedDog=userDogsArray[selectedIndex];
            
            selectedDogFriend=nil;
        }
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (IBAction)toButtonClicked:(id)sender {
    
    if(selectedDogFriendssArray==nil){
        [self sxc_showErrorViewWithMessage:@"Please select dog from your pack."];
    }
    else if(selectedDogFriendssArray.count==0){
        [self sxc_showErrorViewWithMessage:@"Your selected dog doesn't have any friend"];
    }
    else
    {
        [self sxc_showActionSheetWithTitle:@"To:" pickerWithRows:[selectedDogFriendssArray valueForKeyPath:@"name"] itemSelectedAtIndex:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            _textfieldTo.text=selectedValue;
            
            selectedDogFriend=selectedDogFriendssArray[selectedIndex];
        
            
            [imageViewTO sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,[SXCUtility cleanArray:[selectedDogFriendssArray[selectedIndex] valueForKey:@"image_url"] ]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];

            
        } cancelBlock:^(ActionSheetStringPicker *picker) {
            
        } origin:self.view];
    }
}

-(IBAction)createMessage:(id)sender{
    
    if([self isValid]){
        [self startLoader];
        [[PWPService sharedInstance] newMessageWithIntiatorString:selectedDog[@"id"] withReciepientString:selectedDogFriend[@"id"] withDictionary:@{@"body":_textViewMessage.text} withSuccessBlock:^(id response) {
            [self dismissLoader];
            [self sxc_showSuccessWithMessage:@"Message has been sent successfully."];
            
            [self resetView];
            
        } withErrorBlock:^(NSError *error) {
            [self sxc_showErrorViewWithMessage:error.domain];
            [self dismissLoader];
        }];
    }
}

- (IBAction)cancelMessageClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)resetView{
    _textfieldTo.text=@"";
    _textViewMessage.text = @"";
    _textfieldFrom.text=@"";
    selectedDog= nil;
    selectedDogFriend = nil;
    selectedDogFriendssArray = nil;
    selectedPark = nil;
    imageviewFrom.image = [UIImage imageNamed:@"img_dog_sample"];
    imageViewTO.image = [UIImage imageNamed:@"img_dog_sample"];

}

-(void)sxc_startLoading{}
-(void)sxc_stopLoading{}
@end
