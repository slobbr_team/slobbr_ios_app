//
//  PWPMessageThreadViewController.h
//  PawPark
//
//  Created by daffolap19 on 6/2/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPBaseViewController.h"

@interface PWPMessageThreadViewController : PWPBaseViewController
@property(nonatomic,strong)NSString * threadIDString;
@property(nonatomic,strong)NSArray * messages;
@property(nonatomic,strong)NSString * initiatorIDString;
@property(nonatomic,strong)NSString * recipientIDString;


@end
