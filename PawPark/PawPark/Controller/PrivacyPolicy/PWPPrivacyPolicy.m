//
//  PWPPrivacyPolicy.m
//  PawPark
//
//  Created by Mac on 8/27/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPPrivacyPolicy.h"

@interface PWPPrivacyPolicy ()<UIWebViewDelegate>

@end

@implementation PWPPrivacyPolicy

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*NSString *path = [[NSBundle mainBundle] pathForResource:@"Slobbr.pdf" ofType:nil];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSData *data = [NSData dataWithContentsOfFile:path];
    self.webvw.delegate = self;
    self.webvw.contentMode = UIViewContentModeScaleAspectFit;
    [self.webvw loadData:data MIMEType:@"application/vnd.openxmlformats-officedocument.wordprocessingml.document" textEncodingName:@"UTF-8" baseURL:url];
    */
    [self sxc_setNavigationBarCrossItem];
    [self sxc_navigationBarCustomizationWithTitle:@"Privacy Policy"];
    
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Slobbr" ofType:@"pdf"];
  
    NSString *strURLPath = [self trimString: path];
    NSURL *url = [NSURL fileURLWithPath:strURLPath];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webvw setScalesPageToFit:YES];
    [self.webvw loadRequest:request];
    
    
}

-(NSString*) trimString:(NSString *)theString {
    if (theString != [NSNull null])
        theString = [theString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return theString;
}




-(void)webViewDidStartLoad:(UIWebView *)webView
{
    
}



-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
}



-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



- (IBAction)closeClicked:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}


#pragma mark - Navigation related Methods

-(void)sxc_navigationBarCustomizationWithTitle:(NSString *)navigationTitle
{
    [self.navigationItem setTitle:[navigationTitle uppercaseString]];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    
    self.navigationController.navigationBarHidden=false;
}




-(void)sxc_setNavigationBarCrossItem
{
    UIImage * image=[UIImage imageNamed:@"ic_cross"];
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [menuButton setImage:image forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(sxc_dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setTintColor:[UIColor whiteColor]];
    
    
    menuButton.frame = CGRectMake(0, 0,30,30);
    
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    
    
    [self.navigationItem setLeftBarButtonItem:menuBarButton];
}



-(void)sxc_dismissViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(void)sxc_customizeRightNavigationBarMenuItem
{
    UIImage *backButtonImage = [UIImage imageNamed:(@"ic_add_header_bar")];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    backButton.frame = CGRectMake(0, 0 ,35, 35);
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton addTarget:self
                   action:@selector(rightMenuButtonClicked:)
         forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = backBarButtonItem;
    
}

-(IBAction)rightMenuButtonClicked:(id)sender{}

-(void)sxc_customizeBackButton
{
    if(self.navigationController.viewControllers.count>1){
        self.navigationItem.hidesBackButton = YES;
        [self.navigationItem setHidesBackButton:YES animated:YES];
        
        UIImage *backButtonImage = [UIImage imageNamed:(@"btn_back")];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [backButton setImage:backButtonImage forState:UIControlStateNormal];
        backButton.frame = CGRectMake(0, 0 ,23, 23);
        
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        [backButton addTarget:self
                       action:@selector(sxc_backButtonClicked:)
             forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = backBarButtonItem;
    }
}


//-(void)sxc_setLeftButtonWithBlank
//{
//    self.navigationItem.leftBarButtonItem=nil;
//    [self.navigationItem setHidesBackButton:YES animated:YES];
//
//    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
//        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
//    }
//
//    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [backButton setTitle:@"" forState:UIControlStateNormal];
//    [backButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
//    backButton.frame = CGRectMake(0, 0 ,23, 23);
//    [backButton setEnabled:NO];
//
//    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
//    backBarButtonItem.title=@"";
//    [backButton addTarget:self
//                   action:@selector(sxc_backButtonClicked:)
//         forControlEvents:UIControlEventTouchUpInside];
//    self.navigationItem.leftBarButtonItem = backBarButtonItem;
//}
//
//
-(void)sxc_backButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//
//-(void)sxc_toggleLeftMenu
//{
//    [self.menuContainerViewController toggleRightSideMenuCompletion:nil];
//
//}
//




@end
