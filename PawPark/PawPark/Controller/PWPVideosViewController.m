//
//  PWPVideosViewController.m
//  PawPark
//
//  Created by xyz on 7/26/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPVideosViewController.h"
#import "UIColor+PWPColor.h"
#import "PWPService.h"
#import "UIViewController+UBRComponents.h"
#import "PWPVideoCell.h"
#import <XCDYouTubeKit/XCDYouTubeKit.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "PWPDownloadBackgroundImage.h"

@interface PWPVideosViewController () <UITableViewDelegate , UITableViewDataSource>{

    __weak IBOutlet UIImageView * imageViewBG;
    
    NSMutableArray * videos;
}
@property(nonatomic,strong)UIRefreshControl * refreshControl;
@property(nonatomic,weak)IBOutlet UITableView * tableview;
@end

@implementation PWPVideosViewController

- (void)viewDidLoad {
  
    [super viewDidLoad];
    
    videos = [[NSMutableArray alloc] init];
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    self.tableview.delegate=self;
    self.tableview.dataSource=self;
    [self sxc_navigationBarCustomizationWithTitle:@"Slobbr Videos"];
    [self sxc_setNavigationBarMenuItem];
    
    [self addPullToRefresh];
    [self automaticallyRefresh];
    [self videosAPI];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)addPullToRefresh
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor=[UIColor sxc_themeColor];
    [self.refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    [self.tableview addSubview:self.refreshControl];
}

- (void)refreshView:(UIRefreshControl *)sender {
    [self videosAPI];
}

-(void)videosAPI
{
    [[PWPService sharedInstance] youtubeVideoUrlWithSuccessBlock:^(id response) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            videos = [[NSMutableArray alloc] initWithArray:response];
            [self.tableview reloadData];
            [self.refreshControl endRefreshing];
        });

        
    } withErrorBlock:^(NSError *error) {
        [self sxc_showErrorViewWithMessage:error.domain];
    }];
}

-(void)automaticallyRefresh{
    [self.tableview setContentOffset:CGPointMake(0, -self.refreshControl.frame.size.height) animated:YES];
    [self.refreshControl beginRefreshing];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    NSInteger rowCount = videos.count;
    return rowCount;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PWPVideoCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PWPVideoCell"];
    [cell configureCellWithData:videos[indexPath.row]];
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString * youtubeURL = videos[indexPath.row][@"url"];
    
    NSRange range = [youtubeURL rangeOfString:@"v="];
    
    if(range.location != NSNotFound){
        NSString * youtubeID = [youtubeURL substringFromIndex:range.location+2];
        XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:youtubeID];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerPlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerViewController.moviePlayer];
        [self presentMoviePlayerViewControllerAnimated:videoPlayerViewController];
    }
    else{
    
        AVPlayer * player = [[AVPlayer alloc] initWithURL:[NSURL URLWithString:youtubeURL]];
        AVPlayerViewController * avplayerController = [[AVPlayerViewController alloc] init];
        avplayerController.player = player;
        [self presentViewController:avplayerController animated:YES completion:nil];
        [player play];
    }

}
- (void) moviePlayerPlaybackDidFinish:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:notification.object];
    MPMovieFinishReason finishReason = [notification.userInfo[MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] integerValue];
    if (finishReason == MPMovieFinishReasonPlaybackError)
    {
        NSString *title = NSLocalizedString(@"Video Playback Error", @"Full screen video error alert - title");
        NSError *error = notification.userInfo[XCDMoviePlayerPlaybackDidFinishErrorUserInfoKey];
        NSString *message = [NSString stringWithFormat:@"%@\n%@ (%@)", error.localizedDescription, error.domain, @(error.code)];
        NSString *cancelButtonTitle = NSLocalizedString(@"OK", @"Full screen video error alert - cancel button");
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
        [alertView show];
    }
}

@end
