
//
//  PWPAmenititesViewController.m
//  PawPark
//
//  Created by Samarth Singla on 18/09/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPAmenititesViewController.h"
#import "UIColor+PWPColor.h"
#import "UIViewController+UBRComponents.h"
#import "SXCUtility.h"
#import "PWPDownloadBackgroundImage.h"


@interface PWPAmenititesViewController ()<UIAlertViewDelegate, UISearchBarDelegate>
{
    __weak IBOutlet UIImageView * imageViewBG;
    NSMutableArray * searchedZipCodeArray;
}


@property(nonatomic,weak)IBOutlet UITableView * tableViewAmenities;

@end

@implementation PWPAmenititesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"Amenities";
    
    self.amenities = [self.amenities sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)].mutableCopy;
    [self.amenities addObject:@"Other"];

    searchedZipCodeArray = [self.amenities mutableCopy];
    
    [self sxc_customizeBackButton];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
  }

-(void)sxc_backButtonClicked:(id)sender
{
    NSMutableDictionary * amenitiesDic = [SXCUtility cleanDictionary:[SXCUtility getNSObject:@"Amenities"]].mutableCopy;
    
    [self.amenities removeObject:@"Other"];
   
    [amenitiesDic setObject:self.amenities forKey:self.categoryName];
    
    [SXCUtility saveNSObject:amenitiesDic forKey:@"Amenities"];

    [super sxc_backButtonClicked:sender];
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return searchedZipCodeArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"amenties_cell" forIndexPath:indexPath];
    cell.textLabel.text=searchedZipCodeArray[indexPath.row];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.backgroundColor =[UIColor clearColor];
    cell.contentView.backgroundColor =[UIColor clearColor];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.font =[UIFont fontWithName:@"Roboto-Regular" size:20.0f];
    
    UIView *customColorView = [[UIView alloc] init];
    customColorView.backgroundColor = [UIColor sxc_themeColorWithAplha];
    cell.selectedBackgroundView =  customColorView;
    
    if([_selectedAmenities containsObject:searchedZipCodeArray[indexPath.row]]){
        cell.accessoryType=UITableViewCellAccessoryCheckmark;
        
    }else{
        cell.accessoryType=UITableViewCellAccessoryNone;
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0f;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryCheckmark){
        
        if([_selectedAmenities containsObject:searchedZipCodeArray[indexPath.row]]){
            [_selectedAmenities removeObject:searchedZipCodeArray[indexPath.row]];
        }
    }
    else{
        if(![_selectedAmenities containsObject:searchedZipCodeArray[indexPath.row]]){
            
            if((indexPath.row ==  searchedZipCodeArray.count-1)&&([searchedZipCodeArray[indexPath.row] isEqualToString:@"Other"])){
                [self openAlertViewForManualInput];
            }
            else{
                [_selectedAmenities addObject:searchedZipCodeArray[indexPath.row]];
            }
        }
    }
    
    [tableView reloadData];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

-(void)openAlertViewForManualInput{

    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Amenities" message:@"Enter Other Amenity Name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit",nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alert.delegate = self;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{

    UITextField * alertTextField = [alertView textFieldAtIndex:0];
    if(buttonIndex == 1 && alertTextField.text.length >0){
        
        if([searchedZipCodeArray containsObject:alertTextField.text.capitalizedString]){
            [_selectedAmenities addObject:alertTextField.text.capitalizedString];
            [_tableViewAmenities reloadData];
            return;
        }
        
        
        [self.amenities addObject:alertTextField.text];
        
        [searchedZipCodeArray removeObject:@"Other"];
        [searchedZipCodeArray addObject:alertTextField.text];
        [searchedZipCodeArray addObject:@"Other"];
        
        [_selectedAmenities addObject:alertTextField.text];
        [_tableViewAmenities reloadData];
    }

}

- (IBAction)doneClicked:(id)sender {
    
    NSMutableDictionary * amenitiesDic = [SXCUtility cleanDictionary:[SXCUtility getNSObject:@"Amenities"]].mutableCopy;
    [self.amenities removeObject:@"Other"];

    [amenitiesDic setObject:self.amenities forKey:self.categoryName];
    
    [SXCUtility saveNSObject:amenitiesDic forKey:@"Amenities"];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if(searchText.length==0){
        searchedZipCodeArray= [self.amenities mutableCopy];
    }
    else{
        
        NSPredicate * predicate=[NSPredicate predicateWithFormat:@"self CONTAINS[cd] %@ OR self CONTAINS[cd] %@",searchText,searchText];
        
        searchedZipCodeArray=(id)[self.amenities filteredArrayUsingPredicate:predicate];
    }
    [_tableViewAmenities reloadData];
    
}


@end
