//
//  PWPCommentsViewController.m
//  PawPark
//
//  Created by Samarth Singla on 10/09/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPCommentsViewController.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPService.h"
#import "PWPCommentListCell.h"
#import "PWPCommentImageCell.h"

#import "UIViewController+UBRComponents.h"
#import "UIColor+PWPColor.h"
#import "SXCUtility.h"

#import <UIImageView+WebCache.h>

@interface PWPCommentsViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSArray * comments;

}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintPostHeight;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPost;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewBg;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewUser;
@property (weak, nonatomic) IBOutlet UILabel *labelUserName;
@property (weak, nonatomic) IBOutlet UILabel *labelComments;
@property (weak, nonatomic) IBOutlet UITextField *tetfieldComment;

@property (weak, nonatomic) IBOutlet UILabel *labelDate;

@end

@implementation PWPCommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self sxc_customizeBackButton];
    [self sxc_navigationBarCustomizationWithTitle:@"COMMENTS"];

   
    
    [self.imageViewUser sd_setImageWithURL:[NSURL URLWithString:self.userImageUrlString] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
    [self.labelUserName setText:self.userName];
    self.labelComments.text = [NSString stringWithFormat:@"%@ Comments",self.commentCount];
    self.labelDate.text = self.dateString;
    
    self.imageViewBg.image = [PWPDownloadBackgroundImage backgroundImage];
    [self.tableView registerNib:[UINib nibWithNibName:@"PWPCommentListCell" bundle:nil] forCellReuseIdentifier:@"PWPCommentListCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"PWPCommentImageCell" bundle:nil] forCellReuseIdentifier:@"PWPCommentImageCell"];
    
    [self fetchComments];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return comments.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if(indexPath.row == 0){
        PWPCommentImageCell * imageCell = [tableView dequeueReusableCellWithIdentifier:@"PWPCommentImageCell"];
        if(_postImageUrl != nil && _postImageUrl.length > 0) {
            [imageCell.imageViewComment sd_setImageWithURL:[NSURL URLWithString:self.postImageUrl] placeholderImage:[UIImage imageNamed:@"default_park"]];
        }
        return imageCell;
    }
    else{
        PWPCommentListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PWPCommentListCell"];
        [cell configureUIWithData:[comments objectAtIndex:indexPath.row-1]];
        return cell;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if(indexPath.row == 0){
        if(_postImageUrl != nil && _postImageUrl.length > 0) {
            return 250;
        }
        else {
            return 0;
        }
        
    } else {
        return UITableViewAutomaticDimension;
    }
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0){
        if(_postImageUrl != nil && _postImageUrl.length > 0) {
            return 150;
        }
        else {
            return 0;
        }
        
    } else {
        return UITableViewAutomaticDimension;
    }
    
}

-(void)fetchComments {
    
    [self startLoader];
    [[PWPService sharedInstance] post:self.postId comment:self.communityID withRequestDictionary:nil withData:nil withSuccessBlock:^(id responseDicitionary) {
        [self dismissLoader];
        comments = [SXCUtility cleanArray:responseDicitionary];
        [_tableView reloadData];
        
        [self.delegate commentCountUpdate:comments.count withIndexPath:nil withPostId:self.postId];
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        [self sxc_showErrorViewWithMessage:error.domain];
    }];
}

- (IBAction)postComment:(id)sender {
    
    if(self.tetfieldComment.text == nil || self.tetfieldComment.text.length == 0) {
        [self sxc_showErrorViewWithMessage:@"Please enter the comment"];
        return;
    }
    
    [self startLoader];
    [[PWPService sharedInstance] newPost:self.postId comment:self.communityID withRequestDictionary:@{@"body":self.tetfieldComment.text}.mutableCopy withData:nil withSuccessBlock:^(id responseDicitionary) {
        
        _tetfieldComment.text = @"";
        
        NSInteger aCommentsCount = self.commentCount.integerValue;
        aCommentsCount++;
        
        self.labelComments.text = [NSString stringWithFormat:@"%ld Comments",(long)aCommentsCount];
        
        [self dismissLoader];
        [self sxc_showSuccessWithMessage:@"New comment has been posted successfully."];
        [self fetchComments];
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        [self sxc_showErrorViewWithMessage:error.domain];
    }];
    
    
}

-(void)sxc_startLoading {}

-(void)sxc_stopLoading {}
@end
