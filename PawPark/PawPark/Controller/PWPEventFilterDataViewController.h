//
//  PWPEventFilterDataViewController.h
//  PawPark
//
//  Created by HupendraRaghuwanshi on 08/03/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PWPEventListFilterDelegate <NSObject>
@optional
-(void)selectedFilter:(NSMutableDictionary*)filterDictionary;

@end

@interface PWPEventFilterDataViewController : UIViewController


@property(nonatomic,strong)NSMutableDictionary * parkFilterDictionary;

@property(nonatomic,weak)id <PWPEventListFilterDelegate> delegate;
@property(nonatomic,strong) NSString * selectedState;
@property(nonatomic,strong) NSString * selectedStateCode;
@property(nonatomic,strong) NSString * selectedCity;
@property(nonatomic,strong) NSString * selectedZipcode;
@property(nonatomic,strong) NSString * selectedString;
@property(nonatomic,strong) NSString * minTime;
@property(nonatomic,strong) NSString * maxTime;
@property(nonatomic,strong) NSString * search;
@end
