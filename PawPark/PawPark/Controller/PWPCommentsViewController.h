//
//  PWPCommentsViewController.h
//  PawPark
//
//  Created by Samarth Singla on 10/09/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWPCommunityHomeViewController.h"

@interface PWPCommentsViewController : UIViewController
@property(nonatomic,strong)NSString * communityID;
@property(nonatomic,strong)NSString * postId;
@property(nonatomic,strong)NSString * userName;
@property(nonatomic,strong)NSString * userImageUrlString;
@property(nonatomic,strong)NSString * commentCount;
@property(nonatomic,strong)NSString * dateString;
@property (strong, nonatomic) NSString * postImageUrl;

@property(nonatomic,weak)id <PWPCommunityHomeDelegate> delegate;

@end
