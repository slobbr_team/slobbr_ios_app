//
//  PWPSelectCategoryViewController.h
//  PawPark
//
//  Created by Vibhore Sharma on 27/07/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPSelectCategoryViewController : UIViewController
@property(nonatomic,strong)NSMutableDictionary * selectedCategory;
@property(nonatomic,strong)NSMutableArray * selectedAmenties;

@end
