//
//  PWPSTADescriptionViewController.m
//  PawPark
//
//  Created by daffolap19 on 8/2/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPSTADescriptionViewController.h"
#import "UIViewController+UBRComponents.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPSTADescriptionViewController ()
{
    __weak IBOutlet UIImageView * imageViewBG;
}
@property (weak, nonatomic) IBOutlet UILabel *labelS;
@property (weak, nonatomic) IBOutlet UILabel *labelT;
@property (weak, nonatomic) IBOutlet UILabel *labelA;

@end

@implementation PWPSTADescriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    [self sxc_navigationBarCustomizationWithTitle:@"S.T.A.y Score"];
    
    if(self.navigationController.viewControllers.count>1){
        [self sxc_customizeBackButton];
    }
    else{
        [self.navigationItem setHidesBackButton:YES];
    }
    
    [self.navigationController.navigationBar setTranslucent:YES];
    
    _labelS.clipsToBounds=YES;
    _labelT.clipsToBounds=YES;
    _labelA.clipsToBounds=YES;
    
    _labelS.layer.cornerRadius=40.0f;
    _labelT.layer.cornerRadius=40.0f;
    _labelA.layer.cornerRadius=40.0f;
    
    [self configureWithlabels];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configureWithlabels
{
    
    _labelT.text=[NSString stringWithFormat:@"t:%@",_t];
    _labelS.text=[NSString stringWithFormat:@"s:%@",_s];
    _labelA.text=[NSString stringWithFormat:@"a:%@",_a];
    
}


-(IBAction)skipButtonClicked:(id)sender{
    if(self.navigationController.viewControllers.count>1){
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
@end
