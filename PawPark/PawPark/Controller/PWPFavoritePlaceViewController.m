//
//  PWPFavoritePlaceViewController.m
//  PawPark
//
//  Created by Vibhore Sharma on 28/07/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPFavoritePlaceViewController.h"

#import "PWPApplicationUtility.h"
#import "UIViewController+UBRComponents.h"
#import "PWPService.h"
#import "PWPSearchParkTableViewCell.h"
#import "SXCUtility.h"
#import "PWPParkCheckInViewController.h"
#import "PWPParkCheckedInDogsViewController.h"
#import <Google/Analytics.h>
#import "UBRLocationHandler.h"
#import "UIColor+PWPColor.h"
#import "PWPSchedulePawDateViewController.h"
#import "PWPSTADescriptionViewController.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPCheckInHelper.h"
#import "PWPPlacePicViewController.h"
#import <Mixpanel.h>

@interface PWPFavoritePlaceViewController ()<PWPParkCheckInDelegae,PWPParkCellDelegate,UITableViewDelegate,UITableViewDataSource,PWPPlacePicViewControllerDelegate>
{
    __weak IBOutlet UITableView * tableViewPlace;
    __weak IBOutlet UIImageView * imageViewBG;
    __weak IBOutlet UILabel *labelNoData;
    
    NSInteger selectedIndex;
    NSMutableArray * favoritePlaces;
    UIRefreshControl * refreshControl;
}
@end

@implementation PWPFavoritePlaceViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self addPullToRefresh];
    [self initView];
    [self favoriteParks];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [tableViewPlace reloadData];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Favorite Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Buisness Methods

-(void)initView
{
    selectedIndex=-1;
    
    favoritePlaces = [[NSMutableArray alloc] init];
    
    tableViewPlace.delegate=self;
    tableViewPlace.dataSource=self;
    
    labelNoData.hidden = YES;
    
    [self sxc_navigationBarCustomizationWithTitle:@"Favorite Places"];
    [self sxc_setNavigationBarCrossItem];
    
    [tableViewPlace registerNib:[UINib nibWithNibName:@"PWPSearchParkTableViewCell" bundle:nil] forCellReuseIdentifier:@"PWPParkListiningCell"];
}

#pragma mark - UITableView Delegate And DataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PWPSearchParkTableViewCell * tableViewCell =[tableView dequeueReusableCellWithIdentifier:@"PWPParkListiningCell" forIndexPath:indexPath];
    
    [tableViewCell bindValueWithComponentsWithData:favoritePlaces[indexPath.row]  reloadData:(indexPath.row == selectedIndex)];
    [tableViewCell setDelegate:self];
    return tableViewCell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return favoritePlaces.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == selectedIndex && [SXCUtility cleanArray:[favoritePlaces[indexPath.row] valueForKey:@"filter_amenities"]].count > 0) {
        return 295.0f;
    }else {
        return 238.0f;
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   if(indexPath.row == selectedIndex && [SXCUtility cleanArray:[favoritePlaces[indexPath.row] valueForKey:@"filter_amenities"]].count > 0) {
       return 295.0f;
   }else {
       return 238.0f;
   }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [[Mixpanel sharedInstance] track:@"Fav Place Clicked"];

    NSInteger previousSelectedRow = selectedIndex;
    
    
    if(indexPath.row!=selectedIndex){
        selectedIndex=indexPath.row;
    }
    else{
        selectedIndex=-1;
    }
    
    NSMutableArray * indexes = [[NSMutableArray alloc] init];
    if(previousSelectedRow != -1){
        [indexes addObject:[NSIndexPath indexPathForRow:previousSelectedRow inSection:0]];
    }
    
    if(selectedIndex != -1){
        [indexes addObject:[NSIndexPath indexPathForRow:selectedIndex inSection:0]];
    }
    
    [tableView reloadRowsAtIndexPaths:indexes
                     withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Service Method


-(void)favoriteParks{
    
    [self automaticallyRefresh];
    
    [[PWPService sharedInstance] currentUserParkWithRequestDictionary:nil withSuccessBlock:^(id response)
     {
         favoritePlaces = [[NSMutableArray alloc] initWithArray:response];
         
         NSMutableArray * tempFavPlaces = [[NSMutableArray alloc] init];
         for(NSDictionary * dict in favoritePlaces){
         
             NSMutableDictionary * tempPlace = dict.mutableCopy;
             NSMutableArray * tempAmenities = [[NSMutableArray alloc] init];
             NSMutableDictionary * amenities = [SXCUtility cleanDictionary:[tempPlace valueForKey:@"amenities"]].mutableCopy;
             
             for(NSString * value in amenities.allValues) {
                 if([UIImage imageNamed:value] != nil){
                      [tempAmenities addObject:[value stringByReplacingOccurrencesOfString:@"/" withString:@":"]];
                 }
             }
             [tempPlace setValue:tempAmenities forKey:@"filter_amenities"];
             [tempFavPlaces addObject:tempPlace];
         }
         
         favoritePlaces = tempFavPlaces;
         
         
         [tableViewPlace reloadData];
         
         if(favoritePlaces.count==0){
             labelNoData.hidden=NO;
             [self.view bringSubviewToFront:labelNoData];
         }
         else{
             labelNoData.hidden=YES;
         }
         
         [refreshControl endRefreshing];
         
     } withErrorBlock:^(NSError *error) {

         [self sxc_showErrorViewWithMessage:error.domain];

         [refreshControl endRefreshing];
         
         if(favoritePlaces.count==0){
             labelNoData.hidden=NO;
             [self.view bringSubviewToFront:labelNoData];
         }
         else{
             labelNoData.hidden=YES;
         }
     }];
    
    
}




#pragma mark - Delegate Methods


-(CGFloat)getHeightOfParkBasedOnAmentis:(NSDictionary *)parkDictionary
{
    
    NSMutableString * amentiesString=[[NSMutableString alloc] init];
    
    for(NSString * amenity in parkDictionary[@"amenities"]){
        if(amentiesString.length>0){
            [amentiesString appendString:@"\n"];
        }
        [amentiesString appendString:amenity];
    }
    CGFloat amentiesHeight=[self getHeightOfLabelWithText:amentiesString withFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0f]];
    
    if(amentiesHeight==0){
        return 153;
    }
    return 153+amentiesHeight;
}





-(CGFloat)getHeightOfLabelWithText:(NSString *)text withFont:(UIFont *)font
{
    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    gettingSizeLabel.font = font;
    gettingSizeLabel.text = text;
    gettingSizeLabel.numberOfLines = 0;
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width-16, 9999);
    
    CGSize expectSize = [gettingSizeLabel sizeThatFits:maximumLabelSize];
    return expectSize.height;
}

#pragma mark - PWPParkDelegate
-(void)parkOptionClickedWithData:(NSDictionary *)dictionary
{
    [[Mixpanel sharedInstance] track:@"Phots Clicked On Fav List"];

    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    PWPPlacePicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPPlacePicViewController"];
    viewController.placePics = [SXCUtility cleanArray:[dictionary valueForKey:@"gallery"]].mutableCopy;
    viewController.data = dictionary;
    viewController.placeId = dictionary[@"id"];
    viewController.delegate = self;

    [self.navigationController pushViewController:viewController animated:YES];
}


-(void)parkOption2ClickedWithData:(NSDictionary *)dictionary
{
    if([SXCUtility cleanString:[dictionary[@"active_dogs_count"] description]].integerValue > 0){

        [[Mixpanel sharedInstance] track:@"Checked-In Dogs Clicked Of Fav List"];

        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PWPParkCheckedInDogsViewController * parkCheckInDogsViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckedInDogsViewController"];
        parkCheckInDogsViewController.placeId = [SXCUtility cleanString:[[dictionary valueForKey:@"id"] description]];
        
        [self.navigationController pushViewController:parkCheckInDogsViewController animated:YES];
    }
}

-(void)parkOption3ClickedWithData:(NSDictionary *)dictionary
{
    [self startLoader];

    [[Mixpanel sharedInstance] track:@"Favorite Clicked On Fav List"];

    if([SXCUtility cleanInt:dictionary[@"is_favourite"]] ==1){
        
        [[PWPService sharedInstance] deleteHomePark:dictionary[@"id"] withSuccessBlock:^(id response) {
            [self dismissLoader];
            
            NSInteger index =[favoritePlaces indexOfObject:dictionary];
            NSMutableDictionary * parkDictionary = dictionary.mutableCopy;
            
            if([SXCUtility cleanInt:parkDictionary[@"is_favourite"]] ==1){
                parkDictionary[@"is_favourite"]=@0;
            }
            else{
                parkDictionary[@"is_favourite"]=@1;
            }
            
            [favoritePlaces removeObjectAtIndex:index];
            
            
            [tableViewPlace reloadData];
            [self sxc_showSuccessWithMessage:@"Place has been removed from 'Favorite' Places."];
            
            if(favoritePlaces.count==0){
                labelNoData.hidden=NO;
                [self.view bringSubviewToFront:labelNoData];
            }
            else{
                labelNoData.hidden=YES;
            }
            
            
        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:error.domain];
            
            if(favoritePlaces.count==0){
                labelNoData.hidden=NO;
                [self.view bringSubviewToFront:labelNoData];
            }
            else{
                labelNoData.hidden=YES;
            }
            
        }];
        
    }
    else{
        
        
        [[PWPService sharedInstance] homePark:dictionary[@"id"] withSuccessBlock:^(id response) {
            [self dismissLoader];
            
            NSInteger index =[favoritePlaces indexOfObject:dictionary];
            NSMutableDictionary * parkDictionary = dictionary.mutableCopy;
            
            if([SXCUtility cleanInt:parkDictionary[@"is_favourite"]] ==1){
                parkDictionary[@"is_favourite"]=@0;
            }
            else{
                parkDictionary[@"is_favourite"]=@1;
            }
            
            [favoritePlaces replaceObjectAtIndex:index withObject:parkDictionary];
            
            NSMutableDictionary * userDictionary=[[PWPApplicationUtility getCurrentUserProfileDictionary] mutableCopy];
            [userDictionary setValue:dictionary forKey:@"place"];
            [PWPApplicationUtility saveCurrentUserProfileDictionary:userDictionary];
            
            [tableViewPlace reloadData];
            [self sxc_showSuccessWithMessage:@"Place has been marked as 'Favorite' Place."];
            
        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:error.domain];
        }];
    }
}

-(void)staOptionClickedWithData:(NSDictionary *)dictionary
{
    
    if(((NSNumber *)[SXCUtility getNSObject:@"isStaHelpOff"]).boolValue){
        
        UIStoryboard * storyBoard =[UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];
        
        PWPSTADescriptionViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPSTADescriptionViewController"];
        viewController.t= [dictionary valueForKey:@"sta_score"][@"t"];
        viewController.s= [dictionary valueForKey:@"sta_score"][@"s"];
        viewController.a= [dictionary valueForKey:@"sta_score"][@"a"];
        
        [self.navigationController pushViewController:viewController animated:true];
    }
}

-(void)inviteOptionClicked:(NSDictionary *)dictionary
{
    [[Mixpanel sharedInstance] track:@"Invite Clicked On Fav List"];

    [self pushSchedulePawDateViewController:dictionary];
}

-(void)parkQuickCheckInClicked:(NSDictionary *)dictionary
{
    [[Mixpanel sharedInstance] track:@"Quick Check-in Clicked On Fav List"];

    if([PWPCheckInHelper checkInAllowedForParkId:dictionary[@"id"] forDogs:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"]] == false){
        return;
    }
    
    [self startLoader];
    
    [[PWPService sharedInstance] checkInParkWithRequestDictionary:[self requestDictionary] withParkId:dictionary[@"id"] withSuccessBlock:^(id response) {
        
        [PWPCheckInHelper dogCheckedInParkId:dictionary[@"id"] forDogs:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"]];
        
        [self dismissLoader];
        [self sxc_showSuccessWithMessage:@"You have been checked-in successfully."];
        [self favoriteParks];
        [self automaticallyRefresh];
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];

        if (error.code==403) {
            [self sxc_showErrorViewWithMessage:@"You have already checked-in."];
        }
        else{
            [self sxc_showErrorViewWithMessage:error.domain];
        }
        
    }];
}

-(void)addPlaceClickedWithData:(NSDictionary *)dictionary {
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    
    PWPPlacePicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPPlacePicViewController"];
    viewController.placePics = [SXCUtility cleanArray:[dictionary valueForKey:@"gallery"]].mutableCopy;
    viewController.data = dictionary;
    viewController.placeId = dictionary[@"id"];
    viewController.delegate = self;
    viewController.isGalleryOpen = true;
    [self.navigationController pushViewController:viewController animated:YES];
}



-(NSMutableDictionary *)requestDictionary
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] init];
    
    if([PWPApplicationUtility getCurrentUserDogsArray].count>1){
        [requestDictionary setValue:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"] forKey:@"dogs"];
    }

    [requestDictionary setValue:[SXCUtility cleanString:[SXCUtility getNSObject:@"quickCheckinDefaultMessage"]] forKey:@"message"];
    
    return requestDictionary;
}

-(void)addPullToRefresh
{
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor=[UIColor sxc_themeColor];
    [refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    [tableViewPlace addSubview:refreshControl];
}


-(void)successFullCheckedInWithDictionary:(NSMutableDictionary *)data WithDogs:(NSMutableArray *)dogsArray{
    
    [self favoriteParks];
}


-(void)automaticallyRefresh{
    [tableViewPlace setContentOffset:CGPointMake(0, -refreshControl.frame.size.height) animated:YES];
    [refreshControl beginRefreshing];
}

- (void)refreshView:(UIRefreshControl *)sender {
    [self favoriteParks];
}

-(void)pushSchedulePawDateViewController:(NSDictionary *)parkDictionary{
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    PWPSchedulePawDateViewController *pawDateViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPSchedulePawDateViewController"];
    pawDateViewController.selectedPark = parkDictionary.mutableCopy;
    [self.navigationController pushViewController:pawDateViewController animated:YES];
    
}

-(void)placeProfileUpdated {
    [self favoriteParks];
}



-(void)sxc_stopLoading{}
-(void)sxc_startLoading{}

@end
