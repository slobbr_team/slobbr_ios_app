//
//  PWPEventListingTableViewCell.h
//  PawPark
//
//  Created by HupendraRaghuwanshi on 07/03/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPEventListingTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelEventName;
@property (weak, nonatomic) IBOutlet UILabel *labelEventDate;
@property (weak, nonatomic) IBOutlet UILabel *labelEventAddress;


@property (weak, nonatomic) IBOutlet UIImageView *imageViewEvent;

@property (weak, nonatomic) IBOutlet UIButton *buttonDetails;
@property (weak, nonatomic) IBOutlet UIButton *buttonDirection;

-(void)configureCellWithData:(NSMutableDictionary *)dictionary;

@end
