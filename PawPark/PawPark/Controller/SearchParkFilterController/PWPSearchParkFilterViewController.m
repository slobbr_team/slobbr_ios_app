//
//  PWPSearchParkFilterViewController.m
//  PawPark
//
//  Created by daffomac on 4/24/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPSearchParkFilterViewController.h"
#import "UIViewController+UBRComponents.h"
#import "UBRTextfield.h"
#import "SXCUtility.h"
#import "PWPZipCodeSelectionViewController.h"
#import <Google/Analytics.h>
#import "UBRLocationHandler.h"
#import "SXCTextfieldWithLeftImage.h"
#import "PWPSelectCategoryViewController.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPConstant.h"
#import <UIButton+WebCache.h>
#import "PWPItemSelectionViewController.h"

@interface PWPSearchParkFilterViewController () <PWPCitySelectionDelegate>
{
    __weak IBOutlet UIImageView * imageViewBG;
    __weak IBOutlet UIImageView * imageViewFilter;
    __weak IBOutlet UIButton * buttonAdvertisment;

    NSString * selectedState;
    NSString * selectedStateCode;
    NSString * selectedCity;
    NSString * selectedZipcode;
    NSString * selectedString;
}

@property (weak, nonatomic) IBOutlet UBRTextfield *textfieldDistance;
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textfieldCity;
@property (weak, nonatomic) IBOutlet UIButton *buttonCity;

@property (weak, nonatomic) IBOutlet UBRTextfield *textfieldSearch;
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textfieldCategory;


- (IBAction)filterClearButtonClicked:(id)sender;
- (IBAction)categoryClicked:(id)sender;
- (IBAction)distanceButtonClicked:(id)sender;

@end

@implementation PWPSearchParkFilterViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    selectedZipcode=[[NSMutableString alloc]init];
    
    UIImage * filterImage = [PWPDownloadBackgroundImage filterBg];
    if(filterImage != nil){
        imageViewFilter.image =  filterImage;
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initView];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Search Park Filter Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - BUISNESS METHODS
-(void)updateTopLabel
{
    

}

-(void)initView
{
    if(selectedZipcode==nil){
        selectedZipcode=[[NSString alloc] init];
    }
    
    [self sxc_navigationBarCustomizationWithTitle:@"SEARCH PLACES"];
    [self sxc_customizeBackButton];

    selectedState = [SXCUtility cleanString:_parkFilterDictionary[@"state"]];
    selectedCity = [SXCUtility cleanString: _parkFilterDictionary[@"city"]];
    selectedStateCode = [SXCUtility cleanString: _parkFilterDictionary[@"stateCode"]];
    selectedZipcode = [SXCUtility cleanString: _parkFilterDictionary[@"zipcode"]];

    if (selectedZipcode != nil && selectedZipcode.length > 0){
        selectedString= selectedZipcode;
    }
    else if (selectedCity.length > 0 && selectedStateCode.length > 0 && selectedState.length > 0){
        selectedString = [NSString stringWithFormat:@"%@(%@)",selectedCity,selectedState];
    }
    else if (selectedState.length > 0 && selectedStateCode.length > 0){
        selectedString= selectedState;
    }


    if([SXCUtility isEmptyString:selectedString]){
        self.textfieldCity.text=@"";
    }else{
        self.textfieldCity.text=selectedString;
    }


    _textfieldSearch.text=_parkFilterDictionary[@"s"];
    _textfieldDistance.text=[NSString stringWithFormat:@"%.1f mile(s)",[_parkFilterDictionary[@"distance"] floatValue]];
    

    if(_selectedPlaceCategory == nil){
        _selectedPlaceCategory = [[NSMutableDictionary alloc] init];
    }
    _textfieldCategory.text = _selectedPlaceCategory[@"name"];

    NSString * string = [SXCUtility cleanString:[SXCUtility getNSObject:@"filter_link"]];

    if(string.length > 0){
        [buttonAdvertisment addTarget:self action:@selector(advertisementClicked:) forControlEvents:UIControlEventTouchUpInside];
        buttonAdvertisment.userInteractionEnabled = TRUE;
    }

    else {
        buttonAdvertisment.userInteractionEnabled = FALSE;
    }
}

-(IBAction)advertisementClicked:(id)sender {
    
    if([PWPDownloadBackgroundImage isSkinApplied] == false){
        return;
    }
    
    NSString * string = [SXCUtility cleanString:[SXCUtility getNSObject:@"filter_link"]];


    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:string]]) {

        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Filter Link"
                                                              action:@"Button press"
                                                               label:@"Clicked"
                                                               value:nil] build]];

        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
    }
}

- (IBAction)fetchButtonClicked:(id)sender {

    _parkFilterDictionary[@"distance"]=[_textfieldDistance.text stringByReplacingOccurrencesOfString:@" mile(s)" withString:@""];
    _parkFilterDictionary[@"s"]=_textfieldSearch.text;
    
    _parkFilterDictionary[@"isUserLocation"] = @false;
    _parkFilterDictionary[@"zipcode"]=selectedZipcode;
    _parkFilterDictionary[@"stateCode"]=selectedStateCode;
    _parkFilterDictionary[@"city"]=selectedCity;
    
    [_delegate filterAppliedWithDictionary:_parkFilterDictionary withCategory:_selectedPlaceCategory];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)filterClearButtonClicked:(id)sender {
    _textfieldDistance.text=@"1 mile(s)";
    _textfieldSearch.text=@"";
    _selectedPlaceCategory  = [[NSMutableDictionary alloc] init];
    _textfieldCategory.text=@"";
    _textfieldCity.text=@"";
    selectedStateCode = @"";
    selectedCity = @"";
    selectedZipcode = @"";
    selectedState = @"";
}

- (IBAction)categoryClicked:(id)sender {
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    PWPSelectCategoryViewController * categoryViewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPSelectCategoryViewController"];
    categoryViewController.selectedCategory = _selectedPlaceCategory;
    [self.navigationController pushViewController:categoryViewController animated:YES];
}

- (IBAction)cityButtonClicked:(id)sender {
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    PWPItemSelectionViewController * citySelectedViewController = [storyboard instantiateViewControllerWithIdentifier:@"PWPItemSelectionViewController"];
    citySelectedViewController.delegate = self;
    citySelectedViewController.selectedZipCode = selectedZipcode;
    citySelectedViewController.selectedState = selectedState;
    citySelectedViewController.selectedCity = selectedCity;
    citySelectedViewController.selectedStateCode = selectedStateCode;
    citySelectedViewController.selectedType = PWPSelectionTypeCity;
    citySelectedViewController.selectedItem = selectedString;

    [self.navigationController pushViewController:citySelectedViewController animated:YES];
}

-(void)selectedCity:(NSString *)theSelectedCity selectedState:(NSString *)theSelectedState selectedStateCode:(NSString *)theSelectedStateCode selectedZipCode:(NSString *)theSelectedZipCode type:(PWPSelectionType)type selectedString:(NSString *)theSelectedString
{
    selectedCity = theSelectedCity;
    selectedState = theSelectedState;
    selectedZipcode = theSelectedZipCode;
    selectedStateCode = theSelectedStateCode;
    _textfieldCity.text = theSelectedString;
    selectedString = theSelectedString;

    _parkFilterDictionary[@"state"] = selectedState;
    _parkFilterDictionary[@"stateCode"] = selectedStateCode;
    _parkFilterDictionary[@"city"] = selectedCity;
    _parkFilterDictionary[@"zipcode"] = selectedZipcode;
}

- (IBAction)distanceButtonClicked:(id)sender {
    
    
    [self sxc_showActionSheetWithTitle:@"Select Distance" pickerWithRows:@[@"1 mile(s)",@"2.5 mile(s)",@"5 mile(s)",@"10 mile(s)",@"15 mile(s)"] itemSelectedAtIndex:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        _textfieldDistance.text=[NSString stringWithFormat:@"%@",selectedValue];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
    
}
@end
