//
//  PWPSearchParkFilterViewController.h
//  PawPark
//
//  Created by daffomac on 4/24/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPParkFilterDelegate <NSObject>

-(void)filterApplied;
-(void)filterAppliedWithDictionary:(NSDictionary *)dictionary withCategory:(NSMutableDictionary *)category;

@end


@interface PWPSearchParkFilterViewController : UIViewController

@property(nonatomic,strong)NSMutableDictionary * parkFilterDictionary;
@property (strong, nonatomic)  NSMutableDictionary * selectedPlaceCategory;

@property(nonatomic,weak)id<PWPParkFilterDelegate> delegate;

@end
