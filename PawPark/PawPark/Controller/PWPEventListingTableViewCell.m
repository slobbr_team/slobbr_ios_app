//
//  PWPEventListingTableViewCell.m
//  PawPark
//
//  Created by HupendraRaghuwanshi on 07/03/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPEventListingTableViewCell.h"
#import <UIImageView+WebCache.h>



@implementation PWPEventListingTableViewCell
@synthesize buttonDetails , buttonDirection ;
 NSDictionary * requestDictionary;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}



-(void)configureCellWithData:(NSMutableDictionary *)dictionary
{
    requestDictionary=dictionary;
    
     _labelEventName.text= [requestDictionary valueForKey:@"name"];
     NSString *time = requestDictionary[@"time"];
    _labelEventDate.text = [self dateStringFromString:time sourceFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ" destinationFormat:@"MM-dd-yyyy hh:mm a"];
    
    // _labelEventDate.text= [requestDictionary valueForKey:@"time"];
    if ([requestDictionary valueForKey:@"address"] != nil)
    {
        _labelEventAddress.text=[NSString stringWithFormat:@"%@ %@ %@ %@",[requestDictionary valueForKey:@"address"] ,[requestDictionary valueForKey:@"address_locality"] ,[requestDictionary valueForKey:@"address_region"] ,[requestDictionary valueForKey:@"address_postal_code"]  ];
    }
    else{
        
     _labelEventAddress.text  = @"";
        
    }
    NSString * imageUrl = requestDictionary[@"thumbs"][@"w400"];
    [_imageViewEvent sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"default_park"]];
    
    [self setupDetailButtonOnHighlight];
    [self setupDirectionButtonOnHighlight];
}


-(void)setupDetailButtonOnHighlight{
    UIImage *image = [UIImage imageNamed:@"ic_details"];
    
    [buttonDetails setImage:[UIImage imageNamed:@"ic_details"] forState:UIControlStateHighlighted];
    
    UIImage *image2 = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [buttonDetails setImage:image2 forState:UIControlStateNormal];
    [buttonDetails setTintColor:[UIColor whiteColor]];
    
    
    [buttonDetails setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    
    [buttonDetails setBackgroundImage:[self imageWithColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];
}


-(void)setupDirectionButtonOnHighlight{
    UIImage *image = [UIImage imageNamed:@"ic_direction"];
        
    UIImage *image2 = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [buttonDirection setImage:image2 forState:UIControlStateNormal];
    [buttonDirection setTintColor:[UIColor whiteColor]];
    
    
    [buttonDirection setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    
    [buttonDirection setBackgroundImage:[self imageWithColor:[UIColor whiteColor]] forState:UIControlStateHighlighted];
    
}



- (NSString *)dateStringFromString:(NSString *)sourceString
                      sourceFormat:(NSString *)sourceFormat
                 destinationFormat:(NSString *)destinationFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:sourceFormat];
    NSDate *date = [dateFormatter dateFromString:sourceString];
    [dateFormatter setDateFormat:destinationFormat];
    return [dateFormatter stringFromDate:date];
}


-(UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    NSLog(@"clicked");
    // Configure the view for the selected state
}

@end
