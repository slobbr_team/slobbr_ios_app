//
//  PWPCheckInArroundMeViewController.h
//  PawPark
//
//  Created by Samarth Singla on 04/12/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPCheckInArroundMeViewController : UIViewController

@property(nonatomic,strong)NSString * categorySlug;

@end
