//
//  PWPImageInfoViewController.m
//  PawPark
//
//  Created by xyz on 03/08/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPImageInfoViewController.h"
#import "UIViewController+UBRComponents.h"
#import <UIImageView+WebCache.h>
#import "PWPConstant.h"
#import "SXCUtility.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPImageInfoViewController ()
{

    __weak IBOutlet UIButton *buttonLink;
    __weak IBOutlet UILabel *labelTitle;
    __weak IBOutlet UIImageView *imageViewInfo;
    __weak IBOutlet UIImageView * imageViewBG;

}
@end

@implementation PWPImageInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self sxc_navigationBarCustomizationWithTitle:@"Image Info"];
    [self sxc_setNavigationBarCrossItem];
    

    NSString * url = [SXCUtility cleanString:[[self.dictionary valueForKey:@"thumbs"] valueForKey:@"w1000"]];
    if (url.length > 0){
        [imageViewInfo sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"default_park"]];
    }
    else{
        [imageViewInfo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,self.dictionary[@"image_url"]]] placeholderImage:[UIImage imageNamed:@"default_park"]];
    }

    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    if(_dictionary[@"image_info"] && [_dictionary[@"image_info"] isKindOfClass:[NSDictionary class]]){
        
        if([_dictionary[@"image_info"] valueForKey:@"by"]){
            labelTitle.text = [SXCUtility cleanString:_dictionary[@"image_info"][@"by"]];
        }
        
        if([_dictionary[@"image_info"] valueForKey:@"link"]){
            buttonLink.hidden = false;
            [buttonLink setTitle:[SXCUtility cleanString:_dictionary[@"image_info"][@"link"]] forState:UIControlStateNormal];
        }
    }
    else if(_dictionary[@"submitted_by"] ){
        labelTitle.text = [NSString stringWithFormat:@"Submitted By %@",[SXCUtility cleanString:_dictionary[@"submitted_by"]]];
        buttonLink.hidden = true;
    
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)linkClicked:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:buttonLink.titleLabel.text]];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
