//
//  PWPSelectCommunityCategoryViewController.h
//  PawPark
//
//  Created by Samarth Singla on 09/09/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPSelectCommunityCategoryViewController : UIViewController
@property(nonatomic,strong)NSMutableString * selectedCategory;

@end
