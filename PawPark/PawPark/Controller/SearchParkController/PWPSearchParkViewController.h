//
//  PWPSearchParkViewController.h
//  PawPark
//
//  Created by daffomac on 4/24/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWPBaseViewController.h"
#import "PWPParkCheckInViewController.h"

@protocol PWPSearchParkListDelegate <NSObject>
-(void)fetchDataForIndex:(NSInteger)pageIndex;
@end


@interface PWPSearchParkViewController : PWPBaseViewController <PWPParkCheckInDelegae>

@property(nonatomic,strong) NSMutableDictionary * filterDictionary;


-(void)reloadListWithData:(NSMutableArray *)theParks pageIndex:(NSInteger)pageIndex withMapDragged:(BOOL)isMapDragged;


@property(nonatomic,weak)id <PWPSearchParkListDelegate> delegate;

@end
