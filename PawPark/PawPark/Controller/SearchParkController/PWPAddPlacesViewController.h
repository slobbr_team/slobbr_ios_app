//
//  PWPAddPlacesViewController.h
//  PawPark
//
//  Created by Chuchu on 01/09/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface PWPAddPlacesViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *ImgPlace;
@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtAddress;
@property (strong, nonatomic) IBOutlet UITextField *textfieldCategory;
@property (strong, nonatomic) IBOutlet UILabel *labelAmenties;

@property (strong, nonatomic) IBOutlet UIButton *btnCategory;
@property (strong, nonatomic) IBOutlet UIButton *buttonSubmit;

@property (nonatomic,strong)NSMutableDictionary * selectedCategoryPlace;

@property (strong, nonatomic) IBOutlet UITextField *txthours;
@property (nonatomic) float lat;
@property (nonatomic) float longi;
@property (nonatomic, strong) NSString * formattedAddress;
@property (nonatomic, strong) NSString * city;
@property (nonatomic, strong) NSString * zipcode;
@property (nonatomic, strong) NSString * state;
@end
