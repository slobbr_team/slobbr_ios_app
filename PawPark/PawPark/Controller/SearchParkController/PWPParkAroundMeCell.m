//
//  PWPParkAroundMeCell.m
//  PawPark
//
//  Created by xyz on 3/9/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPParkAroundMeCell.h"

@interface PWPParkAroundMeCell()

@end

@implementation PWPParkAroundMeCell


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)aroundMeClicked:(id)sender {
    
    if(_delegate && [_delegate respondsToSelector:@selector(aroundMeClicked)])
    {
        [_delegate aroundMeClicked];
    }
    
}

@end
