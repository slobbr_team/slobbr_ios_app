//
//  PWPSearchParkMapViewController.m
//  PawPark
//
//  Created by xyz on 7/23/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPSearchParkMapViewController.h"
#import <MapKit/MapKit.h>
#import "MyAnnotation.h"
#import "SXCUtility.h"
#import "PWPConstant.h"
#import <UIImageView+WebCache.h>
#import "UIViewController+UBRComponents.h"
#import "PWPService.h"
#import "PWPApplicationUtility.h"
#import "PWPParkCheckInViewController.h"
#import "PWPParkCheckedInDogsViewController.h"
#import "UIColor+PWPColor.h"
#import "PWPAppDelegate.h"
#import "PWPAddPlacesViewController.h"
#import "PWPSchedulePawDateViewController.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPCheckInHelper.h"
#import "PWPMapPinHelper.h"
#import "PWPCheckInArroundMeViewController.h"
#import "TSDemoClusteredAnnotationView.h"
#import "UBRLocationHandler.h"
#import "PWPPlacePicViewController.h"
#import <Mixpanel.h>
#import <UIButton+WebCache.h>
#import "PWPAmentitiesCell.h"

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width

@interface PWPSearchParkMapViewController () <MKMapViewDelegate, PWPParkCheckInDelegae,UIGestureRecognizerDelegate,TSClusterMapViewDelegate, PWPPlacePicViewControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
{
    __weak IBOutlet UIView *viewDog4;
    __weak IBOutlet UIView *viewDog3;
    __weak IBOutlet UIView *viewDog2;
    __weak IBOutlet UIView *viewDog1;
    
    __weak IBOutlet UIButton *buttonCheckedInDogsNew;
    
    __weak IBOutlet UILabel *labelTotalDogsCheckedin;
    
    __weak IBOutlet NSLayoutConstraint *lcdog1width;
    __weak IBOutlet NSLayoutConstraint *lcdog2width;
    __weak IBOutlet NSLayoutConstraint *lcdog3width;
    __weak IBOutlet NSLayoutConstraint *lcdog4Width;
   
    __weak IBOutlet UIImageView *imageviewCheckedInDog4;
    __weak IBOutlet UIImageView *imageviewCheckinDog3;
    __weak IBOutlet UIImageView *imaegviewCheckedInDog2;
    __weak IBOutlet UIImageView *imageViewCheckedInDog1;
    
    float minLatitude ;
    float maxLatitude ;
    float minLongitude ;
    float maxLongitude ;
    float center_lat ;
    float center_long ;
    
    NSDictionary * selectedPlace;
    
    UITapGestureRecognizer * tapGesture1;
    UITapGestureRecognizer * tapGesture2;
    UITapGestureRecognizer * tapGesture3;
    
    NSString * selectedCategory;
    
    CLLocation * lastAlertLocation;
    
    __weak IBOutlet UIButton *buttonAdvertisement;
    __weak IBOutlet NSLayoutConstraint *layoutConstraintOptionWidth;
    __weak IBOutlet NSLayoutConstraint *layoutConstraintViewHeight;
    __weak IBOutlet NSLayoutConstraint *layoutConstraintYelpButtonHeight;
    __weak IBOutlet NSLayoutConstraint *layoutConstraintStarHeight;
    __weak IBOutlet UIImageView *imageViewStar;
    __weak IBOutlet UILabel *labelAddress;

    __weak IBOutlet NSLayoutConstraint *layoutDetailViewBottom;
    __weak IBOutlet UIButton *buttonServices;
    __weak IBOutlet UIButton *buttonPark;
    __weak IBOutlet UIButton *buttonEat;
    __weak IBOutlet UIButton *buttonStay;

    __weak IBOutlet UIButton *buttonAddPhotoPlace;
    __weak IBOutlet UIButton *buttonFavorite;
    __weak IBOutlet UIButton *buttonInvite;
    __weak IBOutlet UIButton *buttonPhotos;
    __weak IBOutlet UIView *viewAdvertisment;
    __weak IBOutlet UILabel *labelAdTitle;
    __weak IBOutlet UIImageView *imageViewAd;
    __weak IBOutlet UILabel *labelDescription;
    __weak IBOutlet UIButton *buttonAdPopUp;

    __weak IBOutlet UICollectionView *collectioView;

    BOOL islongPressed;
}

- (IBAction)buttonAdvertisemntClicked:(id)sender;

@property (strong, nonatomic) IBOutlet TSClusterMapView *mapViw;
@property (strong, nonatomic) NSMutableArray *places;
@property(nonatomic,weak)IBOutlet UIButton * favoriteButton;
@property (weak, nonatomic) IBOutlet UIButton *useMyLocationButton;

@end

@implementation PWPSearchParkMapViewController


-(void)configureAccordingToCheckInDogs:(NSArray *)checkedinDogs{
    
    
    imageviewCheckedInDog4.layer.borderColor = [UIColor colorWithRed:44/255.0f green:166/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    imageviewCheckinDog3.layer.borderColor = [UIColor colorWithRed:44/255.0f green:166/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    imaegviewCheckedInDog2.layer.borderColor = [UIColor colorWithRed:44/255.0f green:166/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    imageViewCheckedInDog1.layer.borderColor = [UIColor colorWithRed:44/255.0f green:166/255.0f blue:107/255.0f alpha:1.0f].CGColor;
    
    imageviewCheckedInDog4.layer.borderWidth = 1.0f;
    imageviewCheckinDog3.layer.borderWidth = 1.0f;
    imaegviewCheckedInDog2.layer.borderWidth = 1.0f;
    imageViewCheckedInDog1.layer.borderWidth = 1.0f;
    if(checkedinDogs.count==0){
        
        lcdog1width.constant = 0.0f;
        lcdog2width.constant = 0.0f;
        lcdog3width.constant = 0.0f;
        lcdog4Width.constant = 0.0f;
        
        viewDog4.hidden = true;
        viewDog3.hidden = true;
        viewDog2.hidden = true;
        viewDog1.hidden = true;
        
        buttonCheckedInDogsNew.hidden = true;
        
    }
    else if(checkedinDogs.count==1){
        
        lcdog1width.constant = 28.0f;
        lcdog2width.constant = 0.0f;
        lcdog3width.constant = 0.0f;
        lcdog4Width.constant = 0.0f;
        
        viewDog4.hidden = true;
        viewDog3.hidden = true;
        viewDog2.hidden = true;
        viewDog1.hidden = false;
        
        buttonCheckedInDogsNew.hidden = false;
        
        
        if (checkedinDogs[0][@"image_url"] != nil){
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[0][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[0][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[0][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[0][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imageViewCheckedInDog1 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        
        
    }
    else if(checkedinDogs.count==2){
        lcdog1width.constant = 28.0f;
        lcdog2width.constant = 28.0f;
        lcdog3width.constant = 0.0f;
        lcdog4Width.constant = 0.0f;
        
        viewDog4.hidden = true;
        viewDog3.hidden = true;
        viewDog2.hidden = false;
        viewDog1.hidden = false;
        
        buttonCheckedInDogsNew.hidden = false;
        
        if (checkedinDogs[0][@"image_url"] != nil){
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[0][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[0][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[0][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[0][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w1000"]];
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imageViewCheckedInDog1 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        if (checkedinDogs[1][@"image_url"] != nil){
            [imaegviewCheckedInDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[1][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[1][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[1][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[1][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [imaegviewCheckedInDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imaegviewCheckedInDog2 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        
    }
    else if(checkedinDogs.count==3){
        lcdog1width.constant = 28.0f;
        lcdog2width.constant = 28.0f;
        lcdog3width.constant = 28.0f;
        lcdog4Width.constant = 0.0f;
        
        viewDog4.hidden = true;
        viewDog3.hidden = false;
        viewDog2.hidden = false;
        viewDog1.hidden = false;
        
        buttonCheckedInDogsNew.hidden = false;
        
        if (checkedinDogs[0][@"image_url"] != nil){
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[0][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[0][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[0][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[0][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w1000"]];
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imageViewCheckedInDog1 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        if (checkedinDogs[1][@"image_url"] != nil){
            [imaegviewCheckedInDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[1][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[1][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[1][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[1][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [imaegviewCheckedInDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imaegviewCheckedInDog2 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        if (checkedinDogs[2][@"image_url"] != nil){
            [imageviewCheckinDog3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[2][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[2][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[2][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[2][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [imageviewCheckinDog3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imageviewCheckinDog3 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
    }
    else if(checkedinDogs.count>=4){
        
        lcdog1width.constant = 28.0f;
        lcdog2width.constant = 28.0f;
        lcdog3width.constant = 28.0f;
        lcdog4Width.constant = 28.0f;
        
        viewDog4.hidden = false;
        viewDog3.hidden = false;
        viewDog2.hidden = false;
        viewDog1.hidden = false;
        
        
        buttonCheckedInDogsNew.hidden = false;
        
        if (checkedinDogs[0][@"image_url"] != nil){
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[0][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[0][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[0][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[0][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w1000"]];
            [imageViewCheckedInDog1 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imageViewCheckedInDog1 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        if (checkedinDogs[1][@"image_url"] != nil){
            [imaegviewCheckedInDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[1][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[1][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[1][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[1][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [imaegviewCheckedInDog2 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imaegviewCheckedInDog2 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        if (checkedinDogs[2][@"image_url"] != nil){
            [imageviewCheckinDog3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[2][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[2][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[2][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[2][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [imageviewCheckinDog3 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imageviewCheckinDog3 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        
        if (checkedinDogs[3][@"image_url"] != nil){
            [imageviewCheckedInDog4 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,checkedinDogs[3][@"image_url"]]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else if(checkedinDogs[3][@"gallery"]!=nil && [SXCUtility cleanArray:checkedinDogs[3][@"gallery"]].count > 0){
            
            NSDictionary * dogImageDictionary =[SXCUtility cleanArray:checkedinDogs[3][@"gallery"]][0];
            NSString * imageUrl = [SXCUtility cleanString:dogImageDictionary[@"thumbs"][@"w400"]];
            [imageviewCheckedInDog4 sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
        else{
            [imageviewCheckedInDog4 setImage:[UIImage imageNamed:@"img_dog_sample"]];
        }
    }
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIPanGestureRecognizer* panRec = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didDragMap:)];
    [panRec setDelegate:self];
    [panRec setDelaysTouchesBegan:YES];
    [panRec setDelaysTouchesEnded:YES];
    [panRec setCancelsTouchesInView:YES];
    [self.mapViw addGestureRecognizer:panRec];
    
    
    UILongPressGestureRecognizer* longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(didLongPressMap:)];
    [longPress setDelegate:self];
    [self.mapViw addGestureRecognizer:longPress];
    
    minLatitude= 180.0f;
    maxLatitude=-180.0f; ;
    minLongitude= 180.0f;
    maxLongitude=-180.0f;
    
    _mapViw.delegate=self;
    _mapViw.showsUserLocation = YES;
        
    if([[[SXCUtility getNSObject:@"isLocationHelp"] description] boolValue]){
        _useMyLocationButton.hidden=NO;
    }else{
        _useMyLocationButton.hidden=YES;
    }
    
    _labels1.hidden = YES;
    _labels2.hidden = YES;
    _labels3.hidden = YES;

    [self adjustImageAndTitleOffsetsForButton:buttonEat];
    [self adjustImageAndTitleOffsetsForButton:buttonPark];
    [self adjustImageAndTitleOffsetsForButton:buttonServices];
    [self adjustImageAndTitleOffsetsForButton:buttonStay];

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;

    if([SXCUtility getNSObject:@"map_navigation_icon"]) {

        layoutConstraintOptionWidth.constant = screenWidth/5.0f;

        [buttonAdvertisement setTitle:[SXCUtility cleanString:[SXCUtility getNSObject:@"map_navigation_icon_title"]] forState:UIControlStateNormal];

        [buttonAdvertisement sd_setImageWithURL:[NSURL URLWithString:[SXCUtility cleanString:[SXCUtility getNSObject:@"map_navigation_icon"]]] forState:UIControlStateNormal placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

            UIImage * resizeImage = [self imageWithImage:image convertToSize:CGSizeMake(30, 30)];

            [buttonAdvertisement setImage:resizeImage forState:UIControlStateNormal];
            buttonAdvertisement.imageView.contentMode = UIViewContentModeScaleAspectFit;
            [self adjustImageAndTitleOffsetsForButton:buttonAdvertisement];

        }];

        buttonAdvertisement.hidden= false;


    }
    else{
        layoutConstraintOptionWidth.constant = screenWidth/4.0f;
        buttonAdvertisement.hidden= true;
    }

    if([SXCUtility getNSObject:@"map_navigation_popup_body_text"]) {
        labelDescription.text = [SXCUtility cleanString:[SXCUtility getNSObject:@"map_navigation_popup_body_text"]];
    }

    if([SXCUtility getNSObject:@"map_navigation_popup_header_text"]) {
        labelAdTitle.text = [SXCUtility cleanString:[SXCUtility getNSObject:@"map_navigation_popup_header_text"]];
    }

    if([SXCUtility getNSObject:@"map_navigation_popup_button_text"]) {

        [buttonAdPopUp setTitle:[NSString stringWithFormat:@"%@  ",[SXCUtility cleanString:[SXCUtility getNSObject:@"map_navigation_popup_button_text"]]] forState:UIControlStateNormal];

        buttonAdPopUp.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        buttonAdPopUp.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        buttonAdPopUp.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);

        buttonAdPopUp.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        buttonAdPopUp.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        buttonAdPopUp.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    }

    if([SXCUtility getNSObject:@"map_navigation_popup"]) {
        [imageViewAd sd_setImageWithURL:[NSURL URLWithString:[SXCUtility cleanString:[SXCUtility getNSObject:@"map_navigation_popup"]]]];
    }

    viewAdvertisment.hidden = true;
    
    
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0,  [UIScreen mainScreen].bounds.size.width,190);
    
    // Add colors to layer
    UIColor *centerColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.50];
    UIColor *topColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.01];
    UIColor *endColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.95];
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[topColor CGColor],
                       (id)[centerColor CGColor],
                       (id)[endColor CGColor],
                       nil];
    self.parkImageView.layer.name = @"Gradient";
    for (CALayer *layer in self.parkImageView.layer.sublayers) {
        [layer removeFromSuperlayer];
    }
    [self.parkImageView.layer insertSublayer:gradient atIndex:0];
    
    
}

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [_mapViw removeAnnotations:_mapViw.annotations];
    [_mapViw addAnnotations:[self annotations]];
    
    [self resetSelection];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {

    [[Mixpanel sharedInstance] track:@"Map Icon List Clicked"];

    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(((MyAnnotation *)view.annotation).coordinate.latitude,((MyAnnotation *)view.annotation).coordinate.longitude);
    
    
    if ([view.annotation isKindOfClass:[MKUserLocation class]] || [view isKindOfClass:[TSDemoClusteredAnnotationView class]]) {
        return;
    }

    if ([view.annotation isKindOfClass:[MyAnnotation class]]) {

        
        [self bindValueWithComponentsWithData:((MyAnnotation *)view.annotation).park];
      
        layoutConstraintViewHeight.constant = 190.0f;
        
        if(layoutDetailViewBottom.constant != 5){
           layoutDetailViewBottom.constant = 5;
            
            [UIView animateWithDuration:1 animations:^{
                [self.view layoutIfNeeded];
            }completion:^(BOOL finished) {
                [_mapViw setCenterCoordinate:coord animated:true];
            }];

        }
        else{
             [_mapViw setCenterCoordinate:coord animated:true];
        }
    }
    
}


- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    MKAnnotationView *annotationView = nil;
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    
    else if ([annotation isKindOfClass:[MyAnnotation class]]) {
        
        //Annotation View
        MyAnnotation *a = (MyAnnotation *)annotation;
        
        annotationView = (MKAnnotationView *)[aMapView dequeueReusableAnnotationViewWithIdentifier:@"cluster"];
        
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:a reuseIdentifier:@"cluster"];
        }
        annotationView.canShowCallout=NO;
        
        NSString * parkLogo =  a.park[@"logo"];
        
        annotationView.image = [PWPMapPinHelper correspondingToLogo:parkLogo];
    
    }
    
    return annotationView;
}

- (NSArray *)annotations {
    
    NSMutableArray *annotations = [NSMutableArray array];
    MyAnnotation *annotation;
    
    center_long = 0.0f;
    center_lat = 0.0f;
    
    for (NSDictionary  * dictionary in self.places) {
        
        annotation = [[MyAnnotation alloc] init];
        annotation.coordinate = CLLocationCoordinate2DMake([dictionary[@"lat"] description].floatValue,[dictionary[@"lng"] description].floatValue);
        annotation.title=dictionary[@"name"];
        annotation.park= dictionary;
        
        double annotationLat = annotation.coordinate.latitude;
        double annotationLong = annotation.coordinate.longitude;
        
        minLatitude = fmin(annotationLat, minLatitude);
        maxLatitude = fmax(annotationLat, maxLatitude);
        minLongitude = fmin(annotationLong, minLongitude);
        maxLongitude = fmax(annotationLong, maxLongitude);
        
        [annotations addObject:annotation];
        
        center_lat = center_lat + annotationLat;
        center_long = center_long + annotationLong;
    }
    
    center_lat = center_lat / [self.places count];
    center_long = center_long / [self.places count];
    
    return annotations;
    
}

-(void)reloadMapWithData:(NSMutableArray *)parks withCategorySelected:(NSString *)categoryName withMapDragged:(BOOL)isMapDragged lat:(NSNumber *)lat long:(NSNumber *)lng isUserLocationClicked:(BOOL)isUserLocation
{
    
    NSLog(@"+++++++++++++++++++++++++++++");
    NSLog(@"++++++++reloadMapWithData++++++++++++++++++++");
    NSLog(@"+++++++++%@ %d %@ %@ %d+++++++++++++++++++",categoryName,isMapDragged,lat,lng,isUserLocation);
    NSLog(@"+++++++++++++++++++++++++++++");
    
    selectedCategory = [categoryName valueForKey:@"slug"];
    
    if([[categoryName valueForKey:@"name"] isEqualToString:@"Bars & Restaurants"]){
        [buttonEat setSelected:YES];
        buttonStay.selected = false;
        buttonPark.selected = false;
        buttonServices.selected = false;
    }
    else if([[categoryName valueForKey:@"name"] isEqualToString:@"Hotel"]){
        [buttonStay setSelected:YES];
        buttonEat.selected = false;
        buttonPark.selected = false;
        buttonServices.selected = false;
    }
    else if([[categoryName valueForKey:@"name"] isEqualToString:@"Services"]){
        [buttonServices setSelected:YES];
        buttonEat.selected = false;
        buttonPark.selected = false;
        buttonStay.selected = false;
    }
    else if([[categoryName valueForKey:@"name"] isEqualToString:@"Park"]){
        [buttonPark setSelected:YES];
        buttonEat.selected = false;
        buttonServices.selected = false;
        buttonStay.selected = false;
    }
    else{
        [buttonPark setSelected:false];
        buttonEat.selected = false;
        buttonServices.selected = false;
        buttonStay.selected = false;
    }
    
    if(!(isMapDragged == true && parks.count ==0)){
        self.places = parks;
    }
    
    if((isMapDragged == YES && parks.count>0) ||(isMapDragged == NO)){
        [_mapViw removeAnnotations:_mapViw.annotations];
        [_mapViw addClusteredAnnotations:[self annotations]];
    }
    
    if(self.places.count>0 && isMapDragged == false){
        [self setMapRegionToCaptureAllMarkers];
    }
    else if(self.places.count == 0 && isUserLocation == true){
        
        if(_mapViw.userLocation.location.coordinate.latitude == 0.00 && _mapViw.userLocation.location.coordinate.longitude == 0.00){
            [self showLoaderWithText:@"Loading..."];
            [[UBRLocationHandler shareHandler] startLocationHandlerWithCompletion:^(BOOL status) {
                [self dismissLoader];
                [_mapViw setCenterCoordinate:[UBRLocationHandler shareHandler].currentLocation.coordinate animated:YES];
            
            } WithErrorBlock:^(NSError *error) {
                [self dismissLoader];
                [self sxc_showAlertViewWithTitle:@"Error" message:@"Some error occured while fetching current location. Please try again" WithDelegate:nil WithCancelButtonTitle:@"Ok" WithAcceptButtonTitle:nil];
            }];
            
        }else {
            [_mapViw setCenterCoordinate:[UBRLocationHandler shareHandler].currentLocation.coordinate animated:YES];
        }
    }
    else if(self.places.count == 0 && isMapDragged == false && ![UBRLocationHandler shareHandler].isLocationServicesEnable){
        MKCoordinateRegion worldRegion = MKCoordinateRegionForMapRect(MKMapRectWorld);
        _mapViw.region = worldRegion;
    }

    if (isUserLocation == true) {
        _useMyLocationButton.selected = true;
    }
    else{
        _useMyLocationButton.selected = false;
    }
    
}

- (IBAction)inviteButtonClicked:(id)sender {
    
    [[Mixpanel sharedInstance] track:@"Invite Clicked on Map"];

    [self pushSchedulePawDateViewController:selectedPlace];
    
    
}

-(void)pushSchedulePawDateViewController:(NSDictionary *)parkDictionary{
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    PWPSchedulePawDateViewController *pawDateViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPSchedulePawDateViewController"];
    pawDateViewController.selectedPark = parkDictionary.mutableCopy;
    [self.navigationController pushViewController:pawDateViewController animated:YES];
    
}


- (IBAction)favClicked:(id)sender {

    [[Mixpanel sharedInstance] track:@"Favorite Clicked on Map"];

    [self startLoader];
    
    if([SXCUtility cleanInt:selectedPlace[@"is_favourite"]] ==1){
        
        [[PWPService sharedInstance] deleteHomePark:selectedPlace[@"id"] withSuccessBlock:^(id response) {
            [self dismissLoader];
            
            NSInteger index =[_places indexOfObject:selectedPlace];
            NSMutableDictionary * parkDictionary = selectedPlace.mutableCopy;
            
            if([SXCUtility cleanInt:parkDictionary[@"is_favourite"]] ==1){
                parkDictionary[@"is_favourite"]=@0;
            }
            else{
                parkDictionary[@"is_favourite"]=@1;
            }
            
            [_places replaceObjectAtIndex:index withObject:parkDictionary];
            
            
            [_mapViw removeAnnotations:_mapViw.annotations];
            [_mapViw addAnnotations:[self annotations]];
            
            [self sxc_showSuccessWithMessage:@"Place has been removed from 'Favorite' Places."];
            
            [self bindValueWithComponentsWithData:parkDictionary];
            selectedPlace = parkDictionary;
            
        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:error.domain];
        }];
        
    }
    else{
        
        
        [[PWPService sharedInstance] homePark:selectedPlace[@"id"] withSuccessBlock:^(id response) {
            [self dismissLoader];
            
            NSInteger index =[_places indexOfObject:selectedPlace];
            NSMutableDictionary * parkDictionary = selectedPlace.mutableCopy;
            
            if([SXCUtility cleanInt:parkDictionary[@"is_favourite"]] ==1){
                parkDictionary[@"is_favourite"]=@0;
            }
            else{
                parkDictionary[@"is_favourite"]=@1;
            }
            
            [_places replaceObjectAtIndex:index withObject:parkDictionary];
            
            NSMutableDictionary * userDictionary=[[PWPApplicationUtility getCurrentUserProfileDictionary] mutableCopy];
            [userDictionary setValue:parkDictionary forKey:@"place"];
            [PWPApplicationUtility saveCurrentUserProfileDictionary:userDictionary];
            
            [_mapViw removeAnnotations:_mapViw.annotations];
            [_mapViw addAnnotations:[self annotations]];
            
            [self sxc_showSuccessWithMessage:@"Place has been marked as 'Favorite' Place."];
            
            [self bindValueWithComponentsWithData:parkDictionary];
            
            
        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:error.domain];
        }];
    }
    
    
}

- (IBAction)checkInClicked:(id)sender {

    [[Mixpanel sharedInstance] track:@"Photos Clicked on Map"];

    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    PWPPlacePicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPPlacePicViewController"];
    viewController.placePics = [SXCUtility cleanArray:[selectedPlace valueForKey:@"gallery"]].mutableCopy;
    viewController.data = selectedPlace;
    viewController.placeId = selectedPlace[@"id"];
    viewController.delegate = self;

    [self.navigationController pushViewController:viewController animated:YES];

}
- (IBAction)addPlaceClicked:(id)sender {
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    PWPPlacePicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPPlacePicViewController"];
    viewController.placePics = [SXCUtility cleanArray:[selectedPlace valueForKey:@"gallery"]].mutableCopy;
    viewController.data = selectedPlace;
    viewController.placeId = selectedPlace[@"id"];
    viewController.delegate = self;
    viewController.isGalleryOpen = true;
    
    [self.navigationController pushViewController:viewController animated:YES];
    
}

-(void)addPlaceClickedWithData:(NSDictionary *)dictionary {
   
}


-(void)placeProfileUpdated {
    if([_delegate respondsToSelector:@selector(fetchParks)]){
        [_delegate fetchParks];
    }
}


-(void) setMapRegionToCaptureAllMarkers{
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in _mapViw.annotations)
    {
        if ([annotation isKindOfClass:[MKUserLocation class]]) {
            continue;
        }
        
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 200, 200);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    [_mapViw setVisibleMapRect:zoomRect animated:YES];
    
}


- (IBAction)useMyLocation:(id)sender {

    [[Mixpanel sharedInstance] track:@"Find my current location clicked"];
 
    if(self.useMyLocationButton.isSelected == true && [_delegate respondsToSelector:@selector(aroundHomeZipCodeClicked)]){
            [_delegate aroundHomeZipCodeClicked];
    }
    else if(self.useMyLocationButton.isSelected == false && [_delegate respondsToSelector:@selector(aroundMeClicked)] && [[UBRLocationHandler shareHandler] isLocationServicesEnable] == false){
        [self sxc_showAlertViewWithTitle:@"Error" message:@"Slobbr require location access to determine your location. Please give access from device settings." WithDelegate:nil WithCancelButtonTitle:@"Ok" WithAcceptButtonTitle:nil];
        return;
    }
    else if(self.useMyLocationButton.isSelected == false && [_delegate respondsToSelector:@selector(aroundMeClicked)]  && [[UBRLocationHandler shareHandler] isLocationServicesEnable] == true){
        [_delegate aroundMeClicked];
    }
    
    self.useMyLocationButton.selected = !self.useMyLocationButton.isSelected;
}

-(void)resetSelection{
    
    
    if([[[SXCUtility getNSObject:@"isLocationHelp"] description] boolValue]){
        _useMyLocationButton.hidden=NO;
    }else{
        _useMyLocationButton.hidden=YES;
    }
    
    layoutConstraintViewHeight.constant = 190.0f;
    layoutDetailViewBottom.constant = -260.0f;
    
    [UIView animateWithDuration:1 animations:^{
        [self.view layoutIfNeeded];
    }];
    
    
}


-(IBAction)checkDogsInButtonClicked:(id)sender
{
    if([SXCUtility cleanString:[selectedPlace[@"active_dogs_count"] description]].integerValue > 0){

        [[Mixpanel sharedInstance] track:@"Checked-In Dogs Clicked on Map"];

        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PWPParkCheckedInDogsViewController * parkCheckInDogsViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckedInDogsViewController"];
        parkCheckInDogsViewController.placeId = [SXCUtility cleanString:[[selectedPlace valueForKey:@"id"] description]];
        
        [self.navigationController pushViewController:parkCheckInDogsViewController animated:YES];
    }
}

-(void)successFullCheckedInWithDictionary:(NSMutableDictionary *)data WithDogs:(NSMutableArray *)dogsArray{
    
    if([_delegate respondsToSelector:@selector(fetchParks)]){
        [_delegate fetchParks];
    }
}

-(void)sxc_stopLoading{
    
}
-(void)sxc_startLoading{
}

-(IBAction)staViewClicked:(id)sender{
    
    PWPAppDelegate * delegate =(PWPAppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate showSTAHelpViewWithS:[selectedPlace valueForKey:@"sta_score"][@"s"] withT:[selectedPlace valueForKey:@"sta_score"][@"t"] withA:[selectedPlace valueForKey:@"sta_score"][@"a"]];
    
}

// Mark MapViewDelegate


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)didDragMap:(UIGestureRecognizer*)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        
        
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        NSLog(@"%f %f",_mapViw.centerCoordinate.latitude,_mapViw.centerCoordinate.longitude);
        float a = _mapViw.centerCoordinate.latitude;
        float b = _mapViw.centerCoordinate.longitude;
        
        MKMapRect mapRect = self.mapViw.visibleMapRect;
        MKMapPoint cornerPointNW = MKMapPointMake(mapRect.origin.x, mapRect.origin.y);
        CLLocationCoordinate2D cornerCoordinate = MKCoordinateForMapPoint(cornerPointNW);
        
        CLLocation * cornerLocation = [[CLLocation alloc] initWithLatitude:cornerCoordinate.latitude longitude:cornerCoordinate.longitude];
        
        
        // Then get the center coordinate of the mapView (just a shortcut for convenience)
        CLLocationCoordinate2D centerCoordinate = self.mapViw.centerCoordinate;
        CLLocation * centerLocation = [[CLLocation alloc] initWithLatitude:centerCoordinate.latitude longitude:centerCoordinate.longitude];

        // And then calculate the distance
        CLLocationDistance distance = [cornerLocation distanceFromLocation:centerLocation];

        if([_delegate respondsToSelector:@selector(draggingMapClickedwithlatitude:andlongitde: withRadius:)]){
            [_delegate draggingMapClickedwithlatitude:a andlongitde:b withRadius:distance*0.00062];
        }  
    }
    
}
-(float)milesfromPlace:(CLLocationCoordinate2D)from andToPlace:(CLLocationCoordinate2D)to  {
    
    CLLocation *userloc = [[CLLocation alloc]initWithLatitude:from.latitude longitude:from.longitude];
    CLLocation *dest = [[CLLocation alloc]initWithLatitude:to.latitude longitude:to.longitude];
    
    CLLocationDistance dist = [userloc distanceFromLocation:dest]* 0.000621371;
    
    //NSLog(@"%f",dist);
    NSString *distance = [NSString stringWithFormat:@"%f",dist];
    
    return [distance floatValue];
    
}


- (void)didLongPressMap:(UIGestureRecognizer*)gestureRecognizer {
    
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;

    CGPoint touchPoint = [gestureRecognizer locationInView:_mapViw];
    CLLocationCoordinate2D location =
    [_mapViw convertPoint:touchPoint toCoordinateFromView:_mapViw];

    [self startLoader];

    [[PWPService sharedInstance] googleAPIGeoCodeWithLat:[NSNumber numberWithFloat:location.latitude] withLongi:[NSNumber numberWithFloat:location.longitude] withSuccessBlock:^(id responseDicitionary) {

        NSString * zipcode = @"";
        NSString * city = @"";
        NSString * state = @"";
        NSMutableString * formattedAddress = [[NSMutableString alloc] init];;

        NSArray * addresses = [SXCUtility cleanArray:[responseDicitionary valueForKey:@"results"]];
        if(addresses.count > 0) {

            NSDictionary * address = addresses[0];
            
            NSArray * addressComponents = [address valueForKey:@"address_components"];
            for(NSDictionary * addressComponent in addressComponents){

                if([[SXCUtility cleanArray:[addressComponent valueForKey:@"types"]] containsObject:@"street_number"]){
                    [formattedAddress insertString:[NSString stringWithFormat:@"%@ ",[SXCUtility cleanString:[addressComponent valueForKey:@"long_name"]]] atIndex:0];
                }
                
                if([[SXCUtility cleanArray:[addressComponent valueForKey:@"types"]] containsObject:@"route"]){
                    [formattedAddress appendString:[SXCUtility cleanString:[addressComponent valueForKey:@"long_name"]]];
                }
                
                if([[SXCUtility cleanArray:[addressComponent valueForKey:@"types"]] containsObject:@"administrative_area_level_1"]){
                    state = [SXCUtility cleanString:[addressComponent valueForKey:@"short_name"]];
                }

                if([[SXCUtility cleanArray:[addressComponent valueForKey:@"types"]] containsObject:@"locality"]){
                    city = [SXCUtility cleanString:[addressComponent valueForKey:@"long_name"]];
                }

                if([[SXCUtility cleanArray:[addressComponent valueForKey:@"types"]] containsObject:@"postal_code"]){
                    zipcode = [SXCUtility cleanString:[addressComponent valueForKey:@"long_name"]];
                }
            }
        }

        
        if(zipcode.length != 0 && state.length != 0 && city.length != 0){
            
            UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
            
            PWPAddPlacesViewController *parkFilterViewController=[storyBoard instantiateViewControllerWithIdentifier:@"PWPAddPlacesViewController"];
            parkFilterViewController.zipcode = zipcode;
            parkFilterViewController.city = city;
            parkFilterViewController.state = state;
            parkFilterViewController.formattedAddress = formattedAddress;
            parkFilterViewController.lat = location.latitude ;
            parkFilterViewController.longi = location.longitude ;
            [self.navigationController pushViewController:parkFilterViewController animated:YES];
        }
        else {
            
            [self sxc_showAlertViewWithTitle:@"Error" message:@"Some error occured while fetching current location address. Please try again" WithDelegate:nil WithCancelButtonTitle:@"Ok" WithAcceptButtonTitle:nil];
        }
        [self dismissLoader];
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        [self sxc_showAlertViewWithTitle:@"Error" message:@"Some error occured while fetching current location address. Please try again" WithDelegate:nil WithCancelButtonTitle:@"Ok" WithAcceptButtonTitle:nil];
    }];
}

- (IBAction)hideShadowAction:(id)sender {
    self.shadowView.hidden = true;
}

- (IBAction)addPlacesAction:(id)sender {
    self.shadowView.hidden = false;
    [[Mixpanel sharedInstance] track:@"Add Location Info Clicked"];
}

-(CGFloat)getHeightOfParkBasedOnAmentis:(NSDictionary *)parkDictionary {
    
    NSMutableString * amentiesString=[[NSMutableString alloc] init];
    
    for(NSString * amenity in parkDictionary[@"amenities"]) {
        if(amentiesString.length>0) {
            [amentiesString appendString:@"\n"];
        }
        [amentiesString appendString:amenity];
    }
    
    CGFloat amentiesHeight=[self getHeightOfLabelWithText:amentiesString withFont:[UIFont fontWithName:@"Roboto-Regular" size:12.0f]];
    
    if(amentiesHeight>16) {
        return amentiesHeight - 6 ;
    }
    
    return 0;
}


-(CGFloat)getHeightOfLabelWithText:(NSString *)text withFont:(UIFont *)font
{
    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    gettingSizeLabel.font = font;
    gettingSizeLabel.text = text;
    gettingSizeLabel.numberOfLines = 0;
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(SCREEN_WIDTH - 35, CGFLOAT_MAX);
    
    CGSize expectSize = [gettingSizeLabel sizeThatFits:maximumLabelSize];
    return expectSize.height;
}
- (IBAction)viewClicked:(id)sender {
    
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(((MyAnnotation *)self.mapViw.selectedAnnotations.firstObject).coordinate.latitude,((MyAnnotation *)self.mapViw.selectedAnnotations.firstObject).coordinate.longitude);
    
    if ([SXCUtility cleanArray:[selectedPlace valueForKey:@"filter_amenities"]].count > 0) {
    
        if(layoutConstraintViewHeight.constant == 190){
            
             [[Mixpanel sharedInstance] track:@"View Expanded Clicked"];
            
            layoutConstraintViewHeight.constant = 247;
            
            [UIView animateWithDuration:1 animations:^{
                [self.view layoutIfNeeded];
            }completion:^(BOOL finished) {
                
                MKCoordinateRegion region = {coord,{0.001f,0.001f}};
                [_mapViw setRegion:region animated:YES];
            }];
  
        }
        else {
        
            layoutConstraintViewHeight.constant = 190;
            
            [UIView animateWithDuration:1 animations:^{
                [self.view layoutIfNeeded];
            }completion:^(BOOL finished) {
                
                MKCoordinateRegion region = {coord,{0.001f,0.001f}};
                [_mapViw setRegion:region animated:YES];
            }];
        
        }
    }

}
- (IBAction)closeButtonClicked:(id)sender {
    
    layoutConstraintViewHeight.constant = 190.0f;
    layoutDetailViewBottom.constant = -260.0f;
    
    NSArray *selectedAnnotations = self.mapViw.selectedAnnotations;
    for (MyAnnotation *annotationView in selectedAnnotations) {
        [self.mapViw deselectAnnotation:annotationView animated:YES];
    }
    
    [UIView animateWithDuration:1 animations:^{
        [self.view layoutIfNeeded];
    }completion:^(BOOL finished) {
        
    }];
}

-(void)bindValueWithComponentsWithData:(NSDictionary *)data{
    
    selectedPlace = data;
    
    if([data valueForKey:@"yelp_url"] !=nil){
        layoutConstraintYelpButtonHeight.constant = 33.0f;
    }else {
        layoutConstraintYelpButtonHeight.constant = 0.0f;
    }
    
    
    if([data valueForKey:@"yelp_rating"] !=nil){
        layoutConstraintStarHeight.constant = 33.0f;
        
        float ratingValue = [[[data valueForKey:@"yelp_rating"] description] floatValue];
        
        int integerRatingValue = ratingValue;
        
        float roundOffValue = integerRatingValue + 0.5f;
        if(ratingValue < roundOffValue) {
            roundOffValue = roundOffValue - 0.5f;
        }
        
        NSString* formattedNumber = [NSString stringWithFormat:@"star_%.01f", roundOffValue];
        
        [imageViewStar setImage:[UIImage imageNamed:formattedNumber]];
        
        
    }else {
        layoutConstraintStarHeight.constant = 0.0f;
    }

    
    
    _pakrName.text=data[@"name"];
    
    
    if([SXCUtility cleanInt:data[@"is_favourite"]] == 1){
        buttonFavorite.selected = true;
    }
    else{
        buttonFavorite.selected = false;
    }
    
    NSMutableString * checkInDogs = [[NSMutableString alloc] init];
    
    if([data valueForKey:@"operating_hours"]&&([SXCUtility cleanString:[data valueForKey:@"operating_hours"]].length>0)){
        [checkInDogs appendString:[NSString stringWithFormat:@"Opening Hours: %@",data[@"operating_hours"]]];
    }
    else{
        [checkInDogs appendString:[NSString stringWithFormat:@"Opening Hours: Not Available"]];
    }
    
    checkInDogs = [[checkInDogs componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "].mutableCopy;
    
    _numberOfDogs.text = checkInDogs;
    
    _parkOpeningTime.text=[NSString stringWithFormat:@"%@, %@, %@",data[@"address"],data[@"address_locality"],data[@"address_postal_code"]];
    
    NSString * imageUrl = @"";
    if([data valueForKey:@"thumbs"] && [[data valueForKey:@"thumbs"] valueForKey:@"w400"]){
        imageUrl= [SXCUtility cleanString:[[SXCUtility cleanDictionary:data[@"thumbs"]] valueForKey:@"w400"]];
        imageUrl=[imageUrl stringByReplacingOccurrencesOfString:@"uploads/park//" withString:@"media/cache/park_thumb/"];
    }
    
    buttonAddPhotoPlace.hidden = false;
    if(imageUrl != nil && imageUrl.length > 0) {
        [_parkImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"default_park"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if(error == nil){
                [_parkImageView setBackgroundColor:[UIColor clearColor]];
                buttonAddPhotoPlace.hidden = true;
            } else {
                buttonAddPhotoPlace.hidden = false;
            }
        }];
    }
    else {
          buttonAddPhotoPlace.hidden = true;
        [_parkImageView setImage:[UIImage imageNamed:@"default_park"]];
    }
    
    
    [_parkImageView setContentMode:UIViewContentModeScaleAspectFill];
    [_parkImageView setBackgroundColor:[UIColor clearColor]];
    
    
    _labels3.clipsToBounds=YES;
    _labels1.clipsToBounds=YES;
    _labels2.clipsToBounds=YES;
    
    _labels3.layer.cornerRadius=_labels3.frame.size.width/2.0f;
    _labels2.layer.cornerRadius=_labels2.frame.size.width/2.0f;
    _labels1.layer.cornerRadius=_labels1.frame.size.width/2.0f;
    
    
    if(([data valueForKey:@"sta_score"][@"t"])&&([data valueForKey:@"sta_score"][@"s"])&&([data valueForKey:@"sta_score"][@"a"])){
        _labels3.hidden=NO;
        _labels1.hidden=NO;
        _labels2.hidden=NO;
        
        _labels2.text=[NSString stringWithFormat:@"t:%@",[data valueForKey:@"sta_score"][@"t"]];
        _labels1.text=[NSString stringWithFormat:@"s:%@",[data valueForKey:@"sta_score"][@"s"]];
        _labels3.text=[NSString stringWithFormat:@"a:%@",[data valueForKey:@"sta_score"][@"a"]];
        
    }
    else{
        _labels3.hidden=YES;
        _labels2.hidden=YES;
        _labels1.hidden=YES;
    }
    
    labelAddress.text=[NSString stringWithFormat:@"%@, %@, %@",data[@"address"],data[@"address_locality"],data[@"address_postal_code"]];
    
    [collectioView registerNib:[UINib nibWithNibName:@"PWPAmentitiesCell" bundle:nil] forCellWithReuseIdentifier:@"PWPAmentitiesCell"];
    [collectioView setDelegate:self];
    [collectioView setDataSource:self];
    [collectioView reloadData];
    
    NSString * checkInDogsCount = [SXCUtility cleanString:[[data valueForKey:@"active_dogs_count"] description]];
    labelTotalDogsCheckedin.text = [NSString stringWithFormat:@"%@ Dogs here",checkInDogsCount];
    
    NSArray * checkInDogs1 = [SXCUtility cleanArray:[data valueForKey:@"last_active_dogs"]];
    [self configureAccordingToCheckInDogs:checkInDogs1];
    
}

-(NSMutableDictionary *)requestDictionary
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] init];
    
    if([PWPApplicationUtility getCurrentUserDogsArray].count>1){
        [requestDictionary setValue:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"] forKey:@"dogs"];
    }
    [requestDictionary setValue:[SXCUtility cleanString:[SXCUtility getNSObject:@"quickCheckinDefaultMessage"]] forKey:@"message"];
    
    return requestDictionary;
}

- (IBAction)directionClicked:(id)sender {
    
    if([selectedPlace valueForKey:@"lat"] != nil && [selectedPlace valueForKey:@"lng"] != nil) {
        
        NSString *current_lat = [NSString stringWithFormat:@"%f",[UBRLocationHandler shareHandler].currentLocation.coordinate.latitude];
        NSString *current_long = [NSString stringWithFormat:@"%f",[UBRLocationHandler shareHandler].currentLocation.coordinate.longitude];
        
        NSString *dest_lat = [[selectedPlace valueForKey:@"lat"] description];
        NSString *dest_longi = [[selectedPlace valueForKey:@"lng"] description];
        
        NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%@,%@&daddr=%@,%@",current_lat, current_long, dest_lat, dest_longi];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: directionsURL]];
    }
    
}


- (IBAction)quickCheckInClicked:(id)sender {
    
    [[Mixpanel sharedInstance] track:@"Quick Check-In Clicked on Map"];

    if([PWPCheckInHelper checkInAllowedForParkId:selectedPlace[@"id"] forDogs:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"]] == false){
        return;
    }
    [self startLoader];
    
    [[PWPService sharedInstance] checkInParkWithRequestDictionary:[self requestDictionary] withParkId:selectedPlace[@"id"] withSuccessBlock:^(id response) {
        
        [PWPCheckInHelper dogCheckedInParkId:selectedPlace[@"id"] forDogs:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"]];

        [self dismissLoader];
        [self sxc_showSuccessWithMessage:@"You have been checked-in successfully."];
        if([_delegate respondsToSelector:@selector(fetchParks)]){
            [_delegate fetchParks];
        }
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        
        if (error.code==403) {
            [self sxc_showErrorViewWithMessage:@"You have already checked-in."];
        }
        else{
            [self sxc_showErrorViewWithMessage:error.domain];
        }
        
    }];
    
    
    
    
}

-(void)adjustImageAndTitleOffsetsForButton:(UIButton *)button {

    CGFloat spacing = 1.0f;
    CGSize imageSize = button.imageView.frame.size;
    button.titleEdgeInsets = UIEdgeInsetsMake(0, -imageSize.width, -(imageSize.height + spacing), 0);
    CGSize titleSize = button.titleLabel.frame.size;
    button.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0, 0, -titleSize.width);
    
    
}

- (IBAction)placeCategorySelected:(id)sender {

    [[Mixpanel sharedInstance] track:@"Quick Filters Clicked on Map"];


    UIButton * button = (UIButton *)sender;
    [button setSelected:!button.isSelected];
    
    if(button.tag == 1){
        buttonEat.selected = false;
        buttonPark.selected = false;
        buttonServices.selected = false;
        
        [self.delegate placeCategorySelected:button.isSelected?@{@"id":@3,@"name":@"Hotel",@"slug":@"hotel",@"child":@true}:@{}];
        
        
    }
    else if(button.tag == 2){
        buttonStay.selected = false;
        buttonPark.selected = false;
        buttonServices.selected = false;
        
          [self.delegate placeCategorySelected:button.isSelected?@{@"id":@2,@"name":@"Bars & Restaurants",@"slug":@"bars-restaurants",@"child":@true}:@{}];
    }
    else if(button.tag == 3){
        buttonStay.selected = false;
        buttonEat.selected = false;
        buttonServices.selected = false;
        
        [self.delegate placeCategorySelected:button.isSelected?@{@"id":@7,@"name":@"Park",@"slug":@"park",@"child":@true}:@{}];

    }
    else if(button.tag == 4){
        buttonStay.selected = false;
        buttonEat.selected = false;
        buttonPark.selected = false;
        
        [self.delegate placeCategorySelected:button.isSelected?@{@"id":@4,@"name":@"Services",@"slug":@"services",@"child":@true}:@{}];

    }
}
- (IBAction)buttonCheckIn:(id)sender {

    [[Mixpanel sharedInstance] track:@"Check-in to closest locations Clicked"];

    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    
    PWPCheckInArroundMeViewController * aroundViewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPCheckInArroundMeViewController"];
    aroundViewController.categorySlug = selectedCategory;
    
    [self.navigationController pushViewController:aroundViewController animated:true];
}


#pragma mark - ADClusterMapView Delegate

- (MKAnnotationView *)mapView:(TSClusterMapView *)mapView viewForClusterAnnotation:(id<MKAnnotation>)annotation {

    TSDemoClusteredAnnotationView * view = (TSDemoClusteredAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([TSDemoClusteredAnnotationView class])];
    if (!view) {
        view = [[TSDemoClusteredAnnotationView alloc] initWithAnnotation:annotation
                                                         reuseIdentifier:NSStringFromClass([TSDemoClusteredAnnotationView class])];
    }

    return view;
}

- (void)mapView:(TSClusterMapView *)mapView willBeginBuildingClusterTreeForMapPoints:(NSSet<ADMapPointAnnotation *> *)annotations {
    NSLog(@"Kd-tree will begin mapping item count %lu", (unsigned long)annotations.count);


}

- (void)mapView:(TSClusterMapView *)mapView didFinishBuildingClusterTreeForMapPoints:(NSSet<ADMapPointAnnotation *> *)annotations {
    NSLog(@"Kd-tree finished mapping item count %lu", (unsigned long)annotations.count);
    
}

- (void)mapViewWillBeginClusteringAnimation:(TSClusterMapView *)mapView{}

- (void)mapViewDidCancelClusteringAnimation:(TSClusterMapView *)mapView {}

- (void)mapViewDidFinishClusteringAnimation:(TSClusterMapView *)mapView{}

- (void)userWillPanMapView:(TSClusterMapView *)mapView {}

- (void)userDidPanMapView:(TSClusterMapView *)mapView {}

- (BOOL)mapView:(TSClusterMapView *)mapView shouldForceSplitClusterAnnotation:(ADClusterAnnotation *)clusterAnnotation {
    return YES;
}

- (BOOL)mapView:(TSClusterMapView *)mapView shouldRepositionAnnotations:(NSArray<ADClusterAnnotation *> *)annotations toAvoidClashAtCoordinate:(CLLocationCoordinate2D)coordinate {
    return YES;
}

- (IBAction)rideNowButtonClicked:(id)sender {

    [[Mixpanel sharedInstance] track:@"Advertisement Pop Up Button Clicked"];

    viewAdvertisment.hidden = true;

    NSString * link = [SXCUtility cleanString:[SXCUtility getNSObject:@"map_navigation_popup_link"]];

    if ([self lyftInstalled]) {
        [self open:link];
    }
    else {
        NSString * clientId = [link stringByReplacingOccurrencesOfString:@"lyft://partner=" withString:@""];
        [self open:[NSString stringWithFormat:@"https://www.lyft.com/signup/SDKSIGNUP?clientId=%@&sdkName=iOS_direct",clientId]];
    }
}

- (IBAction)buttonAdvertisemntClicked:(id)sender {

    [[Mixpanel sharedInstance] track:@"Advertisement Pop Up open"];


    viewAdvertisment.hidden = false;
}
- (IBAction)popCloseClicked:(id)sender {

    [[Mixpanel sharedInstance] track:@"Advertisement Pop Up close"];

    viewAdvertisment.hidden = true;
}



- (BOOL)lyftInstalled {
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"lyft://"]];
}

- (void)open:(NSString *)scheme {
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:scheme];

    if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [application openURL:URL options:@{} completionHandler:nil];
    } else {
        [application openURL:URL];
    }
}

- (IBAction)yelpClicked:(id)sender {
    if([selectedPlace valueForKey:@"yelp_url"] !=nil){
        if ([self isYelpInstalled]) {
            NSString * string = [[[selectedPlace valueForKey:@"yelp_url"] description] stringByReplacingOccurrencesOfString:@"http://yelp.com" withString:@"yelp://"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
        } else {
            // Use the Yelp touch site
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[selectedPlace valueForKey:@"yelp_url"] description]]];
        }
    }
    
    
}

- (BOOL)isYelpInstalled {
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"yelp:"]];
}



-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [SXCUtility cleanArray:[selectedPlace valueForKey:@"filter_amenities"]].count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(50,30);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)theCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PWPAmentitiesCell * amenitiesCell = [theCollectionView dequeueReusableCellWithReuseIdentifier:@"PWPAmentitiesCell" forIndexPath:indexPath];
    amenitiesCell.imageViewAmenities.image = [UIImage imageNamed:[SXCUtility cleanArray:[selectedPlace valueForKey:@"filter_amenities"]][indexPath.row]];
    return amenitiesCell;
}

@end
