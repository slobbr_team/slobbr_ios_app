//
//  PWPParkCheckInViewController.m
//  PawPark
//
//  Created by daffolap19 on 5/23/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPParkCheckInViewController.h"
#import "PWPSelectDogTableViewController.h"
#import "PWPService.h"
#import "PWPApplicationUtility.h"
#import "SXCUtility.h"
#import "UIViewController+UBRComponents.h"
#import <UIImageView+WebCache.h>
#import "SXCTextfieldWithLeftImage.h"
#import "UIColor+PWPColor.h"
#import "SZTextView.h"
#import <Google/Analytics.h>
#import "PWPSelectStatusViewController.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPCheckInHelper.h"

@interface PWPParkCheckInViewController ()
{
    NSMutableArray * selectedDogsArray;
    NSMutableArray * selectedDogsNameArray;
    NSMutableArray * selectedDogsObjectArray;
    NSMutableString * status;
    
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldStatus;
    __weak IBOutlet UIImageView * imageViewBG;
    __weak IBOutlet NSLayoutConstraint *layoutConstraintTextviewTopMargin;
    __weak IBOutlet NSLayoutConstraint *layoutConstraintTextviewHeight;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textfieldSelectDog;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewGoogleMaps;
@property (weak, nonatomic) IBOutlet UILabel *labelParkName;
@property (weak, nonatomic) IBOutlet UILabel *labelParkAddress;
@property (weak, nonatomic) IBOutlet SZTextView *textViewMessage;
@property (weak, nonatomic) IBOutlet UIButton *buttonSelectDog;


- (IBAction)cancelButtonClicked:(id)sender;
- (IBAction)checkInButtonClicked:(id)sender;
- (IBAction)selectDogButtonClicked:(id)sender;

@end

@implementation PWPParkCheckInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setDogsNameStringToTextField];

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Park Check In Filter Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
   
    textfieldStatus.text = status;
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if([status isEqualToString:@"Other"]){
        layoutConstraintTextviewTopMargin.constant = 65;
        layoutConstraintTextviewHeight.constant = 146;
    }
    else{
        
        layoutConstraintTextviewTopMargin.constant = 58;
        layoutConstraintTextviewHeight.constant = 0;
    }
    
    [UIView animateWithDuration:.5 animations:^{
        [self.scrollview layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Buisness Methods
-(void)setDogsNameStringToTextField{
    NSMutableString * dogsString=[[NSMutableString alloc] init];
    for(NSString * string in selectedDogsNameArray){
        if(dogsString.length >0){
            [dogsString appendString:@", "];
        }
        [dogsString appendString:string];
    }
    _textfieldSelectDog.text=dogsString;
}

-(NSMutableDictionary *)requestDictionary
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] init];
    
    if([PWPApplicationUtility getCurrentUserDogsArray].count>1){
        [requestDictionary setValue:selectedDogsArray forKey:@"dogs"];
    }
    
    if([status isEqualToString:@"Other"]){
        [requestDictionary setValue:_textViewMessage.text forKey:@"message"];
    }
    else{
        [requestDictionary setValue:status forKey:@"message"];
    }
    
    return requestDictionary;


}

-(BOOL)isValid
{
    if(_textfieldSelectDog.text.length==0){
        [self sxc_showErrorViewWithMessage:@"Please select the dogs from your pack."];
        return NO;
    }
    return YES;
}

-(void)initView
{
    [self sxc_navigationBarCustomizationWithTitle:@"Slobbr Check-IN"];
    [self sxc_customizeBackButton];
    
    _textViewMessage.placeholder=@"Add your status here";
    _textViewMessage.placeholderTextColor=[UIColor sxc_hintColor];
    _textViewMessage.font=[UIFont fontWithName:@"Roboto" size:14.0f];

    
    selectedDogsArray=[[NSMutableArray alloc] init];
    selectedDogsNameArray=[[NSMutableArray alloc] init];
    selectedDogsObjectArray=[[NSMutableArray alloc] init];
    
    _textViewMessage.layer.borderColor=[UIColor sxc_hintColor].CGColor;
    _textViewMessage.layer.borderWidth=1.0f;

    _labelParkAddress.text=_parkDictionary[@"address"];
    _labelParkName.text=_parkDictionary[@"name"];
 
    NSString * imageUrlString=[[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/staticmap?&size=%.0fx%.0f&markers=icon:http://test.pawparks.nyc/bundles/pawstyle/img/map-marker-raster.png|%@ %@ %@&sensor=false",_imageViewGoogleMaps.frame.size.width,_imageViewGoogleMaps.frame.size.height,_parkDictionary[@"address"],_parkDictionary[@"address_locality"],_parkDictionary[@"address_postal_code"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [_imageViewGoogleMaps sd_setImageWithURL:[NSURL URLWithString:imageUrlString] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    status = [[NSMutableString alloc] init];
}


-(void)addDogsToLocalArray
{
    if(!_parkDictionary[@"active_checkins"]){
        
        NSMutableDictionary * lastCheckInDictionary=[[NSMutableDictionary alloc] init];
        [lastCheckInDictionary setValue:[[NSMutableArray alloc] init] forKey:@"dogs"];
        [_parkDictionary setValue:lastCheckInDictionary forKey:@"active_checkins"];
        
    }
    
    NSMutableArray * dogsArray=[[NSMutableArray alloc] initWithArray:[SXCUtility cleanArray:_parkDictionary[@"active_checkins"][@"dogs"]]];
    [dogsArray addObjectsFromArray:selectedDogsObjectArray];
    
    NSMutableDictionary * dogsDictionary=[[NSMutableDictionary alloc] init];
    [dogsDictionary setObject:dogsArray forKey:@"dogs"];
    
    [_parkDictionary setObject:dogsDictionary forKey:@"active_checkins"];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"segue_select_dog"]){
        PWPSelectDogTableViewController * selectDogViewController=segue.destinationViewController;
        selectDogViewController.selectedDogsArray=selectedDogsArray;
        selectDogViewController.selectedDogsNameArray=selectedDogsNameArray;
        selectDogViewController.selectedDogsObjectArray=selectedDogsObjectArray;
    }
    
}


- (IBAction)cancelButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)checkInButtonClicked:(id)sender {
    if([self isValid]){
        
        if([PWPCheckInHelper checkInAllowedForParkId:_parkDictionary[@"id"] forDogs:selectedDogsArray] == false){
            return;
        }

        [self startLoader];
        
        [[PWPService sharedInstance] checkInParkWithRequestDictionary:[self requestDictionary] withParkId:_parkDictionary[@"id"] withSuccessBlock:^(id response) {
            
            [PWPCheckInHelper dogCheckedInParkId:_parkDictionary[@"id"] forDogs:selectedDogsArray];

            [self dismissLoader];
            [self sxc_showSuccessWithMessage:@"You have been checked-in successfully."];
            [_delegate successFullCheckedInWithDictionary:(id)_parkDictionary WithDogs:selectedDogsObjectArray];
            
            [self.navigationController popViewControllerAnimated:YES];
            
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Check In Park"     // Event category (required)
                                                                  action:@"Check- In"  // Event action (required)
                                                                   label:_parkDictionary[@"name"]          // Event label
                                                                   value:nil] build]];    // Event valu
            
        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            
            if (error.code==403) {
                [self sxc_showErrorViewWithMessage:@"You have already checked-in."];
            }
            else{
                [self sxc_showErrorViewWithMessage:error.domain];
            }

        }];
    }
    
}

- (IBAction)selectDogButtonClicked:(id)sender {
}
-(void)sxc_startLoading{}
-(void)sxc_stopLoading{}

- (IBAction)predefinedStatusClicked:(id)sender {
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    
    PWPSelectStatusViewController * selectStatusViewController = [storyboard instantiateViewControllerWithIdentifier:@"PWPSelectStatusViewController"];
    selectStatusViewController.selectedStatus = status;
    [self.navigationController pushViewController:selectStatusViewController animated:YES];

}

@end
