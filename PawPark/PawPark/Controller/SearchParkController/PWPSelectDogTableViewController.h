//
//  PWPSelectDogTableViewController.h
//  PawPark
//
//  Created by daffolap19 on 5/23/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPSelectDogTableViewController : UIViewController
@property(nonatomic,strong)NSMutableArray * selectedDogsArray;
@property(nonatomic,strong)NSMutableArray * selectedDogsNameArray;
@property(nonatomic,strong)NSMutableArray * selectedDogsObjectArray;

@end
