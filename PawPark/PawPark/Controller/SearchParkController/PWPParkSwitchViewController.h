//
//  PWPParkSwitchViewController.h
//  PawPark
//
//  Created by xyz on 7/17/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPParkSwitchViewController : UIViewController

@property(nonatomic,strong) NSMutableDictionary * filterDictionary;

-(void)searchParksForPageOffset:(NSNumber *)offset forDragingMap:(BOOL)isMapDragged;
@end
