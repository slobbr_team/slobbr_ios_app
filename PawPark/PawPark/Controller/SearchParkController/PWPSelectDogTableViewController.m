//
//  PWPSelectDogTableViewController.m
//  PawPark
//
//  Created by daffolap19 on 5/23/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPSelectDogTableViewController.h"
#import "UIViewController+UBRComponents.h"
#import "PWPApplicationUtility.h"
#import "SXCUtility.h"
#import "UIColor+PWPColor.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPSelectDogTableViewController ()
{
    __weak IBOutlet UIImageView * imageViewBG;
}
@property(nonatomic,strong)NSArray * dogsArray;
@end


#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height

@implementation PWPSelectDogTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self customizeBackButton];
    [self sxc_customizeDoneButton];
    [self sxc_navigationBarCustomizationWithTitle:@"SELECT DOG"];
    
    _dogsArray=[SXCUtility cleanArray:[PWPApplicationUtility getCurrentUserDogsArray]];
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)sxc_customizeDoneButton{
    
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIFont *font = [UIFont fontWithName:@"Roboto-Regular" size:17.0f];
    doneButton.titleLabel.font = font;
    doneButton.frame = CGRectMake(0, 0 ,50, 30);
    UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
    [doneButton addTarget:self
                   action:@selector(sxc_doneButtonClicked:)
         forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = doneBarButtonItem;
    
}


-(void)sxc_doneButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)customizeBackButton
{
    if(self.navigationController.viewControllers.count>1){
        self.navigationItem.hidesBackButton = YES;
        [self.navigationItem setHidesBackButton:YES animated:YES];
        
        UIImage *backButtonImage = [UIImage imageNamed:(@"btn_back")];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [backButton setImage:backButtonImage forState:UIControlStateNormal];
        backButton.frame = CGRectMake(0, 0 ,23, 23);
        
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        [backButton addTarget:self
                       action:@selector(backButtonClicked:)
             forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = backBarButtonItem;
        
    }
}



-(void)backButtonClicked:(id)sender
{
    [_selectedDogsArray removeAllObjects];
    [_selectedDogsNameArray removeAllObjects];
    [_selectedDogsObjectArray removeAllObjects];
    [self.navigationController popViewControllerAnimated:YES];
}





#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dogsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pwp_dog_name" forIndexPath:indexPath];
    cell.textLabel.text=_dogsArray[indexPath.row][@"name"];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.backgroundColor =[UIColor clearColor];
    cell.contentView.backgroundColor =[UIColor clearColor];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font =[UIFont fontWithName:@"Roboto-Regular" size:20.0f];
    
    UIView *customColorView = [[UIView alloc] init];
    customColorView.backgroundColor = [UIColor sxc_themeColorWithAplha];
    cell.selectedBackgroundView =  customColorView;
    
    if([_selectedDogsArray containsObject:_dogsArray[indexPath.row][@"id"]]){
        cell.accessoryType=UITableViewCellAccessoryCheckmark;

    }else{
        cell.accessoryType=UITableViewCellAccessoryNone;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryCheckmark){
      
        [_selectedDogsArray removeObject:_dogsArray[indexPath.row][@"id"]];
        [_selectedDogsNameArray removeObject:_dogsArray[indexPath.row][@"name"]];
        [_selectedDogsObjectArray removeObject:_dogsArray[indexPath.row]];
        
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    }else{
        [_selectedDogsArray addObject:_dogsArray[indexPath.row][@"id"]];
        [_selectedDogsNameArray addObject:_dogsArray[indexPath.row][@"name"]];
        [_selectedDogsObjectArray addObject:_dogsArray[indexPath.row]];
        
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{


}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
