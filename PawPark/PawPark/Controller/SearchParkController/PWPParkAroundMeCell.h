//
//  PWPParkAroundMeCell.h
//  PawPark
//
//  Created by xyz on 3/9/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPParkAroundDelegate <NSObject>

-(void)aroundMeClicked;

@end

@interface PWPParkAroundMeCell : UITableViewCell

@property(nonatomic,strong)id<PWPParkAroundDelegate> delegate;

@end
