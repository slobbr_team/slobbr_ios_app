//
//  PWPParkCheckedInDogsViewController.h
//  PawPark
//
//  Created by xyz on 07/06/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWPBaseViewController.h"

@interface PWPParkCheckedInDogsViewController : PWPBaseViewController

@property(nonatomic,strong)NSMutableArray * dogsArray;
@property(nonatomic,strong)NSString * placeId;
@property(nonatomic,assign)BOOL isFromCommunity;

@end
