//
//  PWPParkSwitchViewController.m
//  PawPark
//
//  Created by xyz on 7/17/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPParkSwitchViewController.h"
#import "UIViewController+UBRComponents.h"
#import "SXCUtility.h"
#import "PWPService.h"
#import "PWPApplicationUtility.h"
#import "PWPSearchParkMapViewController.h"
#import "PWPSearchParkViewController.h"
#import "PWPSearchParkFilterViewController.h"
#import "UBRLocationHandler.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPMapPinHelper.h"
#import <Mixpanel.h>

#define K_PARKS_SEARCH_LIMIT_SIZE @25


@interface PWPParkSwitchViewController () <PWPSearchParkMapDelegate, PWPParkFilterDelegate,PWPSearchParkListDelegate,PWPSearchParkMapDelegate>
{
    __weak IBOutlet UIImageView * imageViewBG;

    BOOL isUserLocation;
}

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;

@property(nonatomic,weak)IBOutlet UIView * containerView;
@property(nonatomic,weak)IBOutlet UILabel *labelTopFilterInformation;

@property(nonatomic,strong)PWPSearchParkMapViewController * locationViewController;
@property(nonatomic,strong)PWPSearchParkViewController * listViewController;

@property(nonatomic,strong)NSMutableArray * places;
@property (nonatomic,strong)NSMutableDictionary * selectedCategoryPlace;

@end

@implementation PWPParkSwitchViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    isUserLocation = false;

    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    _selectedCategoryPlace = [[NSMutableDictionary alloc] init];

    [self sxc_navigationBarCustomizationWithTitle:@"SLOBBR PLACES"];
    if(self.navigationController.viewControllers.count==1){
        [self sxc_setNavigationBarMenuItem];
    }
    else{
        [self sxc_customizeBackButton];
    }
    
    [self initView];
    [self searchParksForPageOffset:@0 forDragingMap:false];

}

-(void)checkAndDownloadPins {


    if([[SXCUtility getNSObject:@"pins_failed"]  isEqual: @true]){

        [[PWPService sharedInstance] parksCategoriesImageUrlsWithSuccessBlock:^(id response) {

            [PWPMapPinHelper downloadPlacesPins:response withCompletionBlock:^(BOOL isSuccess) {
            }];

        } withErrorBlock:^(NSError *error) {
        }];
    }

}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self checkAndDownloadPins];

    [self updateTopHeader];
}

-(IBAction)filterClicked:(id)sender{

    [[Mixpanel sharedInstance] track:@"Place Filter List Clicked"];

    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];
    PWPSearchParkFilterViewController * parkFilterViewController=[storyBoard instantiateViewControllerWithIdentifier:@"PWPSearchParkFilterViewController"];
    parkFilterViewController.delegate=self;
    parkFilterViewController.parkFilterDictionary=_filterDictionary;
    parkFilterViewController.selectedPlaceCategory = _selectedCategoryPlace;
    [self.navigationController pushViewController:parkFilterViewController animated:YES];
}

-(void)initView{
   
    if(_filterDictionary==nil){
        
        if([[UBRLocationHandler shareHandler] isLocationServicesEnable] == true) {
            _filterDictionary=[[NSMutableDictionary alloc] init];
            _filterDictionary[@"lat"]=[NSNumber numberWithDouble:[UBRLocationHandler shareHandler].currentLocation.coordinate.latitude];
            _filterDictionary[@"lng"]=[NSNumber numberWithDouble:[UBRLocationHandler shareHandler].currentLocation.coordinate.longitude];
            _filterDictionary[@"distance"]=@"2";
            _filterDictionary[@"isUserLocation"]=@true;
            isUserLocation = true;
        }
        else {
            _filterDictionary=[[NSMutableDictionary alloc] init];
            _filterDictionary[@"distance"]=@"2";
            _filterDictionary[@"zipcode"]=[SXCUtility cleanString:[PWPApplicationUtility getCurrentUserProfileDictionary][@"zipcode"]];
            isUserLocation = false;
        }
    }

    [SXCUtility saveNSObject:@false forKey:@"setting_dirty"];
    
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];
    _listViewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPSearchParkViewController"];
    _listViewController.view.translatesAutoresizingMaskIntoConstraints = false;
    _listViewController.delegate = self;
    
    _locationViewController = [[UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"PWPSearchParkMapViewController"];
    _locationViewController.delegate = self;
    _locationViewController.view.translatesAutoresizingMaskIntoConstraints = false;
    [self cycleFromViewController:self.listViewController toViewController:_locationViewController];

}


- (void)cycleFromViewController:(UIViewController*) oldViewController
               toViewController:(UIViewController*) newViewController {
    [oldViewController willMoveToParentViewController:nil];
    [self addChildViewController:newViewController];
    [self addSubview:newViewController.view toView:self.containerView];
    [newViewController.view layoutIfNeeded];
    
    // set starting state of the transition
    newViewController.view.alpha = 0;
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         newViewController.view.alpha = 1;
                         oldViewController.view.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                         [oldViewController.view removeFromSuperview];
                         [oldViewController removeFromParentViewController];
                         [newViewController didMoveToParentViewController:self];
                     }];
}

- (void)addSubview:(UIView *)subView toView:(UIView*)parentView {
    [parentView addSubview:subView];
    
    NSDictionary * views = @{@"subView" : subView,};
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[subView]|"
                                                                   options:0
                                                                   metrics:0
                                                                     views:views];
    [parentView addConstraints:constraints];
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[subView]|"
                                                          options:0
                                                          metrics:0
                                                            views:views];
    [parentView addConstraints:constraints];
}
- (IBAction)segmentValueChanged:(id)sender {
    
    UISegmentedControl * segmentControl = sender;
    
    if (segmentControl.selectedSegmentIndex == 0) {
       
        [self cycleFromViewController:self.listViewController toViewController:_locationViewController];
    } else {
        [[Mixpanel sharedInstance] track:@"Place List Clicked"];
        [self cycleFromViewController:self.locationViewController toViewController:_listViewController];
    }
}

#pragma mark - Service Method
-(void)searchParksForPageOffset:(NSNumber *)offset forDragingMap:(BOOL)isMapDragged{
    
    [SXCUtility saveNSObject:@false forKey:@"setting_dirty"];
    
    if(offset.integerValue == 0){
        if(isMapDragged == false){
            [self showLoader];
        }
        [_locationViewController resetSelection];
    }
    
    [[PWPService sharedInstance] getParksWithRequestDictionary:[self requestDictionaryForOffset:offset] withPageIndex:offset withPlaceCategory:_selectedCategoryPlace[@"slug"] withSuccessBlock:^(id response, NSNumber * pageIndex) {
        
        [self updateTopHeader];
        
        [self hideLoader];
        
        
        NSMutableArray * tempFavPlaces = [[NSMutableArray alloc] init];
        for(NSDictionary * dict in response){
            
            NSMutableDictionary * tempPlace = dict.mutableCopy;
            NSMutableArray * tempAmenities = [[NSMutableArray alloc] init];
            NSMutableDictionary * amenities = [SXCUtility cleanDictionary:[tempPlace valueForKey:@"amenities"]].mutableCopy;
            
            NSMutableArray * amentiesAllValues = [SXCUtility cleanArray:amenities.allValues];
            
            for(NSString * value in amentiesAllValues) {
                if([UIImage imageNamed:value] != nil){
                      [tempAmenities addObject:[value stringByReplacingOccurrencesOfString:@"/" withString:@":"]];
                }
            }
            [tempPlace setValue:tempAmenities forKey:@"filter_amenities"];
            [tempFavPlaces addObject:tempPlace];
        }
        
        if([pageIndex isEqualToNumber:@0]){
            _places=[[NSMutableArray alloc]initWithArray:tempFavPlaces];
            
            if(_places.count == 0 && isMapDragged == NO){
                [self sxc_showErrorViewWithMessage:@"There are no places within that search radius. Please use the filter and increase the search radius to find more places."];
            }
        }
        else{
            [_places addObjectsFromArray:tempFavPlaces];
        }
        
        [_locationViewController reloadMapWithData:_places  withCategorySelected:_selectedCategoryPlace withMapDragged:isMapDragged lat:_filterDictionary[@"lat"] long:_filterDictionary[@"lng"] isUserLocationClicked:isUserLocation];
        [_listViewController reloadListWithData:_places pageIndex:pageIndex.integerValue withMapDragged:isMapDragged];

    } withErrorBlock:^(NSError *error, NSNumber * pageIndex) {

        [self sxc_showErrorViewWithMessage:error.domain];
        
        [self hideLoader];
        
        [_locationViewController reloadMapWithData:_places withCategorySelected:_selectedCategoryPlace withMapDragged:false lat:_filterDictionary[@"lat"] long:_filterDictionary[@"lng"] isUserLocationClicked:isUserLocation];
        [_listViewController reloadListWithData:_places pageIndex:pageIndex.integerValue withMapDragged:false];
    }];
}


-(void)updateTopHeader
{


    NSString * homeZipCode = [PWPApplicationUtility getCurrentUserProfileDictionary][@"zipcode"];

    if([_filterDictionary valueForKey:@"lng"]){
        _labelTopFilterInformation.text=[NSString stringWithFormat:@"Showing Places within a %.1f mile radius of %@",[_filterDictionary[@"distance"] floatValue],@"'current' location"];
    }
    else if([homeZipCode isEqualToString:_filterDictionary[@"zipcode"]]){
        _labelTopFilterInformation.text=[NSString stringWithFormat:@"Showing Places within a %.1f mile radius of %@",[_filterDictionary[@"distance"] floatValue],@"'home' location"];
    }
    else{
        _labelTopFilterInformation.text=[NSString stringWithFormat:@"Showing Places within a %.1f mile radius of %@",[_filterDictionary[@"distance"] floatValue],[SXCUtility cleanString:_filterDictionary[@"zipcode"]]];
    }
}

-(NSMutableDictionary *)requestDictionaryForOffset:(NSNumber *)offset
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] initWithDictionary:_filterDictionary.mutableCopy];
    [requestDictionary setValue:K_PARKS_SEARCH_LIMIT_SIZE forKey:@"limit"];
    [requestDictionary setValue:offset forKey:@"offset"];
    [requestDictionary setValue:[SXCUtility getNSObject:@"apikey"] forKey:@"apikey"];
    [requestDictionary setValue:@"8.9.0" forKey:@"app_version"];
    
    if([requestDictionary valueForKey:@"state"] || [requestDictionary valueForKey:@"city"] || [requestDictionary valueForKey:@"zipcode"]){
        [requestDictionary removeObjectForKey:@"lng"];
        [requestDictionary removeObjectForKey:@"lat"];
    }

    [requestDictionary removeObjectForKey:@"state"];
    [requestDictionary removeObjectForKey:@"isUserLocation"];


    return requestDictionary;
}




-(NSString *)radiusOnBasisOfZipCode:(NSString *)zipCode
{
    NSArray * zipCodes =@[
                          @"11507",
                          @"11003",
                          @"11580",
                          @"11930",
                          @"11701",
                          @"11931",
                          @"11692",
                          @"11768",
                          @"11102",
                          @"11103",
                          @"11105",
                          @"11106",
                          @"11509",
                          @"11702",
                          @"11933",
                          @"11510",
                          @"11050",
                          @"11706",
                          @"11359",
                          @"11360",
                          @"11361",
                          @"11705",
                          @"11709",
                          @"11426",
                          @"11710",
                          @"11713",
                          @"11777",
                          @"11714",
                          @"11715",
                          @"11716",
                          @"11717",
                          @"11697",
                          @"11932",
                          @"11718",
                          @"11719",
                          @"11256",
                          @"11545",
                          @"11933",
                          @"11411",
                          @"11514",
                          @"11516",
                          @"11934",
                          @"11720",
                          @"11721",
                          @"11722",
                          @"11782",
                          @"11724",
                          @"11356",
                          @"11725",
                          @"11726",
                          @"11727",
                          @"11368",
                          @"11935",
                          @"11772",
                          @"11729",
                          @"11746",
                          @"11369",
                          @"11370",
                          @"11937",
                          @"11730",
                          @"11939",
                          @"11940",
                          @"11554",
                          @"11732",
                          @"11731",
                          @"11772",
                          @"11941",
                          @"11942",
                          @"11518",
                          @"11733",
                          @"11596",
                          @"11768",
                          @"11717",
                          @"11373",
                          @"11380",
                          @"11003",
                          @"11731",
                          @"11706",
                          @"11690",
                          @"11691",
                          @"11693",
                          @"11695",
                          @"11735",
                          @"11738",
                          @"11782",
                          @"11901",
                          @"11003",
                          @"11005",
                          @"11050",
                          @"11576",
                          @"11351",
                          @"11352",
                          @"11354",
                          @"11355",
                          @"11358",
                          @"11367",
                          @"11371",
                          @"11381",
                          @"11390",
                          @"11375",
                          @"11768",
                          @"11010",
                          @"11520",
                          @"11365",
                          @"11366",
                          @"11530",
                          @"11040",
                          @"11581",
                          @"11542",
                          @"11545",
                          @"11004",
                          @"11547",
                          @"11763",
                          @"11020",
                          @"11739",
                          @"11581",
                          @"11740",
                          @"11944",
                          @"11548",
                          @"11743",
                          @"11946",
                          @"11788",
                          @"11550",
                          @"11552",
                          @"11040",
                          @"11557",
                          @"11801",
                          @"11741",
                          @"11423",
                          @"11742",
                          @"11414",
                          @"11743",
                          @"11746",
                          @"11096",
                          @"11749",
                          @"11558",
                          @"11756",
                          @"11751",
                          @"11752",
                          @"11372",
                          @"11405",
                          @"11424",
                          @"11425",
                          @"11430",
                          @"11431",
                          @"11432",
                          @"11433",
                          @"11434",
                          @"11435",
                          @"11436",
                          @"11439",
                          @"11451",
                          @"11484",
                          @"11499",
                          @"11947",
                          @"11753",
                          @"11021",
                          @"11415",
                          @"11754",
                          @"11024",
                          @"11706",
                          @"11755",
                          @"11779",
                          @"11020",
                          @"11779",
                          @"11570",
                          @"11552",
                          @"11560",
                          @"11948",
                          @"11791",
                          @"11559",
                          @"11756",
                          @"11561",
                          @"11757",
                          @"11362",
                          @"11363",
                          @"11743",
                          @"11560",
                          @"11561",
                          @"11101",
                          @"11109",
                          @"11120",
                          @"11563",
                          @"11565",
                          @"11030",
                          @"11040",
                          @"11050",
                          @"11949",
                          @"11378",
                          @"11758",
                          @"11762",
                          @"11950",
                          @"11951",
                          @"11560",
                          @"11952",
                          @"11763",
                          @"11763",
                          @"11747",
                          @"11566",
                          @"11953",
                          @"11379",
                          @"11764",
                          @"11765",
                          @"11501",
                          @"11954",
                          @"11955",
                          @"11766",
                          @"11010",
                          @"11545",
                          @"11935",
                          @"11767",
                          @"11710",
                          @"11590",
                          @"11040",
                          @"11956",
                          @"11780",
                          @"11703",
                          @"11752",
                          @"11581",
                          @"11768",
                          @"10960",
                          @"11702",
                          @"11769",
                          @"11364",
                          @"11770",
                          @"11572",
                          @"11804",
                          @"11545",
                          @"11733",
                          @"11568",
                          @"11957",
                          @"11771",
                          @"11416",
                          @"11417",
                          @"11772",
                          @"11958",
                          @"11756",
                          @"11803",
                          @"11030",
                          @"11569",
                          @"11706",
                          @"11733",
                          @"11777",
                          @"11776",
                          @"11050",
                          @"11427",
                          @"11428",
                          @"11429",
                          @"11959",
                          @"11374",
                          @"11960",
                          @"11418",
                          @"11961",
                          @"11385",
                          @"11386",
                          @"11901",
                          @"11694",
                          @"11570",
                          @"11778",
                          @"11779",
                          @"11422",
                          @"11575",
                          @"11530",
                          @"11576",
                          @"11577",
                          @"11021",
                          @"11023",
                          @"11963",
                          @"11962",
                          @"11412",
                          @"11780",
                          @"11706",
                          @"11754",
                          @"11050",
                          @"11704",
                          @"11782",
                          @"11579",
                          @"11783",
                          @"11507",
                          @"11784",
                          @"11733",
                          @"11964",
                          @"11965",
                          @"11968",
                          @"11967",
                          @"11786",
                          @"11787",
                          @"11789",
                          @"11735",
                          @"11968",
                          @"11970",
                          @"11420",
                          @"11419",
                          @"11720",
                          @"11971",
                          @"11972",
                          @"11413",
                          @"11937",
                          @"11530",
                          @"11790",
                          @"11104",
                          @"11791",
                          @"11973",
                          @"11553",
                          @"11580",
                          @"11581",
                          @"11792",
                          @"11975",
                          @"11793",
                          @"11976",
                          @"11772",
                          @"11704",
                          @"11702",
                          @"11552",
                          @"11795",
                          @"11796",
                          @"11590",
                          @"11977",
                          @"11978",
                          @"11798",
                          @"11357",
                          @"11596",
                          @"11797",
                          @"11421",
                          @"11598",
                          @"11377",
                          @"11798",
                          @"11980"
                          ];
    
    if([zipCodes containsObject:zipCode]){
        return @"10";
    }
    else{
        return @"1";
    }
    
}

-(void)fetchDataForIndex:(NSInteger)pageIndex
{
    [self searchParksForPageOffset:[NSNumber numberWithInteger:pageIndex] forDragingMap:false];
}

-(void)fetchParks
{
    [self searchParksForPageOffset:@0 forDragingMap:false];
}

-(void)filterApplied
{
    [self showLoader];

    [self updateTopHeader];

    isUserLocation = false;

    [self searchParksForPageOffset:@0 forDragingMap:false];
}

-(void)filterAppliedWithDictionary:(NSDictionary *)dictionary withCategory:(NSMutableDictionary *)category
{
    [self showLoader];
    _filterDictionary = dictionary.mutableCopy;
    _selectedCategoryPlace = category;
    
    [self updateTopHeader];
    [self searchParksForPageOffset:@0 forDragingMap:false];
    
}

-(void)showLoader{
    [self.loader startAnimating];
    [self.containerView setHidden:YES];
    [self.loader setHidden:NO];
}

-(void)hideLoader{
    [self.loader stopAnimating];
    [self.containerView setHidden:NO];
    [self.loader setHidden:YES];
}

#pragma mark - PWPParkAroundDelegate
-(void)aroundMeClicked
{
    isUserLocation = true;
    
    _filterDictionary = [[NSMutableDictionary alloc] init];
    _filterDictionary[@"lat"]=[NSNumber numberWithDouble:[UBRLocationHandler shareHandler].currentLocation.coordinate.latitude];
    _filterDictionary[@"lng"]=[NSNumber numberWithDouble:[UBRLocationHandler shareHandler].currentLocation.coordinate.longitude];
    _filterDictionary[@"distance"]=@"2";
    _filterDictionary[@"isUserLocation"]=@true;
    
    [self showLoader];
    [self updateTopHeader];
    [self searchParksForPageOffset:@0 forDragingMap:false];
}

-(void)aroundHomeZipCodeClicked {

    _filterDictionary=[[NSMutableDictionary alloc] init];
    _filterDictionary[@"distance"]=@"2";
    _filterDictionary[@"zipcode"]=[SXCUtility cleanString:[PWPApplicationUtility getCurrentUserProfileDictionary][@"zipcode"]];

    isUserLocation = false;
    [self showLoader];
    [self updateTopHeader];
    [self searchParksForPageOffset:@0 forDragingMap:false];
}

-(void)draggingMapClickedwithlatitude:(float )latitide andlongitde:(float )longitude withRadius:(float)radius
{
    isUserLocation = false;

    _filterDictionary = [[NSMutableDictionary alloc] init];
    _filterDictionary[@"lat"]=[NSNumber numberWithFloat:latitide];
    _filterDictionary[@"lng"]=[NSNumber numberWithFloat:longitude];
    _filterDictionary[@"distance"]=[NSNumber numberWithFloat:radius];;
    _filterDictionary[@"isUserLocation"]=@false;

//    [self showLoader];
//    [self updateTopHeader];
    [self searchParksForPageOffset:@0 forDragingMap:true];
    
}

-(void)placeCategorySelected:(NSDictionary *)categorySelected
{
    [self showLoader];
    _selectedCategoryPlace = categorySelected.mutableCopy;
    
    [self updateTopHeader];
    [self searchParksForPageOffset:@0 forDragingMap:false];

}

-(void)sxc_startLoading{}
-(void)sxc_stopLoading{}
@end
