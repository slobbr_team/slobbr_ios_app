//
//  PWPSearchParkMapViewController.h
//  PawPark
//
//  Created by xyz on 7/23/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSClusterMapView.h"


@protocol PWPSearchParkMapDelegate <NSObject>
-(void)aroundMeClicked;
-(void)aroundHomeZipCodeClicked;
-(void)fetchParks;
-(void)placeCategorySelected:(NSDictionary *)categorySelected;

-(void)draggingMapClickedwithlatitude:(float)latitide andlongitde:(float)longitude withRadius:(float)radius;

@end

@interface PWPSearchParkMapViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *pakrName;
@property (weak, nonatomic) IBOutlet UIImageView *parkImageView;
@property (weak, nonatomic) IBOutlet UILabel *numberOfDogs;
@property (weak, nonatomic) IBOutlet UILabel *parkOpeningTime;
@property (weak, nonatomic) IBOutlet UILabel *labels1;
@property (weak, nonatomic) IBOutlet UILabel *labels3;
@property (weak, nonatomic) IBOutlet UILabel *labels2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintMapBottomSpace;

@property (strong, nonatomic) IBOutlet UIView *shadowView;


@property(nonatomic,weak)id <PWPSearchParkMapDelegate> delegate;

-(void)reloadMapWithData:(NSMutableArray *)parks withCategorySelected:(NSMutableDictionary *)category withMapDragged:(BOOL)isMapDragged lat:(NSNumber *)lat long:(NSNumber *)lng isUserLocationClicked:(BOOL)isUserLocation ;

- (IBAction)favClicked:(id)sender;
- (IBAction)checkInClicked:(id)sender;
-(void)resetSelection;
@end
