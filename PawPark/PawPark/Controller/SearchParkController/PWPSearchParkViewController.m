//
//  PWPSearchParkViewController.m
//  PawPark
//
//  Created by daffomac on 4/24/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//
#import "PWPApplicationUtility.h"
#import "PWPSearchParkViewController.h"
#import "UIViewController+UBRComponents.h"
#import "PWPService.h"
#import "SXCTableViewLoadMoreDelegate.h"
#import "PWPSearchParkTableViewCell.h"
#import "SXCUtility.h"
#import "PWPParkCheckInViewController.h"
#import <MapKit/MapKit.h>
#import "MyAnnotation.h"
#import "PWPParkCheckedInDogsViewController.h"
#import <Google/Analytics.h>
#import "PWPParkAroundMeCell.h"
#import "UBRLocationHandler.h"
#import "PWPSchedulePawDateViewController.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPPlacePicViewController.h"

#define K_PARKS_SEARCH_LIMIT_SIZE @25

@interface PWPSearchParkViewController ()<UITableViewDataSource,UITableViewDelegate,SXCLoadMoreDelegate,PWPParkCellDelegate, PWPPlacePicViewControllerDelegate>
{
    __weak IBOutlet UILabel *labelTopFilterInformation;
    __weak IBOutlet UITableView * tableViewPark;
    __weak IBOutlet NSLayoutConstraint *layoutConstraintTopLabelHeight;
    __weak IBOutlet UIImageView * imageViewBG;
    
    SXCTableViewLoadMoreDelegate * tableViewLoadMoreDelegate;
    
    __weak IBOutlet UILabel *labelNoData;
    
    NSMutableArray * parks;
    
    NSInteger selectedRow;
    

}
@end

@implementation PWPSearchParkViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    [self initView];
    [self initViewTableviewLoadMoreDelegate];
    
    [tableViewPark registerNib:[UINib nibWithNibName:@"PWPSearchParkTableViewCell" bundle:nil] forCellReuseIdentifier:@"PWPParkListiningCell"];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [tableViewPark reloadData];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Search Park Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
 
    
}


#pragma mark - Buisness Methods

-(void)initView
{
    selectedRow=-1;
}

-(void)initViewTableviewLoadMoreDelegate
{
    
    tableViewLoadMoreDelegate=[[SXCTableViewLoadMoreDelegate alloc] initWithTableView:tableViewPark WithTotalCount:@0 WithPageSize:K_PARKS_SEARCH_LIMIT_SIZE WithPageIndex:@0];
    tableViewLoadMoreDelegate.currentArrayCount=@0;
    
    tableViewLoadMoreDelegate.loadMoreDelegate=self;
    tableViewLoadMoreDelegate.delegate=self;
    tableViewLoadMoreDelegate.dataSource=self;
    [tableViewLoadMoreDelegate reloadData];
    
    [tableViewLoadMoreDelegate automaticallyRefreshControllOnView];
    
}

-(NSMutableDictionary *)requestDictionaryForOffset:(NSNumber *)offset
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] initWithDictionary:_filterDictionary];
    [requestDictionary setValue:K_PARKS_SEARCH_LIMIT_SIZE forKey:@"limit"];
    [requestDictionary setValue:offset forKey:@"offset"];
    
      
    [requestDictionary setValue:[SXCUtility getNSObject:@"apikey"] forKey:@"apikey"];
    return requestDictionary;
}



#pragma mark - UITableView Delegate And DataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(parks.count <=5 && [SXCUtility cleanString:_filterDictionary[@"s"]].length==0 && indexPath.row ==  parks.count){
        
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"PWPParkIncreaseRangeCell" forIndexPath:indexPath];
        return cell;
    }
    else{
        
        PWPSearchParkTableViewCell * tableViewCell =[tableView dequeueReusableCellWithIdentifier:@"PWPParkListiningCell" forIndexPath:indexPath];
        
        [tableViewCell bindValueWithComponentsWithData:parks[indexPath.row] reloadData:(indexPath.row == selectedRow)];
        [tableViewCell setDelegate:self];
        return tableViewCell;
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(parks.count <=5 && parks.count >0 && [SXCUtility cleanString:_filterDictionary[@"s"]].length==0){
        return parks.count+1;
    }
    return parks.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == selectedRow && [SXCUtility cleanArray:[parks[indexPath.row] valueForKey:@"filter_amenities"]].count > 0) {
        return 295.0f;
    }else {
        return 238.0f;
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == selectedRow && [SXCUtility cleanArray:[parks[indexPath.row] valueForKey:@"filter_amenities"]].count > 0) {
        return 295.0f;
    }else {
        return 238.0f;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSInteger previousSelectedRow = selectedRow;
    
    
    if(indexPath.row!=selectedRow){
        selectedRow=indexPath.row;
    }
    else{
        selectedRow=-1;
    }
    
    NSMutableArray * indexes = [[NSMutableArray alloc] init];
    if(previousSelectedRow != -1 && previousSelectedRow < parks.count){
        [indexes addObject:[NSIndexPath indexPathForRow:previousSelectedRow inSection:0]];
    }
    
    if(selectedRow != -1){
        [indexes addObject:[NSIndexPath indexPathForRow:selectedRow inSection:0]];
    }
    
    [tableView reloadRowsAtIndexPaths:indexes
                     withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Service Method

-(void)reloadListWithData:(NSMutableArray *)theParks pageIndex:(NSInteger)pageIndex withMapDragged:(BOOL)isMapDragged
{
    selectedRow = -1;
    
    [SXCUtility saveNSObject:@false forKey:@"setting_dirty"];
    
    
    if(isMapDragged && theParks.count == 0){
        [tableViewLoadMoreDelegate reloadData];
        return;
    }
    
    
    parks = theParks;
    
    
    NSNumber * totalCount = @0;
    
    if(parks.count==(K_PARKS_SEARCH_LIMIT_SIZE.integerValue+pageIndex)){
        totalCount=[NSNumber numberWithInteger:parks.count+1];
    }
    else{
        totalCount=[NSNumber numberWithInteger:parks.count];
    }
    
    if(parks.count <=5 && parks.count >0 &&  [SXCUtility cleanString:_filterDictionary[@"s"]].length==0)
    {
        totalCount=[NSNumber numberWithInteger:totalCount.integerValue+1];
    }
    
    tableViewLoadMoreDelegate.totalCount =totalCount;
    
    tableViewLoadMoreDelegate.limitstart= [NSNumber numberWithInteger:pageIndex];
    
    tableViewLoadMoreDelegate.currentArrayCount=totalCount;
    
    
    [tableViewLoadMoreDelegate reloadData];
    
    if(parks.count==0){
        labelNoData.text=@"There are no places within that search radius.  Please use the filter and increase the search radius to find great dog friendly places nearby!";
        labelNoData.hidden=NO;
        [self.view bringSubviewToFront:labelNoData];
    }
    else{
        labelNoData.hidden=YES;
    }
}




#pragma mark - Delegate Methods
-(void)tableView:(UITableView *)tableView loadMoreWithNextPageIndex:(NSNumber *)nextPageIndex WithPageSize:(NSNumber *)pageSize{
    
    if([_delegate respondsToSelector:@selector(fetchDataForIndex:)]){
        [_delegate fetchDataForIndex:nextPageIndex.integerValue];
    }
}



-(CGFloat)getHeightOfParkBasedOnAmentis:(NSDictionary *)parkDictionary
{
    
    NSMutableString * amentiesString=[[NSMutableString alloc] init];
    
    for(NSString * amenity in parkDictionary[@"amenities"]){
        if(amentiesString.length>0){
            [amentiesString appendString:@"\n"];
        }
        [amentiesString appendString:amenity];
    }
    
    CGFloat amentiesHeight=[self getHeightOfLabelWithText:amentiesString withFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0f]];
    
    if(amentiesHeight==0){
        return 153;
    }
    return 153+amentiesHeight;
}





-(CGFloat)getHeightOfLabelWithText:(NSString *)text withFont:(UIFont *)font
{
    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    gettingSizeLabel.font = font;
    gettingSizeLabel.text = text;
    gettingSizeLabel.numberOfLines = 0;
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width-16, 9999);
    
    CGSize expectSize = [gettingSizeLabel sizeThatFits:maximumLabelSize];
    return expectSize.height;
}

#pragma mark - PWPParkDelegate

-(void)addPlaceClickedWithData:(NSDictionary *)dictionary {
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    
    PWPPlacePicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPPlacePicViewController"];
    viewController.placePics = [SXCUtility cleanArray:[dictionary valueForKey:@"gallery"]].mutableCopy;
    viewController.data = dictionary;
    viewController.placeId = dictionary[@"id"];
    viewController.delegate = self;
    viewController.isGalleryOpen = true;
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)parkOptionClickedWithData:(NSDictionary *)dictionary
{
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    PWPPlacePicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPPlacePicViewController"];
    viewController.placePics = [SXCUtility cleanArray:[dictionary valueForKey:@"gallery"]].mutableCopy;
    viewController.data = dictionary;
    viewController.placeId = dictionary[@"id"];
    viewController.delegate = self;

    [self.navigationController pushViewController:viewController animated:YES];

//
//    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    PWPParkCheckInViewController * viewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckInViewController"];
//    viewController.parkDictionary=[[NSMutableDictionary alloc] initWithDictionary:dictionary];
//    viewController.delegate=self;
//    
//    [self.navigationController pushViewController:viewController animated:YES];
}
-(void)parkOption2ClickedWithData:(NSDictionary *)dictionary
{
    NSString * checkInDogsCount = [SXCUtility cleanString:[[dictionary valueForKey:@"active_dogs_count"] description]];
    if(checkInDogsCount.integerValue>0){
        
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PWPParkCheckedInDogsViewController * parkCheckInDogsViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckedInDogsViewController"];
        parkCheckInDogsViewController.placeId = [SXCUtility cleanString:[[dictionary valueForKey:@"id"] description]];
        
        [self.navigationController pushViewController:parkCheckInDogsViewController animated:YES];
    }
}
-(void)parkOption3ClickedWithData:(NSDictionary *)dictionary
{
    [self startLoader];
    
    if([SXCUtility cleanInt:dictionary[@"is_favourite"]] ==1){
   
        [[PWPService sharedInstance] deleteHomePark:dictionary[@"id"] withSuccessBlock:^(id response) {
            [self dismissLoader];
            
            NSInteger index =[parks indexOfObject:dictionary];
            NSMutableDictionary * parkDictionary = dictionary.mutableCopy;
            
            if([SXCUtility cleanInt:parkDictionary[@"is_favourite"]] ==1){
                parkDictionary[@"is_favourite"]=@0;
            }
            else{
                parkDictionary[@"is_favourite"]=@1;
            }
            
            [parks replaceObjectAtIndex:index withObject:parkDictionary];
            
            
            [tableViewLoadMoreDelegate reloadData];
            [self sxc_showSuccessWithMessage:@"Place has been removed from 'Favorite' Places."];
            
        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:error.domain];
        }];
    
    }
    else{
    
    
    [[PWPService sharedInstance] homePark:dictionary[@"id"] withSuccessBlock:^(id response) {
        [self dismissLoader];
        
        NSInteger index =[parks indexOfObject:dictionary];
        NSMutableDictionary * parkDictionary = dictionary.mutableCopy;
        
        if([SXCUtility cleanInt:parkDictionary[@"is_favourite"]] ==1){
            parkDictionary[@"is_favourite"]=@0;
        }
        else{
            parkDictionary[@"is_favourite"]=@1;
        }

        [parks replaceObjectAtIndex:index withObject:parkDictionary];
        
        NSMutableDictionary * userDictionary=[[PWPApplicationUtility getCurrentUserProfileDictionary] mutableCopy];
        [userDictionary setValue:dictionary forKey:@"place"];
        [PWPApplicationUtility saveCurrentUserProfileDictionary:userDictionary];
        
        [tableViewLoadMoreDelegate reloadData];
        [self sxc_showSuccessWithMessage:@"Place has been marked as 'Favorite' Place."];
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        [self sxc_showErrorViewWithMessage:error.domain];
    }];
    }
}

-(void)successFullCheckedInWithDictionary:(NSMutableDictionary *)data WithDogs:(NSMutableArray *)dogsArray{
    
    [tableViewLoadMoreDelegate automaticallyRefreshControllOnView];
    if([_delegate respondsToSelector:@selector(fetchDataForIndex:)]){
        [_delegate fetchDataForIndex:0];
    }
}

-(void)inviteOptionClicked:(NSDictionary *)dictionary
{
    [self pushSchedulePawDateViewController:dictionary];
}

-(void)parkQuickCheckInClicked:(NSDictionary *)dictionary
{
    
    [self startLoader];
    
    [[PWPService sharedInstance] checkInParkWithRequestDictionary:[self requestDictionary] withParkId:dictionary[@"id"] withSuccessBlock:^(id response) {
        
        [self dismissLoader];
        [self sxc_showSuccessWithMessage:@"You have been checked-in successfully."];
        [tableViewLoadMoreDelegate automaticallyRefreshControllOnView];
        if([_delegate respondsToSelector:@selector(fetchDataForIndex:)]){
            [_delegate fetchDataForIndex:0];
        }
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        
        if (error.code==403) {
            [self sxc_showErrorViewWithMessage:@"You have already checked-in."];
        }
        else{
            [self sxc_showErrorViewWithMessage:error.domain];
        }
        
    }];
}

-(NSMutableDictionary *)requestDictionary
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] init];
    
    if([PWPApplicationUtility getCurrentUserDogsArray].count>1){
        [requestDictionary setValue:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"] forKey:@"dogs"];
    }
    
    [requestDictionary setValue:[SXCUtility cleanString:[SXCUtility getNSObject:@"quickCheckinDefaultMessage"]] forKey:@"message"];
    
    return requestDictionary;
}


-(void)pushSchedulePawDateViewController:(NSDictionary *)parkDictionary{
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    PWPSchedulePawDateViewController *pawDateViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPSchedulePawDateViewController"];
    pawDateViewController.selectedPark = parkDictionary.mutableCopy;
    [self.navigationController pushViewController:pawDateViewController animated:YES];
    
}

-(void)placeProfileUpdated {
    [tableViewLoadMoreDelegate automaticallyRefreshControllOnView];
    if([_delegate respondsToSelector:@selector(fetchDataForIndex:)]){
        [_delegate fetchDataForIndex:0];
    }
}

@end
