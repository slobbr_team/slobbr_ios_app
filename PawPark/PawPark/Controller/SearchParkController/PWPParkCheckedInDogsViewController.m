
//
//  PWPParkCheckedInDogsViewController.m
//  PawPark
//
//  Created by xyz on 07/06/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPParkCheckedInDogsViewController.h"
#import "PWPSearchPackCellTableViewCell.h"
#import "PWPEventInquireViewController.h"
#import "PWPApplicationUtility.h"
#import "SXCUtility.h"
#import "PWPService.h"
#import "UIViewController+UBRComponents.h"
#import <Google/Analytics.h>
#import "PWPDownloadBackgroundImage.h"
#import "PWPDogPicViewController.h"
#import "SXCUtility.h"
#import "SXCTableViewLoadMoreDelegate.h"

#define K_PARKS_SEARCH_LIMIT_SIZE @10

@interface PWPParkCheckedInDogsViewController ()<PWPSearckPackDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,SXCLoadMoreDelegate>
{
    NSInteger selectedIndex;
    NSString *fromString;
    NSString *toString;
    __weak IBOutlet UIImageView * imageViewBG;
    
    NSNumber * currentOffset;
    SXCTableViewLoadMoreDelegate * tableViewLoadMoreDelegate;

}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation PWPParkCheckedInDogsViewController

- (void)viewDidLoad {
   
    [super viewDidLoad];
    
    if(_isFromCommunity == true){
        [self sxc_navigationBarCustomizationWithTitle:@"Community Members"];
    }
    else {
        [self sxc_navigationBarCustomizationWithTitle:@"Dogs Checked-In"];
    }

    [self sxc_customizeBackButton];


    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    selectedIndex=-1;
    
    if(_placeId && _placeId.length >0) {
      
        [self initViewTableviewLoadMoreDelegate];
        [self checkedInDogs:@(0)];
    }
    else {
        
        tableViewLoadMoreDelegate=[[SXCTableViewLoadMoreDelegate alloc] initWithTableView:self.tableView WithTotalCount:@(self.dogsArray.count) WithPageSize:K_PARKS_SEARCH_LIMIT_SIZE WithPageIndex:@0];
        tableViewLoadMoreDelegate.currentArrayCount= @(self.dogsArray.count);
        tableViewLoadMoreDelegate.totalCount =@(self.dogsArray.count);
        tableViewLoadMoreDelegate.limitstart= @(0);
        tableViewLoadMoreDelegate.delegate=self;
        tableViewLoadMoreDelegate.dataSource=self;
        [tableViewLoadMoreDelegate reloadData];
        
    }
    
}


-(void)checkedInDogs:(NSNumber *)offset {
    
    currentOffset = offset;
    
    [[PWPService sharedInstance] checkedIdDogsOfParkId:_placeId withPageIndex:offset WithRequestDictionary:[self requestDictionaryForOffset:offset] withSuccessBlock:^(id responseDicitionary,NSNumber * limitStart) {
        
        if(currentOffset.integerValue ==  limitStart.integerValue){
            
            if([limitStart isEqualToNumber:@0]) {
                _dogsArray=[[NSMutableArray alloc] init];
            }
            
            NSMutableArray * tempDogsArray = [SXCUtility cleanArray:responseDicitionary];
            [_dogsArray addObjectsFromArray:tempDogsArray];


            NSNumber * totalCount = @0;
            if(_dogsArray.count==(K_PARKS_SEARCH_LIMIT_SIZE.integerValue+limitStart.integerValue)){
                totalCount=[NSNumber numberWithInteger:_dogsArray.count+1];
            }
            else{
                totalCount=[NSNumber numberWithInteger:_dogsArray.count];
            }
            
            tableViewLoadMoreDelegate.totalCount =totalCount;
            tableViewLoadMoreDelegate.limitstart= [NSNumber numberWithInteger:limitStart.integerValue];
            tableViewLoadMoreDelegate.currentArrayCount=@(_dogsArray.count);
            
            [tableViewLoadMoreDelegate reloadData];
            
        }
        [self dismissLoader];
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        [self sxc_showErrorViewWithMessage:error.domain];
        
    }];
    
}


-(NSMutableDictionary *)requestDictionaryForOffset:(NSNumber *)offset
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] init];
    [requestDictionary setValue:K_PARKS_SEARCH_LIMIT_SIZE forKey:@"limit"];
    [requestDictionary setValue:offset forKey:@"offset"];
    [requestDictionary setValue:_placeId forKey:@"place"];
    [requestDictionary setValue:[SXCUtility getNSObject:@"apikey"] forKey:@"apikey"];
    
    return requestDictionary;
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Checked In Dogs List Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)initViewTableviewLoadMoreDelegate
{
    tableViewLoadMoreDelegate=[[SXCTableViewLoadMoreDelegate alloc] initWithTableView:self.tableView WithTotalCount:@0 WithPageSize:K_PARKS_SEARCH_LIMIT_SIZE WithPageIndex:@0];
    tableViewLoadMoreDelegate.currentArrayCount=@0;
    
    tableViewLoadMoreDelegate.loadMoreDelegate=self;
    tableViewLoadMoreDelegate.delegate=self;
    tableViewLoadMoreDelegate.dataSource=self;
    [tableViewLoadMoreDelegate addPullToRefresh];
    [tableViewLoadMoreDelegate automaticallyRefreshControllOnView];
    
}


#pragma mark - UITableView Delegate and datasource

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PWPSearchPackCellTableViewCell * tableViewCell =[tableView dequeueReusableCellWithIdentifier:@"PWPMyPawsCells"];
    tableViewCell.delegate=self;
    tableViewCell.isFromSearch=YES;
    tableViewCell.isFromCheckInUser=YES;
    tableView.clipsToBounds=YES;

    [tableViewCell configureCellWithData:_dogsArray[indexPath.row]];
    
    return tableViewCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * kuttaDictionary = _dogsArray[indexPath.row];
    
    if([kuttaDictionary valueForKey:@"last_checkin"] && [[kuttaDictionary valueForKey:@"last_checkin"] isKindOfClass:[NSDictionary class]] && indexPath.row==selectedIndex)
    {
        return 208.0f;
    }
    else if(indexPath.row==selectedIndex){
        return 165.0f;
    }
    return 95.0f;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary * kuttaDictionary = _dogsArray[indexPath.row];
    
    if([kuttaDictionary valueForKey:@"last_checkin"] && [[kuttaDictionary valueForKey:@"last_checkin"] isKindOfClass:[NSDictionary class]] && indexPath.row==selectedIndex)
    {
        return 208.0f;
    }
    else if(indexPath.row==selectedIndex){
        return 165.0f;
    }
    return 95.0f;
}

-(CGFloat)getHeightOfParkBasedOnAmentis:(NSDictionary *)kuttaDictionary
{
    
    NSMutableString * checkInString=[[NSMutableString alloc] init];

    if([kuttaDictionary valueForKey:@"message"]){
        [checkInString appendFormat:@"%@\n\n",[kuttaDictionary valueForKey:@"message"]];
    }
    
    if([kuttaDictionary valueForKey:@"last_checkin"] && [[kuttaDictionary valueForKey:@"last_checkin"] isKindOfClass:[NSDictionary class]])
    {
        NSDictionary * lastCheckInDictionary = [kuttaDictionary valueForKey:@"last_checkin"];
        
        NSString * checkInTime =lastCheckInDictionary[@"created_at"];
        
        NSDateFormatter* df = [[NSDateFormatter alloc]init];
        [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        
        NSDate * date=[df dateFromString:checkInTime];
        [df setDateFormat:@"MM/dd/yyyy HH:mma"];
        
        NSString * dateString=[df stringFromDate:date];
        

        [checkInString appendFormat:@"Last check-in was at %@ %@",lastCheckInDictionary[@"place"][@"name"],dateString];

    }

    
    
    CGFloat amentiesHeight=[self getHeightOfLabelWithText:checkInString withFont:[UIFont fontWithName:@"Roboto-Light" size:12.0f]];
    
    
    if(amentiesHeight<=39){
        return 226.0;
    }
    return 226 + amentiesHeight - 39;
}

-(CGFloat)getHeightOfLabelWithText:(NSString *)text withFont:(UIFont *)font
{
    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    gettingSizeLabel.font = font;
    gettingSizeLabel.text = text;
    gettingSizeLabel.numberOfLines = 0;
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width-32, 9999);
    
    CGSize expectSize = [gettingSizeLabel sizeThatFits:maximumLabelSize];
    return expectSize.height;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row!=selectedIndex){
        selectedIndex=indexPath.row;
    }
    else{
        selectedIndex=-1;
    }
    
    [tableView reloadRowsAtIndexPaths:[tableView indexPathsForVisibleRows]
                     withRowAnimation:UITableViewRowAnimationAutomatic];
}



#pragma mark - Navigation Methods
-(void)rightMenuButtonClicked:(id)sender{
    [self performSegueWithIdentifier:@"segue_add_pawk" sender:nil];
}

#pragma mark - PWPPAWList Delegate

-(void)optionPackClickedWithData:(id)data
{
    if([PWPApplicationUtility getCurrentUserDogsArray].count==0){
        [self sxc_showErrorViewWithMessage:@"Please add a dog to your packs."];
        return;
    }
    
    [super selectDogFromPackToSendRequest:^(NSString * dogId) {
        
        [self sendFriendRequestServiceCallWithData:data fromDogId:dogId];
        
    } withCancelblock:^{
        [self sxc_showErrorViewWithMessage:@"Please select the dog from your pack to send the firend request"];
    }];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1){
        
        [[PWPService sharedInstance] cancelDogFriendsRequest:toString fromDog:fromString withSuccessBlock:^(id responseDicitionary) {
            
            [self dismissLoaderWithSuccessText:@"Request has been cancelled successfully."];
            
        } withErrorBlock:^(NSError *error) {
            
            [self dismissLoaderWithErrorText:@"An error has occurred. Please try again"];
            
        }];
    }
}

-(void)sendFriendRequestServiceCallWithData:(id)data fromDogId:(NSString *)fromDogId
{
    fromString=fromDogId;
    toString=data[@"id"];
    
    
    [self startLoader];
    
    [[PWPService sharedInstance]sendDogFriendsRequest:data[@"id"] fromDog:fromDogId withSuccessBlock:^(id responseDicitionary) {
        
        [self dismissLoaderWithSuccessText:@"Request has been sent successfully."];
        
    } withErrorBlock:^(NSError *error) {
        
        if(error.code==403){
            [self sxc_showAlertViewWithTitle:@"Error" message:@"You have already sent the friend request to this dog. Do you want to cancel the friend reqeust?" WithDelegate:self WithCancelButtonTitle:@"NO" WithAcceptButtonTitle:@"YES"];
            [self dismissLoader];
        }else{
            [self dismissLoaderWithErrorText:@"An error has occurred. Please try again"];
        }
    }];
}


-(void)option2PackClickedWithData:(id)data
{
    
}
-(void)option4PackClickedWithData:(id)data
{
    [self performSegueWithIdentifier:@"segure_inquire" sender:data];

}

-(void)option5PackClickedWithData:(id)data
{
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    PWPDogPicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPDogPicViewController"];
    viewController.dogsPics = [SXCUtility cleanArray:[data valueForKey:@"gallery"]].mutableCopy;
    viewController.data = data;
    viewController.dogId = data[@"id"];


    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"segure_inquire"]){
        PWPEventInquireViewController * eventInquireViewController = segue.destinationViewController;
        eventInquireViewController.dogIdString=sender[@"id"];
        eventInquireViewController.dogNameString=sender[@"name"];
    }

}

-(void)sxc_stopLoading {}
-(void)sxc_startLoading {}

-(void)tableView:(UITableView *)tableView loadMoreWithNextPageIndex:(NSNumber *)nextPageIndex WithPageSize:(NSNumber *)pageSize {
    [self checkedInDogs:nextPageIndex];
}

@end
