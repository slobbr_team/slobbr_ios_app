//
//  PWPParkCheckInViewController.h
//  PawPark
//
//  Created by daffolap19 on 5/23/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPParkCheckInDelegae <NSObject>

-(void)successFullCheckedInWithDictionary:(NSMutableDictionary *)data WithDogs:(NSMutableArray *)dogsArray;

@end


@interface PWPParkCheckInViewController : UIViewController

@property(nonatomic,strong)NSMutableDictionary * parkDictionary;

@property(nonatomic,weak)id<PWPParkCheckInDelegae> delegate;

@end
