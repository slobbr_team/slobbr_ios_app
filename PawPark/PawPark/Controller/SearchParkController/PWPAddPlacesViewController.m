//
//  PWPAddPlacesViewController.m
//  PawPark
//
//  Created by Chuchu on 01/09/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPAddPlacesViewController.h"
#import "UIViewController+UBRComponents.h"
#import <UIImagePickerController+BlocksKit.h>
#import "SXCTextfieldWithLeftImage.h"
#import "IQUIView+Hierarchy.h"
#import "PWPApplicationUtility.h"
#import "SXCUtility.h"
#import "PWPService.h"
#import <UIImageView+WebCache.h>
#import "PWPConstant.h"
#import <Google/Analytics.h>
#import "PWPBreedSelectionViewController.h"
#import "SZTextView.h"
#import "UIColor+PWPColor.h"
#import "PWPSelectCategoryViewController.h"
#import "MainViewController.h"
#import "UIViewController+UBRComponents.h"
#import "PWPAppDelegate.h"
#import "PWPAmenititesViewController.h"
#import "PWPDownloadBackgroundImage.h"
#import <MWPhotoBrowser.h>
#import "PWPDogImageCell.h"
#import <CTAssetsPickerController/CTAssetsPickerController.h>
#import <Mixpanel.h>
#import "PWPMapPinHelper.h"
#import <SVProgressHUD.h>

char base64EncodingTable[64] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
};



@interface PWPAddPlacesViewController ()<UIAlertViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,MWPhotoBrowserDelegate,
CTAssetsPickerControllerDelegate,PWPDogImageCellProtocoal>
{
    NSData * profilePicture;
    NSString * openTimings;
    NSString * closeTImings;
    
    NSDate * openTimingsDate;
    NSDate * closeTimingsDate;
    
    NSDictionary * amentiesDictionary;
    NSMutableArray * selectedAmenties;
    NSMutableArray * picArray;
    NSMutableArray * photos;
    NSMutableArray * thumbs;
    NSDictionary * parkDictionary;

    NSInteger numberOfPics;
    CGFloat totalProgress;
    
    __weak IBOutlet UISwitch *switchIndoors;
    __weak IBOutlet UISwitch *switchCovered;
    __weak IBOutlet UISwitch *switchTreat;
    __weak IBOutlet UISwitch *switchWater;
    __weak IBOutlet UISwitch *switchPatio;
    __weak IBOutlet UIImageView * imageViewBG;
    __weak IBOutlet NSLayoutConstraint *layoutConstraintAmentiesViewHeight;
    __weak IBOutlet NSLayoutConstraint *layoutConstraintPicHeight;

    __weak IBOutlet UICollectionView * collectionView;

}
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textfieldOperatingHours;
@end

@implementation PWPAddPlacesViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    picArray = [[NSMutableArray alloc] init];

    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    _selectedCategoryPlace = [[NSMutableDictionary alloc] init];
    [self sxc_navigationBarCustomizationWithTitle:@"ADD PLACE"];
    [self sxc_customizeBackButton];
    [self updateUI];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self fetchDataFromAmentiesPlist];
    
    _textfieldCategory.text = _selectedCategoryPlace[@"name"];
    
    if([[SXCUtility cleanString:_selectedCategoryPlace[@"name"]] isEqualToString:@"Bars & Restaurants"]){
        layoutConstraintAmentiesViewHeight.constant = 212;
    }
    else if(_selectedCategoryPlace && [SXCUtility cleanString:_selectedCategoryPlace[@"name"]].length>0 && [amentiesDictionary valueForKey:[SXCUtility cleanString:_selectedCategoryPlace[@"name"]]]){
        layoutConstraintAmentiesViewHeight.constant = 26;
    }
    else{
        layoutConstraintAmentiesViewHeight.constant = 26;
    }

    if(selectedAmenties.count > 0){
        _labelAmenties.text = [NSString stringWithFormat:@"Amenities (%lu selected)",(unsigned long)selectedAmenties.count];
    }
    else{
        _labelAmenties.text = @"Amenities";

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)submitButtonPressed:(id)sender {
    
    if([SXCUtility trimString:_txtName.text].length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter place name."];
    }
    else if([SXCUtility trimString:_txtAddress.text].length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter place address."];
    }
    else if([SXCUtility trimString:_selectedCategoryPlace[@"name"]].length==0){
        [self sxc_showErrorViewWithMessage:@"Please select place category."];
    }
    else{
        [self addlocationWithConfirmNew:false];
    }
}

-(void)updateUI
{
    selectedAmenties = [[NSMutableArray alloc] init];

    _txtAddress.text = [SXCUtility cleanString:self.formattedAddress];
    
    _txthours.backgroundColor=[UIColor clearColor];
    _txthours.textColor=[UIColor whiteColor];
    [_txthours setValue:[UIColor whiteColor]forKeyPath:@"_placeholderLabel.textColor"];
    
    _txtName.backgroundColor=[UIColor clearColor];
    _txtName.textColor=[UIColor whiteColor];
    [_txtName setValue:[UIColor whiteColor]forKeyPath:@"_placeholderLabel.textColor"];
    
    _txtAddress.backgroundColor=[UIColor clearColor];
    _txtAddress.textColor=[UIColor whiteColor];
    [_txtAddress setValue:[UIColor whiteColor]forKeyPath:@"_placeholderLabel.textColor"];
    
    _textfieldCategory.backgroundColor=[UIColor clearColor];
    _textfieldCategory.textColor=[UIColor whiteColor];
    [_textfieldCategory setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    _textfieldOperatingHours.backgroundColor=[UIColor clearColor];
    _textfieldOperatingHours.textColor=[UIColor whiteColor];
    [_textfieldOperatingHours setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    _buttonSubmit.layer.borderColor = [UIColor whiteColor].CGColor;
    _buttonSubmit.layer.borderWidth = 1.0f;
}



-(void)uploadNewPicGroup:(dispatch_group_t)group {


    for (NSDictionary * dictionary in picArray){

        UIImage * image = [PWPMapPinHelper resizeImageForUploading:[dictionary valueForKey:@"image"]];

        if (image != nil){

            dispatch_group_enter(group);

            numberOfPics += 1;
            
            NSMutableDictionary * requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
            NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];

            NSString * imageDataString =[[NSString alloc] initWithFormat:@"data:image/jpeg;base64,%@",[imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
            [requestDictionary setValue:imageDataString forKey:@"imageFile"];
            [requestDictionary removeObjectForKey:@"image"];

            [[PWPService sharedInstance] uploadNewPlaceImage:requestDictionary withDogId:parkDictionary[@"id"] withProgress:^(float progress){
                totalProgress += progress;
                [self updateProgressLoader];
            } withSuccessBlock:^(id responseDicitionary) {
                dispatch_group_leave(group);
            } withErrorBlock:^(NSError *error) {
                dispatch_group_leave(group);
            }];
        }
    }


}


-(void)addlocationWithConfirmNew:(BOOL)confirmNew
{
    [self sxc_startLoading];
    
    [[PWPService sharedInstance] placeSubmissionRequestDictionary:[self getRequestDictionaryWithConfirmNew:confirmNew] pictureData:profilePicture withSuccessBlock:^(id responseDicitionary) {

        parkDictionary = responseDicitionary;

        dispatch_group_t group = dispatch_group_create();
        
        numberOfPics = 0;
        totalProgress = 0;

        [self uploadNewPicGroup:group];

        [[Mixpanel sharedInstance] track:@"New Place Created"];

        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            [self dismissLoader];
            [self sxc_showSuccessWithMessage:@"Thanks for adding a Place.  We’ll make sure it’s live in the next 24 hours!"];
            [self.navigationController popViewControllerAnimated:YES];
        });


    } withErrorBlock:^(NSError *error) {
        
        [self dismissLoader];
        
        if(error.code == 400) {
            [self sxc_showAlertViewWithTitle:@"Alert" message:[NSString stringWithFormat:@"There's already somewhere nearby similar to %@ in our system. Are you certain this is a new one?",_txtName.text] WithDelegate:self WithCancelButtonTitle:@"Skip" WithAcceptButtonTitle:@"Confirm Add"];
        } else {
            [self sxc_showErrorViewWithMessage:@"An error has occured. Please try again."];
        }
    }];
}

-(NSMutableDictionary *)getRequestDictionaryWithConfirmNew:(BOOL)confirmNew
{
    NSMutableDictionary * dictionary=[[NSMutableDictionary alloc] init];

    if(layoutConstraintAmentiesViewHeight.constant == 212){
    
        if(switchPatio.isOn){
            [selectedAmenties addObject:@"Dogs allowed on Patio"];
        }
        
        if(switchWater.isOn){
            [selectedAmenties addObject:@"Water bowls provided"];
        }
        
        if(switchTreat.isOn){
            [selectedAmenties addObject:@"Treats Provided"];
        }
        
        if(switchCovered.isOn){
            [selectedAmenties addObject:@"Covered Outdoor Seating"];
        }
        
        if(switchIndoors.isOn){
            [selectedAmenties addObject:@"Dogs allowed Indoors"];
        }
    }
    
    [dictionary setValue:selectedAmenties forKey:@"amenities"];
    [dictionary setValue:[SXCUtility trimString:_txtName.text] forKey:@"name"];
    [dictionary setValue:[SXCUtility trimString:_txtAddress.text] forKey:@"address"];
    [dictionary setValue:[NSNumber numberWithFloat:_lat] forKey:@"lat"];
    [dictionary setValue:[NSNumber numberWithFloat:_longi] forKey:@"lng"];
    [dictionary setValue:[SXCUtility trimString:_textfieldOperatingHours.text] forKey:@"operatingHours"];
    [dictionary setValue:[SXCUtility trimString:_selectedCategoryPlace[@"name"] ]forKey:@"category"];
    [dictionary setValue:self.city  forKey:@"addressLocality"];
    [dictionary setValue:self.state forKey:@"addressRegion"];
    [dictionary setValue:self.zipcode forKey:@"addressPostalCode"];
    [dictionary setValue:[NSNumber numberWithBool:confirmNew] forKey:@"confirmNew"];

    NSString *str = [self base64StringFromData:profilePicture length:[profilePicture length]];
    [dictionary setValue:str forKey:@"imageFile"];

    return dictionary;
}


- (NSString *) base64StringFromData: (NSData *)data length: (NSInteger)length {
    unsigned long ixtext, lentext;
    long ctremaining;
    unsigned char input[3], output[4];
    short i, charsonline = 0, ctcopy;
    const unsigned char *raw;
    NSMutableString *result;
    
    lentext = [data length];
    if (lentext < 1)
        return @"";
    result = [NSMutableString stringWithCapacity: lentext];
    raw = [data bytes];
    ixtext = 0;
    
    while (true) {
        ctremaining = lentext - ixtext;
        if (ctremaining <= 0)
            break;
        for (i = 0; i < 3; i++) {
            unsigned long ix = ixtext + i;
            if (ix < lentext)
                input[i] = raw[ix];
            else
                input[i] = 0;
        }
        output[0] = (input[0] & 0xFC) >> 2;
        output[1] = ((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4);
        output[2] = ((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6);
        output[3] = input[2] & 0x3F;
        ctcopy = 4;
        switch (ctremaining) {
            case 1:
                ctcopy = 2;
                break;
            case 2:
                ctcopy = 3;
                break;
        }
        
        for (i = 0; i < ctcopy; i++)
            [result appendString: [NSString stringWithFormat: @"%c", base64EncodingTable[output[i]]]];
        
        for (i = ctcopy; i < 4; i++)
            [result appendString: @"="];
        
        ixtext += 3;
        charsonline += 4;
        
        if ((length > 0) && (charsonline >= length))
            charsonline = 0;
    }
    return result;
}


- (IBAction)selectOperatingHoursClicked:(id)sender {

    [self sxc_showTimeActionSheetWithTitle:@"Select Open Time" pickerWithRows:nil dateSelected:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        openTimings = selectedDate;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm a"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
        openTimings=[dateFormatter stringFromDate:selectedDate];
        [self closeTimingPicker];
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

-(void)closeTimingPicker{

    [self sxc_showTimeActionSheetWithTitle:@"Select Close Time" pickerWithRows:nil dateSelected:openTimingsDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm a"];
        [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
        
        closeTImings=[dateFormatter stringFromDate:selectedDate];
        
        _textfieldOperatingHours.text = [NSString stringWithFormat:@"%@ - %@",openTimings,closeTImings].lowercaseString;
        
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];

}



- (IBAction)selectCategoryAction:(id)sender {
    
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    PWPSelectCategoryViewController * categoryViewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPSelectCategoryViewController"];
    categoryViewController.selectedCategory = _selectedCategoryPlace;
    categoryViewController.selectedAmenties = selectedAmenties;

    [self.navigationController pushViewController:categoryViewController animated:YES];
    
}

- (IBAction)selectAmentiesClicked:(id)sender {
    
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    PWPAmenititesViewController * categoryViewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPAmenititesViewController"];
    categoryViewController.selectedAmenities = selectedAmenties;
    categoryViewController.amenities = [SXCUtility cleanArray:[amentiesDictionary valueForKey:[[SXCUtility cleanString:_selectedCategoryPlace[@"name"]] capitalizedString]]].mutableCopy;
    categoryViewController.categoryName = [_selectedCategoryPlace[@"name"] capitalizedString];
    
    [self.navigationController pushViewController:categoryViewController animated:YES];
    
}


-(void)fetchDataFromAmentiesPlist{

    NSDictionary *dictRoot = [SXCUtility cleanDictionary:[SXCUtility getNSObject:@"Amenities"]];

    amentiesDictionary =  dictRoot;

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1){
        [self addlocationWithConfirmNew:true];
    }
 }

#pragma mark - Loading Methods
-(void)sxc_stopLoading
{
    [self dismissLoader];
}
-(void)sxc_startLoading
{
    [self startLoader];
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return picArray.count + 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;

    return CGSizeMake((screenWidth-32)/3,(screenWidth-32)/3);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)theCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == picArray.count){
        UICollectionViewCell * cell = [theCollectionView dequeueReusableCellWithReuseIdentifier:@"PWPDogImageAddCell" forIndexPath:indexPath];
        return cell;
    }
    PWPDogImageCell * collectionViewCell = [theCollectionView dequeueReusableCellWithReuseIdentifier:@"PWPDogImageCell" forIndexPath:indexPath];
    [collectionViewCell configureViewWithDelegate:self WithIndexPath:indexPath];

    UIImage * image = [picArray[indexPath.row] valueForKey:@"image"];
    if (image != nil){
        [collectionViewCell.imageViewDog setImage:image];
    }else {
        NSString * dogUrlString = [SXCUtility cleanString:[[picArray[indexPath.row] valueForKey:@"thumbs"] valueForKey:@"w400"]];
        [collectionViewCell.imageViewDog sd_setImageWithURL:[NSURL URLWithString:dogUrlString] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
    }

    return collectionViewCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row == picArray.count){
        [self addPlacePic];
        return;
    }

    photos = [[NSMutableArray alloc] init];
    thumbs = [[NSMutableArray alloc] init];

    for (NSDictionary * dog in picArray) {

        if([dog valueForKey:@"image"]){
            [photos addObject:[MWPhoto photoWithImage:[dog valueForKey:@"image"]]];
            [thumbs addObject:[MWPhoto photoWithImage:[dog valueForKey:@"image"]]];
        }
        else {

        }
    }

    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = YES;
    browser.displayNavArrows = YES;
    browser.displaySelectionButtons = NO;
    browser.alwaysShowControls = YES;
    browser.zoomPhotosToFill = YES;
    browser.enableGrid = NO;
    browser.startOnGrid = NO;
    browser.enableSwipeToDismiss = YES;
    browser.autoPlayOnAppear = NO;
    [browser setCurrentPhotoIndex:indexPath.row];

    UINavigationController * controller = [[UINavigationController alloc] initWithRootViewController:browser];

    [self presentViewController:controller animated:YES completion:^{
        
    }];
    
}

-(void)addPlacePic
{

    void (^picPhotosFromGallery)(void) = ^void() {

        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status){
            dispatch_async(dispatch_get_main_queue(), ^{

                CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];

                picker.delegate = self;

                [self presentViewController:picker animated:YES completion:nil];
            });
        }];

    };
    void (^picPhotosFromCamera)(void) = ^void() {

        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePicker setAllowsEditing:YES];

        [imagePicker setBk_didCancelBlock:^(UIImagePickerController * imagePickerController) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];

        [imagePicker setBk_didFinishPickingMediaBlock:^(UIImagePickerController * controller, NSDictionary *info) {

            UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];

            [self createNewPlacePic:image];

            [self dismissViewControllerAnimated:YES completion:nil];

            [collectionView reloadData];

            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenWidth = screenRect.size.width;

            int roundedUp = ceil((picArray.count + 1)/3.0f);
            layoutConstraintPicHeight.constant = screenWidth / 3 * roundedUp;

        }];

        [self presentViewController:imagePicker animated:YES completion:nil];

    };


    if (TARGET_IPHONE_SIMULATOR) {
        [self sxc_showActionSheetWithActionTitle:nil optionTitle:@[@"Choose"] WithActionArray:@[[picPhotosFromGallery copy]]];
    }else{
        [self sxc_showActionSheetWithActionTitle:nil optionTitle:@[@"Take Photo",@"Choose"] WithActionArray:@[[picPhotosFromCamera copy],[picPhotosFromGallery copy]]];
    }

}

-(void)createNewPlacePic:(UIImage *)image{

    NSMutableDictionary * dictionary =[[NSMutableDictionary alloc] init];
    dictionary[@"description"] = @"Upload New image dog pciture from mobile app";
    dictionary[@"imageInfo"] = @[@{
                                     @"key":@"License",@"value":@"Mobile User"
                                     },
                                 @{
                                     @"key":@"by",@"value":@"Mobile User"
                                     
                                     }];
    dictionary[@"image"] = image;
    
    [picArray addObject:dictionary];
    
}


-(void)imageDeleteClicked:(NSIndexPath *)indexPath
{
    [picArray removeObjectAtIndex:indexPath.row];
    [collectionView reloadData];

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;

    int roundedUp = ceil((picArray.count + 1)/3.0f);
    layoutConstraintPicHeight.constant = screenWidth / 3 * roundedUp;

}


#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < thumbs.count)
        return [thumbs objectAtIndex:index];
    return nil;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
}

- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index {
    return false;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected {
    NSLog(@"Photo at index %lu selected %@", (unsigned long)index, selected ? @"YES" : @"NO");
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets {

    PHImageRequestOptions * requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    requestOptions.synchronous = true;

    PHImageManager *manager = [PHImageManager defaultManager];

    for (PHAsset *asset in assets) {

        [manager requestImageForAsset:asset
                           targetSize:PHImageManagerMaximumSize
                          contentMode:PHImageContentModeDefault
                              options:requestOptions
                        resultHandler:^void(UIImage *image, NSDictionary *info) {
                            [self createNewPlacePic:image];
                        }];

    }

    [collectionView reloadData];

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;

    int roundedUp = ceil((picArray.count + 1)/3.0f);
    layoutConstraintPicHeight.constant = screenWidth / 3 * roundedUp;

    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)updateProgressLoader {

    if (numberOfPics == 1) {
        NSString * uploadingText = [NSString stringWithFormat:@"Uploading\n(%.0f%%)...",totalProgress*1.0f/numberOfPics*100];
        [[[PWPAppDelegate sharedInstance] window] setUserInteractionEnabled:NO];
        [SVProgressHUD showProgress:totalProgress*1.0f/numberOfPics status:uploadingText];
    } else {
        
        NSInteger uploadedImageNumber = totalProgress;
        if(uploadedImageNumber >= numberOfPics) {
            uploadedImageNumber  = numberOfPics-1;
        }
        
        NSInteger totalProgressInInt = totalProgress;
        CGFloat percentage = (totalProgress - totalProgressInInt)*100;
        
        NSString * uploadingText = [NSString stringWithFormat:@"Uploading\n%.0f%%\n(%.0ld/%ld)",percentage,(long)uploadedImageNumber+1,(long)numberOfPics];
        [[[PWPAppDelegate sharedInstance] window] setUserInteractionEnabled:NO];
        [SVProgressHUD showProgress:totalProgress*1.0f/numberOfPics status:uploadingText];
    }

}

@end
