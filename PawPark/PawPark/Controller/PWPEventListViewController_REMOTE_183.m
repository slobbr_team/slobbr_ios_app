//
//  PWPEventListViewController.m
//  PawPark
//
//  Created by HupendraRaghuwanshi on 07/03/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//


//
//@interface PWPEventListViewController ()
//
//@end
//
//@implementation PWPEventListViewController



#import "PWPEventListViewController.h"
#import "PWPEventListingTableViewCell.h"
#import "UIViewController+UBRComponents.h"
#import "SXCUtility.h"
#import "PWPService.h"
#import "PWPApplicationUtility.h"
#import "PWPSearchParkMapViewController.h"
#import "PWPSearchParkViewController.h"
#import "PWPSearchParkFilterViewController.h"
#import "UBRLocationHandler.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPMapPinHelper.h"
#import "PWPEventTableViewCell.h"
#import "PWPEventListDetailsViewController.h"
#import "PWPEventFilterDataViewController.h"



@interface PWPEventListViewController () <UITableViewDelegate,UITableViewDataSource,PWPEventListFilterDelegate>
{
    __weak IBOutlet UIImageView * imageViewBG;
     NSMutableArray * currentUserPacks;
     NSString *selectedString;
     BOOL useLocation;
     NSMutableSet * errorSet;
}





@end

@implementation PWPEventListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _allEvents = [[NSMutableDictionary alloc] init];
    _localEvents = [[NSMutableDictionary alloc] init];
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    [self sxc_navigationBarCustomizationWithTitle:@"EVENTS "];
    if(self.navigationController.viewControllers.count==1){
        [self sxc_setNavigationBarMenuItem];
    }
    else{
        [self sxc_customizeBackButton];
    }
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    selectedString = @"";
    [self hideLoader];
    [self setHeaderLabel:@"Showing all events"];
}


-(void)viewWillAppear:(BOOL)animated
{
    useLocation = false;
    [_locationButton setHidden:true];
    [super viewWillAppear:animated];
    if (_segmentControl.selectedSegmentIndex == 0){
       [self EventList:_allEvents];
    }else if (_segmentControl.selectedSegmentIndex == 1){
       [self EventList:_localEvents];
    }
    errorSet = [[NSMutableSet alloc] init];
}




-(void)setHeaderLabel:(NSString*)title{
    _labelTopFilterInformation.text = title;
}


#pragma mark - Services Method
-(void)EventList:(NSMutableDictionary*)dict{

    [self startLoader];
    [[PWPService sharedInstance] currentUserEventWithRequestDictionary:dict withSuccessBlock:^(id responseDicitionary) {
        
        [PWPApplicationUtility saveCurrentEventListDictionary:responseDicitionary];
        currentUserPacks=[[NSMutableArray alloc] initWithArray:[PWPApplicationUtility getCurrentUserEventList]];
        [self.tableView reloadData];
        
        [self dismissLoader];
      
        
    } withErrorBlock:^(NSError *error) {
        [errorSet addObject:error.domain];
        if(errorSet.allObjects.count > 0 ){
            NSString * errorString = [errorSet.allObjects componentsJoinedByString:@"\n"];
            [self sxc_showErrorViewWithMessage:errorString];
            return;
        }
        [self dismissLoader];
       
    }];
    
}



-(void)selectedFilter:(NSMutableDictionary *)filterDictionary{
    _allEvents = [[NSMutableDictionary alloc] initWithDictionary:filterDictionary];
    selectedString = _allEvents[@"selectedString"];
    [self setHeaderLabel:[NSString stringWithFormat:@"Showing Events within %@",selectedString]];
    [_allEvents removeObjectForKey:@"selectedString"];
}




-(IBAction)filterClicked:(id)sender{
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PWPEventFilterDataViewController * parkFilterViewController=[storyBoard instantiateViewControllerWithIdentifier:@"PWPEventFilterDataViewController"];
    parkFilterViewController.delegate = self;
    [self.navigationController pushViewController:parkFilterViewController animated:YES];
    _allEvents[@"selectedString"] = selectedString;
    parkFilterViewController.parkFilterDictionary = _allEvents;
}



- (IBAction)locationDidClicked:(id)sender {
    useLocation = true;
    [_localEvents removeObjectForKey:@"zipcode"];
    _localEvents[@"lat"]=[NSNumber numberWithDouble:[UBRLocationHandler shareHandler].currentLocation.coordinate.latitude];
    _localEvents[@"lng"]=[NSNumber numberWithDouble:[UBRLocationHandler shareHandler].currentLocation.coordinate.longitude];
    _localEvents[@"distance"] = @"30";
    [self setHeaderLabel:@"Showing Events within a 30 mile radius of 'current' location"];
    [self EventList:_localEvents];
}



#pragma mark- TableView Delegate Methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PWPEventListingTableViewCell * tableViewCell =[tableView dequeueReusableCellWithIdentifier:@"PWPEventListingTableViewCell"];
    tableViewCell.buttonDetails.tag = indexPath.row;
    [tableViewCell.buttonDetails addTarget:self action:@selector(detailsButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    tableViewCell.buttonDirection.tag = indexPath.row;
    [tableViewCell.buttonDirection addTarget:self action:@selector(navigateButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [tableViewCell configureCellWithData:[currentUserPacks objectAtIndex:[indexPath row]]];
    
  return tableViewCell;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [currentUserPacks count];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PWPEventListDetailsViewController * eventDetailsViewContoller=[self.storyboard instantiateViewControllerWithIdentifier:@"PWPEventListDetailsViewController"];
    eventDetailsViewContoller.eventDictionary = [currentUserPacks objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:eventDetailsViewContoller animated:YES];
}

-(void)detailsButtonTapped:(id)sender{
    int tag = (int)[sender tag];
    PWPEventListDetailsViewController * eventDetailsViewContoller=[self.storyboard instantiateViewControllerWithIdentifier:@"PWPEventListDetailsViewController"];
    eventDetailsViewContoller.eventDictionary = [currentUserPacks objectAtIndex:tag];
    [self.navigationController pushViewController:eventDetailsViewContoller animated:YES];
}


-(void)navigateButtonTapped:(id)sender{
    int tag = (int)[sender tag];
    NSDictionary *event = [currentUserPacks objectAtIndex:tag];
    NSString *dest_lat = event[@"lat"];
    NSString *dest_longi = event[@"lng"];
    
    NSString *current_lat = [NSString stringWithFormat:@"%f",[UBRLocationHandler shareHandler].currentLocation.coordinate.latitude];
    NSString *current_long = [NSString stringWithFormat:@"%f",[UBRLocationHandler shareHandler].currentLocation.coordinate.longitude];
    
    NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%@,%@&daddr=%@,%@",current_lat, current_long, dest_lat, dest_longi];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: directionsURL]];
}


- (IBAction)segmentValueChanges:(id)sender {
    UISegmentedControl *control = (UISegmentedControl*)sender;

    if (control.selectedSegmentIndex == 0 ) {
        
        [_locationButton setHidden:true];
        [_filterButton setEnabled:true];
        if( [selectedString  isEqualToString: @""] || selectedString == nil ){
            [self setHeaderLabel:@"Showing all events"];
        }else{
          [self setHeaderLabel:[NSString stringWithFormat:@"Showing Events within %@",selectedString]];
        }
        [self EventList:_allEvents];
        
    }else if (control.selectedSegmentIndex == 1) {
        
        [_locationButton setHidden:false];
        [_filterButton setEnabled:false];
        if (useLocation == false) {
        NSString *zipcode = [PWPApplicationUtility getCurrentUserProfileDictionary][@"zipcode"];
        _localEvents[@"zipcode"] = zipcode ;
        _localEvents[@"distance"] = @"30";
        [self setHeaderLabel:[NSString stringWithFormat:@"Showing Events within a 30 mile radius of '%@' location",zipcode]];
            
        }else {
        _localEvents[@"lat"]=[NSNumber numberWithDouble:[UBRLocationHandler shareHandler].currentLocation.coordinate.latitude];
        _localEvents[@"lng"]=[NSNumber numberWithDouble:[UBRLocationHandler shareHandler].currentLocation.coordinate.longitude];
         _localEvents[@"distance"] = @"30";
         [self setHeaderLabel:@"Showing Events within a 30 mile radius of 'current' location"];
        }
        [self EventList:_localEvents];
    }
}


-(void)showLoader{
    [self.loader startAnimating];
    [self.loader setHidden:NO];
}


-(void)hideLoader{
    [self.loader stopAnimating];
    [self.loader setHidden:YES];
}



-(void)sxc_startLoading{}
-(void)sxc_stopLoading{}



@end
