//
//  PWPSideMenuTableViewController.h
//  PawPark
//
//  Created by daffomac on 4/24/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPSideMenuTableViewController : UITableViewController

@property(nonatomic,strong)UINavigationController * homeNavigationController;

@end
