//
//  PWPSideMenuTableViewController.m
//  PawPark
//
//  Created by daffomac on 4/24/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPSideMenuTableViewController.h"
#import "PWPNotificationViewController.h"
#import "PWPSchedulePawDateViewController.h"
#import "PWPService.h"
#import "UIViewController+UBRComponents.h"
#import "PWPAppDelegate.h"
#import "PWPService.h"
#import "SXCUtility.h"
#import <TwitterKit/TwitterKit.h>
#import "MainViewController.h"
#import "UIColor+PWPColor.h"
#import "PWPPrivacyPolicy.h"
#import "PWPDownloadBackgroundImage.h"
#import <UIButton+WebCache.h>
#import "PWPConstant.h"
#import <Mixpanel.h>

@interface PWPSideMenuTableViewController ()<UITableViewDelegate>{
    __weak IBOutlet UILabel * labelNotificationCount;
    UIButton * addButton;
}

@property(nonatomic,strong)UINavigationController * settingViewController;
@end

@implementation PWPSideMenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    labelNotificationCount.hidden=YES;
    
    labelNotificationCount.layer.cornerRadius=15.0f;
    labelNotificationCount.clipsToBounds=YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventListenerDidReceiveNotification:) name:@"NotificationCount" object:nil];

}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)eventListenerDidReceiveNotification:(NSNotification *)notif
{
    NSInteger notificationCount = ((NSNumber *)[SXCUtility getNSObject:@"NotificationCount"]).integerValue;
    if(notificationCount == 0){
        labelNotificationCount.hidden=YES;
    }
    else{
        labelNotificationCount.text=[NSString stringWithFormat:@"%ld",(long)((NSNumber *)[SXCUtility getNSObject:@"NotificationCount"]).integerValue];
        labelNotificationCount.hidden=NO;

    }
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{

    UIView *customColorView = [[UIView alloc] init];
    customColorView.backgroundColor = [UIColor sxc_themeColorWithAplha];
    cell.selectedBackgroundView =  customColorView;

}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
    if(indexPath.row==1){
        [[Mixpanel sharedInstance] track:@"Home Click Slide Menu"];
        [self pushHomeViewController];
    }
    else if(indexPath.row==2){
        [[Mixpanel sharedInstance] track:@"Search Park Click Slide Menu"];
        [self pushSearchParkViewController];
    }
    else if(indexPath.row ==3){
        [[Mixpanel sharedInstance] track:@"Video Click Slide Menu"];
        [self pushFavoriteVideoController];
    }
    else if(indexPath.row==4){
        [[Mixpanel sharedInstance] track:@"Search Park Click Slide Menu"];
        [self pushSearchPacksViewController];
    }
    else if(indexPath.row==5){
        [[Mixpanel sharedInstance] track:@"Profile Click Slide Menu"];
        [self pushMyPawViewController];
    }
    else if(indexPath.row==6){
        [[Mixpanel sharedInstance] track:@"Event Click Slide Menu"];
        [self pushToEventsViewController];
    }
    else if(indexPath.row==7){
        [[Mixpanel sharedInstance] track:@"Notification Click Slide Menu"];
        [self pushNotificationViewController];
    }
    else if(indexPath.row==8){
        [[Mixpanel sharedInstance] track:@"Settings Click Slide Menu"];
        [self pushSettingViewController];
    }
    else if(indexPath.row==9){
        [[Mixpanel sharedInstance] track:@"Privacy Policy Click Slide Menu"];
        [self privacyPolicyViewController];
    }
    else if(indexPath.row==10){
        [[Mixpanel sharedInstance] track:@"Logout Click Slide Menu"];
        [self logout];
    }
}

-(void)pushFavoriteVideoController{

//PWPFavoritePlaceViewController

    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    
    UIViewController *  settingsViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPFavoritePlaceViewController"];
    
    UINavigationController * navigationController =[[UINavigationController alloc] initWithRootViewController:settingsViewController];
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
    [kMainViewController presentViewController:navigationController animated:YES completion:nil];
    
}


-(void)pushHomeViewController{
    
    ((UITabBarController *)kTabViewController).selectedIndex = 0;
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
}


-(void)pushSettingViewController{

    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];
    
    UIViewController *  settingsViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPSettingViewController"];
    
    UINavigationController * navigationController =[[UINavigationController alloc] initWithRootViewController:settingsViewController];
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
    [kMainViewController presentViewController:navigationController animated:YES completion:nil];
}


-(void)pushSearchParkViewController{
    
    ((UITabBarController *)kTabViewController).selectedIndex = 1;
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];

    
}

-(void)privacyPolicyViewController{
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *  settingsViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPPrivacyPolicy"];
    
    UINavigationController * navigationController =[[UINavigationController alloc] initWithRootViewController:settingsViewController];
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
    [kMainViewController presentViewController:navigationController animated:YES completion:nil];
}


-(void)pushVideoViewController{
    
    ((UITabBarController *)kTabViewController).selectedIndex = 3
    ;
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
    
    
}

-(void)pushNotificationViewController{
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController * notificationViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPNotificationViewController"];
    
    UINavigationController * navigationController =[[UINavigationController alloc] initWithRootViewController:notificationViewController];
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
    [kMainViewController presentViewController:navigationController animated:YES completion:nil];
}

-(void)pushMessaheNavigationController {
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UINavigationController * notificationViewController=[storyboard instantiateViewControllerWithIdentifier:@"MessageNavigationController"];
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
    [kMainViewController presentViewController:notificationViewController animated:YES completion:nil];

}


-(void)pushMyPawViewController{

    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UINavigationController * pawsListViewController=[storyBoard instantiateViewControllerWithIdentifier:@"PWPPawsListViewController"];
    
    UINavigationController * navigationController =[[UINavigationController alloc] initWithRootViewController:pawsListViewController];
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
    [kMainViewController presentViewController:navigationController animated:YES completion:nil];
    
    /*((UITabBarController *)kTabViewController).selectedIndex = 3
    ;
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
     */

}


-(void)pushToEventsViewController{
    ((UITabBarController *)kTabViewController).selectedIndex = 3
    ;
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
}


-(void)pushSearchPacksViewController{
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *pawsSearchViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPSearchPacksViewController"];
    
    UINavigationController * navigationController =[[UINavigationController alloc] initWithRootViewController:pawsSearchViewController];

    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
    [kMainViewController  presentViewController:navigationController animated:YES completion:nil];
    
}


-(void)pushSchedulePawDateViewController{
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *pawDateViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPSchedulePawDateViewController"];
    
    UINavigationController * navigationController =[[UINavigationController alloc] initWithRootViewController:pawDateViewController];
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
    [kMainViewController  presentViewController:navigationController animated:YES completion:nil];
    
}

-(void)logout{
    [self startLoader];
    [[PWPService sharedInstance] logoutWithSuccessBlock:^(id response) {
        
        [self dismissLoader];

        [[PWPAppDelegate sharedInstance] switchToLoginViewController];

        [SXCUtility removeObject:@"apikey"];
        [SXCUtility removeObject:@"Alert_Parks"];
        [SXCUtility removeObject:@"pwp_user_profile"];
        [SXCUtility removeObject:@"pwp_user_dogs"];
        [SXCUtility removeObject:@"pwp_event_list"];
        [SXCUtility removeObject:@"park_checkin_data"];
        [SXCUtility removeObject:@"NotificationCount"];
        [SXCUtility removeObject:@"eventMode"];
        [SXCUtility removeObject:@"setting_dirty"];

        [[Twitter sharedInstance] logOut];
    
    
    } withErrorBlock:^(NSError *error) {

        [[PWPAppDelegate sharedInstance] switchToLoginViewController];
        
        [SXCUtility removeObject:@"apikey"];
        [SXCUtility removeObject:@"Alert_Parks"];
        [SXCUtility removeObject:@"pwp_user_profile"];
        [SXCUtility removeObject:@"pwp_user_dogs"];
        [SXCUtility removeObject:@"pwp_event_list"];
        [SXCUtility removeObject:@"park_checkin_data"];
        [SXCUtility removeObject:@"NotificationCount"];
        [SXCUtility removeObject:@"eventMode"];
        [SXCUtility removeObject:@"setting_dirty"];
        
        [[Twitter sharedInstance] logOut];
    }];

}
-(void)sxc_startLoading{}
-(void)sxc_stopLoading{}

@end
