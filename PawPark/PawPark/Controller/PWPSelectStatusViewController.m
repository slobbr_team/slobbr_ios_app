//
//  PWPSelectStatusViewController.m
//  PawPark
//
//  Created by Vibhore Sharma on 28/07/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPSelectStatusViewController.h"
#import "UIViewController+UBRComponents.h"
#import "SXCUtility.h"
#import "PWPApplicationUtility.h"
#import "UIColor+PWPColor.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPSelectStatusViewController ()<UITableViewDelegate,UITableViewDataSource, UIAlertViewDelegate> {
    __weak IBOutlet UIImageView * imageViewBG;
    __weak IBOutlet UITableView * tableViewStatus;
}

@property(nonatomic,strong)NSMutableArray * statuses;

@end

@implementation PWPSelectStatusViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self customizeBackButton];
    [self sxc_customizeDoneButton];
    [self sxc_navigationBarCustomizationWithTitle:@"SELECT STATUS"];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    
    NSArray * array = [SXCUtility cleanArray:[SXCUtility getNSObject:@"status_array"]];
    if(array.count == 0){
        
        _statuses=@[@"I just helped feed a dog in need!",
                    @"Time for a pint!",
                    @"Out for an adventure!",
                    @"Time for a spa day!",@"Other"].mutableCopy;
        [SXCUtility saveNSObject:@[@"I just helped feed a dog in need!",
                                   @"Time for a pint!",
                                   @"Out for an adventure!",
                                   @"Time for a spa day!"] forKey:@"status_array"];
    }
    else {
        _statuses  = [[NSMutableArray alloc] initWithArray:array];
        [_statuses addObject:@"Other"];
    }

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)sxc_customizeDoneButton {
    
    UIButton *doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    UIFont *font = [UIFont fontWithName:@"Roboto-Regular" size:17.0f];
    doneButton.titleLabel.font = font;
    doneButton.frame = CGRectMake(0, 0 ,50, 30);
    UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
    [doneButton addTarget:self
                   action:@selector(sxc_doneButtonClicked:)
         forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = doneBarButtonItem;
    
}

-(void)sxc_doneButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)customizeBackButton {
    
    if(self.navigationController.viewControllers.count>1){
        self.navigationItem.hidesBackButton = YES;
        [self.navigationItem setHidesBackButton:YES animated:YES];
        
        UIImage *backButtonImage = [UIImage imageNamed:(@"btn_back")];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [backButton setImage:backButtonImage forState:UIControlStateNormal];
        backButton.frame = CGRectMake(0, 0 ,23, 23);
        
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        [backButton addTarget:self
                       action:@selector(backButtonClicked:)
             forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = backBarButtonItem;
    }
}

-(void)backButtonClicked:(id)sender {
    [_selectedStatus setString:@""];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _statuses.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"park_checkin_status" forIndexPath:indexPath];
    cell.textLabel.text=_statuses[indexPath.row];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor =[UIColor clearColor];
    cell.contentView.backgroundColor =[UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.font =[UIFont fontWithName:@"Roboto-Regular" size:20.0f];
    
    UIView *customColorView = [[UIView alloc] init];
    customColorView.backgroundColor = [UIColor sxc_themeColorWithAplha];
    cell.selectedBackgroundView =  customColorView;
    
    if([_selectedStatus isEqualToString:_statuses[indexPath.row]]) {
        cell.accessoryType=UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType=UITableViewCellAccessoryNone;
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryCheckmark) {
        [_selectedStatus setString:@""];
    }
    else if([_statuses[indexPath.row] isEqualToString:@"Other"]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Status!" message:@"Please enter your personal status" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alertView show];
    }
    else{
        [_selectedStatus setString:_statuses[indexPath.row]];
    }
    [tableView reloadRowsAtIndexPaths:tableView.indexPathsForVisibleRows withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        UITextField *emailTextField = [alertView textFieldAtIndex:0];
        NSString * otherStatusString = emailTextField.text;
        [_statuses insertObject:otherStatusString atIndex:_statuses.count - 1];
        [_selectedStatus setString:otherStatusString];
        [tableViewStatus reloadData];
        
        NSMutableArray * tempStatuses = [[NSMutableArray alloc] initWithArray:_statuses];
        [tempStatuses removeLastObject];
        [SXCUtility saveNSObject:tempStatuses forKey:@"status_array"];
    }
}

@end
