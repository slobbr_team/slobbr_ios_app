//
//  PWPSchedulePawDateViewController.m
//  PawPark
//
//  Created by daffomac on 4/26/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPSchedulePawDateViewController.h"
#import "UIViewController+UBRComponents.h"
#import "SXCUtility.h"
#import "SXCTextfieldWithLeftImage.h"
#import "PWPApplicationUtility.h"
#import "PWPService.h"
#import "PWPAddDogViewController.h"
#import <Google/Analytics.h>
#import "PWPParkSelectionViewController.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPSchedulePawDateViewController ()<PWPAddDogViewDelegate,PWPParkSelectionDelegate>{
    
    __weak IBOutlet UIButton *buttonAdvertisment;
    NSArray * userDogsArray;
    NSArray * selectedDogFriendssArray;
    NSMutableDictionary * selectedDogFriend;
    NSMutableDictionary * selectedDog;
    
    NSString * dateString;
    
    __weak IBOutlet UIView *viewContent;
    __weak IBOutlet UIImageView *imageViewInvite;
    __weak IBOutlet UIImageView * imageViewBG;
}
@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;
@property (weak, nonatomic) IBOutlet UIButton *buttonSubmit;
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textfieldWhoInitiating;
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textfieldWhoInvited;
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textfieldDate;

- (IBAction)buttonWhoIntiatingClicked:(id)sender;
- (IBAction)buttonWhoInvitedClicked:(id)sender;
- (IBAction)buttonDateTimeClicked:(id)sender;
- (IBAction)submitButtonClicked:(id)sender;

@end

@implementation PWPSchedulePawDateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];

    UIImage * filterImage = [PWPDownloadBackgroundImage filterBg];
    if(filterImage != nil){
        imageViewInvite.image =  filterImage;
    }
    else {
        imageViewInvite.image =  [UIImage imageNamed:@"img_play_date"];
    }

    [self initView];
    [self dogs];
    
    NSString * string = [SXCUtility cleanString:[SXCUtility getNSObject:@"filter_link"]];
    
    if(string.length > 0){
        [buttonAdvertisment addTarget:self action:@selector(advertisementClicked:) forControlEvents:UIControlEventTouchUpInside];
        buttonAdvertisment.userInteractionEnabled = TRUE;
    }
    
    else {
        buttonAdvertisment.userInteractionEnabled = FALSE;
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Schedule PlayDate Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}


-(IBAction)advertisementClicked:(id)sender {
    
    if([PWPDownloadBackgroundImage isSkinApplied] == false){
        return;
    }
    
    
    NSString * string = [SXCUtility cleanString:[SXCUtility getNSObject:@"filter_link"]];
    
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:string]]) {
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Filter Link"
                                                              action:@"Button press"
                                                               label:@"Clicked"
                                                               value:nil] build]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
    }
}



-(void)reloadDogs{
    [self dogs];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Buisness Methods

-(void)initView{
    
    viewContent.layer.cornerRadius = 5.0f;
    _buttonCancel.layer.cornerRadius = 5.0f;
    _buttonSubmit.layer.cornerRadius = 5.0f;
    
    [self sxc_customizeBackButton];
    
    [self sxc_navigationBarCustomizationWithTitle:@"SLOBBR INVITE"];
    
}


- (IBAction)buttonWhoIntiatingClicked:(id)sender {
    
    if(userDogsArray == nil || userDogsArray.count ==0 ){
        return;
    }
    
    
    [self sxc_showActionSheetWithTitle:@"Who's Initiating?" pickerWithRows:[userDogsArray valueForKeyPath:@"name"] itemSelectedAtIndex:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        _textfieldWhoInitiating.text=selectedValue;
        _textfieldWhoInvited.text=@"";
        
        if(userDogsArray.count>selectedIndex){
    
            selectedDogFriendssArray=[SXCUtility cleanArray:[userDogsArray[selectedIndex] valueForKey:@"friends"] ];
            
            selectedDog=userDogsArray[selectedIndex];
            
            selectedDogFriend=nil;
        }

    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
    
}
- (IBAction)cancelBtnClicked:(id)sender {
    
   
    [self.navigationController popViewControllerAnimated:YES];
   
}

- (IBAction)buttonWhoInvitedClicked:(id)sender {
    
    if(selectedDogFriendssArray==nil){
        [self sxc_showErrorViewWithMessage:@"Please select dog from your pack."];
    }
    else if(selectedDogFriendssArray.count==0){
        [self sxc_showErrorViewWithMessage:@"Your dog doesn’t currently have any friends.  Go to Pals to meet some pups!"];
    }
    else
    {
        [self sxc_showActionSheetWithTitle:@"Who's invited?" pickerWithRows:[selectedDogFriendssArray valueForKeyPath:@"name"] itemSelectedAtIndex:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            
            _textfieldWhoInvited.text=selectedValue;
            
            selectedDogFriend=selectedDogFriendssArray[selectedIndex];
            
        } cancelBlock:^(ActionSheetStringPicker *picker) {
            
        } origin:self.view];
    }
    
}

- (IBAction)buttonDateTimeClicked:(id)sender {
    
    [self sxc_showDateTimeActionSheetWithTitle:@"Select Invite's Date And Time" pickerWithRows:nil dateSelected:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-18000]];
        
        dateString=[dateFormatter stringFromDate:selectedDate];
        
        NSDateFormatter* df = [[NSDateFormatter alloc]init];
        
        [df setTimeZone:[NSTimeZone defaultTimeZone]];
        
        
        [df setDateFormat:@"MM/dd/yyyy HH:mm a"];
        
        NSString * adateString=[df stringFromDate:selectedDate];
        _textfieldDate.text=adateString;
        
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}


- (IBAction)submitButtonClicked:(id)sender {
    
    if([self isValid]){
        [self pawDate];
    }
}

-(void)sxc_stopLoading{}
-(void)sxc_startLoading{}

-(BOOL)isValid
{
    if(_textfieldWhoInitiating.text.length==0){
        [self sxc_showErrorViewWithMessage:@"Please select a PawPal."];
        return NO;
    }
    
    if(_textfieldWhoInvited.text.length==0){
        [self sxc_showErrorViewWithMessage:@"Please select a friends of your selected dog."];
        return NO;
    }
   
    if(_textfieldDate.text.length==0){
        [self sxc_showErrorViewWithMessage:@"Please select a date for a playdate."];
        return NO;
    }
    
    return YES;
}

-(NSDictionary *)requestDictionary
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] init];
    [requestDictionary setValue:selectedDog[@"id"] forKey:@"dog"];
    [requestDictionary setValue:selectedDogFriend[@"id"] forKey:@"dogs"];
    [requestDictionary setValue:_selectedPark[@"id"] forKey:@"place"];
    [requestDictionary setValue:dateString forKey:@"time"];
    [requestDictionary setValue:@"Message" forKey:@"body"];
    
    return requestDictionary;
}

-(void)dogAdded{

 [self dogs];
}

#pragma mark - Service
-(void)dogs{
    
    [self startLoader];
    
    [[PWPService sharedInstance] currentUserDogsWithRequestDictionary:nil withSuccessBlock:^(id response) {
        
        userDogsArray=[SXCUtility cleanArray:response];

        if(userDogsArray.count==0){
            [self sxc_showErrorViewWithMessage:@"This feature requires you to complete a Profile for your dog"];
        }
        
        selectedDog=nil;
        selectedDogFriend=nil;
        selectedDogFriendssArray=nil;
        _textfieldWhoInitiating.text=@"";
        _textfieldWhoInvited.text=@"";
        _textfieldDate.text=@"";

        [self dismissLoader];
    
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        
        if(userDogsArray.count==0){
            [self sxc_showErrorViewWithMessage:@"This feature requires you to complete a Profile for your dog"];
        }
        else{
            [self sxc_showErrorViewWithMessage:error.domain];
        }
    }];
    
}


-(void)pawDate{
    
    [self startLoader];
    
    [[PWPService sharedInstance] dateParkWithRequestDictionary:(id)[self requestDictionary] withSuccessBlock:^(id response) {
        
        [self sxc_showSuccessWithMessage:@"Invite has been created successfuly."];
        
        selectedDog=nil;
        selectedDogFriend=nil;
        selectedDogFriendssArray=nil;
        _textfieldDate.text=@"";
        _textfieldWhoInitiating.text=@"";
        _textfieldWhoInvited.text=@"";
        
        [self dismissLoader];
        
    } withErrorBlock:^(NSError *error) {
        
        [self sxc_showErrorViewWithMessage:@"Some error has been occured"];
        [self dismissLoader];
    }];
    
}

-(void)parkSelected:(NSDictionary *)parkDictionary
{
}
@end
