//
//  PWPItemSelectionViewController.m
//  PawPark
//
//  Created by Samarth Singla on 22/01/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//


#import "SXCUtility.h"
#import "UIColor+PWPColor.h"
#import "UIViewController+UBRComponents.h"
#import <Google/Analytics.h>
#import "PWPDownloadBackgroundImage.h"
#import "PWPItemSelectionViewController.h"
#import "PWPService.h"

@interface PWPItemSelectionViewController (){
    __weak IBOutlet UIImageView * imageViewBG;
}

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property(nonatomic,weak)IBOutlet UITableView * tableView;

@property(nonatomic,strong)NSMutableArray * itemArray;
@property(nonatomic,strong)NSMutableArray * searchedItemArray;

@end
@implementation PWPItemSelectionViewController


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self sxc_customizeBackButton];
    self.title = @"Select City, State or ZipCode";

    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];

    self.itemArray = [SXCUtility cleanArray:[SXCUtility getNSObject:@"newZipcodes"]];

    if(self.itemArray.count == 0){
        [self statesCityZipcodesWithGroup];
    }
    else{
        self.searchedItemArray = [self.itemArray copy];

        [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ZipCodeCell"];
        [self.tableView reloadData];
    }


    for (UIView *subview in [[_searchBar.subviews lastObject] subviews]) {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [subview removeFromSuperview];
            break;
        }
    }
}

-(void)statesCityZipcodesWithGroup {

    [self startLoader];

    PWPService * service=[PWPService sharedInstance];
    [service zipCodesWithRequestDictionary:nil withSuccessBlock:^(id response){

        NSArray * tempItemArray = response;

        NSMutableArray * itemArray = [[NSMutableArray alloc] init];
        NSMutableArray * cityArray = [[NSMutableArray alloc] init];
        NSMutableArray * zipcodeArray = [[NSMutableArray alloc] init];
        NSMutableArray * stateArray = [[NSMutableArray alloc] init];

        for (NSDictionary * item  in tempItemArray) {

            NSDictionary * cityDictionary = @{@"statecode":item[@"statecode"],@"state":item[@"state"],@"city":item[@"city"],@"type":@"city",@"searchText":[NSString stringWithFormat:@"%@(%@)",item[@"city"],item[@"state"]]};
            NSDictionary * zipcodeDictionary = @{@"zipcode":item[@"zipcode"],@"type":@"zipcode",@"searchText":item[@"zipcode"]};
            NSDictionary * stateDictionary = @{@"state":item[@"state"],@"statecode":item[@"statecode"],@"searchText":item[@"state"],@"type":@"state"};

            if (![cityArray containsObject:cityDictionary[@"searchText"]]){
                [itemArray addObject:cityDictionary];
                [cityArray addObject:cityDictionary[@"searchText"]];
            }

            if (![zipcodeArray containsObject:zipcodeDictionary[@"searchText"]]){
                [itemArray addObject:zipcodeDictionary];
                [zipcodeArray addObject:zipcodeDictionary[@"searchText"]];
            }

            if (![stateArray containsObject:stateDictionary[@"searchText"]]){
                [itemArray addObject:stateDictionary];
                [stateArray addObject:stateDictionary[@"searchText"]];
            }
        }

        [SXCUtility saveNSObject:itemArray forKey:@"newZipcodes"];

        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissLoader];
            self.searchedItemArray = [self.itemArray copy];
            [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ZipCodeCell"];
            [self.tableView reloadData];
        });

    } withErrorBlock:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissLoader];
            self.searchedItemArray = [self.itemArray copy];
            [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ZipCodeCell"];
            [self.tableView reloadData];
        });
    }];
}

#pragma mark - UITableView Delegate and Datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"ZipCodeCell"];
    cell.backgroundColor =[UIColor clearColor];
    cell.contentView.backgroundColor =[UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;

    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font =[UIFont fontWithName:@"Roboto-Regular" size:14.0f];

    NSDictionary * item = [_searchedItemArray objectAtIndex:indexPath.row];
    cell.textLabel.text = item[@"searchText"];

    if ([[[_searchedItemArray objectAtIndex: indexPath.row] valueForKey:@"searchText"] isEqualToString:_selectedItem]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else{
        cell.accessoryType=UITableViewCellAccessoryNone;
    }

    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _searchedItemArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    NSDictionary * selectedDictionary = _searchedItemArray[indexPath.row];

    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    if(cell.accessoryType == UITableViewCellAccessoryCheckmark){
        _selectedCity = @"";
        _selectedState = @"";
        _selectedStateCode = @"";
        _selectedZipCode = @"";
        _selectedItem = @"";
    }
    else{
        _selectedCity = [selectedDictionary valueForKey:@"city"];
        _selectedState = [selectedDictionary valueForKey:@"state"];
        _selectedStateCode = [selectedDictionary valueForKey:@"statecode"];
        _selectedZipCode = [selectedDictionary valueForKey:@"zipcode"];
        _selectedItem = [selectedDictionary valueForKey:@"searchText"];
        NSString * type = [selectedDictionary valueForKey:@"type"];

        if([type isEqualToString:@"city"]){
            _selectedType = PWPSelectionTypeCity;
        }
        else if([type isEqualToString:@"zipcode"]){
            _selectedType = PWPSelectionTypeZipCode;
        }
        else if([type isEqualToString:@"state"]){
            _selectedType = PWPSelectionTypeState;
        }
    }

    [tableView reloadData];

}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.view endEditing:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{

    if(searchText.length==0){
        _searchedItemArray=[_itemArray copy];
    }
    else{
        NSPredicate * predicate=[NSPredicate predicateWithFormat:@"self.searchText BEGINSWITH[cd] %@",searchText];
        _searchedItemArray=(id)[_itemArray filteredArrayUsingPredicate:predicate];
    }
    [_tableView reloadData];
}

- (IBAction)doneClicked:(id)sender {

    if (_delegate && [_delegate respondsToSelector:@selector(selectedCity:selectedState:selectedStateCode:selectedZipCode:type: selectedString:)]){
        [self.delegate selectedCity:_selectedCity selectedState:_selectedState selectedStateCode:_selectedStateCode selectedZipCode:_selectedZipCode type:self.selectedType selectedString:_selectedItem];
    }
    
    [self.navigationController popViewControllerAnimated:true];
}

@end
