//
//  PWPAddPostViewController.m
//  PawPark
//
//  Created by Samarth Singla on 09/09/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPAddPostViewController.h"
#import "PWPSelectCommunityCategoryViewController.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPService.h"
#import "PWPMapPinHelper.h"

#import "UIViewController+UBRComponents.h"
#import "UIColor+PWPColor.h"
#import "SXCUtility.h"

#import <MWPhotoBrowser.h>
#import <CTAssetsPickerController/CTAssetsPickerController.h>
#import <UIImagePickerController+BlocksKit.h>

@interface PWPAddPostViewController ()<CTAssetsPickerControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate> {
    UIImage * imagePost;
    NSMutableString * selectedCategory;
    NSArray * categoryValueArray;
    NSArray * categoryTitleArray;
}

@property (weak, nonatomic) IBOutlet UIButton *buttonClose;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewBg;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewNewPost;
@property (weak, nonatomic) IBOutlet UITextField *textfieldTitle;
@property (weak, nonatomic) IBOutlet UITextField *textfieldBody;
@property (weak, nonatomic) IBOutlet UIButton *buttonCategory;

@end

@implementation PWPAddPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    
    categoryValueArray = @[@"pic",@"intro",@"event",@"question"];
    categoryTitleArray = @[@"Community Pictures", @"Introduction", @"Events", @"Questions"];
    
    selectedCategory = [[NSMutableString alloc] init];
    
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if([selectedCategory isEqualToString:@""]){
         [_buttonCategory setTitle:@"Add Category" forState:UIControlStateNormal];
    }
    else {
        NSInteger index = [categoryValueArray indexOfObject:selectedCategory];
        
        [_buttonCategory setTitle:[categoryTitleArray objectAtIndex:index] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initView{
    [self sxc_customizeBackButton];
    [self sxc_navigationBarCustomizationWithTitle:@"ADD POST"];
    
    self.imageViewBg.image = [PWPDownloadBackgroundImage backgroundImage];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonClicked:)];
    self.navigationItem.rightBarButtonItem = backBarButtonItem;
    
    self.buttonClose.hidden = true;
}


-(IBAction)doneButtonClicked:(id)sender {
    if([self isValid]) {
        [self startLoader];
        [[PWPService sharedInstance] createNewPostWithCommunityId:self.communityId WithRequestDictionary:[self getRequestDictionary] withData:nil withSuccessBlock:^(id responseDicitionary) {
            [self dismissLoader];
            [SXCUtility saveNSObject:@(true) forKey:@"isCommunitiesDirty"];
            [self sxc_showSuccessWithMessage:@"Post has been created successfully."];
            [self.navigationController popViewControllerAnimated:true];
            
        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:error.domain];
            [self sxc_stopLoading];
        }];
    }
}


- (IBAction) pickImage:(id)sender{
    
    
}

#pragma mark -
#pragma mark UIImagePickerControllerDelegate

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:true completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
   
    imagePost = image;
    self.buttonClose.hidden = false;
    self.imageViewNewPost.image = imagePost;
    
    [self dismissViewControllerAnimated:true completion:nil];
    [picker dismissViewControllerAnimated:YES completion:nil];

}


-(IBAction)addPicClicked:(id)sender {

    void (^picPhotosFromGallery)(void) = ^void() {
        
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status){
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIImagePickerController *pickerController = [[UIImagePickerController alloc]
                                                             init];
                pickerController.delegate = self;
                [self presentViewController:pickerController animated:YES completion:nil];
            });
        }];
        
    };
    void (^picPhotosFromCamera)(void) = ^void() {
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePicker setAllowsEditing:YES];
        
        [imagePicker setBk_didCancelBlock:^(UIImagePickerController * imagePickerController) {
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [imagePicker setBk_didFinishPickingMediaBlock:^(UIImagePickerController * controller, NSDictionary *info) {
            
            UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
            
            self.buttonClose.hidden = false;
            
            self.imageViewNewPost.image = image;
            imagePost = image;
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }];
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    };
    
    
    if (TARGET_IPHONE_SIMULATOR) {
        [self sxc_showActionSheetWithActionTitle:nil optionTitle:@[@"Choose"] WithActionArray:@[[picPhotosFromGallery copy]]];
    }else{
        [self sxc_showActionSheetWithActionTitle:nil optionTitle:@[@"Take Photo",@"Choose"] WithActionArray:@[[picPhotosFromCamera copy],[picPhotosFromGallery copy]]];
    }

}

- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets {
    
    PHImageRequestOptions * requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    requestOptions.synchronous = true;
    
    PHImageManager *manager = [PHImageManager defaultManager];
    
    for (PHAsset *asset in assets) {
        
        [manager requestImageForAsset:asset
                           targetSize:PHImageManagerMaximumSize
                          contentMode:PHImageContentModeDefault
                              options:requestOptions
                        resultHandler:^void(UIImage *image, NSDictionary *info) {
                            imagePost = image;
                            self.buttonClose.hidden = false;
                            self.imageViewNewPost.image = imagePost;
                        }];
        
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(BOOL)isValid
{
    if([SXCUtility trimString:_textfieldTitle.text].length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter post title"];
        return NO;
    }
    
    if([SXCUtility trimString:_textfieldBody.text].length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter post message"];
        return NO;
    }
    
    if(selectedCategory.length==0){
        [self sxc_showErrorViewWithMessage:@"Please select post category"];
        return NO;
    }

    return YES;
}


-(NSMutableDictionary *)getRequestDictionary {

    NSMutableDictionary * requestDictionary = @{@"name":_textfieldTitle.text,@"body":_textfieldBody.text,@"type":selectedCategory  }.mutableCopy;
    
    if(imagePost){
    
        imagePost = [PWPMapPinHelper resizeImageForUploading:imagePost];
        
        NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(imagePost)];
        
        NSString * imageDataString =[[NSString alloc] initWithFormat:@"data:image/jpeg;base64,%@",[imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
        [requestDictionary setValue:imageDataString forKey:@"imageFile"];
    }

    return requestDictionary;
}

- (IBAction)buttonCloseClicked:(id)sender {
    
    imagePost = nil;
    self.imageViewNewPost.image = nil;
    
    self.buttonClose.hidden = true;

}

- (IBAction)buttonCategoryClicked:(id)sender {
    
    PWPSelectCommunityCategoryViewController * selectVieWController = [[PWPSelectCommunityCategoryViewController alloc] initWithNibName:@"PWPSelectCommunityCategoryViewController" bundle:nil];
    selectVieWController.selectedCategory = selectedCategory;
    
    [self.navigationController pushViewController:selectVieWController animated:true];
}

-(void)sxc_startLoading {}

-(void)sxc_stopLoading {}

@end
