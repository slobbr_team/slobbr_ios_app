//
//  PWPImageInfoViewController.h
//  PawPark
//
//  Created by xyz on 03/08/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPImageInfoViewController : UIViewController

@property(nonatomic,strong)NSDictionary * dictionary;

@end
