//
//  PWPSettingViewController.m
//  PawPark
//
//  Created by daffomac on 4/24/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPSettingViewController.h"
#import "UIViewController+UBRComponents.h"
#import "SXCTextfieldWithLeftImage.h"
#import "UBRTextfield.h"
#import "SXCUtility.h"
#import "PWPService.h"
#import "PWPApplicationUtility.h"
#import "PWPZipCodeSelectionViewController.h"
#import "PWPSelectStatusViewController.h"
#import <Google/Analytics.h>
#import "PWPDownloadBackgroundImage.h"
#import "PWPAppDelegate.h"
#import "MainViewController.h"
#import "PWPTempHomeViewController.h"

@interface PWPSettingViewController ()<PWPZipCodeSelectionDelegate>
{
    __weak IBOutlet UIImageView * imageViewBG;

}

@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textfieldZipCode;
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textfieldCommunity;
@property (weak, nonatomic) IBOutlet UBRTextfield *textfieldEmail;
@property (strong, nonatomic) NSString *selectZipCodeValue;
@property (weak, nonatomic) IBOutlet UISwitch *switcHomePark;
@property (weak, nonatomic) IBOutlet UISwitch *switchSTAHelp;
@property (weak, nonatomic) IBOutlet UISwitch *switchLocation;

@property (strong, nonatomic) NSString *userID;
- (IBAction)submitButtonClicked:(id)sender;

- (IBAction)buttonZipCodeClicked:(id)sender;
- (IBAction)homeParkChanged:(id)sender;
- (IBAction)staHelpChanged:(id)sender;
@end

@implementation PWPSettingViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [_switcHomePark setOn:((NSNumber *)[SXCUtility getNSObject:@"HomeParkEdit"]).boolValue animated:YES];
    
    
    [_switchSTAHelp setOn:((NSNumber *)[SXCUtility getNSObject:@"isStaHelpOff"]).boolValue animated:YES];
    
    [_switchLocation setOn:((NSNumber *)[SXCUtility getNSObject:@"isLocationHelp"]).boolValue animated:YES];
    

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Settings Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

}

#pragma mark - BL Methods

-(void)initView{


    [self sxc_navigationBarCustomizationWithTitle:@"SLOBBR SETTINGS"];
    
    [self sxc_setNavigationBarCrossItem];

    NSString * zipCodeString=[PWPApplicationUtility getCurrentUserProfileDictionary][@"zipcode"];
    
    NSString * emailString=[PWPApplicationUtility getCurrentUserProfileDictionary][@"email"];
    
    _userID=[PWPApplicationUtility getCurrentUserProfileDictionary][@"id"];
   
    NSArray * zipcodeArray=[[SXCUtility cleanArray:[SXCUtility getNSObject:@"newyorkzipcode"]] valueForKeyPath:@"value"];
    NSArray * zipcodeTitleArray=[[SXCUtility cleanArray:[SXCUtility getNSObject:@"newyorkzipcode"]] valueForKeyPath:@"title"];


    if([zipcodeArray containsObject:zipCodeString]){
        
    
        NSInteger index=[zipcodeArray indexOfObject:zipCodeString];
        _textfieldZipCode.text=[zipcodeTitleArray objectAtIndex:index];
        _selectZipCodeValue=[[NSString alloc] initWithString:[zipcodeArray[index] description]];
        
    
    }
    else{
        _textfieldZipCode.text=zipCodeString;
        _selectZipCodeValue=zipCodeString;
    }

    _textfieldEmail.text=emailString;
    
}

-(NSDictionary *)requestDictionary
{
    NSDictionary * requestDic=@{@"zipcode":_selectZipCodeValue,@"email":_textfieldEmail.text,@"username":[PWPApplicationUtility getCurrentUserProfileDictionary][@"username"]};
    return requestDic;
}

-(BOOL)isValid{

    if([SXCUtility cleanString:_textfieldZipCode.text].length==0){
        [self sxc_showErrorViewWithMessage:@"Please select the zipcode."];
        return NO;
    }
    else if(![SXCUtility isValidEmail:_textfieldEmail.text]){
        [self sxc_showErrorViewWithMessage:@"Please enter a valid email address."];
        return NO;
    }
    return YES;
}


- (IBAction)submitButtonClicked:(id)sender {
    
    if([self isValid]){
        [self updateInformation];
    }
}

- (IBAction)buttonZipCodeClicked:(id)sender {
  
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];

    PWPZipCodeSelectionViewController * zipCodeSelectionViewController=[storyBoard instantiateViewControllerWithIdentifier:@"PWPZipCodeSelectionViewController"];
    zipCodeSelectionViewController.delegate=self;
    [self.navigationController pushViewController:zipCodeSelectionViewController animated:YES];
}

- (IBAction)homeParkChanged:(id)sender {
    
    NSNumber * switchValue = [NSNumber numberWithBool:_switcHomePark.isOn];
    
    [SXCUtility saveNSObject:switchValue forKey:@"HomeParkEdit"];
    
}

- (IBAction)staHelpChanged:(id)sender {
    
    NSNumber * switchValue = [NSNumber numberWithBool:_switchSTAHelp.isOn];
    [SXCUtility saveNSObject:switchValue forKey:@"isStaHelpOff"];
    
}
- (IBAction)locationHelpChanged:(id)sender {
    NSNumber * switchValue = [NSNumber numberWithBool:_switchLocation.isOn];
    [SXCUtility saveNSObject:switchValue forKey:@"isLocationHelp"];
}


-(void)zipCodeSelected:(NSMutableDictionary *)zipCodeDictionary{
    _textfieldZipCode.text=zipCodeDictionary[@"title"];
    _selectZipCodeValue=zipCodeDictionary[@"value"];
}

-(IBAction)joinCommunityClicked:(id)sender {
    
    
    if([SXCUtility cleanString:self.textfieldCommunity.text].length > 0){
        [self startLoader];
        [[PWPService sharedInstance] joinCommunityId:[SXCUtility cleanString:self.textfieldCommunity.text] WithRequestDictionary:nil withData:nil withSuccessBlock:^(id responseDicitionary) {
            [self dismissLoader];
            [self sxc_showSuccessWithMessage:@"You have successfully joined this community"];
            
            UITabBarController * tabViewController = kTabViewController;
            UINavigationController * navigationController =  tabViewController.viewControllers.firstObject;
            
            PWPTempHomeViewController * communityViewController = [[PWPTempHomeViewController alloc] initWithNibName:@"PWPTempHomeViewController" bundle:nil];
            [navigationController setViewControllers:@[communityViewController] animated:false];
            
         
        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            
            if(error.code == 404){
                [self sxc_showErrorViewWithMessage:@"Please enter correct community code."];
            }
            else if(error.code == 200){
              
                [self sxc_showSuccessWithMessage:@"You have successfully joined this community"];
                
                UITabBarController * tabViewController = kTabViewController;
                UINavigationController * navigationController =  tabViewController.viewControllers.firstObject;
                
                PWPTempHomeViewController * communityViewController = [[PWPTempHomeViewController alloc] initWithNibName:@"PWPTempHomeViewController" bundle:nil];
                [navigationController setViewControllers:@[communityViewController] animated:false];

            }
            else {
                [self sxc_showErrorViewWithMessage:error.domain];
            }
        }];
    }
    else {
        [self sxc_showErrorViewWithMessage:@"Please enter community Id"];
    }

}


-(void)updateInformation{

    [self showLoaderWithText:@"Loading..."];
    [[PWPService sharedInstance] updateCurrentUserProfileWithRequestDictionary:(id)[self         requestDictionary] WithUserId:_userID withSuccessBlock:^(id responseDicitionary) {
       
        NSMutableDictionary * storeDictionary=[[PWPApplicationUtility getCurrentUserProfileDictionary] mutableCopy];
        [storeDictionary setValue:_textfieldEmail.text forKey:@"email"];
        [storeDictionary setValue:_selectZipCodeValue forKey:@"zipcode"];

        [PWPApplicationUtility saveCurrentUserProfileDictionary:storeDictionary];
        
        [SXCUtility saveNSObject:@true forKey:@"setting_dirty"];
        
        [self dismissLoaderWithSuccessText:@"Setting has been saved successfully."];
        
        [self dismissViewControllerAnimated:YES completion:nil];

        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        [self sxc_showErrorViewWithMessage:error.domain];
    }];

}

-(void)sxc_stopLoading{}
-(void)sxc_startLoading{}
@end
