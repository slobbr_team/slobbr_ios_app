//
//  PWPDogPicViewController.m
//  PawPark
//
//  Created by Samarth Singla on 02/02/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPDogPicViewController.h"
#import "UIViewController+UBRComponents.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPDogImageCell.h"
#import <UIImageView+WebCache.h>
#import "PWPApplicationUtility.h"
#import "SXCUtility.h"
#import <MWPhoto.h>
#import <MWPhotoBrowser.h>
#import "PWPService.h"
#import <CTAssetsPickerController/CTAssetsPickerController.h>
#import <UIImagePickerController+BlocksKit.h>
#import "PWPMapPinHelper.h"
#import "PWPAppDelegate.h"
#import <SVProgressHUD.h>
@interface PWPDogPicViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MWPhotoBrowserDelegate, UIAlertViewDelegate, PWPDogImageCellProtocoal, CTAssetsPickerControllerDelegate> {

    __weak IBOutlet UIImageView * imageViewBG;
    __weak IBOutlet UIButton * friendRequest;
    __weak IBOutlet UILabel * noDataLabel;
    __weak IBOutlet UICollectionView * picCollectionView;
    __weak IBOutlet UIBarButtonItem *buttonUpload;

    NSMutableArray * photos;
    NSMutableArray * thumbs;
    NSString * fromString;
    NSString * toString;

    BOOL isCurrentUserDog;

    NSInteger numberOfPics;
    CGFloat totalProgress;

}

@end

@implementation PWPDogPicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self sxc_customizeBackButton];
    [self sxc_navigationBarCustomizationWithTitle:@"Photos"];

    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];

    if([[PWPApplicationUtility currentUserDogsIds] containsObject:[self.data valueForKey:@"id"]]){
        friendRequest.hidden = true;
        isCurrentUserDog = true;
        buttonUpload.title = @"Upload";
    }
    else{
        friendRequest.hidden = false;
        isCurrentUserDog = false;
        buttonUpload.title = @"";
    }



    if (self.dogsPics.count > 0 || isCurrentUserDog == true){
        picCollectionView.hidden = false;
        noDataLabel.hidden = true;
    }
    else{
        picCollectionView.hidden = true;
        noDataLabel.hidden = false;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dogsPics.count + (isCurrentUserDog?1:0);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;

    return CGSizeMake((screenWidth-64)/3,(screenWidth-64)/3);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    if(indexPath.row == self.dogsPics.count){
        UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PWPDogImageAddCell" forIndexPath:indexPath];
        return cell;
    }

    PWPDogImageCell * collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PWPDogImageCell" forIndexPath:indexPath];

    NSString * dogUrlString = [SXCUtility cleanString:[[self.dogsPics[indexPath.row] valueForKey:@"thumbs"] valueForKey:@"w400"]];

    UIImage * image = [self.dogsPics[indexPath.row] valueForKey:@"image"];
    if (image != nil){
        [collectionViewCell.imageViewDog setImage:image];
    }else {
        [collectionViewCell.imageViewDog sd_setImageWithURL:[NSURL URLWithString:dogUrlString] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
    }
    [collectionViewCell configureViewWithDelegate:self WithIndexPath:indexPath];
    collectionViewCell.buttonDogImageDelete.hidden = !isCurrentUserDog;

    return collectionViewCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row == self.dogsPics.count){
        [self addDogPic];
        return;
    }

    photos = [[NSMutableArray alloc] init];
    thumbs = [[NSMutableArray alloc] init];

    for (NSDictionary * dog in self.dogsPics) {

        if([dog valueForKey:@"image"]){
            [photos addObject:[MWPhoto photoWithImage:[dog valueForKey:@"image"]]];
            [thumbs addObject:[MWPhoto photoWithImage:[dog valueForKey:@"image"]]];
        }
        else {
            NSString * fullDogUrlString = [SXCUtility cleanString:[[dog valueForKey:@"thumbs"] valueForKey:@"w1000"]];
            [photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:fullDogUrlString]]];

            NSString * thumbDogUrlString = [SXCUtility cleanString:[[dog valueForKey:@"thumbs"] valueForKey:@"w400"]];
            [thumbs addObject:[MWPhoto photoWithURL:[NSURL URLWithString:thumbDogUrlString]]];
        }
    }


    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];

    browser.displayActionButton = YES;
    browser.displayNavArrows = YES;
    browser.displaySelectionButtons = NO;
    browser.alwaysShowControls = YES;
    browser.zoomPhotosToFill = YES;
    browser.enableGrid = NO;
    browser.startOnGrid = NO;
    browser.enableSwipeToDismiss = YES;
    browser.autoPlayOnAppear = NO;
    [browser setCurrentPhotoIndex:indexPath.row];

    UINavigationController * controller = [[UINavigationController alloc] initWithRootViewController:browser];

    [self presentViewController:controller animated:YES completion:^{

    }];

}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < thumbs.count)
        return [thumbs objectAtIndex:index];
    return nil;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
}

- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index {
    return false;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected {
    NSLog(@"Photo at index %lu selected %@", (unsigned long)index, selected ? @"YES" : @"NO");
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)optionPackClickedWithData:(id)sender
{
    if([PWPApplicationUtility getCurrentUserDogsArray].count==0){
        [self sxc_showErrorViewWithMessage:@"Please add a dog to your packs."];
        return;
    }

    [super selectDogFromPackToSendRequest:^(NSString * dogId) {

        [self sendFriendRequestServiceCallWithData:self.data fromDogId:dogId];
    } withCancelblock:^{
        //        [self sxc_showErrorViewWithMessage:@"Please select the dog from your pack to send the firend request"];
    }];
    
}

-(void)sendFriendRequestServiceCallWithData:(id)data fromDogId:(NSString *)fromDogId
{
    fromString=fromDogId;
    toString=data[@"id"];

    [self startLoader];

    [[PWPService sharedInstance]sendDogFriendsRequest:data[@"id"] fromDog:fromDogId withSuccessBlock:^(id responseDicitionary) {

        [self dismissLoaderWithSuccessText:@"Request has been sent successfully."];

    } withErrorBlock:^(NSError *error) {

        if(error.code==403){
            [self sxc_showAlertViewWithTitle:@"Error" message:@"You have already sent the friend request to this dog. Do you want to cancel the friend reqeust?" WithDelegate:self WithCancelButtonTitle:@"NO" WithAcceptButtonTitle:@"YES"];
            [self dismissLoader];
        }else{
            [self dismissLoaderWithErrorText:@"An error has occurred. Please try again"];
        }
    }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1){

        [[PWPService sharedInstance] cancelDogFriendsRequest:toString fromDog:fromString withSuccessBlock:^(id responseDicitionary) {

            [self dismissLoaderWithSuccessText:@"Request has been cancelled successfully."];

        } withErrorBlock:^(NSError *error) {

            [self dismissLoaderWithErrorText:@"An error has occurred. Please try again"];

        }];
    }
}

-(void)imageDeleteClicked:(NSIndexPath *)indexPath
{
    NSDictionary * dictionary = self.dogsPics[indexPath.row];

    NSString * imageId = [SXCUtility cleanString:[[dictionary valueForKey:@"id"] description]];
    if (imageId.length > 0){
        [self deleteDogWithImageId:imageId indexPath:indexPath];
    }
    else {
        [self.dogsPics removeObjectAtIndex:indexPath.row];
        [picCollectionView reloadData];
    }
    
}


-(void)deleteDogWithImageId:(NSString *)imageId indexPath:(NSIndexPath *) indexPath {

    [self sxc_startLoading];

    [[PWPService sharedInstance] deleteDogImageID:imageId withDogId:_dogId withSuccessBlock:^(id responseDicitionary) {

        [self.dogsPics removeObjectAtIndex:indexPath.row];
        [picCollectionView reloadData];

        [self sxc_stopLoading];

    } withErrorBlock:^(NSError *error) {
        [self sxc_stopLoading];
        [self sxc_showErrorViewWithMessage:@"Some error occured"];
    }];
}

-(void)addDogPic
{

    void (^picPhotosFromGallery)(void) = ^void() {

        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status){
            dispatch_async(dispatch_get_main_queue(), ^{

                // init picker
                CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];

                // set delegate
                picker.delegate = self;

                // Optionally present picker as a form sheet on iPad
                [self presentViewController:picker animated:YES completion:nil];
            });
        }];
    };
    void (^picPhotosFromCamera)(void) = ^void() {

        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePicker setAllowsEditing:YES];

        [imagePicker setBk_didCancelBlock:^(UIImagePickerController * imagePickerController) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];

        [imagePicker setBk_didFinishPickingMediaBlock:^(UIImagePickerController * controller, NSDictionary *info) {

            UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];

            [self createNewDogPic:image];

            [self dismissViewControllerAnimated:YES completion:nil];

            [picCollectionView reloadData];

        }];

        [self presentViewController:imagePicker animated:YES completion:nil];

    };


    if (TARGET_IPHONE_SIMULATOR) {
        [self sxc_showActionSheetWithActionTitle:nil optionTitle:@[@"Choose"] WithActionArray:@[[picPhotosFromGallery copy]]];
    }else{
        [self sxc_showActionSheetWithActionTitle:nil optionTitle:@[@"Take Photo",@"Choose"] WithActionArray:@[[picPhotosFromCamera copy],[picPhotosFromGallery copy]]];
    }

}

-(void)createNewDogPic:(UIImage *)image{

    NSMutableDictionary * dictionary =[[NSMutableDictionary alloc] init];
    dictionary[@"description"] = @"Upload New image dog pciture from mobile app";
    dictionary[@"imageInfo"] = @[@{
                                     @"key":@"License",@"value":@"Mobile User"
                                     },
                                 @{
                                     @"key":@"by",@"value":@"Mobile User"
                                     
                                     }];
    dictionary[@"image"] = image;
    
    [self.dogsPics addObject:dictionary];
    
}


- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets {

    [self dismissViewControllerAnimated:YES completion:nil];

    PHImageRequestOptions * requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    requestOptions.synchronous = true;

    PHImageManager *manager = [PHImageManager defaultManager];

    for (PHAsset *asset in assets) {

        [manager requestImageForAsset:asset
                           targetSize:PHImageManagerMaximumSize
                          contentMode:PHImageContentModeDefault
                              options:requestOptions
                        resultHandler:^void(UIImage *image, NSDictionary *info) {
                            [self createNewDogPic:image];
                        }];

    }

    [picCollectionView reloadData];
}

-(void)uploadNewPicGroup:(dispatch_group_t)group {

    for (NSDictionary * dictionary in self.dogsPics){

        UIImage * image = [PWPMapPinHelper resizeImageForUploading:[dictionary valueForKey:@"image"]];

        if (image != nil){

            dispatch_group_enter(group);

            numberOfPics += 1;
            
            NSMutableDictionary * requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
            NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];

            NSString * imageDataString =[[NSString alloc] initWithFormat:@"data:image/jpeg;base64,%@",[imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
            [requestDictionary setValue:imageDataString forKey:@"imageFile"];
            [requestDictionary removeObjectForKey:@"image"];

            [[PWPService sharedInstance] uploadNewDogImage:requestDictionary withDogId:self.data[@"id"] withProgress:^(float progress){
                totalProgress += progress;
                [self updateProgressLoader];
            }  withSuccessBlock:^(id responseDicitionary) {
                dispatch_group_leave(group);

            } withErrorBlock:^(NSError *error) {
                dispatch_group_leave(group);
            }];
        }
    }
}

-(void)updateProgressLoader {
    
    if (numberOfPics == 1) {
        NSString * uploadingText = [NSString stringWithFormat:@"Uploading\n(%.0f%%)...",totalProgress*1.0f/numberOfPics*100];
        [[[PWPAppDelegate sharedInstance] window] setUserInteractionEnabled:NO];
        [SVProgressHUD showProgress:totalProgress*1.0f/numberOfPics status:uploadingText];
    } else {
        
        NSInteger uploadedImageNumber = totalProgress;
        if(uploadedImageNumber >= numberOfPics) {
            uploadedImageNumber  = numberOfPics-1;
        }
        
        NSInteger totalProgressInInt = totalProgress;
        CGFloat percentage = (totalProgress - totalProgressInInt)*100;
        
        NSString * uploadingText = [NSString stringWithFormat:@"Uploading\n%.0f%%\n(%.0ld/%ld)",percentage,(long)uploadedImageNumber+1,(long)numberOfPics];
        [[[PWPAppDelegate sharedInstance] window] setUserInteractionEnabled:NO];
        [SVProgressHUD showProgress:totalProgress*1.0f/numberOfPics status:uploadingText];
    }
    
}


-(IBAction)doneBtnClicked:(id)sender{

    if(isCurrentUserDog == false){
        return;
    }

    [self startLoader];

    dispatch_group_t group = dispatch_group_create();

    numberOfPics = 0;
    totalProgress = 0;
    
    [self uploadNewPicGroup:group];

    dispatch_group_notify(group, dispatch_get_main_queue(), ^{

        [self.navigationController popViewControllerAnimated:YES];

        [self sxc_showSuccessWithMessage:@"Dog's Profile has been added successfully."];
        [self dismissLoader];

        if(_delegate&&[_delegate respondsToSelector:@selector(dogProfileUpdated)]){
            [_delegate dogProfileUpdated];
        }

    });

}

-(void)sxc_stopLoading{}
-(void)sxc_startLoading{}

@end
