//
//  PWPEventFilterDataViewController.m
//  PawPark
//
//  Created by HupendraRaghuwanshi on 08/03/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPEventFilterDataViewController.h"
#import "UIViewController+UBRComponents.h"
#import "SXCUtility.h"
#import "PWPService.h"
#import "PWPApplicationUtility.h"
#import "SXCTextfieldWithLeftImage.h"
#import "PWPItemSelectionViewController.h"
#import <UIImageView+WebCache.h>
#import "PWPDownloadBackgroundImage.h"
#import "PWPConstant.h"
#import <UIButton+WebCache.h>

@interface PWPEventFilterDataViewController ()<PWPCitySelectionDelegate>{
    __weak IBOutlet UIButton *buttonAdvertisment;
    __weak IBOutlet UIImageView *imageViewSub;

}
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textFromDate;
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textToDate;
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textCityState;
@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textSearch;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewBg;

@end

@implementation PWPEventFilterDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",_parkFilterDictionary);
    if (_parkFilterDictionary == nil) {
      _parkFilterDictionary = [[NSMutableDictionary alloc] init];
    }
}

 - (void)viewWillAppear:(BOOL)animated
{
    [self initView];
    [self sxc_navigationBarCustomizationWithTitle:@"FILTER"];
    [self sxc_customizeBackButton];

    NSString * string = [SXCUtility cleanString:[SXCUtility getNSObject:@"filter_link"]];

    if(string.length > 0){
        [buttonAdvertisment addTarget:self action:@selector(advertisementClicked:) forControlEvents:UIControlEventTouchUpInside];
        buttonAdvertisment.userInteractionEnabled = TRUE;
    }
    else {
        buttonAdvertisment.userInteractionEnabled = FALSE;
    }

    UIImage * filterImage = [PWPDownloadBackgroundImage filterBg];
    if(filterImage != nil){
        imageViewSub.image =  filterImage;
    }

}


-(void)initView
{
    
    if(_selectedZipcode==nil){
        _selectedZipcode=[[NSString alloc] init];
    }
    
    [self sxc_navigationBarCustomizationWithTitle:@"SEARCH PLACES"];
    [self sxc_customizeBackButton];
    
    _selectedString = [SXCUtility cleanString:_parkFilterDictionary[@"selectedString"]];
    _selectedCity = [SXCUtility cleanString: _parkFilterDictionary[@"city"]];
    _selectedStateCode = [SXCUtility cleanString: _parkFilterDictionary[@"stateCode"]];
    _selectedZipcode = [SXCUtility cleanString: _parkFilterDictionary[@"zipcode"]];
    
    if (_selectedZipcode != nil && _selectedZipcode.length > 0){
        _selectedString= _selectedZipcode;
    }
    else if (_selectedCity.length > 0 && _selectedStateCode.length > 0 && _selectedState.length > 0){
        _selectedString = [NSString stringWithFormat:@"%@(%@)",_selectedCity,_selectedState];
    }
    else if (_selectedState.length > 0 && _selectedStateCode.length > 0){
        _selectedString= _selectedState;
    }
    
    
    if([SXCUtility isEmptyString:_selectedString]){
        self.textCityState.text=@"";
    }else{
        self.textCityState.text=_selectedString;
    }
    
    _textSearch.text=_parkFilterDictionary[@"s"];
    _textFromDate.text = [SXCUtility cleanString:_parkFilterDictionary[@"timeMin"]];
    _textToDate.text = [SXCUtility cleanString:_parkFilterDictionary[@"timeMax"]];


    NSString * eventUrl = [PWPDownloadBackgroundImage eventBackgroundImageURL];
    if(eventUrl.length > 0){
        [_imageViewBg sd_setImageWithURL:[NSURL URLWithString:eventUrl] placeholderImage: [PWPDownloadBackgroundImage backgroundImage]];
    }
    else{
        _imageViewBg.image = [PWPDownloadBackgroundImage backgroundImage];
    }

}

- (IBAction)fromDateClicked:(id)sender {
    
    
    [self sxc_showDateTimeActionSheetWithoutMinOrMaxDateWithTitle:@"" pickerWithRows:nil dateSelected:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        NSDate * dateSelected=selectedDate;
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        
        // Extract date components into components1
        NSDateComponents * dateComponents= [gregorianCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:dateSelected];
        
        NSNumber *yearString=[NSNumber numberWithInteger:dateComponents.year];
        NSNumber *monthString=[NSNumber numberWithInteger:dateComponents.month];
        NSString *dayString=[NSString stringWithFormat:@"%ld",(long)dateComponents.day];
        
        _textFromDate.text=[NSString stringWithFormat:@"%@/%@/%@",monthString,dayString,yearString];
        _minTime = [NSString stringWithFormat:@"%@/%@/%@",monthString,dayString,yearString];
        _parkFilterDictionary[@"timeMin"] = _minTime;
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}



- (IBAction)toDateClicked:(id)sender {
    
    [self sxc_showDateTimeActionSheetWithoutMinOrMaxDateWithTitle:@"" pickerWithRows:nil dateSelected:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        NSDate * dateSelected=selectedDate;
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        
        // Extract date components into components1
        NSDateComponents * dateComponents= [gregorianCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:dateSelected];
        
        NSNumber *yearString=[NSNumber numberWithInteger:dateComponents.year];
        NSNumber *monthString=[NSNumber numberWithInteger:dateComponents.month];
        NSString *dayString=[NSString stringWithFormat:@"%ld",(long)dateComponents.day];
        
        _textToDate.text=[NSString stringWithFormat:@"%@/%@/%@",monthString,dayString,yearString];
        _maxTime =[NSString stringWithFormat:@"%@/%@/%@",monthString,dayString,yearString];
        _parkFilterDictionary[@"timeMax"] = _maxTime;
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];

}


- (IBAction)filterClicked:(id)sender {
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    PWPItemSelectionViewController * citySelectedViewController = [storyboard instantiateViewControllerWithIdentifier:@"PWPItemSelectionViewController"];
    citySelectedViewController.delegate = self;
    citySelectedViewController.selectedZipCode = _selectedZipcode;
    citySelectedViewController.selectedState = _selectedState;
    citySelectedViewController.selectedCity = _selectedCity;
    citySelectedViewController.selectedStateCode = _selectedStateCode;
    citySelectedViewController.selectedType = PWPSelectionTypeCity;
    citySelectedViewController.selectedItem = _selectedString;
    _parkFilterDictionary[@"s"] = _textSearch.text;
    _parkFilterDictionary[@"selectedString"] = _textSearch.text;
    [self.navigationController pushViewController:citySelectedViewController animated:YES];
}

-(void)selectedCity:(NSString *)theSelectedCity selectedState:(NSString *)theSelectedState selectedStateCode:(NSString *)theSelectedStateCode selectedZipCode:(NSString *)theSelectedZipCode type:(PWPSelectionType)type selectedString:(NSString *)theSelectedString
{
    _selectedCity = theSelectedCity;
    _selectedState = theSelectedState;
    _selectedZipcode = theSelectedZipCode;
    _selectedStateCode = theSelectedStateCode;
    _textCityState.text = theSelectedString;
    _selectedString = theSelectedString;
    
    _parkFilterDictionary[@"selectedString"] = _selectedString;
    _parkFilterDictionary[@"stateCode"] = _selectedStateCode;
    _parkFilterDictionary[@"city"] = _selectedCity;
    _parkFilterDictionary[@"zipcode"] = _selectedZipcode;
}

- (IBAction)fetchClicked:(id)sender {
    _parkFilterDictionary[@"s"] = _textSearch.text;
    [_delegate selectedFilter:_parkFilterDictionary];
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)clearFilterClicked:(id)sender {
    _textSearch.text = @"";
    _textToDate.text = @"";
    _textFromDate.text = @"";
    _textCityState.text = @"";
    [_parkFilterDictionary removeAllObjects];
    [_delegate selectedFilter:_parkFilterDictionary];
}

-(IBAction)advertisementClicked:(id)sender {

    if([PWPDownloadBackgroundImage isSkinApplied] == false){
        return;
    }
    
    NSString * string = [SXCUtility cleanString:[SXCUtility getNSObject:@"filter_link"]];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:string]]) {

        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
    }
}

@end
