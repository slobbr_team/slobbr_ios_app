//
//  PWPNotificationViewController.m
//  PawPark
//
//  Created by daffolap19 on 5/24/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPNotificationViewController.h"
#import "PWPService.h"
#import "SXCUtility.h"
#import "PWPNotificationFriendRequestCell.h"
#import "UIViewController+UBRComponents.h"
#import "UIColor+PWPColor.h"
#import "PWPNotificationDateTableViewCell.h"
#import "PWPMessageListCell.h"
#import "PWPMessageThreadViewController.h"
#import "Google/Analytics.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPNotificationViewController ()<UITableViewDataSource,UITableViewDelegate,PWPNotificationDelegate,PWPNotificationDateDelegate>
{
    __weak IBOutlet UIImageView * imageViewBG;
}

@property (weak, nonatomic) IBOutlet UILabel *labelNoData;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *friendsRequestArray;
@property(nonatomic,strong) NSMutableArray *datesRequestArray;
@property(nonatomic,strong) NSMutableArray *messageRequestArray;

@property (strong, nonatomic) UIRefreshControl * refreshControl;
@end

@implementation PWPNotificationViewController

- (void)viewDidLoad {
  
    [super viewDidLoad];
    
    [self addPullToRefresh];
    [self sxc_setNavigationBarCrossItem];
    [self sxc_navigationBarCustomizationWithTitle:@"Notifications"];
    [self.view bringSubviewToFront:_labelNoData];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    _labelNoData.text=@"No Notifications";
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self automaticallyRefreshControllOnView];
    [self friendsRequest];
    
   // [SXCUtility saveNSObject:[NSNumber numberWithInteger:0] forKey:@"NotificationCount"];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Notification Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addPullToRefresh
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor=[UIColor sxc_themeColor];
    [self.refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:self.refreshControl];
}

-(void)automaticallyRefreshControllOnView
{
    [_tableView setContentOffset:CGPointMake(0, -self.refreshControl.frame.size.height) animated:YES];
    [self.refreshControl beginRefreshing];
}

-(IBAction)refreshView:(id)sender
{
    [self friendsRequest];
}


#pragma mark - Services
-(void)acceptDateRequestWithDictionary:(NSDictionary *)notificationDictionary
{
    [self showLoaderWithText:@"Accepting"];
        
    [[PWPService sharedInstance] dateRequestsAcceptWithDateIdString:notificationDictionary[@"id"] withSuccessBlock:^(id response) {
        
        
        [self dismissLoaderWithSuccessText:@"Success!"];
        
        [self automaticallyRefreshControllOnView];
        [self refreshView:nil];
        
    } withErrorBlock:^(NSError *error) {
        [self sxc_showErrorViewWithMessage:error.domain];
    }];
}

-(void)declineDateRequestWithDictionary:(NSDictionary *)notificationDictionary
{
    [self showLoaderWithText:@"Declining"];
    
    [[PWPService sharedInstance] dateRequestsDeclineWithDateIdString:notificationDictionary[@"id"] withSuccessBlock:^(id response) {
        
        
        [self dismissLoaderWithSuccessText:@"Success!"];
        
        [self automaticallyRefreshControllOnView];
        [self refreshView:nil];
        
        
        
    } withErrorBlock:^(NSError *error) {
        [self sxc_showErrorViewWithMessage:error.domain];
    }];
}

-(void)acceptRequestWithDictionary:(NSDictionary *)notificaionDictionary
{
    [self showLoaderWithText:@"Accepting"];
    

    
    [[PWPService sharedInstance] friendRequestsAcceptWithIntiatorString:notificaionDictionary[@"initiator"][@"id"] withReciepientString:notificaionDictionary[@"recipient"][@"id"] withSuccessBlock:^(id response) {
        
        
        [self dismissLoaderWithSuccessText:@"Success!"];
        
        [self automaticallyRefreshControllOnView];
        [self refreshView:nil];
        
        
        
    } withErrorBlock:^(NSError *error) {
        [self sxc_showErrorViewWithMessage:error.domain];
    }];

}
-(void)declineRequestWithDictionary:(NSDictionary *)notificaionDictionary
{
    [self showLoaderWithText:@"Declining"];
    
    [[PWPService sharedInstance] friendRequestsDeclineWithIntiatorString:notificaionDictionary[@"initiator"][@"id"] withReciepientString:notificaionDictionary[@"recipient"][@"id"] withSuccessBlock:^(id response) {
        
        
        [self dismissLoaderWithSuccessText:@"Success!"];
        
        [self automaticallyRefreshControllOnView];
        [self refreshView:nil];
        
        
        
    } withErrorBlock:^(NSError *error) {
        [self sxc_showErrorViewWithMessage:error.domain];
    }];
}

-(void)friendsRequest
{
    _labelNoData.hidden=YES;
    
    [[PWPService sharedInstance] friendRequestsWithRequestDictionary:nil withSuccessBlock:^(id response) {
        
        _friendsRequestArray=[[NSMutableArray alloc] init];
        if([response[@"friendships"] isKindOfClass:[NSArray class]]){
            NSArray * friendsShpArray=[SXCUtility cleanArray:[response valueForKey:@"friendships"]];
            [_friendsRequestArray addObjectsFromArray:friendsShpArray];
        }
        else if([response[@"friendships"] isKindOfClass:[NSDictionary class]]){
            NSDictionary * friendshipDictionary=[response valueForKey:@"friendships"];
            for(NSString * key in friendshipDictionary.allKeys){
                [_friendsRequestArray addObjectsFromArray:friendshipDictionary[key]];
            }
        }
        
        _datesRequestArray=[[NSMutableArray alloc] init];
        if([response[@"dates"] isKindOfClass:[NSArray class]]){
            NSArray * friendsShpArray=[SXCUtility cleanArray:[response valueForKey:@"dates"]];
            [_datesRequestArray addObjectsFromArray:friendsShpArray];
        }
        else if([response[@"dates"] isKindOfClass:[NSDictionary class]]){
            NSDictionary * datesDictionary=[response valueForKey:@"dates"];
            for(NSString * key in datesDictionary.allKeys){
                [_datesRequestArray addObjectsFromArray:datesDictionary[key]];
            }
        }
        
        
        _messageRequestArray=[[NSMutableArray alloc] init];
//        if([response[@"messages"] isKindOfClass:[NSArray class]]){
//            NSArray * messagesArray=[SXCUtility cleanArray:[response valueForKey:@"messages"]];
//            [_messageRequestArray addObjectsFromArray:messagesArray];
//        }
//        else if([response[@"messages"] isKindOfClass:[NSDictionary class]]){
//            NSDictionary * messagesDictionary=[response valueForKey:@"messages"];
//            for(NSString * key in messagesDictionary.allKeys){
//                [_messageRequestArray addObjectsFromArray:messagesDictionary[key]];
//            }
//        }
        if((_friendsRequestArray.count==0)&&(_datesRequestArray.count==0)&&(_messageRequestArray.count==0)){
            _labelNoData.hidden=NO;
        }
        else{
            _labelNoData.hidden=YES;
        }
        
        [_tableView reloadData];
        
        NSInteger totalNotifications=_datesRequestArray.count+_messageRequestArray.count+_friendsRequestArray.count;
        
        [SXCUtility saveNSObject:[NSNumber numberWithInteger:totalNotifications] forKey:@"NotificationCount"];

        [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationCount" object:nil];

        [_refreshControl endRefreshing];
        
    } withErrorBlock:^(NSError *error) {

        [self sxc_showErrorViewWithMessage:error.domain];

        [_tableView reloadData];
        [_refreshControl endRefreshing];
        
        if(_friendsRequestArray.count==0){
            _labelNoData.hidden=NO;
        }
        else{
            _labelNoData.hidden=YES;
        }

    }];

}

#pragma mark - TableView Delegate and datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0){
        PWPNotificationFriendRequestCell * cell=[tableView dequeueReusableCellWithIdentifier:@"PWPNotificationFriendRequestCell"];
            [cell configureCellWithData:_friendsRequestArray[indexPath.row]];
        cell.delegate=self;
        return cell;
    }
    else if(indexPath.section==1){
        PWPNotificationDateTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"PWPNotificationDateRequestCell"];
        [cell configueCellWithData:_datesRequestArray[indexPath.row]];
        cell.delegate=self;
        return cell;
    }
    else if(indexPath.section==2){
        PWPMessageListCell * cell=[tableView dequeueReusableCellWithIdentifier:@"PWPMessageListCell"];
        [cell configureCellWithData:_messageRequestArray[indexPath.row]];
        return cell;
    }
    return nil;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section==0){
        UITableViewCell * friendRequestCell=[tableView dequeueReusableCellWithIdentifier:@"Friend_Request_Header"];
        return friendRequestCell;
    }
    else if(section==1){
        UITableViewCell * friendRequestCell=[tableView dequeueReusableCellWithIdentifier:@"Date_Request_Header"];
        return friendRequestCell;
    }
    else if(section==2){
        UITableViewCell * friendRequestCell=[tableView dequeueReusableCellWithIdentifier:@"Message_Request_Header"];
        return friendRequestCell;
    }
    
    return nil;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0){
        return _friendsRequestArray.count;
    }
    else if(section==1){
        return _datesRequestArray.count;
    }
    else if(section==2){
        return _messageRequestArray.count;
    }
    return 0.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
        return 80.0f;
    else if(indexPath.section==1)
        return 121.0f;
    else if(indexPath.section==2)
        return 109.0f;
    return 0.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==0)
    return _friendsRequestArray.count>0?49.0f:1.0f;
    else if(section==1)
    return _datesRequestArray.count>0?49.0f:1.0f;
    else if(section==2)
        return _messageRequestArray.count>0?49.0f:1.0f;
    return 0.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section==2){
    
        NSDictionary * messageDictionary=[_messageRequestArray objectAtIndex:indexPath.row];
        
        PWPMessageThreadViewController * messageThreadViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"PWPMessageThreadViewController"];
        messageThreadViewController.threadIDString=messageDictionary[@"id"];
        messageThreadViewController.initiatorIDString=messageDictionary[@"initiator"][@"id"];
        messageThreadViewController.recipientIDString=messageDictionary[@"recipient"][@"id"];
        messageThreadViewController.messages=messageDictionary[@"messages"];
        
        [self.navigationController pushViewController:messageThreadViewController animated:YES];
    }
}


#pragma mark - Delegate Methods
-(void)notificationFriendRequestCancelWithData:(NSMutableDictionary *)dictionary{
    [self declineRequestWithDictionary:dictionary];
}
-(void)notificationFriendRequestConfirmWithData:(NSMutableDictionary *)dictionary{
    [self acceptRequestWithDictionary:dictionary];
}

-(void)acceptButtonClickedWithDate:(NSDictionary *)dateDictonary{
    [self acceptDateRequestWithDictionary:dateDictonary];
}

-(void)declineButtonClickedWithDate:(NSDictionary *)dateDictonary{
    [self declineDateRequestWithDictionary:dateDictonary];
}

-(void)sxc_startLoading{}
-(void)sxc_stopLoading{}

@end
