//
//  MainViewController.m
//  LGSideMenuControllerDemo
//
//  Created by Grigory Lutkov on 25.04.15.
//  Copyright (c) 2015 Grigory Lutkov. All rights reserved.
//

#import "MainViewController.h"
#import "PWPSideMenuTableViewController.h"
#import "PWPAppDelegate.h"
#import "PWPDownloadBackgroundImage.h"
#import "SXCUtility.h"
#import "PWPConstant.h"
#import <UIButton+WebCache.h>
#import <Google/Analytics.h>
#import <Mixpanel.h>

@interface MainViewController ()
{
    UIButton *addButton;
}

@property (strong, nonatomic) PWPSideMenuTableViewController *leftViewController;
@property (assign, nonatomic) NSUInteger type;

@end

@implementation MainViewController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
                         presentationStyle:(LGSideMenuPresentationStyle)style
                                      type:(NSUInteger)type
{
    self = [super initWithRootViewController:rootViewController];
    if (self)
    {
        
        UIStoryboard * storyboard=[UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];
        
        _leftViewController = [storyboard instantiateViewControllerWithIdentifier:@"PWPSideMenuTableViewController"];
        [self setLeftViewEnabledWithWidth:250.f
                        presentationStyle:style
                     alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnNone];
        
        self.leftViewStatusBarStyle = UIStatusBarStyleLightContent;
        self.leftViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnAll;
        self.leftViewBackgroundImage = [PWPDownloadBackgroundImage menuBackgroundImage];
        
        _leftViewController.tableView.backgroundColor = [UIColor clearColor];
        
        [_leftViewController.tableView reloadData];
        [self.leftView addSubview:_leftViewController.tableView];



        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenHeight = screenRect.size.height;
        CGFloat screenWidth = screenRect.size.width;

        NSString * string = [SXCUtility cleanString:[SXCUtility getNSObject:@"side_menu_add_imageview"]];

        if(string.length > 0){

            [[Mixpanel sharedInstance] track:@"When Skins Activated - Images/Icon"];

            NSString * url = [NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,string];

            addButton = [UIButton buttonWithType:UIButtonTypeCustom];
            addButton.frame = CGRectMake(screenWidth - 120 - 8, screenHeight - 70 , 120, 70);
            [addButton sd_setImageWithURL:[NSURL URLWithString:url] forState:UIControlStateNormal];
            [addButton addTarget:self action:@selector(advertisementClicked:) forControlEvents:UIControlEventTouchUpInside];
            addButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
            addButton.userInteractionEnabled = TRUE;
            [self.view setClipsToBounds:NO];
            [self.view addSubview:addButton];

            [self.view bringSubviewToFront:self.rootViewController.view];
        }
        
    }
    return self;
}

-(IBAction)advertisementClicked:(id)sender {

   

    NSString * string = [SXCUtility cleanString:[SXCUtility getNSObject:@"side_menu_add_link"]];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:string]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
    }
}


- (void)leftViewWillLayoutSubviewsWithSize:(CGSize)size
{
    [super leftViewWillLayoutSubviewsWithSize:size];

    if (![UIApplication sharedApplication].isStatusBarHidden && (_type == 2 || _type == 3))
        _leftViewController.tableView.frame = CGRectMake(0.f , 20.f, size.width, size.height-20.f);
    else
        _leftViewController.tableView.frame = CGRectMake(0.f , 0.f, size.width, size.height);


}


@end
