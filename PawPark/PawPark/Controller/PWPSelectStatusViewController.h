//
//  PWPSelectStatusViewController.h
//  PawPark
//
//  Created by Vibhore Sharma on 28/07/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPSelectStatusViewController : UIViewController

@property(nonatomic,strong)NSMutableString * selectedStatus;
@end
