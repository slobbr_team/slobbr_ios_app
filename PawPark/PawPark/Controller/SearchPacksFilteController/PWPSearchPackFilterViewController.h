//
//  PWPSearchPackFilterViewController.h
//  PawPark
//
//  Created by daffomac on 4/26/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWPPackFilter.h"
#import "PWPSearchPacksDelegate.h"

@interface PWPSearchPackFilterViewController : UIViewController

@property(nonatomic,strong) PWPPackFilter * packFilter;
@property(nonatomic,assign) id<PWPSearchPacksDelegate> delegate;
@end
