//
//  PWPSearchPackFilterViewController.m
//  PawPark
//
//  Created by daffomac on 4/26/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPSearchPackFilterViewController.h"
#import "UIViewController+UBRComponents.h"
#import "PWPApplicationUtility.h"
#import "SXCTextfieldWithLeftImage.h"
#import "SXCUtility.h"
#import <Google/Analytics.h>
#import "PWPBreedSelectionViewController.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPItemSelectionViewController.h"
#import "PWPConstant.h"
#import <UIButton+WebCache.h>

@interface PWPSearchPackFilterViewController()<PWPBreedSelectionDelegate, PWPCitySelectionDelegate>
{
    __weak IBOutlet UIButton *buttonAdvertisment;
    __weak IBOutlet UIView *viewContent;
    __weak IBOutlet SXCTextfieldWithLeftImage *textFieldActivityLevel;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldSociability;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldSearch;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldBreed;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldCity;
    __weak IBOutlet UIImageView * imageViewBG;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldSize;
    __weak IBOutlet UIButton *fetchButton;
    __weak IBOutlet UIImageView * imageViewFilter;

    NSString * selectedBreedIDString;
}

@end

@implementation PWPSearchPackFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    [self initView];
    
    UIImage * filterImage = [PWPDownloadBackgroundImage filterBg];
    if(filterImage != nil){
        imageViewFilter.image =  filterImage;
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Dog Search Filter Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Methods
- (IBAction)resetButtonClicked:(id)sender {
    
    _packFilter.activityLevel=nil;
    _packFilter.sociability=nil;
    _packFilter.s=@"";
    _packFilter.breed=nil;
    _packFilter.size=nil;
    _packFilter.city=nil;
    _packFilter.state=nil;
    _packFilter.zipcode=nil;

    selectedBreedIDString =nil;
    
    textfieldBreed.text=@"";
    textfieldSize.text=@"";
    textfieldSearch.text=@"";
    textFieldActivityLevel.text=@"";
    textfieldSociability.text=@"";
    textfieldCity.text=@"";

}
- (IBAction)breedButtonClicked:(id)sender {

    PWPBreedSelectionViewController * breedSelectionViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"PWPBreedSelectionViewController"];
    breedSelectionViewController.delegate=self;
    [self.navigationController pushViewController:breedSelectionViewController animated:YES];

}

- (IBAction)fetchButtonClicked:(id)sender {
    
    
    _packFilter.s=textfieldSearch.text;
    _packFilter.activityLevel=[PWPApplicationUtility pawAcitivityLevelCorrespondingToActivityLevel:textFieldActivityLevel.text];
    _packFilter.sociability=[PWPApplicationUtility pawSociabilityCorrespondingToSociability:textfieldSociability.text];
    _packFilter.breed=selectedBreedIDString;
    _packFilter.size=[PWPApplicationUtility pawSizeCorrespondingToSize:textfieldSize.text];
    
    
    if(_delegate&&[_delegate respondsToSelector:@selector(applyFiltersAndRefreshSearch:)]){
        [_delegate applyFiltersAndRefreshSearch:_packFilter];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sociabilityButtonClicked:(id)sender {
    
    NSArray * sociabilityArray=[PWPApplicationUtility pawSociability] ;
    
    [self sxc_showActionSheetWithTitle:@"Select Sociability" pickerWithRows:[PWPApplicationUtility pawSociability] itemSelectedAtIndex:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        textfieldSociability.text=sociabilityArray[selectedIndex];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
    

    
}
- (IBAction)activityLevelButtonClicked:(id)sender {
    
    
    NSArray * activityLevelArray=[PWPApplicationUtility pawActivityLevel] ;
    
    [self sxc_showActionSheetWithTitle:@"Select Activity Level" pickerWithRows:activityLevelArray itemSelectedAtIndex:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        textFieldActivityLevel.text=activityLevelArray[selectedIndex];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (IBAction)fetchPackButonClicked:(id)sender {
    
    if(_delegate&&[_delegate respondsToSelector:@selector(applyFiltersAndRefreshSearch:)]){
        [_delegate applyFiltersAndRefreshSearch:_packFilter];
    }
}

- (IBAction)sizeButtonClicked:(id)sender {

    NSArray * sizeArray=[PWPApplicationUtility pawSizesForSearch] ;
    
    [self sxc_showActionSheetWithTitle:@"Select Size" pickerWithRows:sizeArray itemSelectedAtIndex:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        textfieldSize.text=sizeArray[selectedIndex];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

-(IBAction)cityButtonClicked:(id)sender {


    NSString * selectedString = @"";
    if (self.packFilter.zipcode != nil){
        selectedString= self.packFilter.zipcode;
    }
    else if (self.packFilter.city != nil && self.packFilter.stateCode != nil && self.packFilter.state != nil){
        selectedString = [NSString stringWithFormat:@"%@(%@)",self.packFilter.city,self.packFilter.state];
    }
    else if (self.packFilter.state != nil && self.packFilter.stateCode != nil){
        selectedString= self.packFilter.state;
    }

    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    PWPItemSelectionViewController * citySelectedViewController = [storyboard instantiateViewControllerWithIdentifier:@"PWPItemSelectionViewController"];
    citySelectedViewController.delegate = self;
    citySelectedViewController.selectedZipCode = self.packFilter.zipcode;
    citySelectedViewController.selectedState = self.packFilter.state;
    citySelectedViewController.selectedCity = self.packFilter.city;
    citySelectedViewController.selectedStateCode = self.packFilter.stateCode;
    citySelectedViewController.selectedItem = selectedString;

    [self.navigationController pushViewController:citySelectedViewController animated:YES];
}



#pragma mark - Buisness Methods

-(void)initView{
    
    viewContent.layer.cornerRadius=5.0f;
    fetchButton.layer.cornerRadius=5.0f;
    
    [self sxc_navigationBarCustomizationWithTitle:@"FILTER"];
    [self sxc_customizeBackButton];
    
    if(_packFilter==nil){
        _packFilter=[[PWPPackFilter alloc] init];
        
        [self resetButtonClicked:nil];
    }
    else{
        textfieldSearch.text=_packFilter.s;
        
        if([SXCUtility isEmptyString:_packFilter.activityLevel]){
            textFieldActivityLevel.text=@"";
        }else{
            textFieldActivityLevel.text=[PWPApplicationUtility pawActivityLevel][_packFilter.activityLevel.integerValue];
        }
        
        if([SXCUtility isEmptyString:_packFilter.size]){
            textfieldSize.text=@"";
        }else{
            textfieldSize.text=[PWPApplicationUtility pawSizesForSearch][_packFilter.size.integerValue];
        }
        
        if([SXCUtility isEmptyString:_packFilter.sociability]){
            textfieldSociability.text=@"";
        }else{
            textfieldSociability.text=[PWPApplicationUtility pawSociability][_packFilter.sociability.integerValue];
        }


        NSString * selectedString = @"";
        if (self.packFilter.zipcode != nil){
            selectedString= self.packFilter.zipcode;
        }
        else if (self.packFilter.city != nil && self.packFilter.stateCode != nil && self.packFilter.state != nil){
            selectedString = [NSString stringWithFormat:@"%@(%@)",self.packFilter.city,self.packFilter.state];
        }
        else if (self.packFilter.state != nil && self.packFilter.stateCode != nil){
            selectedString= self.packFilter.state;
        }


        if([SXCUtility isEmptyString:selectedString]){
            textfieldCity.text=@"";
        }else{
            textfieldCity.text=selectedString;
        }


        if(![SXCUtility isNotNull:_packFilter.breed]){
            textfieldBreed.text=@"";
        }else{
            
            NSPredicate * predicate=[NSPredicate predicateWithFormat:@"id=%@",_packFilter.breed];
            NSMutableArray * breedArray=[[SXCUtility cleanArray:[SXCUtility getNSObject:@"BreedList"]] mutableCopy];
            NSArray * filteredArray=[breedArray filteredArrayUsingPredicate:predicate];
            if(filteredArray.count>0){
                textfieldBreed.text=filteredArray[0][@"name"];
            }
            else{
                textfieldBreed.text=@"";
            }
            
        }
        selectedBreedIDString = _packFilter.breed;
    }


    NSString * string = [SXCUtility cleanString:[SXCUtility getNSObject:@"filter_link"]];

    if(string.length > 0){
        [buttonAdvertisment addTarget:self action:@selector(advertisementClicked:) forControlEvents:UIControlEventTouchUpInside];
        buttonAdvertisment.userInteractionEnabled = TRUE;
    }
    else {
        buttonAdvertisment.userInteractionEnabled = FALSE;
    }

}

-(IBAction)advertisementClicked:(id)sender {

    if([PWPDownloadBackgroundImage isSkinApplied] == false){
        return;
    }
    
    NSString * string = [SXCUtility cleanString:[SXCUtility getNSObject:@"filter_link"]];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:string]]) {

        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];

        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Filter Link"
                                                              action:@"Button press"
                                                               label:@"Clicked"
                                                               value:nil] build]];



        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
    }
}


-(void)breedSelected:(NSMutableDictionary *)breedDictinary {
    textfieldBreed.text=breedDictinary[@"name"];
    selectedBreedIDString=breedDictinary[@"id"];
}


-(void)selectedCity:(NSString *)selectedCity selectedState:(NSString *)selectedState selectedStateCode:(NSString *)selectedStateCode selectedZipCode:(NSString *)selectedZipCode type:(PWPSelectionType)type selectedString:(NSString *)selectedString
{
    textfieldCity.text = selectedString;

    self.packFilter.city = selectedCity;
    self.packFilter.state = selectedState;
    self.packFilter.zipcode = selectedZipCode;
    self.packFilter.stateCode = selectedStateCode;
}
@end
