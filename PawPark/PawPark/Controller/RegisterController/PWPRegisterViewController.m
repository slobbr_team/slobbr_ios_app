//
//  PWPRegisterViewController.m
//  PawPark
//
//  Created by daffomac on 4/23/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//


#import "PWPRegisterViewController.h"
#import "UIViewController+UBRComponents.h"
#import "SXCTextfieldWithLeftImage.h"
#import "PWPService.h"
#import "SXCUtility.h"
#import "PWPAppDelegate.h"
#import "PWPZipCodeSelectionViewController.h"
#import <Google/Analytics.h>
#import "PWPDownloadBackgroundImage.h"
#import <Mixpanel/Mixpanel.h>
#import "PWPApplicationUtility.h"

@interface PWPRegisterViewController()<PWPZipCodeSelectionDelegate>{

    __weak IBOutlet UIButton *buttonRegister;

    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldCommunityCode;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldZipCode;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldUsername;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldEmailAddress;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldPassword;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldNewPassword;

    __weak IBOutlet UIButton *buttonrescue;
    __weak IBOutlet UIImageView * imageViewBG;

    NSString * selectZipCodeValue;
}
- (IBAction)zipCodeClicked:(id)sender;

- (IBAction)registerButtonClicked:(id)sender;
@end


@implementation PWPRegisterViewController

-(void)viewDidLoad
{
    [super viewDidLoad];

    [self sxc_navigationBarCustomizationWithTitle:@"REGISTER"];
    [self sxc_customizeBackButton];

    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];

    textfieldZipCode.backgroundColor=[UIColor clearColor];
    textfieldZipCode.textColor=[UIColor whiteColor];
    [textfieldZipCode setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];

    textfieldUsername.backgroundColor=[UIColor clearColor];
    textfieldUsername.textColor=[UIColor whiteColor];
    [textfieldUsername setValue:[UIColor whiteColor]
                     forKeyPath:@"_placeholderLabel.textColor"];

    textfieldEmailAddress.backgroundColor=[UIColor clearColor];
    textfieldEmailAddress.textColor=[UIColor whiteColor];
    [textfieldEmailAddress setValue:[UIColor whiteColor]
                         forKeyPath:@"_placeholderLabel.textColor"];

    textfieldPassword.backgroundColor=[UIColor clearColor];
    textfieldPassword.textColor=[UIColor whiteColor];
    [textfieldPassword setValue:[UIColor whiteColor]
                     forKeyPath:@"_placeholderLabel.textColor"];

    textfieldNewPassword.backgroundColor=[UIColor clearColor];
    textfieldNewPassword.textColor=[UIColor whiteColor];
    [textfieldNewPassword setValue:[UIColor whiteColor]
                        forKeyPath:@"_placeholderLabel.textColor"];
    
    textfieldCommunityCode.backgroundColor=[UIColor clearColor];
    textfieldCommunityCode.textColor=[UIColor whiteColor];
    [textfieldCommunityCode setValue:[UIColor whiteColor]
                        forKeyPath:@"_placeholderLabel.textColor"];
    

    buttonRegister.layer.borderColor = [UIColor whiteColor].CGColor;
    buttonRegister.layer.borderWidth = 1.0f;

    buttonrescue.hidden=!_isEventMode;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Settings Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}



- (IBAction)zipCodeClicked:(id)sender {

    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];

    PWPZipCodeSelectionViewController * zipCodeViewController=[storyBoard instantiateViewControllerWithIdentifier:@"PWPZipCodeSelectionViewController"];
    zipCodeViewController.delegate=self;
    [self.navigationController pushViewController:zipCodeViewController animated:YES];

    /*
     NSArray * zipcodeArray=[[SXCUtility cleanArray:[SXCUtility getNSObject:@"newyorkzipcode"]] valueForKeyPath:@"title"];

     [self sxc_showActionSheetWithTitle:@"Select Zipcode" pickerWithRows:zipcodeArray itemSelectedAtIndex:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {

     textfieldZipCode.text=selectedValue;

     NSDictionary * dictionary=[SXCUtility cleanArray:[SXCUtility getNSObject:@"newyorkzipcode"]][selectedIndex];

     selectZipCodeValue=[[NSString alloc] initWithString:[dictionary[@"value"] description]];

     } cancelBlock:^(ActionSheetStringPicker *picker) {

     } origin:self.view];
     */

}
-(void)zipCodeSelected:(NSMutableDictionary *)zipCodeDictionary
{
    textfieldZipCode.text=zipCodeDictionary[@"title"];
    selectZipCodeValue=zipCodeDictionary[@"value"];
}

- (IBAction)registerButtonClicked:(id)sender {
    [self registerUser];
}

- (IBAction)rescueButtonClicked:(id)sender {
    buttonrescue.selected=!buttonrescue.selected;
}

#pragma mark - BL Methods
-(NSDictionary *)getRequestDictionaryForRegister
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] init];
    [requestDictionary setValue:textfieldEmailAddress.text forKey:@"email"];
    [requestDictionary setValue:textfieldPassword.text forKey:@"plainPassword"];
    [requestDictionary setValue:textfieldUsername.text forKey:@"username"];
    [requestDictionary setValue:textfieldZipCode.text forKey:@"zipcode"];

    if(_isEventMode){
        [requestDictionary setValue:[NSNumber numberWithBool:buttonrescue.selected] forKey:@"shelter"];
    }

    return  requestDictionary;
}
-(NSDictionary*)loginRequestDictionary
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] init];
    [requestDictionary setValue:textfieldUsername.text forKey:@"username"];
    [requestDictionary setValue:textfieldPassword.text forKey:@"password"];

    return requestDictionary;

}

-(BOOL)isValid
{
    NSScanner *scanner = [NSScanner scannerWithString:textfieldZipCode.text];
    BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];


    if(![SXCUtility isValidEmail:textfieldEmailAddress.text]){
        [self sxc_showErrorViewWithMessage:@"Please enter a valid email address."];
        return false;
    }
    else if([SXCUtility cleanString:textfieldUsername.text].length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter your username."];
        return false;
    }
    else if(textfieldPassword.text.length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter password."];
        return false;
    }
    else if(textfieldNewPassword.text.length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter the confirm password."];
        return false;
    }
    else if(textfieldNewPassword.text.length<6){
        [self sxc_showErrorViewWithMessage:@"Password should be greater than 5 character."];
        return false;
    }
    else if(![textfieldPassword.text isEqualToString:textfieldNewPassword.text]){
        [self sxc_showErrorViewWithMessage:@"Password mismatch. Please try again."];
        return false;
    }

    else if(textfieldNewPassword.text.length<6){
        [self sxc_showErrorViewWithMessage:@"Confirm Password should be greater than 5 character."];
        return false;
    }
    else if(textfieldZipCode.text.length<5 && !isNumeric){
        [self sxc_showErrorViewWithMessage:@"Invalid Zipcode. Please try again."];
        return false;
    }

    return true;
}



#pragma mark - Service Methods

-(void)registerUser
{
    if([self isValid]){

        [self startLoader];

        [[PWPService sharedInstance] registerWithRequestDictionary:(id)[self getRequestDictionaryForRegister] withSuccessBlock:^(id responseDicitionary) {

            [self loginUser];

            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];

            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Register"     // Event category (required)
                                                                  action:@"button_press"  // Event action (required)
                                                                   label:@"Success"          // Event label
                                                                   value:nil] build]];    // Event valu



        } withErrorBlock:^(NSError *error) {

            if(error.code==400){

                [self sxc_showErrorViewWithMessage:@"Email Address or User-Name already exits. Please try with other one."];

            }
            else{
                [self sxc_showErrorViewWithMessage:error.domain];
            }

            [self dismissLoader];

        }];
    }
}

-(void)loginUser{

    [[PWPService sharedInstance] loginWithRequestDictionary:(id)[self loginRequestDictionary] withSuccessBlock:^(id responseDicitionary) {
       
        if([responseDicitionary valueForKey:@"apikey"]){

            [SXCUtility saveNSObject:responseDicitionary[@"apikey"] forKey:@"apikey"];

            [SXCUtility saveNSObject:@YES forKey:@"isLocationHelp"];
            [SXCUtility saveNSObject:@NO forKey:@"isStaHelpOff"];
            [SXCUtility saveNSObject:@YES forKey:@"HomeParkEdit"];

            [[Mixpanel sharedInstance] track:@"Registration_completed"];
            
            
            if (textfieldCommunityCode.text.length > 0){
                [self communityCode];
            } else {
                [self dismissLoader];
                [self fetchUserProfile];
            }
        }
        else{
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:@"Incorrect login. Please enter correct credentials."];
        }

    } withErrorBlock:^(NSError *error) {

        [self dismissLoader];
        [self sxc_showErrorViewWithMessage:error.domain];
        
    }];
}

-(void)communityCode {

    [[PWPService sharedInstance] joinCommunityId:textfieldCommunityCode.text WithRequestDictionary:nil withData:nil withSuccessBlock:^(id responseDicitionary) {

        [self fetchUserProfile];
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        
        [self fetchUserProfile];
        
        [self sxc_showErrorViewWithMessage:@"Incorrect community code. Please enter correct community in setting screen."];
    }];

}


-(void)sxc_stopLoading
{
}
-(void)sxc_startLoading{
    
}

-(void)fetchUserProfile {
    
    [self startLoader];
    
    [[PWPService sharedInstance] currentUserProfileWithRequestDictionary:nil withSuccessBlock:^(id responseDicitionary) {
        [PWPApplicationUtility saveCurrentUserProfileDictionary:responseDicitionary];
        
        [self dismissLoader];
        
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];
        UIViewController * viewController = [storyboard instantiateViewControllerWithIdentifier:@"PWPSplashViewController"];
        
        PWPAppDelegate * appdelagte=[PWPAppDelegate sharedInstance];
        [appdelagte switchToSplashController:viewController];
        
    } withErrorBlock:^(NSError *error) {
        
        [self dismissLoader];
        
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];
        UIViewController * viewController = [storyboard instantiateViewControllerWithIdentifier:@"PWPSplashViewController"];
        
        PWPAppDelegate * appdelagte=[PWPAppDelegate sharedInstance];
        [appdelagte switchToSplashController:viewController];
        
    }];
    
}

@end
