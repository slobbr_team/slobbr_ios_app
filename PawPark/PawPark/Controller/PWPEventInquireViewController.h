//
//  PWPEventInquireViewController.h
//  PawPark
//
//  Created by admin on 9/13/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPEventInquireViewController : UIViewController
@property(nonatomic,strong)NSString * dogIdString;
@property(nonatomic,strong)NSString * shelterUserIdString;
@property(nonatomic,strong)NSString * dogNameString;

@end
