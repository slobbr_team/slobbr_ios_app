//
//  PWPEventParkViewController.m
//  PawPark
//
//  Created by admin on 9/13/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPEventParkViewController.h"
#import "PWPService.h"
#import "PWPSearchPackCellTableViewCell.h"
#import "PWPSearchParkTableViewCell.h"
#import "PWPParkCheckInViewController.h"
#import "PWPParkCheckedInDogsViewController.h"
#import "PWPApplicationUtility.h"
#import "PWPEventInquireViewController.h"
#import <Google/Analytics.h>

#import "UIViewController+UBRComponents.h"
#import "PWPDownloadBackgroundImage.h"
#import "SXCUtility.h"

@interface PWPEventParkViewController ()<PWPSearckPackDelegate,UITableViewDataSource,UITableViewDelegate,PWPParkCellDelegate,PWPParkCheckInDelegae>
{
    __weak IBOutlet UIImageView * imageViewBG;
    
    NSMutableDictionary * eventDictionary;
    NSMutableArray * dogs;
    
    NSInteger selectedIndex;
}

@property (weak, nonatomic) IBOutlet UITableView *tableViewEvent;
@end

@implementation PWPEventParkViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self initView];
    [self showLoaderWithText:@"Loading..."];
    [self eventParkDetails];
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Event Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"segure_inquire"]){
        PWPEventInquireViewController * inquireViewController = segue.destinationViewController;
        inquireViewController.dogIdString=sender[@"id"];
        inquireViewController.dogNameString=sender[@"name"];
        inquireViewController.shelterUserIdString=sender[@"user"][@"id"];
    }
}

#pragma mark - Buisness methods
-(void)initView{
    [self sxc_customizeBackButton];
    [self sxc_navigationBarCustomizationWithTitle:@"TODAY’S EVENT"];

    selectedIndex=-1;
    
    _tableViewEvent.delegate=self;
    _tableViewEvent.dataSource=self;
}

#pragma mark - Service Methods
-(void)eventParkDetails
{
    [[PWPService sharedInstance] eventParkDetailswithSuccessBlock:^(id response) {
        
        eventDictionary=[SXCUtility cleanDictionary:response].mutableCopy;
        
        dogs=[[NSMutableArray alloc] init];
        
        for(NSDictionary * checkedInDictionary in [SXCUtility cleanArray:eventDictionary[@"active_checkins"]]){
            [dogs addObjectsFromArray:[SXCUtility cleanArray:checkedInDictionary[@"dogs"]]];
        }
        
        [_tableViewEvent reloadData];
        [self dismissLoader];
        
    } withErrorBlock:^(NSError * error) {
        [self sxc_showErrorViewWithMessage:error.domain];
        [self dismissLoader];
    }];
}

#pragma mark - TableView Delegate and DataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row==0){
        PWPSearchParkTableViewCell * tableViewCell =[tableView dequeueReusableCellWithIdentifier:@"PWPParkListiningCell" forIndexPath:indexPath];
        
        [tableViewCell bindValueWithComponentsWithData:(id)eventDictionary reloadData:(indexPath.row == selectedIndex)];
        [tableViewCell setDelegate:self];
        return tableViewCell;
    }
    else{
        
        PWPSearchPackCellTableViewCell * tableViewCell =[tableView dequeueReusableCellWithIdentifier:@"PWPSearchPawsCells"];
        tableViewCell.clipsToBounds=YES;
        tableViewCell.delegate=self;
        tableViewCell.isFromCheckInUser=YES;
        [tableViewCell configureCellWithData:dogs[indexPath.row-1]];
       
        
        return tableViewCell;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (dogs)?(dogs.count+1):0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0){
        
        if(indexPath.row==selectedIndex){
            return [self getHeightOfParkBasedOnAmentis:eventDictionary];
        }
        return 124.0f;
    }
    else
    {
        
        NSDictionary * kuttaDictionary = dogs[indexPath.row -1];
        
        if([kuttaDictionary valueForKey:@"special_needs"] && [[kuttaDictionary valueForKey:@"special_needs"] isKindOfClass:[NSDictionary class]] && (dogs.count >=indexPath.row) &&indexPath.row==selectedIndex)
        {
            return 195.0f;
        }
        else if(indexPath.row==selectedIndex){
            return 164.0f;
        }
        else{
            return 95.0f;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0){
        
        if(indexPath.row==selectedIndex){
            return [self getHeightOfParkBasedOnAmentis:eventDictionary];
        }
        return 124.0f;
    }
    else
    {
        NSDictionary * kuttaDictionary = dogs[indexPath.row -1];
        
        if([kuttaDictionary valueForKey:@"special_needs"] && [[kuttaDictionary valueForKey:@"special_needs"] isKindOfClass:[NSDictionary class]] && (dogs.count >=indexPath.row) &&indexPath.row==selectedIndex)
        {
            return 195.0f;
        }
        else if(indexPath.row==selectedIndex){
            return 164.0f;
        }
        else{
            return 95.0f;
        }
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger previousIndex = selectedIndex;
    if(indexPath.row!=selectedIndex){
        selectedIndex=indexPath.row;
    }
    else{
        selectedIndex=-1;
    }
    
    NSMutableArray * indexes =[[NSMutableArray alloc] init];
    if(previousIndex != -1){
        [indexes addObject:[NSIndexPath indexPathForRow:previousIndex inSection:0]];
    }
    if(selectedIndex != -1){
        [indexes addObject:[NSIndexPath indexPathForRow:selectedIndex inSection:0]];
    }
    
    [tableView reloadRowsAtIndexPaths:indexes
                     withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(CGFloat)getCellHeightWithBioText:(NSString *)bioText
{
    if(bioText.length==0 || bioText ==nil){
        return 195.0f;
    }
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    
    CGSize textSize = [bioText sizeWithFont:[UIFont fontWithName:@"Roboto-Light" size:12.0f] constrainedToSize:CGSizeMake(screenWidth-25, 20000) lineBreakMode: NSLineBreakByWordWrapping]; //Assuming your width is 240
    
    return 195.0f + textSize.height;

}


-(CGFloat)getHeightOfParkBasedOnAmentis:(NSDictionary *)parkDictionary
{
    
    NSMutableString * amentiesString=[[NSMutableString alloc] init];
    
    for(NSString * amenity in parkDictionary[@"amenities"]){
        if(amentiesString.length>0){
            [amentiesString appendString:@"\n"];
        }
        [amentiesString appendString:amenity];
    }
    
    CGFloat amentiesHeight=[self getHeightOfLabelWithText:amentiesString withFont:[UIFont fontWithName:@"Roboto-Regular" size:11.0f]];
    
    if(amentiesHeight==0){
        return 173;
    }
    return 173+amentiesHeight;
}


-(CGFloat)getHeightOfLabelWithText:(NSString *)text withFont:(UIFont *)font
{
    UILabel *gettingSizeLabel = [[UILabel alloc] init];
    gettingSizeLabel.font = font;
    gettingSizeLabel.text = text;
    gettingSizeLabel.numberOfLines = 0;
    gettingSizeLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width-16, 9999);
    
    CGSize expectSize = [gettingSizeLabel sizeThatFits:maximumLabelSize];
    return expectSize.height;
}


#pragma mark - PWPSearckPackDelegate Methods
-(void)optionPackClickedWithData:(id)data
{
    
}
-(void)option4PackClickedWithData:(id)data
{
    [self performSegueWithIdentifier:@"segure_inquire" sender:data];
}

#pragma mark - PWPParkDelegate
-(void)parkOptionClickedWithData:(NSDictionary *)dictionary
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PWPParkCheckInViewController * viewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckInViewController"];    viewController.parkDictionary=[[NSMutableDictionary alloc] initWithDictionary:dictionary];
    viewController.delegate=self;
    
    [self.navigationController pushViewController:viewController animated:YES];
}
-(void)parkOption2ClickedWithData:(NSDictionary *)dictionary
{
    NSString * checkInDogsCount = [SXCUtility cleanString:[[dictionary valueForKey:@"active_dogs_count"] description]];

    if(checkInDogsCount.integerValue>0){
        
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PWPParkCheckedInDogsViewController * parkCheckInDogsViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckedInDogsViewController"];
        parkCheckInDogsViewController.placeId = [SXCUtility cleanString:[[dictionary valueForKey:@"id"] description]];
        
        [self.navigationController pushViewController:parkCheckInDogsViewController animated:YES];
    }
}
-(void)parkOption3ClickedWithData:(NSDictionary *)dictionary
{
    [self startLoader];
    
    [self startLoader];
    
    if([SXCUtility cleanInt:dictionary[@"is_favourite"]] ==1){
        
        [[PWPService sharedInstance] deleteHomePark:dictionary[@"id"] withSuccessBlock:^(id response) {
            [self dismissLoader];
            
            
            if([SXCUtility cleanInt:eventDictionary[@"is_favourite"]] ==1){
                eventDictionary[@"is_favourite"]=@0;
            }
            else{
                eventDictionary[@"is_favourite"]=@1;
            }
            
            
            [_tableViewEvent reloadData];
            [self sxc_showSuccessWithMessage:@"Place has been removed from 'Favorite' Places."];
            
        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:error.domain];
        }];
        
    }
    else{
        
        
        [[PWPService sharedInstance] homePark:dictionary[@"id"] withSuccessBlock:^(id response) {
            [self dismissLoader];
            
            
            if([SXCUtility cleanInt:eventDictionary[@"is_favourite"]] ==1){
                eventDictionary[@"is_favourite"]=@0;
            }
            else{
                eventDictionary[@"is_favourite"]=@1;
            }
            
            [_tableViewEvent reloadData];
            [self sxc_showSuccessWithMessage:@"Place has been marked as 'Favorite' Place."];
            
        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:error.domain];
        }];
    }

    
    [[PWPService sharedInstance] homePark:dictionary[@"id"] withSuccessBlock:^(id response) {
        [self dismissLoader];
        
        NSMutableDictionary * userDictionary=[[PWPApplicationUtility getCurrentUserProfileDictionary] mutableCopy];
        [userDictionary setValue:dictionary forKey:@"place"];
        [PWPApplicationUtility saveCurrentUserProfileDictionary:userDictionary];
        
        [_tableViewEvent reloadData];
        [self sxc_showSuccessWithMessage:@"Park has been marked as 'Home' Park."];
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        [self sxc_showErrorViewWithMessage:error.domain];
    }];
}

-(void)inviteOptionClicked:(NSDictionary *)dictionary{}
-(void)parkQuickCheckInClicked:(NSDictionary *)dictionary{}

-(void)successFullCheckedInWithDictionary:(NSMutableDictionary *)data WithDogs:(NSMutableArray *)dogsArray{
    
    [self eventParkDetails];
}


-(void)sxc_startLoading{}
-(void)sxc_stopLoading{}
@end
