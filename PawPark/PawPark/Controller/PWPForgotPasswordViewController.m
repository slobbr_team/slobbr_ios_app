//
//  PWPForgotPasswordViewController.m
//  PawPark
//
//  Created by Samarth Singla on 22/09/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPForgotPasswordViewController.h"
#import "UIColor+PWPColor.h"
#import "UIViewController+UBRComponents.h"
#import "SXCTextfieldWithLeftImage.h"
#import "SXCUtility.h"
#import "PWPService.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPForgotPasswordViewController ()
{
    __weak IBOutlet UIImageView * imageViewBG;

}


@property (weak, nonatomic) IBOutlet SXCTextfieldWithLeftImage *textfieldUserName;
@property (weak, nonatomic) IBOutlet UIButton *buttonLogin;

@end

@implementation PWPForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self sxc_customizeBackButton];
    
    self.textfieldUserName.backgroundColor = [UIColor clearColor];

    [self.textfieldUserName setValue:[UIColor whiteColor]
                  forKeyPath:@"_placeholderLabel.textColor"];
    self.textfieldUserName.textColor =[UIColor whiteColor];
    
    [self sxc_navigationBarCustomizationWithTitle:@"FORGOT PASSWORD"];
    [self.navigationItem setHidesBackButton:YES];
    
    _buttonLogin.layer.borderColor = [UIColor whiteColor].CGColor;
    _buttonLogin.layer.borderWidth = 0.5f;
    
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(BOOL)isValid{

    
    NSString * userName =  [SXCUtility trimString:self.textfieldUserName.text];
    if(userName.length>0 && ![SXCUtility isValidEmail:userName]){
        [self sxc_showErrorViewWithMessage:@"Please enter valid email address."];
        return false;
    }
    else if(userName.length == 0){
        [self sxc_showErrorViewWithMessage:@"Please enter email address."];
        return false;
    }
    else{
        return true;
    }
}

-(NSMutableDictionary *)requestDictonary {

    NSMutableDictionary * requestDictionary = [[NSMutableDictionary alloc] init];
    
    [requestDictionary setValue:[SXCUtility trimString:self.textfieldUserName.text] forKey:@"username"];

    return requestDictionary;
}


-(IBAction)submitButtonClicked:(id)sender {

    if([self isValid]){
        [self startLoader];
 
        [[PWPService sharedInstance] forgotPasswordWithDictionary:[self requestDictonary] WithSuccessBlock:^(id response) {
            [self dismissLoader];
            [self sxc_showSuccessWithMessage:@"An email has been sent to your email. It contains a link you must click to reset your password."];
            [self.navigationController popViewControllerAnimated:YES];
            
        } withErrorBlock:^(NSError *error) {
            [self dismissLoader];
            
            [self sxc_showErrorViewWithMessage:[NSString stringWithFormat:@"Email address %@ does not exist.",self.textfieldUserName.text]];
        }];
        
    }
}

-(void)sxc_stopLoading{
}

-(void)sxc_startLoading{

}


@end
