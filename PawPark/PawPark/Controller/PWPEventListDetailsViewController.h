//
//  PWPEventListDetailsViewController.h
//  PawPark
//
//  Created by HupendraRaghuwanshi on 07/03/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PWPEventListDetailsViewController : UIViewController
@property(nonatomic,strong)NSDictionary * eventDictionary;
@property (weak, nonatomic) IBOutlet UILabel *labelEventName;
@property (weak, nonatomic) IBOutlet UILabel *labelEventDate;
@property (weak, nonatomic) IBOutlet UILabel *labelEventAddress;

@property (weak, nonatomic) IBOutlet UITextView *labelDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewEvent;
@property (weak, nonatomic) IBOutlet UILabel *labelOwnerName;
@property (weak, nonatomic) IBOutlet UILabel *labelEmail;
@property (weak, nonatomic) IBOutlet UILabel *labelPhone;
@property (weak, nonatomic) IBOutlet UILabel *labelUrl;
@property (weak, nonatomic) IBOutlet UILabel *labelUrl2;
@property (weak, nonatomic) IBOutlet UILabel *title1;
@property (weak, nonatomic) IBOutlet UILabel *title2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fbWidthConstraints;

@end
