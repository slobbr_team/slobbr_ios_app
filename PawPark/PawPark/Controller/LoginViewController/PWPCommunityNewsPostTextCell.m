//
//  PWPCommunityNewsPostTextCell.m
//  PawPark
//
//  Created by Samarth Singla on 14/08/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPCommunityNewsPostTextCell.h"
#import "SXCUtility.h"
#import <UIImageView+WebCache.h>
#import "PWPConstant.h"


@interface PWPCommunityNewsPostTextCell(){
    NSDictionary * theData;
    UITapGestureRecognizer * tapGestureGecognizer;
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPost;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintImageHeight;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewUser;
@property (weak, nonatomic) IBOutlet UILabel *labelUserName;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UIButton *buttonComment;

@end


@implementation PWPCommunityNewsPostTextCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    tapGestureGecognizer= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buttonCommentClicked:)];
    [self.imageViewPost addGestureRecognizer:tapGestureGecognizer];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)commentButtonClicked:(id)sender {
}

-(void)configureWithData:(NSDictionary *)data {

    theData = data;
    
    NSString * dateString = [SXCUtility cleanString:[data valueForKey:@"created_at"]];
    
    NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
    [outputDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [outputDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    
    NSDate * date = [outputDateFormatter dateFromString:dateString];
    [outputDateFormatter setDateFormat:@"hh:mm a dd MMM yyyy"];
   
    _labelDate.text = [outputDateFormatter stringFromDate:date];
    
    [self.buttonComment setTitle:[NSString stringWithFormat:@"  %@ Comment(s)",[SXCUtility cleanString:[[data valueForKey:@"comment_count"] description]]] forState:UIControlStateNormal];
    
    self.labelTitle.text = [SXCUtility cleanString:[data valueForKey:@"name"]];
    self.labelMessage.text = [SXCUtility cleanString:[data valueForKey:@"body"]];
    
    NSDictionary * userData = [SXCUtility cleanDictionary:[data valueForKey:@"user"]];
    
    self.labelUserName.text = [SXCUtility cleanString:[userData valueForKey:@"username"]];
    
    NSDictionary * dogThumb = [SXCUtility cleanDictionary:[userData valueForKey:@"thumbs"]];
    NSString * postDogImage = [SXCUtility cleanString:[dogThumb valueForKey:@"w400"]];
    if(postDogImage.length > 0){
        [self.imageViewUser sd_setImageWithURL:[NSURL URLWithString:postDogImage] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
    }
    else {
        self.imageViewUser.image = [UIImage imageNamed:@"img_dog_sample"];
    }
    
    NSDictionary * thumbs = [SXCUtility cleanDictionary:[data valueForKey:@"thumbs"]];
    
    NSString * postImage = [SXCUtility cleanString:[thumbs valueForKey:@"w1000"]];
    if(postImage.length > 0){
        [self.imageViewPost sd_setImageWithURL:[NSURL URLWithString:postImage] placeholderImage:[UIImage imageNamed:@"default_park"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
        self.layoutConstraintImageHeight.constant = 300.0;
    }
    else {
        
        self.imageViewPost.image = [UIImage imageNamed:@"default_park"];
        
        self.layoutConstraintImageHeight.constant = 0.0f;
    }
    
    
}

- (IBAction)buttonCommentClicked:(id)sender {
    [self.delegate commentsClickedWithData:theData];
}

@end
