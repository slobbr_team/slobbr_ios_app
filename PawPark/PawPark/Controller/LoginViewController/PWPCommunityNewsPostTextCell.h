//
//  PWPCommunityNewsPostTextCell.h
//  PawPark
//
//  Created by Samarth Singla on 14/08/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPCommunityPostDelegate <NSObject>
-(void)commentsClickedWithData:(NSDictionary *)data;
@end


@interface PWPCommunityNewsPostTextCell : UITableViewCell

@property(nonatomic,weak)id<PWPCommunityPostDelegate>delegate;


-(void)configureWithData:(NSDictionary *)data;

@end
