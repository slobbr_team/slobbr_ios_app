//
//  ViewController.m
//  PawPark
//
//  Created by xyz on 05/04/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "LoginViewController.h"
#import "PWPAppDelegate.h"
#import "PWPService.h"
#import "UIViewController+UBRComponents.h"
#import "SXCUtility.h"
#import <TwitterKit/TwitterKit.h>
#import <Google/Analytics.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKAccessToken.h>
#import "PWPRegisterViewController.h"
#import "PWPForgotPasswordViewController.h"
#import "PWPDownloadBackgroundImage.h"
#import <Mixpanel/Mixpanel.h>
#import "PWPApplicationUtility.h"

@interface LoginViewController ()
{

    __weak IBOutlet UIButton *buttonLogin;
    __weak IBOutlet UIButton *buttonTwitter;
    __weak IBOutlet UIButton *buttonMail;
    __weak IBOutlet UIButton *buttonFacebook;
    __weak IBOutlet UIImageView * imageViewBG;
    __weak IBOutlet UITextField *textfieldEmail;
    __weak IBOutlet UITextField *textfieldPassword;

    
}
-(IBAction)loginBtnClicked:(id)sender;
- (IBAction)twitterLoginClicked:(id)sender;
- (IBAction)facebookLoginClicked:(id)sender;
- (IBAction)forgotPasswordClicked:(id)sender;


@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];

    [[Mixpanel sharedInstance] track:@"Login Visit"];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"segue_register_event_mode"]){
        PWPRegisterViewController * registerViewController = segue.destinationViewController;
        registerViewController.isEventMode=YES;
    }
    else if([segue.identifier isEqualToString:@"segue_registration"]){
        [[Mixpanel sharedInstance] track:@"Registration_clicked"];
    }
}

#pragma mark - Buisness Methods
-(void)initView {
    buttonFacebook.imageView.contentMode=UIViewContentModeScaleAspectFit;
    buttonMail.imageView.contentMode=UIViewContentModeScaleAspectFit;
    buttonTwitter.imageView.contentMode=UIViewContentModeScaleAspectFit;
    buttonLogin.imageView.contentMode=UIViewContentModeScaleAspectFit;
 
    textfieldEmail.backgroundColor = [UIColor clearColor];
    textfieldPassword.backgroundColor = [UIColor clearColor];
   
    [textfieldEmail setValue:[UIColor whiteColor]
        forKeyPath:@"_placeholderLabel.textColor"];

    [textfieldPassword setValue:[UIColor whiteColor]
                  forKeyPath:@"_placeholderLabel.textColor"];
    
    textfieldPassword.textColor =[UIColor whiteColor];
    textfieldEmail.textColor =[UIColor whiteColor];

    [self sxc_navigationBarCustomizationWithTitle:@"LOGIN"];
    [self.navigationItem setHidesBackButton:YES];
    
    buttonLogin.layer.borderColor = [UIColor whiteColor].CGColor;
    buttonLogin.layer.borderWidth = 0.5f;
    
    buttonMail.layer.borderColor = [UIColor whiteColor].CGColor;
    buttonMail.layer.borderWidth = 0.5f;
    buttonMail.layer.cornerRadius = 5.0f;

}

-(NSDictionary*)loginRequestDictionary
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] init];
    [requestDictionary setValue:textfieldEmail.text forKey:@"username"];
    [requestDictionary setValue:textfieldPassword.text forKey:@"password"];

    return requestDictionary;

}

-(BOOL)isValidComponents
{
    if((textfieldEmail.text.length==0)||(textfieldPassword.text.length==0)){
        [self sxc_showErrorViewWithMessage:@"Please enter the credentials."];
        return NO;
    }
    return YES;
}



#pragma mark - IBAction methods
-(IBAction)loginBtnClicked:(id)sender{
    [self loginUser];
}

- (IBAction)twitterLoginClicked:(id)sender {
    [self twitterLogin];
}


#pragma mark - Service Method
-(void)loginUser
{
    if([self isValidComponents]){
        [self startLoader];
        
        [[PWPService sharedInstance] loginWithRequestDictionary:(id)[self loginRequestDictionary] withSuccessBlock:^(id responseDicitionary) {
            [self dismissLoader];
            
            if([responseDicitionary valueForKey:@"apikey"]&&[responseDicitionary isKindOfClass:[NSDictionary class]]){
                
                [SXCUtility saveNSObject:responseDicitionary[@"apikey"] forKey:@"apikey"];

                [[Mixpanel sharedInstance] track:@"Login Success"];

                [self fetchUserProfile];
                
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Login Using Email"     // Event category (required)
                                                                      action:@"button_press"  // Event action (required)
                                                                       label:@"Success"          // Event label
                                                                       value:nil] build]];    // Event valu
                
            }
            else{
                [self sxc_showErrorViewWithMessage:@"Incorrect login. Please enter correct credentials."];
            }
            
        } withErrorBlock:^(NSError *error) {
            
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:@"Incorrect login. Please enter correct credentials."];
            
        }];
    }
}

-(void)twitterLogin{

    [[Twitter sharedInstance] logOut];
    
    [[Twitter sharedInstance] logInWithCompletion:^
     (TWTRSession *session, NSError *error) {
         if (session) {
             [self twitterServiceWithDictionary:@{@"id":session.userID,@"token":session.authToken,@"token_secret":session.authTokenSecret}];
         } else {
         }
     }];


}

-(void)twitterServiceWithDictionary:(NSDictionary *)reqDictionary{

    [self startLoader];

    [[PWPService sharedInstance] loginUsingTwitterWithRequestDictionary:(id)reqDictionary withSuccessBlock:^(id responseDicitionary) {
        
        [self dismissLoader];
        
        if([responseDicitionary valueForKey:@"apikey"]){
            
            [SXCUtility saveNSObject:responseDicitionary[@"apikey"] forKey:@"apikey"];
            
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Login Using Twitter"     // Event category (required)
                                                                  action:@"button_press"  // Event action (required)
                                                                   label:@"Success"          // Event label
                                                                   value:nil] build]];    // Event valu
            
            [self fetchUserProfile];
//
        }
        else{
            
            [self sxc_showErrorViewWithMessage:@"Some error occured while login with twitter."];
        }
    } withErrorBlock:^(NSError *error) {
        
        
        [self dismissLoader];
        
        
        [self sxc_showErrorViewWithMessage:@"Some error occured while login with twitter."];
        
    }];

}


-(void)facebookLogin
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if(!result.token || !result.token.userID || !result.token.tokenString){
            return;
        }
        
        [self facebookPawParkApiWithDictionary:@{@"id":result.token.userID,@"token":result.token.tokenString}];
        
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
            }
        }
    }];

}


-(void)facebookPawParkApiWithDictionary:(NSDictionary *)requestDictionary
{
    [self startLoader];

    
    [[PWPService sharedInstance] loginUsingFacebookWithRequestDictionary:(id)requestDictionary withSuccessBlock:^(id responseDicitionary) {
        
        [self dismissLoader];
        
        if([responseDicitionary valueForKey:@"apikey"]){
            
            [SXCUtility saveNSObject:responseDicitionary[@"apikey"] forKey:@"apikey"];
            
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Login Using Facebook"     // Event category (required)
                                                                  action:@"button_press"  // Event action (required)
                                                                   label:@"Success"          // Event label
                                                                   value:nil] build]];    // Event valu
            
            
            [self fetchUserProfile];
   
        }
        else{
            [self sxc_showErrorViewWithMessage:@"Some error occured while login with facebook."];
        }

        
    } withErrorBlock:^(NSError *error) {
        
        [self dismissLoader];
        [self sxc_showErrorViewWithMessage:@"Some error occured while login with facebook."];
    }];

}
- (IBAction)facebookLoginClicked:(id)sender{
    [self facebookLogin];
}

- (IBAction)forgotPasswordClicked:(id)sender {

    [[Mixpanel sharedInstance] track:@"Forgot Password Clicked"];
    UIStoryboard * storyBoard  = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    
    PWPForgotPasswordViewController * forgotPasswordViewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPForgotPasswordViewController"];
    [self.navigationController pushViewController:forgotPasswordViewController animated:YES];    
}

-(void)sxc_stopLoading
{
    [self dismissLoader];
}
-(void)sxc_startLoading {}


-(void)fetchUserProfile {
    
    [self startLoader];
    
    [[PWPService sharedInstance] currentUserProfileWithRequestDictionary:nil withSuccessBlock:^(id responseDicitionary) {
        [PWPApplicationUtility saveCurrentUserProfileDictionary:responseDicitionary];
        
        [self dismissLoader];
        
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];
        UIViewController * viewController = [storyboard instantiateViewControllerWithIdentifier:@"PWPSplashViewController"];
        
        PWPAppDelegate * appdelagte=[PWPAppDelegate sharedInstance];
        [appdelagte switchToSplashController:viewController];
        
    } withErrorBlock:^(NSError *error) {
        
        [self dismissLoader];
        
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];
        UIViewController * viewController = [storyboard instantiateViewControllerWithIdentifier:@"PWPSplashViewController"];
        
        PWPAppDelegate * appdelagte=[PWPAppDelegate sharedInstance];
        [appdelagte switchToSplashController:viewController];
        
    }];
    
}


@end
