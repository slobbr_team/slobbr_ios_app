//
//  PWPAddDogViewController.h
//  PawPark
//
//  Created by daffomac on 4/23/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PWPAddDogViewDelegate <NSObject>

-(void)dogAdded;

@end

@interface PWPAddDogViewController : UIViewController

@property(nonatomic,weak)id<PWPAddDogViewDelegate>delegate;
@property(nonatomic,strong)NSDictionary * dogDictionary;
@property(nonatomic,assign)BOOL isPntViewController;
@property(nonatomic,assign)BOOL isFromRegistration;


@end
