//
//  PWPAddDogViewController.m
//  PawPark
//
//  Created by daffomac on 4/23/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPAddDogViewController.h"
#import "UIViewController+UBRComponents.h"
#import <UIImagePickerController+BlocksKit.h>
#import "SXCTextfieldWithLeftImage.h"
#import "IQUIView+Hierarchy.h"
#import "PWPApplicationUtility.h"
#import "SXCUtility.h"
#import "PWPService.h"
#import <UIImageView+WebCache.h>
#import "PWPConstant.h"
#import <Google/Analytics.h>
#import "PWPBreedSelectionViewController.h"
#import "SZTextView.h"
#import "UIColor+PWPColor.h"
#import "PWPDownloadBackgroundImage.h"
#import <MWPhotoBrowser.h>
#import "PWPDogImageCell.h"
#import <CTAssetsPickerController/CTAssetsPickerController.h>
#import "PWPMapPinHelper.h"
#import "PWPAppDelegate.h"
#import <SVProgressHUD.h>
@interface PWPAddDogViewController()<UITextFieldDelegate,PWPBreedSelectionDelegate, UITextViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,MWPhotoBrowserDelegate,PWPDogImageCellProtocoal, CTAssetsPickerControllerDelegate>
{
    __weak IBOutlet UICollectionView * collectionView;
    __weak IBOutlet UIImageView * imageViewPaw;
    __weak IBOutlet UIImageView * imageViewBG;
    __weak IBOutlet SXCTextfieldWithLeftImage *textFieldName;
    __weak IBOutlet SXCTextfieldWithLeftImage *textFieldGender;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldSize;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldActivityLevel;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldSociability;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldBreed;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldBirthday;
    __weak IBOutlet SXCTextfieldWithLeftImage *textfieldAge;
    __weak IBOutlet UIButton *buttonAdoptable;
    __weak IBOutlet NSLayoutConstraint *layoutConstraintTextViewHeight;

    __weak IBOutlet NSLayoutConstraint *layoutConstraintPicHeight;
    __weak IBOutlet NSLayoutConstraint *layoutConstraintHeightView;
    NSData * profilePicture;
    
    NSNumber * monthString;
    NSString * dayString;
    NSNumber * yearString;
    
    NSMutableArray * breedArray;

    NSMutableArray * picsArray;
    NSMutableArray * photos;
    NSMutableArray * thumbs;

    NSInteger numberOfPics;
    CGFloat totalProgress;

}
- (IBAction)selectBreedClicked:(id)sender;

-(IBAction)cancelBtnClicked:(id)sender;
-(IBAction)doneBtnClicked:(id)sender;
- (IBAction)selectGenderClicked:(id)sender;
- (IBAction)selectSizeClicked:(id)sender;
- (IBAction)selectBirthdayClicked:(id)sender;
- (IBAction)selectActivityLevelClicked:(id)sender;
- (IBAction)selectSociabilityClicked:(id)sender;
@property (weak, nonatomic) IBOutlet SZTextView *textViewMessage;


@end

@implementation PWPAddDogViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self initView];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Add Dog Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textView.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textView.text length] + [text length] - range.length;
    return newLength <= 140;

}

#pragma mark - BL Methods

-(BOOL)isValid
{
    if([SXCUtility trimString:textFieldName.text].length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter your pup’s Name"];
        return NO;
    }
    
    if([SXCUtility trimString:textfieldBreed.text].length==0){
        [self sxc_showErrorViewWithMessage:@"Please select your pup's Breed"];
        return NO;
    }

    if([SXCUtility trimString:textFieldGender.text].length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter your pup's Gender"];
        return NO;
    }

    if([SXCUtility trimString:textfieldSize.text].length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter your pup's Size"];
        return NO;
    }

    if([SXCUtility trimString:textfieldAge.text].length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter your pup's Age"];
        return NO;
    }

    if([SXCUtility trimString:textfieldActivityLevel.text].length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter your pup's Activity Level"];
        return NO;
    }

    if([SXCUtility trimString:textfieldSociability.text].length==0){
        [self sxc_showErrorViewWithMessage:@"Please enter your pup's Sociability"];
        return NO;
    }


    return YES;
}


- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

-(NSDictionary *)getRequestDictionary
{
    NSMutableDictionary * dateOfBirthdayDictionary=[[NSMutableDictionary alloc] init];
    [dateOfBirthdayDictionary setValue:yearString forKey:@"year"];
    [dateOfBirthdayDictionary setValue:monthString forKey:@"month"];
    [dateOfBirthdayDictionary setValue:dayString forKey:@"day"];

    NSMutableDictionary * dictionary=[[NSMutableDictionary alloc] init];
    [dictionary setValue:textFieldName.text forKey:@"name"];
    [dictionary setValue:textfieldBreed.text forKey:@"breed"];
    [dictionary setValue:[[textFieldGender.text substringToIndex:1] lowercaseString]  forKey:@"gender"];
    [dictionary setValue:[PWPApplicationUtility pawSizesCorrespondingToSizes:textfieldSize.text] forKey:@"size"];
    [dictionary setValue:dateOfBirthdayDictionary forKey:@"birthday"];
    [dictionary setValue:[textfieldAge.text stringByReplacingOccurrencesOfString:@" Years" withString:@""] forKey:@"age"];
    [dictionary setValue:[PWPApplicationUtility pawAcitivityLevelCorrespondingToActivityLevel:textfieldActivityLevel.text] forKey:@"activityLevel"];
    [dictionary setValue:[PWPApplicationUtility pawSociabilityCorrespondingToSociability:textfieldSociability.text] forKey:@"sociability"];

    if([PWPApplicationUtility isShelter]){
        [dictionary setValue:[NSNumber numberWithBool:buttonAdoptable.selected] forKey:@"adoptable"];
    }
    if(!buttonAdoptable.hidden && buttonAdoptable.selected && _textViewMessage.text.length>0){
        [dictionary setValue:_textViewMessage.text forKey:@"specialNeeds"];
    }
    else{
        [dictionary removeObjectForKey:@"specialNeeds"];
    }

    return dictionary;
}



-(void)initView
{
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
    
    textfieldSize.delegate=self;
    textfieldAge.delegate=self;
    
    if((_dogDictionary==nil)&&_isFromRegistration){
        [self sxc_navigationBarCustomizationWithTitle:@"PROFILE"];
        picsArray = [[NSMutableArray alloc] init];
    }
    else if((_dogDictionary==nil)&&!_isFromRegistration){
        [self sxc_navigationBarCustomizationWithTitle:@"ADD A DOG"];
        picsArray = [[NSMutableArray alloc] init];
    }
    else{
        [self sxc_navigationBarCustomizationWithTitle:@"EDIT PROFILE"];
        picsArray = [[NSMutableArray alloc] init];
        [self applyInformationToFields];
    }

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;

    int roundedUp = ceil((picsArray.count + 1)/3.0f);
    layoutConstraintPicHeight.constant = screenWidth / 3 * roundedUp;

    [self sxc_customizeBackButton];
    
    breedArray=[[NSMutableArray alloc] initWithArray:[SXCUtility cleanArray:[[SXCUtility getNSObject:@"BreedList"] valueForKeyPath:@"name"]]];
    
    if([PWPApplicationUtility isShelter]){
        buttonAdoptable.hidden=NO;
    }
    else{
        buttonAdoptable.hidden=YES;
    }
    
    [buttonAdoptable setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _textViewMessage.placeholder=@"Add your status here";
    _textViewMessage.placeholderTextColor=[UIColor sxc_hintColor];
    _textViewMessage.layer.borderColor=[UIColor sxc_hintColor].CGColor;
    _textViewMessage.layer.borderWidth=1.0f;
    
    _textViewMessage.hidden=YES;
    layoutConstraintTextViewHeight.constant=0;
    [self.view updateConstraintsIfNeeded];
    [self.view layoutIfNeeded];
    
    textFieldName.textColor=[UIColor whiteColor];
    [textFieldName setValue:[UIColor whiteColor]
        forKeyPath:@"_placeholderLabel.textColor"];
    textFieldName.backgroundColor = [UIColor clearColor];
    
    
    textFieldGender.textColor=[UIColor whiteColor];
    [textFieldGender setValue:[UIColor whiteColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    textFieldGender.backgroundColor = [UIColor clearColor];

    textfieldSize.textColor=[UIColor whiteColor];
    [textfieldSize setValue:[UIColor whiteColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    textfieldSize.backgroundColor = [UIColor clearColor];

    textfieldActivityLevel.textColor=[UIColor whiteColor];
    [textfieldActivityLevel setValue:[UIColor whiteColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    textfieldActivityLevel.backgroundColor = [UIColor clearColor];

    textfieldSociability.textColor=[UIColor whiteColor];
    [textfieldSociability setValue:[UIColor whiteColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    textfieldSociability.backgroundColor = [UIColor clearColor];

    textfieldBreed.textColor=[UIColor whiteColor];
    [textfieldBreed setValue:[UIColor whiteColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    textfieldBreed.backgroundColor = [UIColor clearColor];

    textfieldBirthday.textColor=[UIColor whiteColor];
    [textfieldBirthday setValue:[UIColor whiteColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    textfieldBirthday.backgroundColor = [UIColor clearColor];

    textfieldAge.textColor=[UIColor whiteColor];
    [textfieldAge setValue:[UIColor whiteColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    textfieldAge.backgroundColor = [UIColor clearColor];

}

-(void)applyInformationToFields {

    picsArray = [SXCUtility cleanArray:_dogDictionary[@"gallery"]].mutableCopy;
    
    if(_dogDictionary[@"birthday"]!=nil){
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSString * birthday = _dogDictionary[@"birthday"];
        NSRange range = [birthday rangeOfString:@"T"];
        NSString * subDateString = [birthday substringToIndex:range.location];
        
        NSDate * dateString=[dateFormatter dateFromString:subDateString];
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        
        // Extract date components into components1
        NSDateComponents * dateComponents= [gregorianCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:dateString];
        
        yearString=[NSNumber numberWithInteger:dateComponents.year];
        monthString=[NSNumber numberWithInteger:dateComponents.month];
        dayString=[NSString stringWithFormat:@"%ld",(long)dateComponents.day];
        
        textfieldBirthday.text=[NSString stringWithFormat:@"%@-%@-%@",dayString,monthString,yearString];
    }
    else{
        textfieldBirthday.text=@"";
    }
    
    textFieldName.text=_dogDictionary[@"name"];
    
    NSArray * pawSizeArray=[PWPApplicationUtility pawSizes];
    NSInteger sizeIndex=[[_dogDictionary valueForKey:@"size"] integerValue];
    
    if(sizeIndex<=pawSizeArray.count){
        textfieldSize.text=[pawSizeArray objectAtIndex:sizeIndex-1];
    }
    
    if([[[_dogDictionary[@"gender"] description] lowercaseString] isEqualToString:@"m"]){
        textFieldGender.text=@"Male";
    }
    else if([[[_dogDictionary[@"gender"] description] lowercaseString] isEqualToString:@"f"]){
        textFieldGender.text=@"Female";
    }
   
    if(_dogDictionary[@"breed"]){
        textfieldBreed.text=_dogDictionary[@"breed"];
    }
    textfieldAge.text=[NSString stringWithFormat:@"%@ Years",[SXCUtility cleanString:[_dogDictionary[@"age"] description]]];
    
    NSArray * pawActivityArray=[PWPApplicationUtility pawActivityLevel];
    NSInteger pawActivityIndex=[[_dogDictionary valueForKey:@"activity_level"] integerValue];
    
    if(pawActivityIndex<pawActivityArray.count){
        textfieldActivityLevel.text=[pawActivityArray objectAtIndex:pawActivityIndex];
    }
    
    NSArray * pawSociabilityArray=[PWPApplicationUtility pawSociability];
    NSInteger pawSociabilityIndex=[[_dogDictionary valueForKey:@"sociability"] integerValue];
    
    if(pawSociabilityIndex<pawSociabilityArray.count){
        textfieldSociability.text=[pawSociabilityArray objectAtIndex:pawSociabilityIndex];
    }
    
    imageViewPaw.contentMode=UIViewContentModeScaleAspectFill;
    [imageViewPaw sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",K_URL_HOSTNAME,_dogDictionary[@"image_url"]]]];

}




-(void)addDogPic
{
    
    void (^picPhotosFromGallery)(void) = ^void() {

        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status){
            dispatch_async(dispatch_get_main_queue(), ^{

                // init picker
                CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];

                // set delegate
                picker.delegate = self;

                // Optionally present picker as a form sheet on iPad
                [self presentViewController:picker animated:YES completion:nil];
            });
        }];



/*
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePicker setAllowsEditing:YES];
        
        [imagePicker setBk_didCancelBlock:^(UIImagePickerController * imagePickerController) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [imagePicker setBk_didFinishPickingMediaBlock:^(UIImagePickerController * controller, NSDictionary *info) {
            
            UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
            [self createNewDogPic:image];
            [self dismissViewControllerAnimated:YES completion:nil];

        }];
        
        [self presentViewController:imagePicker animated:YES completion:nil];
 */

    };
    void (^picPhotosFromCamera)(void) = ^void() {
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePicker setAllowsEditing:YES];
        
        [imagePicker setBk_didCancelBlock:^(UIImagePickerController * imagePickerController) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [imagePicker setBk_didFinishPickingMediaBlock:^(UIImagePickerController * controller, NSDictionary *info) {
            
            UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
            
            [self createNewDogPic:image];
            
            [self dismissViewControllerAnimated:YES completion:nil];

            [collectionView reloadData];

            CGRect screenRect = [[UIScreen mainScreen] bounds];
            CGFloat screenWidth = screenRect.size.width;

            int roundedUp = ceil((picsArray.count + 1)/3.0f);
            layoutConstraintPicHeight.constant = screenWidth / 3 * roundedUp;
            
        }];
        
        [self presentViewController:imagePicker animated:YES completion:nil];

    };
    
    
    if (TARGET_IPHONE_SIMULATOR) {
        [self sxc_showActionSheetWithActionTitle:nil optionTitle:@[@"Choose"] WithActionArray:@[[picPhotosFromGallery copy]]];
    }else{
        [self sxc_showActionSheetWithActionTitle:nil optionTitle:@[@"Take Photo",@"Choose"] WithActionArray:@[[picPhotosFromCamera copy],[picPhotosFromGallery copy]]];
    }
    
}

-(void)createNewDogPic:(UIImage *)image{

    NSMutableDictionary * dictionary =[[NSMutableDictionary alloc] init];
    dictionary[@"description"] = @"Upload New image dog pciture from mobile app";
    dictionary[@"imageInfo"] = @[@{
                                     @"key":@"License",@"value":@"Mobile User"
                                     },
                                    @{
                                     @"key":@"by",@"value":@"Mobile User"

                                     }];
    dictionary[@"image"] = image;

    [picsArray addObject:dictionary];


}




#pragma mark - Services

-(void)addNewDog{
    
    [self sxc_startLoading];

    [[PWPService sharedInstance] createNewDogWithRequestDictionary:(id)[self getRequestDictionary] withData:profilePicture withSuccessBlock:^(id responseDicitionary) {

        dispatch_group_t group = dispatch_group_create();

        self.dogDictionary = responseDicitionary;
        [self uploadNewPicGroup:group];

        dispatch_group_notify(group, dispatch_get_main_queue(), ^{

            if(_isPntViewController){
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else{
                [self.navigationController popViewControllerAnimated:YES];
            }

            [self sxc_showSuccessWithMessage:@"Dog has been added successfully."];
            [self sxc_stopLoading];

            if(_delegate&&[_delegate respondsToSelector:@selector(dogAdded)]){
                [_delegate dogAdded];
            }
            
        });


    } withErrorBlock:^(NSError *error) {
        [self sxc_showErrorViewWithMessage:error.domain];
        [self sxc_stopLoading];
    }];


}

-(void)updateDog
{
    [self sxc_startLoading];
    [[PWPService sharedInstance] updateDogWithRequestDictionary:(id)[self getRequestDictionary] withDogId:_dogDictionary[@"id"] withData:profilePicture withSuccessBlock:^(id responseDicitionary) {

        dispatch_group_t group = dispatch_group_create();

        numberOfPics = 0;
        totalProgress = 0;
        
        [self uploadNewPicGroup:group];

        dispatch_group_notify(group, dispatch_get_main_queue(), ^{

            [self.navigationController popViewControllerAnimated:YES];
            [self sxc_showSuccessWithMessage:@"Dog has been updated successfully."];
            [self sxc_stopLoading];


            if(_delegate&&[_delegate respondsToSelector:@selector(dogAdded)]){
                [_delegate dogAdded];
            }
        });

    } withErrorBlock:^(NSError *error) {
        [self sxc_stopLoading];
        [self sxc_showErrorViewWithMessage:error.domain];
    }];



}

-(void)uploadNewPicGroup:(dispatch_group_t)group {


    for (NSDictionary * dictionary in picsArray){

        UIImage * image = [PWPMapPinHelper resizeImageForUploading:[dictionary valueForKey:@"image"]];
        

        if (image != nil){

            numberOfPics += 1;

            dispatch_group_enter(group);

            NSMutableDictionary * requestDictionary = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
            NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];

            NSString * imageDataString =[[NSString alloc] initWithFormat:@"data:image/jpeg;base64,%@",[imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
            [requestDictionary setValue:imageDataString forKey:@"imageFile"];
            [requestDictionary removeObjectForKey:@"image"];

            [[PWPService sharedInstance] uploadNewDogImage:requestDictionary withDogId:self.dogDictionary[@"id"] withProgress:^(float progress){
                totalProgress += progress;
                [self updateProgressLoader];
            }withSuccessBlock:^(id responseDicitionary) {
                dispatch_group_leave(group);

            } withErrorBlock:^(NSError *error) {
                dispatch_group_leave(group);
            }];
        }
    }

}

-(void)updateProgressLoader {
    
    if (numberOfPics == 1) {
        NSString * uploadingText = [NSString stringWithFormat:@"Uploading\n(%.0f%%)...",totalProgress*1.0f/numberOfPics*100];
        [[[PWPAppDelegate sharedInstance] window] setUserInteractionEnabled:NO];
        [SVProgressHUD showProgress:totalProgress*1.0f/numberOfPics status:uploadingText];
    } else {
        
        NSInteger uploadedImageNumber = totalProgress;
        if(uploadedImageNumber >= numberOfPics) {
            uploadedImageNumber  = numberOfPics-1;
        }
        NSInteger totalProgressInInt = totalProgress;
        CGFloat percentage = (totalProgress - totalProgressInInt)*100;
        
        NSString * uploadingText = [NSString stringWithFormat:@"Uploading\n%.0f%%\n(%.0ld/%ld)",percentage,(long)uploadedImageNumber+1,(long)numberOfPics];
        [[[PWPAppDelegate sharedInstance] window] setUserInteractionEnabled:NO];
        [SVProgressHUD showProgress:totalProgress*1.0f/numberOfPics status:uploadingText];
    }
    
}

#pragma mark - IBAction Methods
-(IBAction)addPawProfilePicture:(id)sender{
    [self addDogPic];
}

- (IBAction)selectBreedClicked:(id)sender {
    PWPBreedSelectionViewController * breedSelectionViewController=[self.storyboard instantiateViewControllerWithIdentifier:@"PWPBreedSelectionViewController"];
    breedSelectionViewController.delegate=self;
    breedSelectionViewController.from=@"Add";
    [self.navigationController pushViewController:breedSelectionViewController animated:YES];
}

-(IBAction)cancelBtnClicked:(id)sender{
    
    if(_isPntViewController){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(IBAction)doneBtnClicked:(id)sender{
    if([self isValid]&&!_dogDictionary){
        [self addNewDog];
    }
    else if([self isValid]&&_dogDictionary){
        [self updateDog];
    }
}

- (IBAction)selectGenderClicked:(id)sender {

     [self sxc_showActionSheetWithTitle:@"Select Gender" pickerWithRows:[PWPApplicationUtility pawGender] itemSelectedAtIndex:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
         textFieldGender.text=selectedValue;
     } cancelBlock:^(ActionSheetStringPicker *picker) {
     } origin:self.view];

}

- (IBAction)selectSizeClicked:(id)sender {
    
    [self sxc_showActionSheetWithTitle:@"Select Size" pickerWithRows:[PWPApplicationUtility pawSizes] itemSelectedAtIndex:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        textfieldSize.text=selectedValue;
    } cancelBlock:^(ActionSheetStringPicker *picker) {
    } origin:self.view];
}

- (IBAction)selectBirthdayClicked:(id)sender {

    [self sxc_showDateOfBirthActionSheetWithTitle:@"Birthday" pickerWithRows:nil dateSelected:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
        NSDate * dateSelected=selectedDate;
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        
        // Extract date components into components1
        NSDateComponents * dateComponents= [gregorianCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:dateSelected];
        
        yearString=[NSNumber numberWithInteger:dateComponents.year];
        monthString=[NSNumber numberWithInteger:dateComponents.month];
        dayString=[NSString stringWithFormat:@"%ld",(long)dateComponents.day];
        
        textfieldBirthday.text=[NSString stringWithFormat:@"%@/%@/%@",monthString,dayString,yearString];
        
        
        
        NSDateComponents *components;
        NSInteger years;
        
        components = [gregorianCalendar components: NSCalendarUnitYear
                                                     fromDate: dateSelected toDate: [NSDate date] options: 0];
        years = [components year];
        
        textfieldAge.text=[NSString stringWithFormat:@"%ld Years",years];
        
        
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];

}

- (IBAction)selectActivityLevelClicked:(id)sender {
    
    [self sxc_showActionSheetWithTitle:@"Select Activity Level" pickerWithRows:[PWPApplicationUtility pawActivityLevelForNewDog] itemSelectedAtIndex:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        textfieldActivityLevel.text=selectedValue;
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (IBAction)selectSociabilityClicked:(id)sender {
    
    [self sxc_showActionSheetWithTitle:@"Select Sociability" pickerWithRows:[PWPApplicationUtility pawSociabilityForNewDog] itemSelectedAtIndex:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        textfieldSociability.text=selectedValue;
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];

}
- (IBAction)dogAdoptableClicked:(id)sender {
    buttonAdoptable.selected=!buttonAdoptable.selected;
    
    if(buttonAdoptable.selected){
        _textViewMessage.hidden=NO;
        layoutConstraintTextViewHeight.constant=124;
        [self.view updateConstraintsIfNeeded];
        [self.view layoutIfNeeded];
    }
    else{
        _textViewMessage.hidden=YES;
        layoutConstraintTextViewHeight.constant=0;
        [self.view updateConstraintsIfNeeded];
        [self.view layoutIfNeeded];
    }
}

#pragma mark - UItextfield Delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if((textField==textfieldSize)&&(textFieldGender.canBecomeFirstResponder)){
    
        [self selectSizeClicked:nil];
        return NO;
    }
    
    if(textField==textfieldAge){
        
        NSString * currentString =textfieldAge.text;
        NSString * newString=[currentString stringByReplacingOccurrencesOfString:@" Years" withString:@""];
        
        textfieldAge.text=newString;
    }

    return true;
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if((textField==textfieldAge)&(textfieldAge.text.length>0)){
        NSString * dogAgeString=[textfieldAge.text stringByReplacingOccurrencesOfString:@" Years" withString:@""];
        textfieldAge.text=[NSString stringWithFormat:@"%@ Years",dogAgeString];
    }
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==textfieldAge)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 2;
    }
    return YES;

}

#pragma mark - Loading Methods
-(void)sxc_stopLoading
{
    [self dismissLoader];
}
-(void)sxc_startLoading
{
    [self startLoader];
}



-(void)breedSelected:(NSMutableDictionary *)breedDictionary
{
    textfieldBreed.text=breedDictionary[@"name"];
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return picsArray.count + 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;

    return CGSizeMake((screenWidth-32)/3,(screenWidth-32)/3);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)theCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == picsArray.count){
        UICollectionViewCell * cell = [theCollectionView dequeueReusableCellWithReuseIdentifier:@"PWPDogImageAddCell" forIndexPath:indexPath];
        return cell;
    }
    PWPDogImageCell * collectionViewCell = [theCollectionView dequeueReusableCellWithReuseIdentifier:@"PWPDogImageCell" forIndexPath:indexPath];
    [collectionViewCell configureViewWithDelegate:self WithIndexPath:indexPath];

    UIImage * image = [picsArray[indexPath.row] valueForKey:@"image"];
    if (image != nil){
        [collectionViewCell.imageViewDog setImage:image];
    }else {
        NSString * dogUrlString = [SXCUtility cleanString:[[picsArray[indexPath.row] valueForKey:@"thumbs"] valueForKey:@"w400"]];
        [collectionViewCell.imageViewDog sd_setImageWithURL:[NSURL URLWithString:dogUrlString] placeholderImage:[UIImage imageNamed:@"img_dog_sample"]];
    }

    return collectionViewCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row == picsArray.count){
        [self addDogPic];
        return;
    }

    photos = [[NSMutableArray alloc] init];
    thumbs = [[NSMutableArray alloc] init];

    for (NSDictionary * dog in picsArray) {

        if([dog valueForKey:@"image"]){
            [photos addObject:[MWPhoto photoWithImage:[dog valueForKey:@"image"]]];
            [thumbs addObject:[MWPhoto photoWithImage:[dog valueForKey:@"image"]]];
        }
        else {
            NSString * fullDogUrlString = [SXCUtility cleanString:[[dog valueForKey:@"thumbs"] valueForKey:@"w1000"]];
            [photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:fullDogUrlString]]];

            NSString * thumbDogUrlString = [SXCUtility cleanString:[[dog valueForKey:@"thumbs"] valueForKey:@"w400"]];
            [thumbs addObject:[MWPhoto photoWithURL:[NSURL URLWithString:thumbDogUrlString]]];
        }
    }

    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = YES;
    browser.displayNavArrows = YES;
    browser.displaySelectionButtons = NO;
    browser.alwaysShowControls = YES;
    browser.zoomPhotosToFill = YES;
    browser.enableGrid = NO;
    browser.startOnGrid = NO;
    browser.enableSwipeToDismiss = YES;
    browser.autoPlayOnAppear = NO;
    [browser setCurrentPhotoIndex:indexPath.row];

    UINavigationController * controller = [[UINavigationController alloc] initWithRootViewController:browser];

    [self presentViewController:controller animated:YES completion:^{

    }];

}

-(void)deleteDogWithImageId:(NSString *)imageId indexPath:(NSIndexPath *) indexPath {

    [self sxc_startLoading];

    [[PWPService sharedInstance] deleteDogImageID:imageId withDogId:self.dogDictionary[@"id"] withSuccessBlock:^(id responseDicitionary) {

        [picsArray removeObjectAtIndex:indexPath.row];
        [collectionView reloadData];

        [self sxc_stopLoading];

        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;

        int roundedUp = ceil((picsArray.count + 1)/3.0f);
        layoutConstraintPicHeight.constant = screenWidth / 3 * roundedUp;

    } withErrorBlock:^(NSError *error) {
        [self sxc_stopLoading];
        [self sxc_showErrorViewWithMessage:error.domain];
    }];
}

-(void)imageDeleteClicked:(NSIndexPath *)indexPath
{
    NSDictionary * dictionary = picsArray[indexPath.row];

    NSString * imageId = [SXCUtility cleanString:[[dictionary valueForKey:@"id"] description]];
    if (imageId.length > 0){
        [self deleteDogWithImageId:imageId indexPath:indexPath];
    }
    else {
        [picsArray removeObjectAtIndex:indexPath.row];
        [collectionView reloadData];

        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;

        int roundedUp = ceil((picsArray.count + 1)/3.0f);
        layoutConstraintPicHeight.constant = screenWidth / 3 * roundedUp;
    }

}


#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < thumbs.count)
        return [thumbs objectAtIndex:index];
    return nil;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
}

- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index {
    return false;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected {
    NSLog(@"Photo at index %lu selected %@", (unsigned long)index, selected ? @"YES" : @"NO");
}

- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets {

    PHImageRequestOptions * requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    requestOptions.synchronous = true;

    PHImageManager *manager = [PHImageManager defaultManager];

    for (PHAsset *asset in assets) {

        [manager requestImageForAsset:asset
                           targetSize:PHImageManagerMaximumSize
                          contentMode:PHImageContentModeDefault
                              options:requestOptions
                        resultHandler:^void(UIImage *image, NSDictionary *info) {
                            [self createNewDogPic:image];
                        }];

    }

    [collectionView reloadData];

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;

    int roundedUp = ceil((picsArray.count + 1)/3.0f);
    layoutConstraintPicHeight.constant = screenWidth / 3 * roundedUp;

    [self dismissViewControllerAnimated:YES completion:nil];

}

@end
