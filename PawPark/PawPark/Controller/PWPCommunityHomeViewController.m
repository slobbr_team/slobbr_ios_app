//
//  PWPCommunityHomeViewController.m
//  PawPark
//
//  Created by Samarth Singla on 13/08/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPCommunityHomeViewController.h"
#import "PWPService.h"
#import "PWPHomeCell.h"
#import "PWPNoParkCell.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPAppDelegate.h"
#import "PWPPlacePicViewController.h"
#import "PWPParkCheckedInDogsViewController.h"
#import "PWPCheckInHelper.h"
#import "PWPApplicationUtility.h"
#import "PWPSelectStatusViewController.h"
#import "PWPCommunityTopCell.h"
#import "PWPCommunityPostCell.h"
#import "PWPCommunityNewsPostTextCell.h"
#import "PWPAddPostViewController.h"
#import "PWPHomeViewController.h"
#import "PWPCommentsViewController.h"
#import "PWPConstant.h"

#import <Mixpanel.h>
#import "MainViewController.h"

#import "SXCUtility.h"
#import "UIViewController+UBRComponents.h"
#import "UIColor+PWPColor.h"

@interface PWPCommunityHomeViewController ()<PWPHomeCellDelegate,PWPPlacePicViewControllerDelegate, PWPCommunityDelegate, PWPCommunityTopDelegate, PWPCommunityPostDelegate, PWPCommunityHomeDelegate> {

    BOOL isLoading;
    
    NSMutableSet * errorSet;
    NSMutableArray * homeParkDictionary;
    NSMutableString * selectedStatus;
    NSMutableArray * posts;
    NSMutableArray * filteredPost;
    NSArray * categoryValueArray;
    NSArray * categoryTitleArray;
    
    NSString * selectedCategory;
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIRefreshControl * refreshControl;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewBg;

@end

@implementation PWPCommunityHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectedStatus = [SXCUtility cleanString:[SXCUtility getNSObject:@"quickCheckinDefaultMessage"]].mutableCopy;
    
    categoryValueArray = @[@"pic",@"intro",@"event",@"question",@"rule",@"announce"];
    categoryTitleArray = @[@"Community Pictures", @"Introduction", @"Events", @"Questions", @"Community Rules", @"Official Announcement"];
    
    selectedCategory = @"All Posts";
    
    [self addPullToRefresh];
    [self initView];
    
    [SXCUtility saveNSObject:@(true) forKey:@"isCommunitiesDirty"];
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [SXCUtility saveNSObject:selectedStatus forKey:@"quickCheckinDefaultMessage"];
    
    if([[[SXCUtility getNSObject:@"isCommunitiesDirty"] description] boolValue] == true){
        
        [SXCUtility saveNSObject:@(false) forKey:@"isCommunitiesDirty"];
       
        if(posts == nil){
            _tableView.hidden = true;
            [self startLoader];
        }
        else{
            [self automaticallyRefresh];
        }
       
        [self services];
    }
    else {
        [_tableView reloadData];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)services
{
    if(isLoading)
        return;
    
    isLoading=YES;
    
    errorSet = [[NSMutableSet alloc] init];
    
    [[PWPService sharedInstance] community:[SXCUtility cleanString:[[self.communitiesDictionary valueForKey:@"id"] description]] homeBatchRequestWithSuccessBlock:^(id responseDicitionary) {

        
        NSString * communities = [NSString stringWithFormat:@"/apis/v1/communities/%@/posts",[SXCUtility cleanString:[[self.communitiesDictionary valueForKey:@"id"] description]]];
        NSString * favPlaces = [NSString stringWithFormat:@"/apis/v1/current/place"];
        
        for (NSDictionary * dictionary in responseDicitionary) {
            if([[dictionary valueForKey:@"request"] isEqualToString:favPlaces]) {
                
                NSData *objectData = [[dictionary valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSDictionary *actualResponse = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&jsonError];
                [self homeParkWithDispatch:actualResponse];

            }
            else if([[dictionary valueForKey:@"request"] isEqualToString:communities]) {
                NSData *objectData = [[dictionary valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSError *jsonError;
                NSDictionary *actualResponse = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers error:&jsonError];
                [self communitiesPost:actualResponse];

            }
        }
        
        
        isLoading=NO;
        
        [_tableView reloadData];
        [_tableView setHidden:NO];
       
        [self.refreshControl endRefreshing];
        [self dismissLoader];
        
        if((!homeParkDictionary)&&([self.navigationController.viewControllers.lastObject isKindOfClass:[PWPCommunityHomeViewController class]])){
            [self sxc_showErrorViewWithMessage:@"You haven't set your Favorite PLACE or PARK! Go to PLACES to mark your favs!"];
            return ;
        }
        
    } withErrorBlock:^(NSError *error) {
        
        [self dismissLoader];
        
        isLoading=NO;
        
        [_tableView reloadData];
        [_tableView setHidden:NO];
        [super hideLoader];
        [self.refreshControl endRefreshing];
        
        [self sxc_showErrorViewWithMessage:error.domain];
        [SXCUtility saveNSObject:[NSNumber numberWithInteger:0] forKey:@"NotificationCount"];
        
    }];
}

-(void)homeParkWithDispatch:(NSDictionary *) response
{
    
    homeParkDictionary=[SXCUtility cleanArray:response].mutableCopy;
    
    NSMutableDictionary * communityPlaceDictionary = [SXCUtility cleanDictionary:[self.communitiesDictionary valueForKey:@"place"]].mutableCopy;
    [communityPlaceDictionary setValue:@true forKey:@"isCommunityPlace"];
    
    if(homeParkDictionary.count > 0) {
        NSDictionary * firstDictionary = homeParkDictionary.firstObject;
       
        if([[[firstDictionary valueForKey:@"id"] description] isEqualToString:[communityPlaceDictionary valueForKey:@"id"]]){
            [homeParkDictionary replaceObjectAtIndex:0 withObject:communityPlaceDictionary];
        }
        else {
            [homeParkDictionary insertObject:communityPlaceDictionary atIndex:0];
        }
    }
    
    else if(communityPlaceDictionary.allKeys.count > 0){
        [homeParkDictionary insertObject:communityPlaceDictionary atIndex:0];
    }
    
    if(!homeParkDictionary)
        [SXCUtility saveNSObject:@YES forKey:@"HomeParkEdit"];
}

-(void)communitiesPost:(NSDictionary *) response{
    posts = [SXCUtility cleanArray:response];
    
    if([categoryTitleArray indexOfObject:selectedCategory] == NSNotFound){
        filteredPost = [posts mutableCopy];
    }
    else {
        NSInteger categoryIndex = [categoryTitleArray indexOfObject:selectedCategory];
        NSString * categoryId = [categoryValueArray objectAtIndex:categoryIndex];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"type == %@", categoryId];
        filteredPost = [posts filteredArrayUsingPredicate:predicate].mutableCopy;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if((indexPath.section==0)&&(indexPath.row==0)&&(homeParkDictionary.count>0)){
        PWPHomeCell * tableViewCell =[tableView dequeueReusableCellWithIdentifier:@"PWPHomeCell"];
        tableViewCell.delegate = self;
        [tableViewCell configureCellWithData:homeParkDictionary];
        return tableViewCell;
    }
    else if((indexPath.section==0)&&(indexPath.row==0)&&(homeParkDictionary.count == 0)){
        PWPNoParkCell *tableViewcell = [tableView dequeueReusableCellWithIdentifier:@"PWPNoParkCell"];
        tableViewcell.btnFavPlace.tag=indexPath.row;
        [tableViewcell.btnFavPlace addTarget:self action:@selector(aMethod:) forControlEvents:UIControlEventTouchDown];
        return tableViewcell;
    }
    else if(indexPath.section==0 && indexPath.row ==1){
        PWPCommunityTopCell *tableViewcell = [tableView dequeueReusableCellWithIdentifier:@"PWPCommunityTopCell"];
        tableViewcell.delegate = self;
        [tableViewcell confugureUIWithData:self.communitiesDictionary];
        return tableViewcell;
    }
    else if(indexPath.section==0 && indexPath.row ==2){
        PWPCommunityPostCell *tableViewcell = [tableView dequeueReusableCellWithIdentifier:@"PWPCommunityPostCell"];
        tableViewcell.selectedCategory = selectedCategory;
        tableViewcell.delegate = self;
        
        [tableViewcell reloadDataUI];
        
        return tableViewcell;
    }
    
    PWPCommunityNewsPostTextCell  *tableViewcell = [tableView dequeueReusableCellWithIdentifier:@"PWPCommunityNewsPostTextCell"];
    [tableViewcell configureWithData: filteredPost [indexPath.row - 3]];
    tableViewcell.delegate = self;
    
    return tableViewcell;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(errorSet.allObjects.count > 0 ){
        return 0;
    }
    
    if(section==0)
        return 3+ filteredPost .count;
    return 0;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(errorSet.allObjects.count > 0 ){
        return 0;
    }
    
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(errorSet.allObjects.count > 0 ){
        return 0;
    }
    
    if(indexPath.section==0&&indexPath.row==0&&homeParkDictionary.count>0){
        return 290.0f;
    }
    else if(indexPath.section==0&&indexPath.row==0&&homeParkDictionary.count ==0){
        return 180.0f;
    }
    else if(indexPath.section==0&&indexPath.row == 1) {
        return 167.0f;
    }
    else if(indexPath.section==0&&indexPath.row == 2) {
        return 55.0f;
    }
    
    return UITableViewAutomaticDimension;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(errorSet.allObjects.count > 0 ){
        return 0;
    }
    
    if(indexPath.section==0&&indexPath.row==0&&homeParkDictionary.count>0){
        return 290.0f;
    }
    else if(indexPath.section==0&&indexPath.row==0&&homeParkDictionary.count == 0){
        return 180.0f;
    }
    else if(indexPath.section == 0 && indexPath.row == 1) {
        return 167.0f;
    }
    else if(indexPath.section==0&&indexPath.row == 2) {
        return 55.0f;
    }
    return UITableViewAutomaticDimension;
    
}

-(void)initView{
    [self sxc_setNavigationBarMenuItem];
    [self sxc_navigationBarCustomizationWithTitle:@"SLOBBR HOME"];
    
    self.imageViewBg.image = [PWPDownloadBackgroundImage backgroundImage];

    [self.tableView registerNib:[UINib nibWithNibName:@"PWPHomeCell" bundle:nil] forCellReuseIdentifier:@"PWPHomeCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"PWPNoParkCell" bundle:nil] forCellReuseIdentifier:@"PWPNoParkCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"PWPCommunityTopCell" bundle:nil] forCellReuseIdentifier:@"PWPCommunityTopCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"PWPCommunityPostCell" bundle:nil] forCellReuseIdentifier:@"PWPCommunityPostCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"PWPCommunityNewsPostTextCell" bundle:nil] forCellReuseIdentifier:@"PWPCommunityNewsPostTextCell"];
    
    self.tabBarController.tabBar.translucent = false;
    _tableView.hidden = true;
}

-(void)addPullToRefresh
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor=[UIColor sxc_themeColor];
    [self.refreshControl addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:self.refreshControl];
}
- (void)refreshView:(UIRefreshControl *)sender {
    [self services];
}
-(void)automaticallyRefresh{
    [_tableView setContentOffset:CGPointMake(0, -self.refreshControl.frame.size.height) animated:YES];
    [self.refreshControl beginRefreshing];
}

-(void)sxc_startLoading{}
-(void)sxc_stopLoading{}


-(void)aMethod:(UIButton*)sender
{
    ((UITabBarController *)kTabViewController).selectedIndex = 1;
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
}

-(void)checkInClickedWithPark:(NSDictionary *)parkDictionary;
{
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"PhotosStoryboard" bundle:nil];
    PWPPlacePicViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPPlacePicViewController"];
    viewController.placePics = [SXCUtility cleanArray:[parkDictionary valueForKey:@"gallery"]].mutableCopy;
    viewController.data = parkDictionary;
    viewController.placeId = parkDictionary[@"id"];
    viewController.delegate = self;
    
    [self.navigationController pushViewController:viewController animated:YES];
    
}



-(void)checkInDogsClicked:(NSDictionary *)dictionary
{
    if([SXCUtility cleanString:[dictionary[@"active_dogs_count"] description]].integerValue > 0){
        
        [[Mixpanel sharedInstance] track:@"Check-In Dogs Clicked Home Screen"];
        
        
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PWPParkCheckedInDogsViewController * parkCheckInDogsViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckedInDogsViewController"];
        parkCheckInDogsViewController.placeId = [SXCUtility cleanString:[[dictionary valueForKey:@"id"] description]];
        
        [self.navigationController pushViewController:parkCheckInDogsViewController animated:YES];
    }
    
}

-(void)quickCheckInClicked:(NSDictionary *)dictionary
{
    
    if([PWPCheckInHelper checkInAllowedForParkId:dictionary[@"id"] forDogs:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"]] == false){
        return;
    }
    
    [self startLoader];
    
    [[PWPService sharedInstance] checkInParkWithRequestDictionary:[self requestDictionaryForQuickCheckIn] withParkId:dictionary[@"id"] withSuccessBlock:^(id response) {
        
        [PWPCheckInHelper dogCheckedInParkId:dictionary[@"id"] forDogs:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"]];
        
        if([[[dictionary valueForKey:@"isCommunityPlace"] description] boolValue] == true){
            NSMutableDictionary * communityData = self.communitiesDictionary.mutableCopy;
        
            NSMutableDictionary * placeDictionary = [SXCUtility cleanDictionary:[communityData valueForKey:@"place"]].mutableCopy;
            [placeDictionary setObject:[PWPApplicationUtility getCurrentUserDogsArray] forKey:@"last_active_dogs"];
            
            NSString * checkInCount = [SXCUtility cleanString:[[placeDictionary valueForKey:@"active_dogs_count"] description]];
            [placeDictionary setObject:@(checkInCount.integerValue+[PWPApplicationUtility getCurrentUserDogsArray].count) forKey:@"active_dogs_count"];

            communityData[@"place"] = placeDictionary;
        
            self.communitiesDictionary = communityData;
        }
        
        [self dismissLoader];
        [self sxc_showSuccessWithMessage:@"You have been checked-in successfully."];
        [self automaticallyRefresh];
        [self services];
        
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        
        if (error.code==403) {
            [self sxc_showErrorViewWithMessage:@"You have already checked-in."];
        }
        else{
            [self sxc_showErrorViewWithMessage:error.domain];
        }
    }];
}

-(NSMutableDictionary *)requestDictionaryForQuickCheckIn
{
    NSMutableDictionary * requestDictionary=[[NSMutableDictionary alloc] init];
    
    if([PWPApplicationUtility getCurrentUserDogsArray].count>1){
        [requestDictionary setValue:[[PWPApplicationUtility getCurrentUserDogsArray] valueForKeyPath:@"id"] forKey:@"dogs"];
    }
    
    [requestDictionary setValue:[SXCUtility cleanString:[SXCUtility getNSObject:@"quickCheckinDefaultMessage"]] forKey:@"message"];
    
    return requestDictionary;
}

-(void)quickCheckInStatusClicked
{
    [[Mixpanel sharedInstance] track:@"Status Change Clicked Home Screen"];
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    
    PWPSelectStatusViewController * selectStatusViewController = [storyboard instantiateViewControllerWithIdentifier:@"PWPSelectStatusViewController"];
    selectStatusViewController.selectedStatus = selectedStatus;
    [self.navigationController pushViewController:selectStatusViewController animated:YES];
    
}

-(void)placeProfileUpdated {
    [self automaticallyRefresh];
    [self services];
}

- (IBAction)addNewPostButtonClicked:(id)sender {
    
    PWPAddPostViewController * addPostViewController = [[PWPAddPostViewController alloc] initWithNibName:@"PWPAddPostViewController" bundle:nil];
    addPostViewController.communityId = [SXCUtility cleanString:[[self.communitiesDictionary valueForKey:@"id"] description]];
    
    [self.navigationController pushViewController:addPostViewController animated:true];
}

-(void)communitySelected:(NSString *)communityId {

    selectedCategory = communityId;
    
    if([categoryTitleArray indexOfObject:communityId] == NSNotFound){
        filteredPost = [posts mutableCopy];
    }
    else {
        NSInteger categoryIndex = [categoryTitleArray indexOfObject:communityId];
        NSString * categoryId = [categoryValueArray objectAtIndex:categoryIndex];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"type == %@", categoryId];
        filteredPost = [posts filteredArrayUsingPredicate:predicate].mutableCopy;
    }
    
    [_tableView reloadData];
    
}

-(void)optOutClicked {

    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Opt Out!"
                                                                  message:@"Do you want to opt out the current community?"
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
    {
        [self startLoader];
        
        [[PWPService sharedInstance] optOuttWithCommunityId:[SXCUtility cleanString:[[self.communitiesDictionary valueForKey:@"id"] description]] WithRequestDictionary:nil withData:nil withSuccessBlock:^(id responseDicitionary) {
            
            [self dismissLoader];
            
            UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PWPHomeViewController * homeViewController = [storyboard instantiateViewControllerWithIdentifier:@"PWPHomeViewController"];
            homeViewController.isFromRegistration = false;
            [self.navigationController setViewControllers:@[homeViewController] animated:true];
            
        } withErrorBlock:^(NSError *error) {
            
            [self dismissLoader];
            [self sxc_showErrorViewWithMessage:error.domain];
            
        }];
    }];
    
    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"No"
                                                       style:UIAlertActionStyleCancel
                                                     handler:^(UIAlertAction * action)
    {}];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
  
}

-(void)commentsClickedWithData:(NSDictionary *)data {
    
    PWPCommentsViewController * commentViewController = [[PWPCommentsViewController alloc] initWithNibName:@"PWPCommentsViewController" bundle:nil];
    commentViewController.communityID = [SXCUtility cleanString:[[self.communitiesDictionary valueForKey:@"id"] description]];
    commentViewController.postId = [SXCUtility cleanString:[[data valueForKey:@"id"] description]];
    commentViewController.delegate = self;
    
    
    NSDictionary * thumbs = [SXCUtility cleanDictionary:[data valueForKey:@"thumbs"]];
    
    NSString * postImage = [SXCUtility cleanString:[thumbs valueForKey:@"w1000"]];
    if(postImage.length > 0){
        commentViewController.postImageUrl =postImage;
    }
    else {
        commentViewController.postImageUrl = @"";
        
    }

    NSDictionary * userData = [SXCUtility cleanDictionary:[data valueForKey:@"user"]];
    
    commentViewController.userName = [SXCUtility cleanString:[userData valueForKey:@"username"]];
    commentViewController.commentCount = [SXCUtility cleanString:[[data valueForKey:@"comment_count"] description]];
    
    NSString * userImage = [SXCUtility cleanString:[[SXCUtility cleanDictionary:[userData valueForKey:@"thumbs"]] valueForKey:@"w1000"]];
    commentViewController.userImageUrlString = userImage;
    
    NSString * dateString = [SXCUtility cleanString:[data valueForKey:@"created_at"]];
    
    NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
    [outputDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [outputDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZ"];
    
    NSDate * date = [outputDateFormatter dateFromString:dateString];
    [outputDateFormatter setDateFormat:@"hh:mm a dd MMM yyyy"];

    commentViewController.dateString = [outputDateFormatter stringFromDate:date];
    
    
    [self.navigationController pushViewController:commentViewController animated:true];
}



-(void)commentCountUpdate:(NSInteger)commentCount withIndexPath:(NSIndexPath *)indexPath withPostId:(NSString *)postId {

    NSPredicate * perdicate = [NSPredicate predicateWithFormat:@"self.id == %@",@(postId.integerValue)];
    NSArray * tempFilteredArray = [posts filteredArrayUsingPredicate:perdicate];
    if(tempFilteredArray.count > 0){
        NSDictionary * dictionary = [tempFilteredArray firstObject];
        NSDictionary * mutatedDictionary = dictionary.mutableCopy;
        [mutatedDictionary setValue:@(commentCount) forKey:@"comment_count"];
        
        NSInteger index = [posts indexOfObject:dictionary];
        [posts replaceObjectAtIndex:index withObject:mutatedDictionary];
    }
    
    tempFilteredArray =  [filteredPost filteredArrayUsingPredicate:perdicate];
    if(tempFilteredArray.count > 0){
        NSDictionary * dictionary = [tempFilteredArray firstObject];
        NSDictionary * mutatedDictionary = dictionary.mutableCopy;
        [mutatedDictionary setValue:@(commentCount) forKey:@"comment_count"];
        
        NSInteger index = [filteredPost indexOfObject:dictionary];
        [filteredPost replaceObjectAtIndex:index withObject:mutatedDictionary];
    }
}


-(void)communityMembersClicked {
    
    [self startLoader];
    
    [[PWPService sharedInstance] membersCommunityId:[SXCUtility cleanString:[[self.communitiesDictionary valueForKey:@"id"] description]] WithRequestDictionary:nil withData:nil withSuccessBlock:^(id responseDicitionary) {
        
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        PWPParkCheckedInDogsViewController * parkCheckInDogsViewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckedInDogsViewController"];
        NSMutableArray * dogsArray=[[NSMutableArray alloc] init];
        
        for(NSDictionary * checkedInDictionary in [SXCUtility cleanArray:responseDicitionary]){
            
            for(NSDictionary * dictionary in checkedInDictionary[@"dogs"]){
                
                NSMutableDictionary * newDogDictionary = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
                
                if([checkedInDictionary valueForKey:@"message"]){
                    [newDogDictionary setObject:checkedInDictionary[@"message"] forKey:@"message"];
                }
                
                [dogsArray addObject:newDogDictionary];
            }
            
        }
        
        parkCheckInDogsViewController.isFromCommunity = true;
        parkCheckInDogsViewController.dogsArray=dogsArray;
        
        [self.navigationController pushViewController:parkCheckInDogsViewController animated:YES];
        
        [self dismissLoader];
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
        [self sxc_showErrorViewWithMessage:error.domain];
    }];
}

@end
