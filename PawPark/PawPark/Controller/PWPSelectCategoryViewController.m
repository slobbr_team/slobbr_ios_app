
//
//  PWPSelectCategoryViewController.m
//  PawPark
//
//  Created by Vibhore Sharma on 27/07/16.
//  Copyright © 2016 AtSCh. All rights reserved.
//

#import "PWPSelectCategoryViewController.h"
#import "UIViewController+UBRComponents.h"
#import "SXCUtility.h"
#import "PWPApplicationUtility.h"
#import "UIColor+PWPColor.h"
#import "PWPDownloadBackgroundImage.h"

@interface PWPSelectCategoryViewController ()
{
    __weak IBOutlet UIImageView * imageViewBG;
}
@property(nonatomic,strong)NSArray * categoryArray;

@end

@implementation PWPSelectCategoryViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self backPresses];
    [self sxc_navigationBarCustomizationWithTitle:@"SELECT PLACE CATEGORY"];
    
    _categoryArray=[SXCUtility cleanArray:[PWPApplicationUtility placeCategory]];
    imageViewBG.image = [PWPDownloadBackgroundImage backgroundImage];
}


-(void)backPresses
{
    if(self.navigationController.viewControllers.count>1){
        self.navigationItem.hidesBackButton = YES;
        [self.navigationItem setHidesBackButton:YES animated:YES];
        
        UIImage *backButtonImage = [UIImage imageNamed:(@"btn_back")];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [backButton setImage:backButtonImage forState:UIControlStateNormal];
        backButton.frame = CGRectMake(0, 0 ,23, 23);
        
        UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        [backButton addTarget:self
                       action:@selector(sxc_backButtonClicked:)
             forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = backBarButtonItem;
        
    }

}
-(void)sxc_backButtonClicked:(id)sender
{
    if(self.navigationController.viewControllers.count>1){
    [self.navigationController popViewControllerAnimated:YES];
}
    
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _categoryArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"park_category_cell" forIndexPath:indexPath];
    cell.textLabel.text=_categoryArray[indexPath.row][@"name"];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.backgroundColor =[UIColor clearColor];
    cell.contentView.backgroundColor =[UIColor clearColor];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.font =[UIFont fontWithName:@"Roboto-Regular" size:14.0f];
    
    UIView *customColorView = [[UIView alloc] init];
    customColorView.backgroundColor = [UIColor sxc_themeColorWithAplha];
    cell.selectedBackgroundView =  customColorView;
    
    if([_selectedCategory[@"slug"] isEqualToString:_categoryArray[indexPath.row][@"slug"]]){
        cell.accessoryType=UITableViewCellAccessoryCheckmark;
        
    }else{
        cell.accessoryType=UITableViewCellAccessoryNone;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryCheckmark){
        
        _selectedCategory = [[NSMutableDictionary alloc] init];
    }
    else{
        [_selectedCategory setDictionary:_categoryArray[indexPath.row]];
    }
    
    [_selectedAmenties removeAllObjects];
    
    [tableView reloadRowsAtIndexPaths:tableView.indexPathsForVisibleRows withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

- (IBAction)doneClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
