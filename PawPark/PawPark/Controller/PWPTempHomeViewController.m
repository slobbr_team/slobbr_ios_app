//
//  PWPTempHomeViewController.m
//  PawPark
//
//  Created by Samarth Singla on 10/09/17.
//  Copyright © 2017 AtSCh. All rights reserved.
//

#import "PWPTempHomeViewController.h"
#import "SXCUtility.h"
#import "UIViewController+UBRComponents.h"
#import "PWPDownloadBackgroundImage.h"
#import "PWPService.h"
#import "PWPCommunityHomeViewController.h"
#import "PWPHomeViewController.h"


@interface PWPTempHomeViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageViewbg;

@end

@implementation PWPTempHomeViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self sxc_setNavigationBarMenuItem];
    [self sxc_navigationBarCustomizationWithTitle:@"SLOBBR HOME"];

    self.imageViewbg.image = [PWPDownloadBackgroundImage backgroundImage];
}

-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    [self startLoader];
    
    [[PWPService sharedInstance] currentCommunityWithRequestDictionary:@{}.mutableCopy withSuccessBlock:^(id responseDicitionary) {
        [self dismissLoader];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            NSArray * communities = [SXCUtility cleanArray:responseDicitionary];
            if(communities.count>0) {
                
                PWPCommunityHomeViewController * homeViewController = [[PWPCommunityHomeViewController alloc] initWithNibName:@"PWPCommunityHomeViewController" bundle:nil];
                homeViewController.communitiesDictionary = communities[0];
                [self.navigationController setViewControllers:@[homeViewController] animated:false];
                
            }
            else {
                
                UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                PWPHomeViewController * homeViewController = [storyboard instantiateViewControllerWithIdentifier:@"PWPHomeViewController"];
                homeViewController.isFromRegistration = self.isfromRegistration;
                [self.navigationController setViewControllers:@[homeViewController] animated:false];
                
            }
        });
        
       
        
    } withErrorBlock:^(NSError *error) {
        [self dismissLoader];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
