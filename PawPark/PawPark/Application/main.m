  //
//  main.m
//  PawPark
//
//  Created by xyz on 05/04/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWPAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PWPAppDelegate class]));
    }
}
