//
//  AppDelegate.h
//  PawPark
//
//  Created by xyz on 05/04/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kMainViewController (MainViewController *)[UIApplication sharedApplication].delegate.window.rootViewController

#define kNavigationController (UINavigationController *)[(MainViewController *)[UIApplication sharedApplication].delegate.window.rootViewController rootViewController]

#define kTabViewController (UITabBarController *)[(MainViewController *)[UIApplication sharedApplication].delegate.window.rootViewController rootViewController]

@interface PWPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (PWPAppDelegate*) sharedInstance;

-(void)switchToHomeViewControllerWithRegistration:(BOOL)isRegistration;
-(void)switchToLoginViewController;

-(void)showSTAHelpViewWithS:(NSString *)s withT:(NSString *)t withA:(NSString *)a;
-(void)switchToSplashController:(UIViewController *)viewController;


@end

