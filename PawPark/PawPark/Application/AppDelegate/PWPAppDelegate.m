
//
//  AppDelegate.m
//  PawPark
//
//  Created by xyz on 05/04/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPAppDelegate.h"
#import "UIColor+PWPColor.h"
#import <TwitterKit/TwitterKit.h>
#import "PWPSideMenuTableViewController.h"
#import "PWPHomeViewController.h"
#import <Google/Analytics.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "PWPSTADescriptionViewController.h"
#import <KLCPopup.h>
#import "SXCUtility.h"
#import <AFHTTPRequestOperationLogger.h>
#import "UBRLocationHandler.h"
#import "MainViewController.h"
#import "PWPService.h"
#import "PWPApplicationUtility.h"
#import "PWPInvalidZipCodeViewController.h"
#import "PWPParkCheckInViewController.h"
#import <Mixpanel/Mixpanel.h>
#import "PWPCommunityHomeViewController.h"
#import "SVProgressHUD.h"
#import <IQKeyboardManager.h>
#import "PWPTempHomeViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


@interface PWPAppDelegate () <UIAlertViewDelegate, UITabBarControllerDelegate>
{
    MainViewController * container;
    NSDictionary * localNotificationDictionary;
}
@end

@implementation PWPAppDelegate



- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
 
    
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}


+ (PWPAppDelegate*) sharedInstance{
    
    return (PWPAppDelegate*)[[UIApplication sharedApplication] delegate];
}

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [Fabric with:@[[Crashlytics class]]];
    
    [[IQKeyboardManager sharedManager] setEnable:true];
    [[IQKeyboardManager sharedManager] setToolbarManageBehaviour:IQAutoToolbarByPosition];
    
    [[UBRLocationHandler shareHandler] startLocationHandlerWithCompletion:nil WithErrorBlock:nil];

    [[AFHTTPRequestOperationLogger sharedLogger] startLogging];
    
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];

    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:43/255.0f green:195/255.0f blue:154/255.0f alpha:1.0f]];
    [[UITabBar appearance] setTranslucent:YES];
    
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName,[UIFont systemFontOfSize:18.0f],NSFontAttributeName,
                                                           nil]];
    
    if(![SXCUtility getNSObject:@"isLocationHelp"]){
        [SXCUtility saveNSObject:@YES forKey:@"isLocationHelp"];
    }

    [SXCUtility saveNSObject:@NO forKey:@"isStaHelpOff"];

    [Mixpanel sharedInstanceWithToken:@"16379bec719a089539cad7dc5da2d2e3"];

    if(![SXCUtility getNSObject:@"HomeParkEdit"]){
        [SXCUtility saveNSObject:@YES forKey:@"HomeParkEdit"];
    }
    

    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



-(void)switchToLoginViewController{

    
    UIStoryboard * storyboard=[UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];
    
    UIViewController * viewController=[storyboard instantiateViewControllerWithIdentifier:@"LoginNavigationController"];
    
    
    [UIView transitionFromView:self.window.rootViewController.view toView:viewController.view duration:0.7f options:UIViewAnimationOptionTransitionFlipFromRight completion:^(BOOL finished)
     {
         self.window.rootViewController = viewController;
         [self.window makeKeyAndVisible];
         
     }];

}

-(void)switchToSplashController:(UIViewController *)viewController {
    
    [UIView transitionFromView:self.window.rootViewController.view toView:viewController.view duration:0.7f options:UIViewAnimationOptionTransitionFlipFromRight completion:^(BOOL finished)
     {
         self.window.rootViewController = viewController;
         [self.window makeKeyAndVisible];
         
     }];
}


-(void)switchToHomeViewControllerWithRegistration:(BOOL)isRegistration {
    
    UIStoryboard * storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UITabBarController * tabViewController = [storyboard instantiateViewControllerWithIdentifier:@"tabbarviewcontroller"];
    tabViewController.selectedIndex = 0;
    tabViewController.delegate = self;
    
    UINavigationController * navigationController =  tabViewController.viewControllers.firstObject;
    
    PWPTempHomeViewController * communityViewController = [[PWPTempHomeViewController alloc] initWithNibName:@"PWPTempHomeViewController" bundle:nil];
    communityViewController.isfromRegistration = isRegistration;
    
    [navigationController setViewControllers:@[communityViewController] animated:false];
    
    container = [[MainViewController alloc] initWithRootViewController:tabViewController presentationStyle:LGSideMenuPresentationStyleScaleFromLittle type:0];
    
    [UIView transitionFromView:self.window.rootViewController.view toView:container.view duration:0.7f options:UIViewAnimationOptionTransitionFlipFromRight completion:^(BOOL finished)
     {
         if(finished){
             self.window.rootViewController = container;
             [self userProfileWithDispatch];
         }
     }];
}

-(void)userProfileWithDispatch
{
    [[PWPService sharedInstance] currentUserProfileWithRequestDictionary:nil withSuccessBlock:^(id responseDicitionary) {
        
        [PWPApplicationUtility saveCurrentUserProfileDictionary:responseDicitionary];
        
        NSArray * zipcodeValueArray=[[SXCUtility cleanArray:[SXCUtility getNSObject:@"newyorkzipcode"]] valueForKeyPath:@"value"];

        NSString * selectedZipCode = [PWPApplicationUtility getCurrentUserProfileDictionary][@"zipcode"];
        
    } withErrorBlock:^(NSError *error) {
    }];
}


-(void)showSTAHelpViewWithS:(NSString *)s withT:(NSString *)t withA:(NSString *)a
{
    if(((NSNumber *)[SXCUtility getNSObject:@"isStaHelpOff"]).boolValue){
        
        UIStoryboard * storyBoard =[UIStoryboard storyboardWithName:@"OtherStoryboard" bundle:nil];
        
        PWPSTADescriptionViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPSTADescriptionViewController"];
        viewController.t=t;
        viewController.s=s;
        viewController.a=a;
        
        UINavigationController *navigationController =[[UINavigationController alloc] initWithRootViewController:viewController];
        
        navigationController.navigationBar.translucent=YES;
        
        [self.window.rootViewController presentViewController:navigationController animated:YES completion:nil];
        
    }
}

-(void)showInvalidZipCodeScreen
{
    
    UIStoryboard * storyBoard =[UIStoryboard storyboardWithName:@"NewFeaturesStoryboard" bundle:nil];
    
    PWPInvalidZipCodeViewController * viewController = [storyBoard instantiateViewControllerWithIdentifier:@"PWPInvalidZipCodeViewController"];
    
    UINavigationController *navigationController =[[UINavigationController alloc] initWithRootViewController:viewController];
    
    navigationController.navigationBar.translucent=YES;
    
    [self.window.rootViewController presentViewController:navigationController animated:YES completion:nil];
    
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    
    localNotificationDictionary = notification.userInfo;
    
    if ( application.applicationState == UIApplicationStateActive ){
    
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"Location Alert" message:@"Woof!  You are near one of your Favorite Places, Tap Here to Check-in your dog(s)!" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Check-In", nil];
        [alertView show];
        
    }
    else
    {
        UITabBarController * tabViewController =(UITabBarController *) [container rootViewController];
        tabViewController.selectedIndex = 0;
        
        UINavigationController * navigationController =  tabViewController.selectedViewController;
        
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PWPParkCheckInViewController * viewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckInViewController"];    viewController.parkDictionary=[[NSMutableDictionary alloc] initWithDictionary:notification.userInfo];
        
        [navigationController pushViewController:viewController animated:true];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1){
    
        UITabBarController * tabViewController =(UITabBarController *) [container rootViewController];
        tabViewController.selectedIndex = 0;
        
        UINavigationController * navigationController =  tabViewController.selectedViewController;
        
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PWPParkCheckInViewController * viewController=[storyboard instantiateViewControllerWithIdentifier:@"PWPParkCheckInViewController"];    viewController.parkDictionary=[[NSMutableDictionary alloc] initWithDictionary:localNotificationDictionary];
        
        [navigationController pushViewController:viewController animated:true];
    
    }

}





- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{

    if (tabBarController.selectedIndex == 0) {
        [[Mixpanel sharedInstance] track:@"Home Click On Bottom Bar"];
    }
    else if (tabBarController.selectedIndex == 1) {
        [[Mixpanel sharedInstance] track:@"Map Click On Bottom Bar"];
    }
    else if (tabBarController.selectedIndex == 2) {
        [[Mixpanel sharedInstance] track:@"Kibble Counter Click On Bottom Bar"];
    }
    else if (tabBarController.selectedIndex == 3) {
        [[Mixpanel sharedInstance] track:@"Profile Click On Bottom Bar"];
    }

}


@end
