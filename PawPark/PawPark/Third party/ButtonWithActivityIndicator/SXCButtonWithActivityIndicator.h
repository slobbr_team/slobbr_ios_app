//
//  testbutton.h
//  TestButton
//
//  Created by xyz on 06/02/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SXCButtonWithActivityIndicator : UIButton

@property(nonatomic,strong)UIColor * activityIndicatorColor;

-(void)startAnimatingLoader;
-(void)stopAnimatingLoader;
-(void)setEnabled:(BOOL)enabled;


@end
