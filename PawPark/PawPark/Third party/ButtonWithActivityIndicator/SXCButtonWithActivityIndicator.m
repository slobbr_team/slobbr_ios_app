//
//  testbutton.m
//  TestButton
//
//  Created by xyz on 06/02/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import "SXCButtonWithActivityIndicator.h"

@interface SXCButtonWithActivityIndicator()
{
    UIActivityIndicatorView * activityIndicator;
    UIImage * previousUnSelectedImage;
    UIImage * previousSelectedImage;
}
@end


@implementation SXCButtonWithActivityIndicator

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self= [super initWithCoder:aDecoder];
    if(self){
        [self setEnabled:NO];
    }
    return self;
    
}

- (void)drawRect:(CGRect)rect {

    if(activityIndicator==nil)
    {
        activityIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame=CGRectMake(self.bounds.size.width-40, 0, 20, 20);
        activityIndicator.center=CGPointMake(self.bounds.size.width/2.0f,self.bounds.size.height/2.0f);
        activityIndicator.color=[UIColor whiteColor];
        [activityIndicator setHidden:YES];
        
        [self addSubview:activityIndicator];
    }

}

-(void)setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    
    if(enabled){
        self.alpha=1.0f;
        [self stopAnimatingLoader];
    }
    else{
        self.alpha=.6f;
    }
}

-(void)startAnimatingLoader
{
    [self setUserInteractionEnabled:NO];
    
    previousSelectedImage=[self imageForState:UIControlStateSelected];
    
    previousUnSelectedImage=[self imageForState:UIControlStateNormal];
    
    [self setImage:nil forState:UIControlStateNormal];
    [self setImage:nil forState:UIControlStateSelected];
    
    self.titleLabel.layer.opacity = 0.0f;
    self.titleLabel.alpha = 0.0f;

    if(_activityIndicatorColor){
        activityIndicator.color=_activityIndicatorColor;
    }
    
    activityIndicator.hidden=NO;
    [activityIndicator startAnimating];

}

-(void)stopAnimatingLoader
{
    
    [self setImage:previousUnSelectedImage forState:UIControlStateNormal];
    [self setImage:previousSelectedImage forState:UIControlStateSelected];
    
    
    self.imageView.alpha=1.0f;
    
    [self setUserInteractionEnabled:YES];

    self.titleLabel.layer.opacity = 1.0f;
    self.titleLabel.alpha = 1.0f;

    activityIndicator.hidden=YES;
    [activityIndicator stopAnimating];
}



@end
