//
//  SXCLabelWithActivityIndicator.m
//  SixthContinent
//
//  Created by xyz on 12/02/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import "SXCLabelWithActivityIndicator.h"

@interface SXCLabelWithActivityIndicator()

@end

@implementation SXCLabelWithActivityIndicator


- (id)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    
    if(self)
    {
        _activityIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicator.color=[UIColor whiteColor];
        _activityIndicator.center=CGPointMake(self.bounds.size.width/2.0f,self.bounds.size.height/2.0f);
        _activityIndicator.autoresizingMask=UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
        [_activityIndicator setHidden:YES];
        [self addSubview:_activityIndicator];
    
    }
    
    return self;
    
}


-(void)startAnimatingLoader
{
    self.text=@"";
    if(_activityIndicatorColor){
        _activityIndicator.color=_activityIndicatorColor;
    }
    _activityIndicator.hidden=NO;
    [_activityIndicator startAnimating];
    
}

-(void)stopAnimatingLoader
{
    _activityIndicator.hidden=YES;
    [_activityIndicator stopAnimating];
}

-(void)leftAlignLoader{
    
    _activityIndicator.autoresizingMask=UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleRightMargin;
    _activityIndicator.center=CGPointMake(10,self.bounds.size.height/2.0f);
    _activityIndicator.color=[UIColor whiteColor];

}


@end
