//
//  SXCLabelWithActivityIndicator.h
//  SixthContinent
//
//  Created by xyz on 12/02/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SXCLabelWithActivityIndicator : UILabel

@property(nonatomic,strong)UIColor * activityIndicatorColor;
@property(nonatomic,strong)UIActivityIndicatorView * activityIndicator;

-(void)startAnimatingLoader;
-(void)stopAnimatingLoader;

-(void)leftAlignLoader;
@end
