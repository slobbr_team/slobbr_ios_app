//
//  UBRLabel.m
//  SixthContinent
//
//  Created by xyz on 19/02/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import "SXCLabelWithPadding.h"

#define PADDING {10, 16, 10, 16}

@implementation SXCLabelWithPadding


- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines
{
    UIEdgeInsets insets = PADDING;
  
    if(self.text.length>0){
        
        CGRect rect = [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, insets)
                        limitedToNumberOfLines:numberOfLines];
        
        
        rect.origin.x    -= insets.left;
        rect.origin.y    -= insets.top;
        rect.size.width  += (insets.left + insets.right);
        rect.size.height += (insets.top + insets.bottom);
        
        return rect;
    }
    else{
        return [super textRectForBounds:CGRectMake(0, 0, 0, 0) limitedToNumberOfLines:numberOfLines];
    }

}

- (void)drawTextInRect:(CGRect)rect
{
    UIEdgeInsets insets = PADDING;
   
    if(self.text.length>0){
        [super drawTextInRect:UIEdgeInsetsInsetRect(rect,insets)];
    }else{
        [super drawTextInRect:CGRectMake(0, 0, 0, 0)];
    }
}

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    
        // If this is a multiline label, need to make sure
        // preferredMaxLayoutWidth always matches the frame width
        // (i.e. orientation change can mess this up)
    
    if (self.numberOfLines == 0 && bounds.size.width != self.preferredMaxLayoutWidth) {
        self.preferredMaxLayoutWidth = self.bounds.size.width;
        [self setNeedsUpdateConstraints];
    }
}


@end
