//
//  SXCTextfieldWithLeftImage.h
//  SixthContinent
//
//  Created by xyz on 17/02/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SXCTextfieldWithLeftImage : UITextField
@property(nonatomic,strong)UIImage * imageLeftSide;

@property(nonatomic,strong)UIImage * imageRightSide;
@property(nonatomic,strong)UIColor * borderColor;

-(void)setText:(NSString *)text;

@end
