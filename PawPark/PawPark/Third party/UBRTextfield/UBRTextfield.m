
    //
//  UBRTextfield.m
//  UrbanRunr
//
//  Created by xyz on 26/12/14.
//  Copyright (c) 2014 Damilola. All rights reserved.
//

#import "UBRTextfield.h"
#import "UIColor+PWPColor.h"

@implementation UBRTextfield

-(void)drawRect:(CGRect)rect
{
//    [self.layer setBorderColor:[UIColor sxc_hintColor].CGColor];
//    [self.layer setBorderWidth:1.0f];
//    [self.layer setCornerRadius:1.0f];
    
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    if(self){
        self.backgroundColor=[UIColor sxc_appBackgroundWhiteColor];
        self.font=[UIFont fontWithName:@"Roboto-Light" size:14.0f];
        self.textColor=[UIColor sxc_appTextfieldTextColor];
        [self setValue:[UIColor sxc_hintColor]
                        forKeyPath:@"_placeholderLabel.textColor"];
    }
    return self;
}


-(CGRect)textRectForBounds:(CGRect)bounds {
    
    if(self.clearButtonMode==UITextFieldViewModeAlways)
    {
        return CGRectMake(bounds.origin.x + 10, bounds.origin.y + 8,
                          bounds.size.width - 35, bounds.size.height - 16);
    }
    
    return CGRectMake(bounds.origin.x + 10, bounds.origin.y + 4,
                      bounds.size.width - 20
                      , bounds.size.height - 10);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}

@end
