//
//  SXCTextfieldWithLeftImage.m
//  SixthContinent
//
//  Created by xyz on 17/02/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import "SXCTextfieldWithLeftImage.h"
#import "UIColor+PWPColor.h"

@implementation SXCTextfieldWithLeftImage


-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    if(self){
        
        self.backgroundColor=[UIColor whiteColor];
        
        self.font=[UIFont fontWithName:@"Roboto-Regular" size:16.0f];
        
        
        self.textColor=[UIColor sxc_appTextfieldTextColor];
        
        [self setValue:[UIColor sxc_hintColor]
            forKeyPath:@"_placeholderLabel.textColor"];
        
        self.autocorrectionType=UITextAutocorrectionTypeNo;
        
        self.spellCheckingType=UITextSpellCheckingTypeNo;
    
    }
    
    return self;
}


- (CGRect) leftViewRectForBounds:(CGRect)bounds {
    
    CGRect textRect = [super leftViewRectForBounds:bounds];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(5.0f, self.bounds.size.height-1, self.bounds.size.width-10, 1.0f);
    bottomBorder.backgroundColor = [UIColor whiteColor].CGColor;
    [self.layer addSublayer:bottomBorder];
    
    if(_imageLeftSide)
    textRect.origin.x=bounds.origin.x + 15;
    
    return textRect;
}


- (CGRect) rightViewRectForBounds:(CGRect)bounds {
    
    CGRect textRect = [super rightViewRectForBounds:bounds];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(5.0f, self.bounds.size.height-1, self.bounds.size.width-10, 1.0f);
    bottomBorder.backgroundColor = [UIColor whiteColor].CGColor;
    [self.layer addSublayer:bottomBorder];
    
    if(_imageRightSide)
        textRect.origin.x=bounds.size.width-18-15;
    
    return textRect;
}

-(CGRect)textRectForBounds:(CGRect)bounds {

    return CGRectMake(bounds.origin.x + ((_imageLeftSide!=nil)?45:16), bounds.origin.y + 8,
                      bounds.size.width-((_imageLeftSide!=nil)?35:16)-((_imageRightSide!=nil)?45:16), bounds.size.height - 16);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    
    return [self textRectForBounds:bounds];
}

-(void)setImageLeftSide:(UIImage *)imageLeftSide
{
    _imageLeftSide=imageLeftSide;
    
    UIImageView * imageView = [[UIImageView alloc]initWithImage:imageLeftSide];
    imageView.frame=CGRectMake(6, 0, imageLeftSide.size.width, self.frame.size.height-10);
    [imageView setContentMode:UIViewContentModeCenter];
    [self setLeftView:imageView];
    [self setLeftViewMode:UITextFieldViewModeAlways];
}

-(void)setImageRightSide:(UIImage *)imageRightSide
{
    
    _imageRightSide=imageRightSide;

    UIImageView * imageView=[[UIImageView alloc]initWithImage:imageRightSide];
    imageView.frame=CGRectMake(0, 0, 12, 18);
    imageView.center=CGPointMake(imageView.center.x, self.bounds.size.height/2.0f);
   
    [imageView setTintColor:[UIColor blackColor]];
    
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    [imageView setBackgroundColor:[UIColor clearColor]];
    
    [self setRightView:imageView];
    
    [self setRightViewMode:UITextFieldViewModeAlways];
}
-(void)setBorderColor:(UIColor *)borderColor
{
    _borderColor=borderColor;
    
    self.layer.borderColor=borderColor.CGColor;

}


-(void)setText:(NSString *)text
{
    [super setText:text];
    [[NSNotificationCenter defaultCenter] postNotificationName:UITextFieldTextDidChangeNotification object:self userInfo:nil];
}

@end
