//
//  UBRTextfield.h
//  UrbanRunr
//
//  Created by xyz on 26/12/14.
//  Copyright (c) 2014 Damilola. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UBRTextfield : UITextField

-(CGRect)textRectForBounds:(CGRect)bounds ;
- (CGRect)editingRectForBounds:(CGRect)bounds;
@end
