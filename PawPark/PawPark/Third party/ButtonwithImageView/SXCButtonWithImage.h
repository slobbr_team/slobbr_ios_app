//
//  SXCButtonWithimage.h
//  SixthContinent
//
//  Created by Umang on 25/02/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SXCButtonWithImage : UIButton

@property(nonatomic,strong)UIImage * imageRightSide;

@end
