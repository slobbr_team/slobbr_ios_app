//
//  SXCButtonWithimage.m
//  SixthContinent
//
//  Created by Umang on 25/02/15.
//  Copyright (c) 2015 xyz. All rights reserved.
//

#import "SXCButtonWithImage.h"

@implementation SXCButtonWithImage

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)drawRect:(CGRect)rect
{
   
    [self.layer setBorderColor:[UIColor colorWithRed:222/255.0f green:222/255.0f blue:222/255.0f alpha:1.0f].CGColor];
    [self.layer setBorderWidth:1.0f];
    [self.layer setCornerRadius:1.0f];
    
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    if(self){
        self.backgroundColor=[UIColor whiteColor];
        self.titleLabel.font=[UIFont fontWithName:@"Open Sans" size:16.0f];
       self.titleLabel.frame = CGRectMake(self.titleLabel.frame.origin.x + 10, self.titleLabel.frame.origin.y + 8,
                  self.titleLabel.frame.size.width - 20
                  , self.titleLabel.frame.size.height - 16);

        self.titleLabel.textColor=[UIColor colorWithRed:222/255.0f green:222/255.0f blue:222/255.0f alpha:1.0f];
       
    }
    
    return self;
}

@end
