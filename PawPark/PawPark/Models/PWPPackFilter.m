//
//  PWPPackFilter.m
//  PawPark
//
//  Created by daffolap19 on 5/10/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "PWPPackFilter.h"
#import "SXCUtility.h"

@implementation PWPPackFilter

-(instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err
{
    self=[super initWithDictionary:dict error:err];
    if(self){
    
        self.apiKey=[NSString stringWithFormat:@"%@",[SXCUtility cleanString:[SXCUtility getNSObject:@"apikey"]]];
    }

    return self;
}

@end
