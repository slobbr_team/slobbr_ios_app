//
//  PWPPackFilter.h
//  PawPark
//
//  Created by daffolap19 on 5/10/15.
//  Copyright (c) 2015 AtSCh. All rights reserved.
//

#import "JSONAPI.h"

@interface PWPPackFilter : JSONModel

@property(nonatomic,strong) NSString <Optional> * apiKey;
@property(nonatomic,strong) NSString <Optional>* s;
@property(nonatomic,strong) NSString <Optional>* breed;
@property(nonatomic,strong) NSString <Optional>* size;
@property(nonatomic,strong) NSString <Optional>* activityLevel;
@property(nonatomic,strong) NSString <Optional>* sociability;
@property(nonatomic,strong) NSString <Optional>* city;
@property(nonatomic,strong) NSString <Optional>* state;
@property(nonatomic,strong) NSString <Optional>* zipcode;
@property(nonatomic,strong) NSString <Optional>* stateCode;

@end
