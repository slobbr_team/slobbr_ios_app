//
//  DatabaseController.swift
//  Databases
//
//  Created by Electronic Armory on 10/15/16.
//  Copyright © 2016 Electronic Armory. All rights reserved.
//

import Foundation
import CoreData

class DatabaseController{
    
    private init(){
        
    }
    
    
    
    class func getContext() -> NSManagedObjectContext {
        return DatabaseController.persistentContainer.viewContext
    }
    
    // MARK: - Core Data stack
    
    static var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Alarm")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    class func saveContext (completion: @escaping (_ success:Bool)->()) {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
                completion(true)
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                completion(false)
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                
            }
        }
    }
    
    
    class  func saveAlarmInCoreData(alarmModel:Alarm,completion: @escaping (_ success:Bool ,_ createdAlarm:EMIAlarm?)->()) {
        let alarm = EMIAlarm(context:persistentContainer.viewContext)
        alarm.label = alarmModel.label
        alarm.date = alarmModel.date as NSDate?
        alarm.enabled = alarmModel.enabled
        alarm.mediaID = alarmModel.mediaID
        alarm.mediaLabel = alarmModel.mediaLabel
        alarm.repeatWeekdays = alarmModel.repeatWeekdays as NSObject?
        alarm.snoozeEnabled = alarmModel.snoozeEnabled
        alarm.uuid = alarmModel.uuid
        alarm.id = alarmModel.id
        
        saveContext { (success) in
            completion(success , alarm)
        }
    }
    
    //MARK: - Update an alarm from database
    class func update(alarm:Alarm){
        let fetchRequest:NSFetchRequest<EMIAlarm> = EMIAlarm.fetchRequest()
        
        let userPredicate = NSPredicate(format: "id == %@", alarm.id)
        
        fetchRequest.predicate = userPredicate
        do{
            let searchResults = try persistentContainer.viewContext.fetch(fetchRequest)
            print("number of results: \(searchResults.count)")
             searchResults.first?.enabled = alarm.enabled
             searchResults.first?.label = alarm.label
             searchResults.first?.snoozeEnabled = alarm.snoozeEnabled
             searchResults.first?.date = alarm.date as NSDate?
             searchResults.first?.id = alarm.id
             searchResults.first?.mediaID = alarm.mediaID
             searchResults.first?.mediaLabel = alarm.mediaLabel
             searchResults.first?.repeatWeekdays = alarm.repeatWeekdays as NSObject?
             searchResults.first?.uuid = alarm.uuid
            saveContext { (success) in
                
            }
            
        }
        catch{
            print("Error: \(error)")
        }
    }
    
    
    
    //MARK: - Delete an alarm from database
    class func delete(alarm:Alarm){
        let fetchRequest:NSFetchRequest<EMIAlarm> = EMIAlarm.fetchRequest()
        
        let userPredicate = NSPredicate(format: "id == %@", alarm.id)
        
        fetchRequest.predicate = userPredicate
        do{
            let searchResults = try persistentContainer.viewContext.fetch(fetchRequest)
            print("number of results: \(searchResults.count)")
            persistentContainer.viewContext.delete(searchResults.first!)
            saveContext { (success) in
                
            }
            
        }
        catch{
            print("Error: \(error)")
        }
    }
    //MARK: -
    
    
    //MARK: - Fetching all alarms from database
    class func fetchAlarms(completion:@escaping(_ result:[EMIAlarm]? , _ error:Error?)->()){
        let fetchRequest:NSFetchRequest<EMIAlarm> = EMIAlarm.fetchRequest()
        
        do{
            let searchResults = try persistentContainer.viewContext.fetch(fetchRequest)
            print("number of results: \(searchResults.count)")
            completion(searchResults,nil)
        }
        catch{
            completion(nil,error)
            print("Error: \(error)")
        }
    }
    //MARK: -
}









